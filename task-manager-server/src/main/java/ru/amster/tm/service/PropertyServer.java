package ru.amster.tm.service;


import lombok.NoArgsConstructor;
import ru.amster.tm.api.servise.IPropertyServer;

import java.io.InputStream;
import java.util.Properties;

@NoArgsConstructor
public class PropertyServer implements IPropertyServer {

    private final String NAME = "/application.properties";

    private final Properties properties = new Properties();

    @Override
    public void init() throws Exception {
        final InputStream inputStream = PropertyServer.class.getResourceAsStream(NAME);
        properties.load(inputStream);
    }

    @Override
    public String getServerHost() {
        final String propertyHost = properties.getProperty("server.host");
        final String envHost = System.getProperty("server.host");
        if (envHost != null) return envHost;
        return propertyHost;
    }

    @Override
    public Integer getServerPort() {
        final String propertyPort = properties.getProperty("server.port");
        final String envPort = System.getProperty("server.port");
        String value = propertyPort;
        if (envPort != null) value = envPort;
        return Integer.parseInt(value);
    }

}