package ru.amster.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IDomainService;
import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.api.servise.ITaskService;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.dto.Domain;
import ru.amster.tm.exception.empty.EmptyDomainException;

public final class DomainService implements IDomainService{

    @NotNull
    final IProjectService projectService;

    @NotNull
    final ITaskService taskService;

    @NotNull
    final IUserService userService;

    public DomainService(
            IProjectService projectService,
            ITaskService taskService,
            IUserService userService
    ) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.userService = userService;
    }

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) throw new EmptyDomainException();
        userService.load(domain.getUsers());
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
    }

    @Override
    public void export(@Nullable final Domain domain) {
        if (domain == null) throw new EmptyDomainException();
        domain.setUsers(userService.export());
        domain.setProjects(projectService.export());
        domain.setTasks(taskService.export());
    }

}