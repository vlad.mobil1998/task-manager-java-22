package ru.amster.tm.exception.empty;

import ru.amster.tm.exception.AbstractException;

public final class EmptyValueException extends AbstractException {

    public EmptyValueException() {
        super("ERROR! The value to convert the hash is empty...");
    }

}