package ru.amster.tm.exception.system;

import ru.amster.tm.exception.AbstractException;

public final class InvalidArgumentException extends AbstractException {

    public InvalidArgumentException() {
        super("ERROR! Invalid argument...");
    }

}