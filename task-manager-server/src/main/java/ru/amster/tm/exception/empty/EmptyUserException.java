package ru.amster.tm.exception.empty;

import ru.amster.tm.exception.AbstractException;

public final class EmptyUserException extends AbstractException {

    public EmptyUserException() {
        super("ERROR! User is empty...");
    }

}