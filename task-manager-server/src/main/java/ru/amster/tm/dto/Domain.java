package ru.amster.tm.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import ru.amster.tm.entity.Project;
import ru.amster.tm.entity.Task;
import ru.amster.tm.entity.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public final class Domain implements Serializable {

    private List<Project> projects = new ArrayList<>();

    private List<Task> tasks = new ArrayList<>();

    private List<User> users = new ArrayList<>();

}