package ru.amster.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@UtilityClass
public class SignatureUtil {

    @Nullable
    public static String sign(@NotNull final Object value) {
        try {
            final ObjectMapper objectMapper = new ObjectMapper();
            final String json = objectMapper.writeValueAsString(value);
            return sign(json);
        } catch (final JsonProcessingException e) {
            return null;
        }
    }

    @Nullable
    public static String sign(@Nullable String value) {
        if (value == null || value.isEmpty()) return null;
        @Nullable String result = HashUtil.salt(value);
        if (result == null) return null;
        return result;
        }
    }