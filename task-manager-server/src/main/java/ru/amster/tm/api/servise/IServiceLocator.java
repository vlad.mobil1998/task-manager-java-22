package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import ru.amster.tm.endpoint.*;

public interface IServiceLocator {

    @NotNull AdminUserEndpoint getAdminUserEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();
    
    @NotNull
    UserEndpoint getUserEndpoint();
    
    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    IUserService getUserService();

    @NotNull
    
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionService getSessionService();

    @NotNull IPropertyServer getPropertyServer();

    @NotNull SessionEndpoint getSessionEndpoint();
}