package ru.amster.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository {

    List<Session> findByUserId(@Nullable String userId);

    void removeByUserId(@NotNull String userId);
}