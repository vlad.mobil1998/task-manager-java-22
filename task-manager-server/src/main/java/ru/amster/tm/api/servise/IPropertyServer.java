package ru.amster.tm.api.servise;

public interface IPropertyServer {
    void init() throws Exception;

    String getServerHost();

    Integer getServerPort();
}
