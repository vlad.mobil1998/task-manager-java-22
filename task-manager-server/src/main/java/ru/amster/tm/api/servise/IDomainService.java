package ru.amster.tm.api.servise;

import org.jetbrains.annotations.Nullable;
import ru.amster.tm.dto.Domain;

public interface IDomainService {

    void load(@Nullable Domain domain);

    void  export(@Nullable Domain domain);

}