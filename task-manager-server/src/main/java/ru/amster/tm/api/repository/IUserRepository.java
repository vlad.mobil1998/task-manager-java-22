package ru.amster.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.amster.tm.entity.User;

public interface IUserRepository extends IRepository {

    @NotNull
    User findById(@NotNull String id);

    @NotNull
    User findByLogin(@NotNull String login);

    @NotNull
    void removeById(@NotNull String id);

    @NotNull
    void removeByLogin(@NotNull String login);

}