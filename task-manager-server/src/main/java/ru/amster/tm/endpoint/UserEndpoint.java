package ru.amster.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.api.servise.ISessionService;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Session;
import ru.amster.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService
public final class UserEndpoint {

    IServiceLocator serviceLocator;

    public UserEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    @NotNull
    public User findUserById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final IUserService userService  = serviceLocator.getUserService();
        return userService.findById(id);
    }

    @WebMethod
    public void createWithThreeParamUser(
            @WebParam(name = "log", partName = "log") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "role", partName = "role") @Nullable Role role
    ) {
        IUserService userService  = serviceLocator.getUserService();
        userService.create(login, password, role);
    }

    @WebMethod
    public void createWithFourParamUser(
            @WebParam(name = "log", partName = "log") @Nullable final String login,
            @WebParam(name = "password", partName = "password") @Nullable final String password,
            @WebParam(name = "email", partName = "email") @Nullable final String email,
            @WebParam(name = "role", partName = "role") @Nullable Role role
    ) {
        @NotNull final IUserService userService  = serviceLocator.getUserService();
        userService.create(login, password, email, role);
    }

    @WebMethod
    public void removeUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final IUserService userService  = serviceLocator.getUserService();
        userService.removeById(session.getUserId());
        sessionService.close(session);
    }

    @WebMethod
    @NotNull
    public User updateUserEmail(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "email", partName = "email") @Nullable final String email
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final IUserService userService  = serviceLocator.getUserService();
        return userService.updateEmail(session.getUserId(), email);
    }

    @WebMethod
    @NotNull
    public User updateUserFirstName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "firstName", partName = "firstName") @Nullable final String firstName
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final IUserService userService  = serviceLocator.getUserService();
        return userService.updateFirstName(session.getUserId(), firstName);
    }

    @WebMethod
    @NotNull
    public User updateUserLastName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "lastName", partName = "lastName") @Nullable final String lastName
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final IUserService userService  = serviceLocator.getUserService();
        return userService.updateLastName(session.getUserId(), lastName);
    }

    @WebMethod
    @NotNull
    public User updateUserMiddleName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "middleName", partName = "middleName") @Nullable final String middleName
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final IUserService userService  = serviceLocator.getUserService();
        return userService.updateMiddleName(session.getUserId(), middleName);
    }

    @WebMethod
    @NotNull
    public User updateUserPassword(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final IUserService userService  = serviceLocator.getUserService();
        return userService.updatePassword(session.getUserId(), password);
    }

}