package ru.amster.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IProjectService;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.api.servise.ISessionService;
import ru.amster.tm.entity.Project;
import ru.amster.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService
public final class ProjectEndpoint {
    
    IServiceLocator serviceLocator;

    public ProjectEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    public void createWithTwoParamProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.create(session.getUserId(), name);
    }

    @WebMethod
    public List<Project> findAllProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final IProjectService projectService  = serviceLocator.getProjectService();
        return projectService.findAll();
    }

    @WebMethod
    public void createWithThreeParamProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        projectService.create(session.getUserId(), name, description);
    }

    @WebMethod
    @NotNull
    public Project findProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        return projectService.findOneById(session.getUserId(), id);
    }

    @WebMethod
    @NotNull
    public Project findProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        return projectService.findOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @NotNull
    public Project findProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        return projectService.findOneByName(session.getUserId(), name);
    }

    @WebMethod
    @NotNull
    public Project removeProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        return projectService.removeOneByName(session.getUserId(), name);
    }

    @WebMethod
    @NotNull
    public Project removeProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        return projectService.removeOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @NotNull
    public Project removeProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        return projectService.removeOneById(session.getUserId(), id);
    }

    @WebMethod
    @NotNull
    public Project updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        return projectService.updateProjectById(session.getUserId(), id, name, description);
    }

    @WebMethod
    @NotNull
    public Project updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        return projectService.updateProjectByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    @NotNull
    public Integer numberOfAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        return projectService.numberOfAllProjects(session.getUserId());
    }

}