package ru.amster.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.api.servise.ISessionService;
import ru.amster.tm.api.servise.ITaskService;
import ru.amster.tm.entity.Session;
import ru.amster.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService
public final class TaskEndpoint {

    IServiceLocator serviceLocator;

    public TaskEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    @Nullable
    public List<Task> findAllTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final ITaskService taskService  = serviceLocator.getTaskService();
        return taskService.findAll();
    }

    @WebMethod
    public void createWithTwoParamTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final ITaskService taskService  = serviceLocator.getTaskService();
        taskService.create(session.getUserId(), name);
    }

    @WebMethod
    public void createWithThreeParamTask(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final ITaskService taskService  = serviceLocator.getTaskService();
        taskService.create(session.getUserId(), name, description);
    }

    @WebMethod
    @NotNull
    public Task findTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final ITaskService taskService  = serviceLocator.getTaskService();
        return taskService.findOneById(session.getUserId(), id);
    }

    @WebMethod
    @NotNull
    public Task findTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final ITaskService taskService  = serviceLocator.getTaskService();
        return taskService.findOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @NotNull
    public Task findTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final ITaskService taskService  = serviceLocator.getTaskService();
        return taskService.findOneByName(session.getUserId(), name);
    }

    @WebMethod
    @NotNull
    public Task removeTaskByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final ITaskService taskService  = serviceLocator.getTaskService();
        return taskService.removeOneByName(session.getUserId(), name);
    }

    @WebMethod
    @NotNull
    public Task removeTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final ITaskService taskService  = serviceLocator.getTaskService();
        return taskService.removeOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @NotNull
    public Task removeTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final ITaskService taskService  = serviceLocator.getTaskService();
        return taskService.removeOneById(session.getUserId(), id);
    }

    @WebMethod
    @NotNull
    public Task updateTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final ITaskService taskService  = serviceLocator.getTaskService();
        return taskService.updateTaskById(session.getUserId(), id, name, description);
    }

    @WebMethod
    @NotNull
    public Task updateTaskByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final ITaskService taskService  = serviceLocator.getTaskService();
        return taskService.updateTaskByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    @NotNull
    public Integer numberOfAllTasks(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    ) {
        @NotNull final ISessionService sessionService =  serviceLocator.getSessionService();
        sessionService.validate(session);
        @NotNull final ITaskService taskService  = serviceLocator.getTaskService();
        return taskService.numberOfAllTasks(session.getUserId());
    }

}