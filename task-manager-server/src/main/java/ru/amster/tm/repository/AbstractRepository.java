package ru.amster.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.amster.tm.api.repository.IRepository;
import ru.amster.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.List;


public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository {

    @NotNull
    protected List<E> records = new ArrayList<>();

    @Override
    public void merge(@NotNull final Object record) {
        records.add((E) record);
    }

    @Override
    public void removeAll() {
        records.clear();
    }

    @NotNull
    @Override
    public List<E> getEntity() {
        return records;
    }

    @Override
    public void remove(@NotNull final Object record) {
        records.remove(record);
    }

    @Override
    public Boolean contains(@NotNull String id) {
        String tempId;
        for (E temp: records) {
            tempId = temp.getId();
            if (!tempId.isEmpty() && tempId.equals(id)) return true;
        }
        return false;
    }

}