#import "enunciate-common.h"
#ifndef DEF_TASK_MANAGER_SERVERNS1Role_H
#define DEF_TASK_MANAGER_SERVERNS1Role_H

/**
 * (no documentation provided)
 */
enum TASK_MANAGER_SERVERNS1Role
{

  /**
   * (no documentation provided)
   */
  TASK_MANAGER_SERVER_NS1_ROLE_USER,

  /**
   * (no documentation provided)
   */
  TASK_MANAGER_SERVER_NS1_ROLE_ADMIN
};
/**
 * Reads a Role from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Role, or NULL if unable to be read.
 */
static enum TASK_MANAGER_SERVERNS1Role *xmlTextReaderReadTASK_MANAGER_SERVERNS1RoleType(xmlTextReaderPtr reader);

/**
 * Writes a Role to XML.
 *
 * @param writer The XML writer.
 * @param _role The Role to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteTASK_MANAGER_SERVERNS1RoleType(xmlTextWriterPtr writer, enum TASK_MANAGER_SERVERNS1Role *_role);

/**
 * Utility method for getting the enum value for a string.
 *
 * @param _role The string to format.
 * @return The enum value or NULL on error.
 */
static enum TASK_MANAGER_SERVERNS1Role *formatStringToTASK_MANAGER_SERVERNS1RoleType(NSString *_role);

/**
 * Utility method for getting the string value of Role.
 *
 * @param _role The Role to format.
 * @return The string value or NULL on error.
 */
static NSString *formatTASK_MANAGER_SERVERNS1RoleTypeToString(enum TASK_MANAGER_SERVERNS1Role *_role);
#endif /* DEF_TASK_MANAGER_SERVERNS1Role_H */

@class TASK_MANAGER_SERVERNS1AbstractEntity;
@class TASK_MANAGER_SERVERNS1Domain;
@class TASK_MANAGER_SERVERNS1Task;
@class TASK_MANAGER_SERVERNS1User;
@class TASK_MANAGER_SERVERNS1Session;
@class TASK_MANAGER_SERVERNS1Project;

#ifndef DEF_TASK_MANAGER_SERVERNS1AbstractEntity_H
#define DEF_TASK_MANAGER_SERVERNS1AbstractEntity_H

/**
 * (no documentation provided)
 */
@interface TASK_MANAGER_SERVERNS1AbstractEntity : NSObject
{
  @private
    NSString *_identifier;
}

/**
 * (no documentation provided)
 */
- (NSString *) identifier;

/**
 * (no documentation provided)
 */
- (void) setIdentifier: (NSString *) newIdentifier;
@end /* interface TASK_MANAGER_SERVERNS1AbstractEntity */

#endif /* DEF_TASK_MANAGER_SERVERNS1AbstractEntity_H */
#ifndef DEF_TASK_MANAGER_SERVERNS1Domain_H
#define DEF_TASK_MANAGER_SERVERNS1Domain_H

/**
 * (no documentation provided)
 */
@interface TASK_MANAGER_SERVERNS1Domain : NSObject
{
  @private
    NSArray *_projects;
    NSArray *_tasks;
    NSArray *_users;
}

/**
 * (no documentation provided)
 */
- (NSArray *) projects;

/**
 * (no documentation provided)
 */
- (void) setProjects: (NSArray *) newProjects;

/**
 * (no documentation provided)
 */
- (NSArray *) tasks;

/**
 * (no documentation provided)
 */
- (void) setTasks: (NSArray *) newTasks;

/**
 * (no documentation provided)
 */
- (NSArray *) users;

/**
 * (no documentation provided)
 */
- (void) setUsers: (NSArray *) newUsers;
@end /* interface TASK_MANAGER_SERVERNS1Domain */

#endif /* DEF_TASK_MANAGER_SERVERNS1Domain_H */
#ifndef DEF_TASK_MANAGER_SERVERNS1Task_H
#define DEF_TASK_MANAGER_SERVERNS1Task_H

/**
 * (no documentation provided)
 */
@interface TASK_MANAGER_SERVERNS1Task : TASK_MANAGER_SERVERNS1AbstractEntity
{
  @private
    NSString *_name;
    NSString *_description;
    NSString *_userId;
}

/**
 * (no documentation provided)
 */
- (NSString *) name;

/**
 * (no documentation provided)
 */
- (void) setName: (NSString *) newName;

/**
 * (no documentation provided)
 */
- (NSString *) description;

/**
 * (no documentation provided)
 */
- (void) setDescription: (NSString *) newDescription;

/**
 * (no documentation provided)
 */
- (NSString *) userId;

/**
 * (no documentation provided)
 */
- (void) setUserId: (NSString *) newUserId;
@end /* interface TASK_MANAGER_SERVERNS1Task */

#endif /* DEF_TASK_MANAGER_SERVERNS1Task_H */
#ifndef DEF_TASK_MANAGER_SERVERNS1User_H
#define DEF_TASK_MANAGER_SERVERNS1User_H

/**
 * (no documentation provided)
 */
@interface TASK_MANAGER_SERVERNS1User : TASK_MANAGER_SERVERNS1AbstractEntity
{
  @private
    NSString *_login;
    NSString *_passwordHash;
    NSString *_email;
    NSString *_fistName;
    NSString *_lastName;
    NSString *_middleName;
    enum TASK_MANAGER_SERVERNS1Role *_role;
    BOOL *_locked;
}

/**
 * (no documentation provided)
 */
- (NSString *) login;

/**
 * (no documentation provided)
 */
- (void) setLogin: (NSString *) newLogin;

/**
 * (no documentation provided)
 */
- (NSString *) passwordHash;

/**
 * (no documentation provided)
 */
- (void) setPasswordHash: (NSString *) newPasswordHash;

/**
 * (no documentation provided)
 */
- (NSString *) email;

/**
 * (no documentation provided)
 */
- (void) setEmail: (NSString *) newEmail;

/**
 * (no documentation provided)
 */
- (NSString *) fistName;

/**
 * (no documentation provided)
 */
- (void) setFistName: (NSString *) newFistName;

/**
 * (no documentation provided)
 */
- (NSString *) lastName;

/**
 * (no documentation provided)
 */
- (void) setLastName: (NSString *) newLastName;

/**
 * (no documentation provided)
 */
- (NSString *) middleName;

/**
 * (no documentation provided)
 */
- (void) setMiddleName: (NSString *) newMiddleName;

/**
 * (no documentation provided)
 */
- (enum TASK_MANAGER_SERVERNS1Role *) role;

/**
 * (no documentation provided)
 */
- (void) setRole: (enum TASK_MANAGER_SERVERNS1Role *) newRole;

/**
 * (no documentation provided)
 */
- (BOOL *) locked;

/**
 * (no documentation provided)
 */
- (void) setLocked: (BOOL *) newLocked;
@end /* interface TASK_MANAGER_SERVERNS1User */

#endif /* DEF_TASK_MANAGER_SERVERNS1User_H */
#ifndef DEF_TASK_MANAGER_SERVERNS1Session_H
#define DEF_TASK_MANAGER_SERVERNS1Session_H

/**
 * (no documentation provided)
 */
@interface TASK_MANAGER_SERVERNS1Session : TASK_MANAGER_SERVERNS1AbstractEntity
{
  @private
    long long *_timestamp;
    NSString *_userId;
    NSString *_signature;
}

/**
 * (no documentation provided)
 */
- (long long *) timestamp;

/**
 * (no documentation provided)
 */
- (void) setTimestamp: (long long *) newTimestamp;

/**
 * (no documentation provided)
 */
- (NSString *) userId;

/**
 * (no documentation provided)
 */
- (void) setUserId: (NSString *) newUserId;

/**
 * (no documentation provided)
 */
- (NSString *) signature;

/**
 * (no documentation provided)
 */
- (void) setSignature: (NSString *) newSignature;
@end /* interface TASK_MANAGER_SERVERNS1Session */

#endif /* DEF_TASK_MANAGER_SERVERNS1Session_H */
#ifndef DEF_TASK_MANAGER_SERVERNS1Project_H
#define DEF_TASK_MANAGER_SERVERNS1Project_H

/**
 * (no documentation provided)
 */
@interface TASK_MANAGER_SERVERNS1Project : TASK_MANAGER_SERVERNS1AbstractEntity
{
  @private
    NSString *_name;
    NSString *_description;
    NSString *_userId;
}

/**
 * (no documentation provided)
 */
- (NSString *) name;

/**
 * (no documentation provided)
 */
- (void) setName: (NSString *) newName;

/**
 * (no documentation provided)
 */
- (NSString *) description;

/**
 * (no documentation provided)
 */
- (void) setDescription: (NSString *) newDescription;

/**
 * (no documentation provided)
 */
- (NSString *) userId;

/**
 * (no documentation provided)
 */
- (void) setUserId: (NSString *) newUserId;
@end /* interface TASK_MANAGER_SERVERNS1Project */

#endif /* DEF_TASK_MANAGER_SERVERNS1Project_H */
