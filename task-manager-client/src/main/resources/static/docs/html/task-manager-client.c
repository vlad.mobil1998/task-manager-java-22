#include <enunciate-common.c>
#ifndef DEF_task_manager_client_ns0_domain_H
#define DEF_task_manager_client_ns0_domain_H

/**
 *  <p>Java class for domain complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="domain"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="projects" type="{http://endpoint.tm.amster.ru/}project" maxOccurs="unbounded" minOccurs="0"/&gt;
 *          &lt;element name="tasks" type="{http://endpoint.tm.amster.ru/}task" maxOccurs="unbounded" minOccurs="0"/&gt;
 *          &lt;element name="users" type="{http://endpoint.tm.amster.ru/}user" maxOccurs="unbounded" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_domain {


  /**
   * (no documentation provided)
   */
  struct task_manager_client_ns0_project *projects;

  /**
   * Size of the projects array.
   */
  int _sizeof_projects;

  /**
   * (no documentation provided)
   */
  struct task_manager_client_ns0_task *tasks;

  /**
   * Size of the tasks array.
   */
  int _sizeof_tasks;

  /**
   * (no documentation provided)
   */
  struct task_manager_client_ns0_user *users;

  /**
   * Size of the users array.
   */
  int _sizeof_users;
};

/**
 * Reads a Domain from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Domain, or NULL in case of error.
 */
static struct task_manager_client_ns0_domain *xmlTextReaderReadNs0DomainType(xmlTextReaderPtr reader);

/**
 * Writes a Domain to XML.
 *
 * @param writer The XML writer.
 * @param _domain The Domain to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0DomainType(xmlTextWriterPtr writer, struct task_manager_client_ns0_domain *_domain);

/**
 * Frees the elements of a Domain.
 *
 * @param _domain The Domain to free.
 */
static void freeNs0DomainType(struct task_manager_client_ns0_domain *_domain);

#endif /* DEF_task_manager_client_ns0_domain_H */
#ifndef DEF_task_manager_client_ns0_export_H
#define DEF_task_manager_client_ns0_export_H

/**
 *  <p>Java class for export complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="export"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_export {


  /**
   * (no documentation provided)
   */
  struct task_manager_client_ns0_session *session;
};

/**
 * Reads a Export element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}export", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The Export, or NULL in case of error.
 */
struct task_manager_client_ns0_export *xml_read_task_manager_client_ns0_export(xmlTextReaderPtr reader);

/**
 * Writes a Export to XML under element name "{http://endpoint.tm.amster.ru/}export".
 *
 * @param writer The XML writer.
 * @param _export The Export to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
int xml_write_task_manager_client_ns0_export(xmlTextWriterPtr writer, struct task_manager_client_ns0_export *_export);

/**
 * Frees a Export.
 *
 * @param _export The Export to free.
 */
void free_task_manager_client_ns0_export(struct task_manager_client_ns0_export *_export);

/**
 * Reads a Export element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}export", and
 * it is assumed that the reader is already pointing to the element.
 *
 * @param reader The XML reader.
 * @return The Export, or NULL in case of error.
 */
struct task_manager_client_ns0_export *xmlTextReaderReadNs0ExportElement(xmlTextReaderPtr reader);

/**
 * Writes a Export to XML under element name "{http://endpoint.tm.amster.ru/}export".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _export The Export to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0ExportElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_export *_export);

/**
 * Writes a Export to XML under element name "{http://endpoint.tm.amster.ru/}export".
 *
 * @param writer The XML writer.
 * @param _export The Export to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0ExportElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_export *_export, int writeNamespaces);

/**
 * Frees the children of a Export.
 *
 * @param _export The Export whose children are to be free.
 */
static void freeNs0ExportElement(struct task_manager_client_ns0_export *_export);

/**
 * Reads a Export from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Export, or NULL in case of error.
 */
static struct task_manager_client_ns0_export *xmlTextReaderReadNs0ExportType(xmlTextReaderPtr reader);

/**
 * Writes a Export to XML.
 *
 * @param writer The XML writer.
 * @param _export The Export to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0ExportType(xmlTextWriterPtr writer, struct task_manager_client_ns0_export *_export);

/**
 * Frees the elements of a Export.
 *
 * @param _export The Export to free.
 */
static void freeNs0ExportType(struct task_manager_client_ns0_export *_export);

#endif /* DEF_task_manager_client_ns0_export_H */
#ifndef DEF_task_manager_client_ns0_exportResponse_H
#define DEF_task_manager_client_ns0_exportResponse_H

/**
 *  <p>Java class for exportResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="exportResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="return" type="{http://endpoint.tm.amster.ru/}domain" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_exportResponse {


  /**
   * (no documentation provided)
   */
  struct task_manager_client_ns0_domain *_return;
};

/**
 * Reads a ExportResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}exportResponse", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The ExportResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_exportResponse *xml_read_task_manager_client_ns0_exportResponse(xmlTextReaderPtr reader);

/**
 * Writes a ExportResponse to XML under element name "{http://endpoint.tm.amster.ru/}exportResponse".
 *
 * @param writer The XML writer.
 * @param _exportResponse The ExportResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
int xml_write_task_manager_client_ns0_exportResponse(xmlTextWriterPtr writer, struct task_manager_client_ns0_exportResponse *_exportResponse);

/**
 * Frees a ExportResponse.
 *
 * @param _exportResponse The ExportResponse to free.
 */
void free_task_manager_client_ns0_exportResponse(struct task_manager_client_ns0_exportResponse *_exportResponse);

/**
 * Reads a ExportResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}exportResponse", and
 * it is assumed that the reader is already pointing to the element.
 *
 * @param reader The XML reader.
 * @return The ExportResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_exportResponse *xmlTextReaderReadNs0ExportResponseElement(xmlTextReaderPtr reader);

/**
 * Writes a ExportResponse to XML under element name "{http://endpoint.tm.amster.ru/}exportResponse".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _exportResponse The ExportResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0ExportResponseElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_exportResponse *_exportResponse);

/**
 * Writes a ExportResponse to XML under element name "{http://endpoint.tm.amster.ru/}exportResponse".
 *
 * @param writer The XML writer.
 * @param _exportResponse The ExportResponse to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0ExportResponseElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_exportResponse *_exportResponse, int writeNamespaces);

/**
 * Frees the children of a ExportResponse.
 *
 * @param _exportResponse The ExportResponse whose children are to be free.
 */
static void freeNs0ExportResponseElement(struct task_manager_client_ns0_exportResponse *_exportResponse);

/**
 * Reads a ExportResponse from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The ExportResponse, or NULL in case of error.
 */
static struct task_manager_client_ns0_exportResponse *xmlTextReaderReadNs0ExportResponseType(xmlTextReaderPtr reader);

/**
 * Writes a ExportResponse to XML.
 *
 * @param writer The XML writer.
 * @param _exportResponse The ExportResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0ExportResponseType(xmlTextWriterPtr writer, struct task_manager_client_ns0_exportResponse *_exportResponse);

/**
 * Frees the elements of a ExportResponse.
 *
 * @param _exportResponse The ExportResponse to free.
 */
static void freeNs0ExportResponseType(struct task_manager_client_ns0_exportResponse *_exportResponse);

#endif /* DEF_task_manager_client_ns0_exportResponse_H */
#ifndef DEF_task_manager_client_ns0_findAllUser_H
#define DEF_task_manager_client_ns0_findAllUser_H

/**
 *  <p>Java class for findAllUser complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="findAllUser"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_findAllUser {


  /**
   * (no documentation provided)
   */
  struct task_manager_client_ns0_session *session;
};

/**
 * Reads a FindAllUser element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findAllUser", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The FindAllUser, or NULL in case of error.
 */
struct task_manager_client_ns0_findAllUser *xml_read_task_manager_client_ns0_findAllUser(xmlTextReaderPtr reader);

/**
 * Writes a FindAllUser to XML under element name "{http://endpoint.tm.amster.ru/}findAllUser".
 *
 * @param writer The XML writer.
 * @param _findAllUser The FindAllUser to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
int xml_write_task_manager_client_ns0_findAllUser(xmlTextWriterPtr writer, struct task_manager_client_ns0_findAllUser *_findAllUser);

/**
 * Frees a FindAllUser.
 *
 * @param _findAllUser The FindAllUser to free.
 */
void free_task_manager_client_ns0_findAllUser(struct task_manager_client_ns0_findAllUser *_findAllUser);

/**
 * Reads a FindAllUser element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findAllUser", and
 * it is assumed that the reader is already pointing to the element.
 *
 * @param reader The XML reader.
 * @return The FindAllUser, or NULL in case of error.
 */
struct task_manager_client_ns0_findAllUser *xmlTextReaderReadNs0FindAllUserElement(xmlTextReaderPtr reader);

/**
 * Writes a FindAllUser to XML under element name "{http://endpoint.tm.amster.ru/}findAllUser".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _findAllUser The FindAllUser to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0FindAllUserElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_findAllUser *_findAllUser);

/**
 * Writes a FindAllUser to XML under element name "{http://endpoint.tm.amster.ru/}findAllUser".
 *
 * @param writer The XML writer.
 * @param _findAllUser The FindAllUser to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0FindAllUserElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_findAllUser *_findAllUser, int writeNamespaces);

/**
 * Frees the children of a FindAllUser.
 *
 * @param _findAllUser The FindAllUser whose children are to be free.
 */
static void freeNs0FindAllUserElement(struct task_manager_client_ns0_findAllUser *_findAllUser);

/**
 * Reads a FindAllUser from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The FindAllUser, or NULL in case of error.
 */
static struct task_manager_client_ns0_findAllUser *xmlTextReaderReadNs0FindAllUserType(xmlTextReaderPtr reader);

/**
 * Writes a FindAllUser to XML.
 *
 * @param writer The XML writer.
 * @param _findAllUser The FindAllUser to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0FindAllUserType(xmlTextWriterPtr writer, struct task_manager_client_ns0_findAllUser *_findAllUser);

/**
 * Frees the elements of a FindAllUser.
 *
 * @param _findAllUser The FindAllUser to free.
 */
static void freeNs0FindAllUserType(struct task_manager_client_ns0_findAllUser *_findAllUser);

#endif /* DEF_task_manager_client_ns0_findAllUser_H */
#ifndef DEF_task_manager_client_ns0_findAllUserResponse_H
#define DEF_task_manager_client_ns0_findAllUserResponse_H

/**
 *  <p>Java class for findAllUserResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="findAllUserResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="return" type="{http://endpoint.tm.amster.ru/}user" maxOccurs="unbounded" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_findAllUserResponse {


  /**
   * (no documentation provided)
   */
  struct task_manager_client_ns0_user *_return;

  /**
   * Size of the _return array.
   */
  int _sizeof__return;
};

/**
 * Reads a FindAllUserResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findAllUserResponse", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The FindAllUserResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_findAllUserResponse *xml_read_task_manager_client_ns0_findAllUserResponse(xmlTextReaderPtr reader);

/**
 * Writes a FindAllUserResponse to XML under element name "{http://endpoint.tm.amster.ru/}findAllUserResponse".
 *
 * @param writer The XML writer.
 * @param _findAllUserResponse The FindAllUserResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
int xml_write_task_manager_client_ns0_findAllUserResponse(xmlTextWriterPtr writer, struct task_manager_client_ns0_findAllUserResponse *_findAllUserResponse);

/**
 * Frees a FindAllUserResponse.
 *
 * @param _findAllUserResponse The FindAllUserResponse to free.
 */
void free_task_manager_client_ns0_findAllUserResponse(struct task_manager_client_ns0_findAllUserResponse *_findAllUserResponse);

/**
 * Reads a FindAllUserResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findAllUserResponse", and
 * it is assumed that the reader is already pointing to the element.
 *
 * @param reader The XML reader.
 * @return The FindAllUserResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_findAllUserResponse *xmlTextReaderReadNs0FindAllUserResponseElement(xmlTextReaderPtr reader);

/**
 * Writes a FindAllUserResponse to XML under element name "{http://endpoint.tm.amster.ru/}findAllUserResponse".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _findAllUserResponse The FindAllUserResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0FindAllUserResponseElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_findAllUserResponse *_findAllUserResponse);

/**
 * Writes a FindAllUserResponse to XML under element name "{http://endpoint.tm.amster.ru/}findAllUserResponse".
 *
 * @param writer The XML writer.
 * @param _findAllUserResponse The FindAllUserResponse to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0FindAllUserResponseElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_findAllUserResponse *_findAllUserResponse, int writeNamespaces);

/**
 * Frees the children of a FindAllUserResponse.
 *
 * @param _findAllUserResponse The FindAllUserResponse whose children are to be free.
 */
static void freeNs0FindAllUserResponseElement(struct task_manager_client_ns0_findAllUserResponse *_findAllUserResponse);

/**
 * Reads a FindAllUserResponse from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The FindAllUserResponse, or NULL in case of error.
 */
static struct task_manager_client_ns0_findAllUserResponse *xmlTextReaderReadNs0FindAllUserResponseType(xmlTextReaderPtr reader);

/**
 * Writes a FindAllUserResponse to XML.
 *
 * @param writer The XML writer.
 * @param _findAllUserResponse The FindAllUserResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0FindAllUserResponseType(xmlTextWriterPtr writer, struct task_manager_client_ns0_findAllUserResponse *_findAllUserResponse);

/**
 * Frees the elements of a FindAllUserResponse.
 *
 * @param _findAllUserResponse The FindAllUserResponse to free.
 */
static void freeNs0FindAllUserResponseType(struct task_manager_client_ns0_findAllUserResponse *_findAllUserResponse);

#endif /* DEF_task_manager_client_ns0_findAllUserResponse_H */
#ifndef DEF_task_manager_client_ns0_findUserById_H
#define DEF_task_manager_client_ns0_findUserById_H

/**
 *  <p>Java class for findUserById complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="findUserById"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_findUserById {


  /**
   * (no documentation provided)
   */
  struct task_manager_client_ns0_session *session;

  /**
   * (no documentation provided)
   */
  xmlChar *id;
};

/**
 * Reads a FindUserById element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findUserById", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The FindUserById, or NULL in case of error.
 */
struct task_manager_client_ns0_findUserById *xml_read_task_manager_client_ns0_findUserById(xmlTextReaderPtr reader);

/**
 * Writes a FindUserById to XML under element name "{http://endpoint.tm.amster.ru/}findUserById".
 *
 * @param writer The XML writer.
 * @param _findUserById The FindUserById to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
int xml_write_task_manager_client_ns0_findUserById(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserById *_findUserById);

/**
 * Frees a FindUserById.
 *
 * @param _findUserById The FindUserById to free.
 */
void free_task_manager_client_ns0_findUserById(struct task_manager_client_ns0_findUserById *_findUserById);

/**
 * Reads a FindUserById element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findUserById", and
 * it is assumed that the reader is already pointing to the element.
 *
 * @param reader The XML reader.
 * @return The FindUserById, or NULL in case of error.
 */
struct task_manager_client_ns0_findUserById *xmlTextReaderReadNs0FindUserByIdElement(xmlTextReaderPtr reader);

/**
 * Writes a FindUserById to XML under element name "{http://endpoint.tm.amster.ru/}findUserById".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _findUserById The FindUserById to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0FindUserByIdElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserById *_findUserById);

/**
 * Writes a FindUserById to XML under element name "{http://endpoint.tm.amster.ru/}findUserById".
 *
 * @param writer The XML writer.
 * @param _findUserById The FindUserById to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0FindUserByIdElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserById *_findUserById, int writeNamespaces);

/**
 * Frees the children of a FindUserById.
 *
 * @param _findUserById The FindUserById whose children are to be free.
 */
static void freeNs0FindUserByIdElement(struct task_manager_client_ns0_findUserById *_findUserById);

/**
 * Reads a FindUserById from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The FindUserById, or NULL in case of error.
 */
static struct task_manager_client_ns0_findUserById *xmlTextReaderReadNs0FindUserByIdType(xmlTextReaderPtr reader);

/**
 * Writes a FindUserById to XML.
 *
 * @param writer The XML writer.
 * @param _findUserById The FindUserById to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0FindUserByIdType(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserById *_findUserById);

/**
 * Frees the elements of a FindUserById.
 *
 * @param _findUserById The FindUserById to free.
 */
static void freeNs0FindUserByIdType(struct task_manager_client_ns0_findUserById *_findUserById);

#endif /* DEF_task_manager_client_ns0_findUserById_H */
#ifndef DEF_task_manager_client_ns0_findUserByIdResponse_H
#define DEF_task_manager_client_ns0_findUserByIdResponse_H

/**
 *  <p>Java class for findUserByIdResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="findUserByIdResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="return" type="{http://endpoint.tm.amster.ru/}user" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_findUserByIdResponse {


  /**
   * (no documentation provided)
   */
  struct task_manager_client_ns0_user *_return;
};

/**
 * Reads a FindUserByIdResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findUserByIdResponse", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The FindUserByIdResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_findUserByIdResponse *xml_read_task_manager_client_ns0_findUserByIdResponse(xmlTextReaderPtr reader);

/**
 * Writes a FindUserByIdResponse to XML under element name "{http://endpoint.tm.amster.ru/}findUserByIdResponse".
 *
 * @param writer The XML writer.
 * @param _findUserByIdResponse The FindUserByIdResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
int xml_write_task_manager_client_ns0_findUserByIdResponse(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByIdResponse *_findUserByIdResponse);

/**
 * Frees a FindUserByIdResponse.
 *
 * @param _findUserByIdResponse The FindUserByIdResponse to free.
 */
void free_task_manager_client_ns0_findUserByIdResponse(struct task_manager_client_ns0_findUserByIdResponse *_findUserByIdResponse);

/**
 * Reads a FindUserByIdResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findUserByIdResponse", and
 * it is assumed that the reader is already pointing to the element.
 *
 * @param reader The XML reader.
 * @return The FindUserByIdResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_findUserByIdResponse *xmlTextReaderReadNs0FindUserByIdResponseElement(xmlTextReaderPtr reader);

/**
 * Writes a FindUserByIdResponse to XML under element name "{http://endpoint.tm.amster.ru/}findUserByIdResponse".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _findUserByIdResponse The FindUserByIdResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0FindUserByIdResponseElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByIdResponse *_findUserByIdResponse);

/**
 * Writes a FindUserByIdResponse to XML under element name "{http://endpoint.tm.amster.ru/}findUserByIdResponse".
 *
 * @param writer The XML writer.
 * @param _findUserByIdResponse The FindUserByIdResponse to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0FindUserByIdResponseElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByIdResponse *_findUserByIdResponse, int writeNamespaces);

/**
 * Frees the children of a FindUserByIdResponse.
 *
 * @param _findUserByIdResponse The FindUserByIdResponse whose children are to be free.
 */
static void freeNs0FindUserByIdResponseElement(struct task_manager_client_ns0_findUserByIdResponse *_findUserByIdResponse);

/**
 * Reads a FindUserByIdResponse from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The FindUserByIdResponse, or NULL in case of error.
 */
static struct task_manager_client_ns0_findUserByIdResponse *xmlTextReaderReadNs0FindUserByIdResponseType(xmlTextReaderPtr reader);

/**
 * Writes a FindUserByIdResponse to XML.
 *
 * @param writer The XML writer.
 * @param _findUserByIdResponse The FindUserByIdResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0FindUserByIdResponseType(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByIdResponse *_findUserByIdResponse);

/**
 * Frees the elements of a FindUserByIdResponse.
 *
 * @param _findUserByIdResponse The FindUserByIdResponse to free.
 */
static void freeNs0FindUserByIdResponseType(struct task_manager_client_ns0_findUserByIdResponse *_findUserByIdResponse);

#endif /* DEF_task_manager_client_ns0_findUserByIdResponse_H */
#ifndef DEF_task_manager_client_ns0_findUserByLogin_H
#define DEF_task_manager_client_ns0_findUserByLogin_H

/**
 *  <p>Java class for findUserByLogin complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="findUserByLogin"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_findUserByLogin {


  /**
   * (no documentation provided)
   */
  struct task_manager_client_ns0_session *session;

  /**
   * (no documentation provided)
   */
  xmlChar *login;
};

/**
 * Reads a FindUserByLogin element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findUserByLogin", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The FindUserByLogin, or NULL in case of error.
 */
struct task_manager_client_ns0_findUserByLogin *xml_read_task_manager_client_ns0_findUserByLogin(xmlTextReaderPtr reader);

/**
 * Writes a FindUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}findUserByLogin".
 *
 * @param writer The XML writer.
 * @param _findUserByLogin The FindUserByLogin to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
int xml_write_task_manager_client_ns0_findUserByLogin(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByLogin *_findUserByLogin);

/**
 * Frees a FindUserByLogin.
 *
 * @param _findUserByLogin The FindUserByLogin to free.
 */
void free_task_manager_client_ns0_findUserByLogin(struct task_manager_client_ns0_findUserByLogin *_findUserByLogin);

/**
 * Reads a FindUserByLogin element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findUserByLogin", and
 * it is assumed that the reader is already pointing to the element.
 *
 * @param reader The XML reader.
 * @return The FindUserByLogin, or NULL in case of error.
 */
struct task_manager_client_ns0_findUserByLogin *xmlTextReaderReadNs0FindUserByLoginElement(xmlTextReaderPtr reader);

/**
 * Writes a FindUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}findUserByLogin".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _findUserByLogin The FindUserByLogin to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0FindUserByLoginElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByLogin *_findUserByLogin);

/**
 * Writes a FindUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}findUserByLogin".
 *
 * @param writer The XML writer.
 * @param _findUserByLogin The FindUserByLogin to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0FindUserByLoginElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByLogin *_findUserByLogin, int writeNamespaces);

/**
 * Frees the children of a FindUserByLogin.
 *
 * @param _findUserByLogin The FindUserByLogin whose children are to be free.
 */
static void freeNs0FindUserByLoginElement(struct task_manager_client_ns0_findUserByLogin *_findUserByLogin);

/**
 * Reads a FindUserByLogin from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The FindUserByLogin, or NULL in case of error.
 */
static struct task_manager_client_ns0_findUserByLogin *xmlTextReaderReadNs0FindUserByLoginType(xmlTextReaderPtr reader);

/**
 * Writes a FindUserByLogin to XML.
 *
 * @param writer The XML writer.
 * @param _findUserByLogin The FindUserByLogin to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0FindUserByLoginType(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByLogin *_findUserByLogin);

/**
 * Frees the elements of a FindUserByLogin.
 *
 * @param _findUserByLogin The FindUserByLogin to free.
 */
static void freeNs0FindUserByLoginType(struct task_manager_client_ns0_findUserByLogin *_findUserByLogin);

#endif /* DEF_task_manager_client_ns0_findUserByLogin_H */
#ifndef DEF_task_manager_client_ns0_findUserByLoginResponse_H
#define DEF_task_manager_client_ns0_findUserByLoginResponse_H

/**
 *  <p>Java class for findUserByLoginResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="findUserByLoginResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="return" type="{http://endpoint.tm.amster.ru/}user" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_findUserByLoginResponse {


  /**
   * (no documentation provided)
   */
  struct task_manager_client_ns0_user *_return;
};

/**
 * Reads a FindUserByLoginResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findUserByLoginResponse", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The FindUserByLoginResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_findUserByLoginResponse *xml_read_task_manager_client_ns0_findUserByLoginResponse(xmlTextReaderPtr reader);

/**
 * Writes a FindUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}findUserByLoginResponse".
 *
 * @param writer The XML writer.
 * @param _findUserByLoginResponse The FindUserByLoginResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
int xml_write_task_manager_client_ns0_findUserByLoginResponse(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByLoginResponse *_findUserByLoginResponse);

/**
 * Frees a FindUserByLoginResponse.
 *
 * @param _findUserByLoginResponse The FindUserByLoginResponse to free.
 */
void free_task_manager_client_ns0_findUserByLoginResponse(struct task_manager_client_ns0_findUserByLoginResponse *_findUserByLoginResponse);

/**
 * Reads a FindUserByLoginResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findUserByLoginResponse", and
 * it is assumed that the reader is already pointing to the element.
 *
 * @param reader The XML reader.
 * @return The FindUserByLoginResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_findUserByLoginResponse *xmlTextReaderReadNs0FindUserByLoginResponseElement(xmlTextReaderPtr reader);

/**
 * Writes a FindUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}findUserByLoginResponse".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _findUserByLoginResponse The FindUserByLoginResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0FindUserByLoginResponseElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByLoginResponse *_findUserByLoginResponse);

/**
 * Writes a FindUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}findUserByLoginResponse".
 *
 * @param writer The XML writer.
 * @param _findUserByLoginResponse The FindUserByLoginResponse to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0FindUserByLoginResponseElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByLoginResponse *_findUserByLoginResponse, int writeNamespaces);

/**
 * Frees the children of a FindUserByLoginResponse.
 *
 * @param _findUserByLoginResponse The FindUserByLoginResponse whose children are to be free.
 */
static void freeNs0FindUserByLoginResponseElement(struct task_manager_client_ns0_findUserByLoginResponse *_findUserByLoginResponse);

/**
 * Reads a FindUserByLoginResponse from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The FindUserByLoginResponse, or NULL in case of error.
 */
static struct task_manager_client_ns0_findUserByLoginResponse *xmlTextReaderReadNs0FindUserByLoginResponseType(xmlTextReaderPtr reader);

/**
 * Writes a FindUserByLoginResponse to XML.
 *
 * @param writer The XML writer.
 * @param _findUserByLoginResponse The FindUserByLoginResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0FindUserByLoginResponseType(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByLoginResponse *_findUserByLoginResponse);

/**
 * Frees the elements of a FindUserByLoginResponse.
 *
 * @param _findUserByLoginResponse The FindUserByLoginResponse to free.
 */
static void freeNs0FindUserByLoginResponseType(struct task_manager_client_ns0_findUserByLoginResponse *_findUserByLoginResponse);

#endif /* DEF_task_manager_client_ns0_findUserByLoginResponse_H */
#ifndef DEF_task_manager_client_ns0_load_H
#define DEF_task_manager_client_ns0_load_H

/**
 *  <p>Java class for load complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="load"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="domain" type="{http://endpoint.tm.amster.ru/}domain" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_load {


  /**
   * (no documentation provided)
   */
  struct task_manager_client_ns0_session *session;

  /**
   * (no documentation provided)
   */
  struct task_manager_client_ns0_domain *domain;
};

/**
 * Reads a Load element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}load", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The Load, or NULL in case of error.
 */
struct task_manager_client_ns0_load *xml_read_task_manager_client_ns0_load(xmlTextReaderPtr reader);

/**
 * Writes a Load to XML under element name "{http://endpoint.tm.amster.ru/}load".
 *
 * @param writer The XML writer.
 * @param _load The Load to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
int xml_write_task_manager_client_ns0_load(xmlTextWriterPtr writer, struct task_manager_client_ns0_load *_load);

/**
 * Frees a Load.
 *
 * @param _load The Load to free.
 */
void free_task_manager_client_ns0_load(struct task_manager_client_ns0_load *_load);

/**
 * Reads a Load element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}load", and
 * it is assumed that the reader is already pointing to the element.
 *
 * @param reader The XML reader.
 * @return The Load, or NULL in case of error.
 */
struct task_manager_client_ns0_load *xmlTextReaderReadNs0LoadElement(xmlTextReaderPtr reader);

/**
 * Writes a Load to XML under element name "{http://endpoint.tm.amster.ru/}load".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _load The Load to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0LoadElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_load *_load);

/**
 * Writes a Load to XML under element name "{http://endpoint.tm.amster.ru/}load".
 *
 * @param writer The XML writer.
 * @param _load The Load to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0LoadElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_load *_load, int writeNamespaces);

/**
 * Frees the children of a Load.
 *
 * @param _load The Load whose children are to be free.
 */
static void freeNs0LoadElement(struct task_manager_client_ns0_load *_load);

/**
 * Reads a Load from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Load, or NULL in case of error.
 */
static struct task_manager_client_ns0_load *xmlTextReaderReadNs0LoadType(xmlTextReaderPtr reader);

/**
 * Writes a Load to XML.
 *
 * @param writer The XML writer.
 * @param _load The Load to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0LoadType(xmlTextWriterPtr writer, struct task_manager_client_ns0_load *_load);

/**
 * Frees the elements of a Load.
 *
 * @param _load The Load to free.
 */
static void freeNs0LoadType(struct task_manager_client_ns0_load *_load);

#endif /* DEF_task_manager_client_ns0_load_H */
#ifndef DEF_task_manager_client_ns0_loadResponse_H
#define DEF_task_manager_client_ns0_loadResponse_H

/**
 *  <p>Java class for loadResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="loadResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_loadResponse {

};

/**
 * Reads a LoadResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}loadResponse", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The LoadResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_loadResponse *xml_read_task_manager_client_ns0_loadResponse(xmlTextReaderPtr reader);

/**
 * Writes a LoadResponse to XML under element name "{http://endpoint.tm.amster.ru/}loadResponse".
 *
 * @param writer The XML writer.
 * @param _loadResponse The LoadResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
int xml_write_task_manager_client_ns0_loadResponse(xmlTextWriterPtr writer, struct task_manager_client_ns0_loadResponse *_loadResponse);

/**
 * Frees a LoadResponse.
 *
 * @param _loadResponse The LoadResponse to free.
 */
void free_task_manager_client_ns0_loadResponse(struct task_manager_client_ns0_loadResponse *_loadResponse);

/**
 * Reads a LoadResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}loadResponse", and
 * it is assumed that the reader is already pointing to the element.
 *
 * @param reader The XML reader.
 * @return The LoadResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_loadResponse *xmlTextReaderReadNs0LoadResponseElement(xmlTextReaderPtr reader);

/**
 * Writes a LoadResponse to XML under element name "{http://endpoint.tm.amster.ru/}loadResponse".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _loadResponse The LoadResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0LoadResponseElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_loadResponse *_loadResponse);

/**
 * Writes a LoadResponse to XML under element name "{http://endpoint.tm.amster.ru/}loadResponse".
 *
 * @param writer The XML writer.
 * @param _loadResponse The LoadResponse to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0LoadResponseElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_loadResponse *_loadResponse, int writeNamespaces);

/**
 * Frees the children of a LoadResponse.
 *
 * @param _loadResponse The LoadResponse whose children are to be free.
 */
static void freeNs0LoadResponseElement(struct task_manager_client_ns0_loadResponse *_loadResponse);

/**
 * Reads a LoadResponse from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The LoadResponse, or NULL in case of error.
 */
static struct task_manager_client_ns0_loadResponse *xmlTextReaderReadNs0LoadResponseType(xmlTextReaderPtr reader);

/**
 * Writes a LoadResponse to XML.
 *
 * @param writer The XML writer.
 * @param _loadResponse The LoadResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0LoadResponseType(xmlTextWriterPtr writer, struct task_manager_client_ns0_loadResponse *_loadResponse);

/**
 * Frees the elements of a LoadResponse.
 *
 * @param _loadResponse The LoadResponse to free.
 */
static void freeNs0LoadResponseType(struct task_manager_client_ns0_loadResponse *_loadResponse);

#endif /* DEF_task_manager_client_ns0_loadResponse_H */
#ifndef DEF_task_manager_client_ns0_lockUserByLogin_H
#define DEF_task_manager_client_ns0_lockUserByLogin_H

/**
 *  <p>Java class for lockUserByLogin complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="lockUserByLogin"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_lockUserByLogin {


  /**
   * (no documentation provided)
   */
  struct task_manager_client_ns0_session *session;

  /**
   * (no documentation provided)
   */
  xmlChar *login;
};

/**
 * Reads a LockUserByLogin element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}lockUserByLogin", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The LockUserByLogin, or NULL in case of error.
 */
struct task_manager_client_ns0_lockUserByLogin *xml_read_task_manager_client_ns0_lockUserByLogin(xmlTextReaderPtr reader);

/**
 * Writes a LockUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}lockUserByLogin".
 *
 * @param writer The XML writer.
 * @param _lockUserByLogin The LockUserByLogin to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
int xml_write_task_manager_client_ns0_lockUserByLogin(xmlTextWriterPtr writer, struct task_manager_client_ns0_lockUserByLogin *_lockUserByLogin);

/**
 * Frees a LockUserByLogin.
 *
 * @param _lockUserByLogin The LockUserByLogin to free.
 */
void free_task_manager_client_ns0_lockUserByLogin(struct task_manager_client_ns0_lockUserByLogin *_lockUserByLogin);

/**
 * Reads a LockUserByLogin element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}lockUserByLogin", and
 * it is assumed that the reader is already pointing to the element.
 *
 * @param reader The XML reader.
 * @return The LockUserByLogin, or NULL in case of error.
 */
struct task_manager_client_ns0_lockUserByLogin *xmlTextReaderReadNs0LockUserByLoginElement(xmlTextReaderPtr reader);

/**
 * Writes a LockUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}lockUserByLogin".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _lockUserByLogin The LockUserByLogin to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0LockUserByLoginElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_lockUserByLogin *_lockUserByLogin);

/**
 * Writes a LockUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}lockUserByLogin".
 *
 * @param writer The XML writer.
 * @param _lockUserByLogin The LockUserByLogin to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0LockUserByLoginElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_lockUserByLogin *_lockUserByLogin, int writeNamespaces);

/**
 * Frees the children of a LockUserByLogin.
 *
 * @param _lockUserByLogin The LockUserByLogin whose children are to be free.
 */
static void freeNs0LockUserByLoginElement(struct task_manager_client_ns0_lockUserByLogin *_lockUserByLogin);

/**
 * Reads a LockUserByLogin from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The LockUserByLogin, or NULL in case of error.
 */
static struct task_manager_client_ns0_lockUserByLogin *xmlTextReaderReadNs0LockUserByLoginType(xmlTextReaderPtr reader);

/**
 * Writes a LockUserByLogin to XML.
 *
 * @param writer The XML writer.
 * @param _lockUserByLogin The LockUserByLogin to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0LockUserByLoginType(xmlTextWriterPtr writer, struct task_manager_client_ns0_lockUserByLogin *_lockUserByLogin);

/**
 * Frees the elements of a LockUserByLogin.
 *
 * @param _lockUserByLogin The LockUserByLogin to free.
 */
static void freeNs0LockUserByLoginType(struct task_manager_client_ns0_lockUserByLogin *_lockUserByLogin);

#endif /* DEF_task_manager_client_ns0_lockUserByLogin_H */
#ifndef DEF_task_manager_client_ns0_lockUserByLoginResponse_H
#define DEF_task_manager_client_ns0_lockUserByLoginResponse_H

/**
 *  <p>Java class for lockUserByLoginResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="lockUserByLoginResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="return" type="{http://endpoint.tm.amster.ru/}user" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_lockUserByLoginResponse {


  /**
   * (no documentation provided)
   */
  struct task_manager_client_ns0_user *_return;
};

/**
 * Reads a LockUserByLoginResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}lockUserByLoginResponse", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The LockUserByLoginResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_lockUserByLoginResponse *xml_read_task_manager_client_ns0_lockUserByLoginResponse(xmlTextReaderPtr reader);

/**
 * Writes a LockUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}lockUserByLoginResponse".
 *
 * @param writer The XML writer.
 * @param _lockUserByLoginResponse The LockUserByLoginResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
int xml_write_task_manager_client_ns0_lockUserByLoginResponse(xmlTextWriterPtr writer, struct task_manager_client_ns0_lockUserByLoginResponse *_lockUserByLoginResponse);

/**
 * Frees a LockUserByLoginResponse.
 *
 * @param _lockUserByLoginResponse The LockUserByLoginResponse to free.
 */
void free_task_manager_client_ns0_lockUserByLoginResponse(struct task_manager_client_ns0_lockUserByLoginResponse *_lockUserByLoginResponse);

/**
 * Reads a LockUserByLoginResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}lockUserByLoginResponse", and
 * it is assumed that the reader is already pointing to the element.
 *
 * @param reader The XML reader.
 * @return The LockUserByLoginResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_lockUserByLoginResponse *xmlTextReaderReadNs0LockUserByLoginResponseElement(xmlTextReaderPtr reader);

/**
 * Writes a LockUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}lockUserByLoginResponse".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _lockUserByLoginResponse The LockUserByLoginResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0LockUserByLoginResponseElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_lockUserByLoginResponse *_lockUserByLoginResponse);

/**
 * Writes a LockUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}lockUserByLoginResponse".
 *
 * @param writer The XML writer.
 * @param _lockUserByLoginResponse The LockUserByLoginResponse to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0LockUserByLoginResponseElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_lockUserByLoginResponse *_lockUserByLoginResponse, int writeNamespaces);

/**
 * Frees the children of a LockUserByLoginResponse.
 *
 * @param _lockUserByLoginResponse The LockUserByLoginResponse whose children are to be free.
 */
static void freeNs0LockUserByLoginResponseElement(struct task_manager_client_ns0_lockUserByLoginResponse *_lockUserByLoginResponse);

/**
 * Reads a LockUserByLoginResponse from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The LockUserByLoginResponse, or NULL in case of error.
 */
static struct task_manager_client_ns0_lockUserByLoginResponse *xmlTextReaderReadNs0LockUserByLoginResponseType(xmlTextReaderPtr reader);

/**
 * Writes a LockUserByLoginResponse to XML.
 *
 * @param writer The XML writer.
 * @param _lockUserByLoginResponse The LockUserByLoginResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0LockUserByLoginResponseType(xmlTextWriterPtr writer, struct task_manager_client_ns0_lockUserByLoginResponse *_lockUserByLoginResponse);

/**
 * Frees the elements of a LockUserByLoginResponse.
 *
 * @param _lockUserByLoginResponse The LockUserByLoginResponse to free.
 */
static void freeNs0LockUserByLoginResponseType(struct task_manager_client_ns0_lockUserByLoginResponse *_lockUserByLoginResponse);

#endif /* DEF_task_manager_client_ns0_lockUserByLoginResponse_H */
#ifndef DEF_task_manager_client_ns0_project_H
#define DEF_task_manager_client_ns0_project_H

/**
 *  <p>Java class for project complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="project"&gt;
 *    &lt;complexContent&gt;
 *      &lt;extension base="{http://endpoint.tm.amster.ru/}abstractEntity"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/extension&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_project {


  /**
   * (no documentation provided)
   */
  xmlChar *id;

  /**
   * (no documentation provided)
   */
  xmlChar *description;

  /**
   * (no documentation provided)
   */
  xmlChar *name;

  /**
   * (no documentation provided)
   */
  xmlChar *userId;
};

/**
 * Reads a Project from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Project, or NULL in case of error.
 */
static struct task_manager_client_ns0_project *xmlTextReaderReadNs0ProjectType(xmlTextReaderPtr reader);

/**
 * Writes a Project to XML.
 *
 * @param writer The XML writer.
 * @param _project The Project to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0ProjectType(xmlTextWriterPtr writer, struct task_manager_client_ns0_project *_project);

/**
 * Frees the elements of a Project.
 *
 * @param _project The Project to free.
 */
static void freeNs0ProjectType(struct task_manager_client_ns0_project *_project);

#endif /* DEF_task_manager_client_ns0_project_H */
#ifndef DEF_task_manager_client_ns0_removeUserById_H
#define DEF_task_manager_client_ns0_removeUserById_H

/**
 *  <p>Java class for removeUserById complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="removeUserById"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_removeUserById {


  /**
   * (no documentation provided)
   */
  struct task_manager_client_ns0_session *session;

  /**
   * (no documentation provided)
   */
  xmlChar *id;
};

/**
 * Reads a RemoveUserById element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}removeUserById", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The RemoveUserById, or NULL in case of error.
 */
struct task_manager_client_ns0_removeUserById *xml_read_task_manager_client_ns0_removeUserById(xmlTextReaderPtr reader);

/**
 * Writes a RemoveUserById to XML under element name "{http://endpoint.tm.amster.ru/}removeUserById".
 *
 * @param writer The XML writer.
 * @param _removeUserById The RemoveUserById to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
int xml_write_task_manager_client_ns0_removeUserById(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserById *_removeUserById);

/**
 * Frees a RemoveUserById.
 *
 * @param _removeUserById The RemoveUserById to free.
 */
void free_task_manager_client_ns0_removeUserById(struct task_manager_client_ns0_removeUserById *_removeUserById);

/**
 * Reads a RemoveUserById element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}removeUserById", and
 * it is assumed that the reader is already pointing to the element.
 *
 * @param reader The XML reader.
 * @return The RemoveUserById, or NULL in case of error.
 */
struct task_manager_client_ns0_removeUserById *xmlTextReaderReadNs0RemoveUserByIdElement(xmlTextReaderPtr reader);

/**
 * Writes a RemoveUserById to XML under element name "{http://endpoint.tm.amster.ru/}removeUserById".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _removeUserById The RemoveUserById to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0RemoveUserByIdElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserById *_removeUserById);

/**
 * Writes a RemoveUserById to XML under element name "{http://endpoint.tm.amster.ru/}removeUserById".
 *
 * @param writer The XML writer.
 * @param _removeUserById The RemoveUserById to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0RemoveUserByIdElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserById *_removeUserById, int writeNamespaces);

/**
 * Frees the children of a RemoveUserById.
 *
 * @param _removeUserById The RemoveUserById whose children are to be free.
 */
static void freeNs0RemoveUserByIdElement(struct task_manager_client_ns0_removeUserById *_removeUserById);

/**
 * Reads a RemoveUserById from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The RemoveUserById, or NULL in case of error.
 */
static struct task_manager_client_ns0_removeUserById *xmlTextReaderReadNs0RemoveUserByIdType(xmlTextReaderPtr reader);

/**
 * Writes a RemoveUserById to XML.
 *
 * @param writer The XML writer.
 * @param _removeUserById The RemoveUserById to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0RemoveUserByIdType(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserById *_removeUserById);

/**
 * Frees the elements of a RemoveUserById.
 *
 * @param _removeUserById The RemoveUserById to free.
 */
static void freeNs0RemoveUserByIdType(struct task_manager_client_ns0_removeUserById *_removeUserById);

#endif /* DEF_task_manager_client_ns0_removeUserById_H */
#ifndef DEF_task_manager_client_ns0_removeUserByIdResponse_H
#define DEF_task_manager_client_ns0_removeUserByIdResponse_H

/**
 *  <p>Java class for removeUserByIdResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="removeUserByIdResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_removeUserByIdResponse {

};

/**
 * Reads a RemoveUserByIdResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}removeUserByIdResponse", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The RemoveUserByIdResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_removeUserByIdResponse *xml_read_task_manager_client_ns0_removeUserByIdResponse(xmlTextReaderPtr reader);

/**
 * Writes a RemoveUserByIdResponse to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByIdResponse".
 *
 * @param writer The XML writer.
 * @param _removeUserByIdResponse The RemoveUserByIdResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
int xml_write_task_manager_client_ns0_removeUserByIdResponse(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByIdResponse *_removeUserByIdResponse);

/**
 * Frees a RemoveUserByIdResponse.
 *
 * @param _removeUserByIdResponse The RemoveUserByIdResponse to free.
 */
void free_task_manager_client_ns0_removeUserByIdResponse(struct task_manager_client_ns0_removeUserByIdResponse *_removeUserByIdResponse);

/**
 * Reads a RemoveUserByIdResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}removeUserByIdResponse", and
 * it is assumed that the reader is already pointing to the element.
 *
 * @param reader The XML reader.
 * @return The RemoveUserByIdResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_removeUserByIdResponse *xmlTextReaderReadNs0RemoveUserByIdResponseElement(xmlTextReaderPtr reader);

/**
 * Writes a RemoveUserByIdResponse to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByIdResponse".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _removeUserByIdResponse The RemoveUserByIdResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0RemoveUserByIdResponseElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByIdResponse *_removeUserByIdResponse);

/**
 * Writes a RemoveUserByIdResponse to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByIdResponse".
 *
 * @param writer The XML writer.
 * @param _removeUserByIdResponse The RemoveUserByIdResponse to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0RemoveUserByIdResponseElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByIdResponse *_removeUserByIdResponse, int writeNamespaces);

/**
 * Frees the children of a RemoveUserByIdResponse.
 *
 * @param _removeUserByIdResponse The RemoveUserByIdResponse whose children are to be free.
 */
static void freeNs0RemoveUserByIdResponseElement(struct task_manager_client_ns0_removeUserByIdResponse *_removeUserByIdResponse);

/**
 * Reads a RemoveUserByIdResponse from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The RemoveUserByIdResponse, or NULL in case of error.
 */
static struct task_manager_client_ns0_removeUserByIdResponse *xmlTextReaderReadNs0RemoveUserByIdResponseType(xmlTextReaderPtr reader);

/**
 * Writes a RemoveUserByIdResponse to XML.
 *
 * @param writer The XML writer.
 * @param _removeUserByIdResponse The RemoveUserByIdResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0RemoveUserByIdResponseType(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByIdResponse *_removeUserByIdResponse);

/**
 * Frees the elements of a RemoveUserByIdResponse.
 *
 * @param _removeUserByIdResponse The RemoveUserByIdResponse to free.
 */
static void freeNs0RemoveUserByIdResponseType(struct task_manager_client_ns0_removeUserByIdResponse *_removeUserByIdResponse);

#endif /* DEF_task_manager_client_ns0_removeUserByIdResponse_H */
#ifndef DEF_task_manager_client_ns0_removeUserByLogin_H
#define DEF_task_manager_client_ns0_removeUserByLogin_H

/**
 *  <p>Java class for removeUserByLogin complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="removeUserByLogin"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_removeUserByLogin {


  /**
   * (no documentation provided)
   */
  struct task_manager_client_ns0_session *session;

  /**
   * (no documentation provided)
   */
  xmlChar *login;
};

/**
 * Reads a RemoveUserByLogin element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}removeUserByLogin", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The RemoveUserByLogin, or NULL in case of error.
 */
struct task_manager_client_ns0_removeUserByLogin *xml_read_task_manager_client_ns0_removeUserByLogin(xmlTextReaderPtr reader);

/**
 * Writes a RemoveUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByLogin".
 *
 * @param writer The XML writer.
 * @param _removeUserByLogin The RemoveUserByLogin to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
int xml_write_task_manager_client_ns0_removeUserByLogin(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByLogin *_removeUserByLogin);

/**
 * Frees a RemoveUserByLogin.
 *
 * @param _removeUserByLogin The RemoveUserByLogin to free.
 */
void free_task_manager_client_ns0_removeUserByLogin(struct task_manager_client_ns0_removeUserByLogin *_removeUserByLogin);

/**
 * Reads a RemoveUserByLogin element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}removeUserByLogin", and
 * it is assumed that the reader is already pointing to the element.
 *
 * @param reader The XML reader.
 * @return The RemoveUserByLogin, or NULL in case of error.
 */
struct task_manager_client_ns0_removeUserByLogin *xmlTextReaderReadNs0RemoveUserByLoginElement(xmlTextReaderPtr reader);

/**
 * Writes a RemoveUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByLogin".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _removeUserByLogin The RemoveUserByLogin to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0RemoveUserByLoginElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByLogin *_removeUserByLogin);

/**
 * Writes a RemoveUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByLogin".
 *
 * @param writer The XML writer.
 * @param _removeUserByLogin The RemoveUserByLogin to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0RemoveUserByLoginElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByLogin *_removeUserByLogin, int writeNamespaces);

/**
 * Frees the children of a RemoveUserByLogin.
 *
 * @param _removeUserByLogin The RemoveUserByLogin whose children are to be free.
 */
static void freeNs0RemoveUserByLoginElement(struct task_manager_client_ns0_removeUserByLogin *_removeUserByLogin);

/**
 * Reads a RemoveUserByLogin from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The RemoveUserByLogin, or NULL in case of error.
 */
static struct task_manager_client_ns0_removeUserByLogin *xmlTextReaderReadNs0RemoveUserByLoginType(xmlTextReaderPtr reader);

/**
 * Writes a RemoveUserByLogin to XML.
 *
 * @param writer The XML writer.
 * @param _removeUserByLogin The RemoveUserByLogin to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0RemoveUserByLoginType(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByLogin *_removeUserByLogin);

/**
 * Frees the elements of a RemoveUserByLogin.
 *
 * @param _removeUserByLogin The RemoveUserByLogin to free.
 */
static void freeNs0RemoveUserByLoginType(struct task_manager_client_ns0_removeUserByLogin *_removeUserByLogin);

#endif /* DEF_task_manager_client_ns0_removeUserByLogin_H */
#ifndef DEF_task_manager_client_ns0_removeUserByLoginResponse_H
#define DEF_task_manager_client_ns0_removeUserByLoginResponse_H

/**
 *  <p>Java class for removeUserByLoginResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="removeUserByLoginResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_removeUserByLoginResponse {

};

/**
 * Reads a RemoveUserByLoginResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}removeUserByLoginResponse", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The RemoveUserByLoginResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_removeUserByLoginResponse *xml_read_task_manager_client_ns0_removeUserByLoginResponse(xmlTextReaderPtr reader);

/**
 * Writes a RemoveUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByLoginResponse".
 *
 * @param writer The XML writer.
 * @param _removeUserByLoginResponse The RemoveUserByLoginResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
int xml_write_task_manager_client_ns0_removeUserByLoginResponse(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByLoginResponse *_removeUserByLoginResponse);

/**
 * Frees a RemoveUserByLoginResponse.
 *
 * @param _removeUserByLoginResponse The RemoveUserByLoginResponse to free.
 */
void free_task_manager_client_ns0_removeUserByLoginResponse(struct task_manager_client_ns0_removeUserByLoginResponse *_removeUserByLoginResponse);

/**
 * Reads a RemoveUserByLoginResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}removeUserByLoginResponse", and
 * it is assumed that the reader is already pointing to the element.
 *
 * @param reader The XML reader.
 * @return The RemoveUserByLoginResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_removeUserByLoginResponse *xmlTextReaderReadNs0RemoveUserByLoginResponseElement(xmlTextReaderPtr reader);

/**
 * Writes a RemoveUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByLoginResponse".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _removeUserByLoginResponse The RemoveUserByLoginResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0RemoveUserByLoginResponseElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByLoginResponse *_removeUserByLoginResponse);

/**
 * Writes a RemoveUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByLoginResponse".
 *
 * @param writer The XML writer.
 * @param _removeUserByLoginResponse The RemoveUserByLoginResponse to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0RemoveUserByLoginResponseElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByLoginResponse *_removeUserByLoginResponse, int writeNamespaces);

/**
 * Frees the children of a RemoveUserByLoginResponse.
 *
 * @param _removeUserByLoginResponse The RemoveUserByLoginResponse whose children are to be free.
 */
static void freeNs0RemoveUserByLoginResponseElement(struct task_manager_client_ns0_removeUserByLoginResponse *_removeUserByLoginResponse);

/**
 * Reads a RemoveUserByLoginResponse from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The RemoveUserByLoginResponse, or NULL in case of error.
 */
static struct task_manager_client_ns0_removeUserByLoginResponse *xmlTextReaderReadNs0RemoveUserByLoginResponseType(xmlTextReaderPtr reader);

/**
 * Writes a RemoveUserByLoginResponse to XML.
 *
 * @param writer The XML writer.
 * @param _removeUserByLoginResponse The RemoveUserByLoginResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0RemoveUserByLoginResponseType(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByLoginResponse *_removeUserByLoginResponse);

/**
 * Frees the elements of a RemoveUserByLoginResponse.
 *
 * @param _removeUserByLoginResponse The RemoveUserByLoginResponse to free.
 */
static void freeNs0RemoveUserByLoginResponseType(struct task_manager_client_ns0_removeUserByLoginResponse *_removeUserByLoginResponse);

#endif /* DEF_task_manager_client_ns0_removeUserByLoginResponse_H */
#ifndef DEF_task_manager_client_ns0_role_H
#define DEF_task_manager_client_ns0_role_H

/**
 *  <p>Java class for role.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  <p>
 *  <pre>
 *  &lt;simpleType name="role"&gt;
 *    &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *      &lt;enumeration value="USER"/&gt;
 *      &lt;enumeration value="ADMIN"/&gt;
 *    &lt;/restriction&gt;
 *  &lt;/simpleType&gt;
 *  </pre>
 *  
 */
enum task_manager_client_ns0_role {

  /**
   * (no documentation provided)
   */
  TASK_MANAGER_CLIENT_NS0_ROLE_USER,

  /**
   * (no documentation provided)
   */
  TASK_MANAGER_CLIENT_NS0_ROLE_ADMIN
};

/**
 * Reads a Role from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Role, or NULL if unable to be read.
 */
static enum task_manager_client_ns0_role *xmlTextReaderReadNs0RoleType(xmlTextReaderPtr reader);

/**
 * Writes a Role to XML.
 *
 * @param writer The XML writer.
 * @param _role The Role to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0RoleType(xmlTextWriterPtr writer, enum task_manager_client_ns0_role *_role);

/**
 * Frees a Role.
 *
 * @param _role The Role to free.
 */
static void freeNs0RoleType(enum task_manager_client_ns0_role *_role);

#endif
#ifndef DEF_task_manager_client_ns0_session_H
#define DEF_task_manager_client_ns0_session_H

/**
 *  <p>Java class for session complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="session"&gt;
 *    &lt;complexContent&gt;
 *      &lt;extension base="{http://endpoint.tm.amster.ru/}abstractEntity"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="signature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="timestamp" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *          &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/extension&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_session {


  /**
   * (no documentation provided)
   */
  xmlChar *id;

  /**
   * (no documentation provided)
   */
  xmlChar *signature;

  /**
   * (no documentation provided)
   */
  long long *timestamp;

  /**
   * (no documentation provided)
   */
  xmlChar *userId;
};

/**
 * Reads a Session from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Session, or NULL in case of error.
 */
static struct task_manager_client_ns0_session *xmlTextReaderReadNs0SessionType(xmlTextReaderPtr reader);

/**
 * Writes a Session to XML.
 *
 * @param writer The XML writer.
 * @param _session The Session to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0SessionType(xmlTextWriterPtr writer, struct task_manager_client_ns0_session *_session);

/**
 * Frees the elements of a Session.
 *
 * @param _session The Session to free.
 */
static void freeNs0SessionType(struct task_manager_client_ns0_session *_session);

#endif /* DEF_task_manager_client_ns0_session_H */
#ifndef DEF_task_manager_client_ns0_task_H
#define DEF_task_manager_client_ns0_task_H

/**
 *  <p>Java class for task complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="task"&gt;
 *    &lt;complexContent&gt;
 *      &lt;extension base="{http://endpoint.tm.amster.ru/}abstractEntity"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/extension&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_task {


  /**
   * (no documentation provided)
   */
  xmlChar *id;

  /**
   * (no documentation provided)
   */
  xmlChar *description;

  /**
   * (no documentation provided)
   */
  xmlChar *name;

  /**
   * (no documentation provided)
   */
  xmlChar *userId;
};

/**
 * Reads a Task from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Task, or NULL in case of error.
 */
static struct task_manager_client_ns0_task *xmlTextReaderReadNs0TaskType(xmlTextReaderPtr reader);

/**
 * Writes a Task to XML.
 *
 * @param writer The XML writer.
 * @param _task The Task to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0TaskType(xmlTextWriterPtr writer, struct task_manager_client_ns0_task *_task);

/**
 * Frees the elements of a Task.
 *
 * @param _task The Task to free.
 */
static void freeNs0TaskType(struct task_manager_client_ns0_task *_task);

#endif /* DEF_task_manager_client_ns0_task_H */
#ifndef DEF_task_manager_client_ns0_unlockUserByLogin_H
#define DEF_task_manager_client_ns0_unlockUserByLogin_H

/**
 *  <p>Java class for unlockUserByLogin complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="unlockUserByLogin"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_unlockUserByLogin {


  /**
   * (no documentation provided)
   */
  struct task_manager_client_ns0_session *session;

  /**
   * (no documentation provided)
   */
  xmlChar *login;
};

/**
 * Reads a UnlockUserByLogin element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}unlockUserByLogin", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The UnlockUserByLogin, or NULL in case of error.
 */
struct task_manager_client_ns0_unlockUserByLogin *xml_read_task_manager_client_ns0_unlockUserByLogin(xmlTextReaderPtr reader);

/**
 * Writes a UnlockUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}unlockUserByLogin".
 *
 * @param writer The XML writer.
 * @param _unlockUserByLogin The UnlockUserByLogin to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
int xml_write_task_manager_client_ns0_unlockUserByLogin(xmlTextWriterPtr writer, struct task_manager_client_ns0_unlockUserByLogin *_unlockUserByLogin);

/**
 * Frees a UnlockUserByLogin.
 *
 * @param _unlockUserByLogin The UnlockUserByLogin to free.
 */
void free_task_manager_client_ns0_unlockUserByLogin(struct task_manager_client_ns0_unlockUserByLogin *_unlockUserByLogin);

/**
 * Reads a UnlockUserByLogin element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}unlockUserByLogin", and
 * it is assumed that the reader is already pointing to the element.
 *
 * @param reader The XML reader.
 * @return The UnlockUserByLogin, or NULL in case of error.
 */
struct task_manager_client_ns0_unlockUserByLogin *xmlTextReaderReadNs0UnlockUserByLoginElement(xmlTextReaderPtr reader);

/**
 * Writes a UnlockUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}unlockUserByLogin".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _unlockUserByLogin The UnlockUserByLogin to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0UnlockUserByLoginElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_unlockUserByLogin *_unlockUserByLogin);

/**
 * Writes a UnlockUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}unlockUserByLogin".
 *
 * @param writer The XML writer.
 * @param _unlockUserByLogin The UnlockUserByLogin to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0UnlockUserByLoginElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_unlockUserByLogin *_unlockUserByLogin, int writeNamespaces);

/**
 * Frees the children of a UnlockUserByLogin.
 *
 * @param _unlockUserByLogin The UnlockUserByLogin whose children are to be free.
 */
static void freeNs0UnlockUserByLoginElement(struct task_manager_client_ns0_unlockUserByLogin *_unlockUserByLogin);

/**
 * Reads a UnlockUserByLogin from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The UnlockUserByLogin, or NULL in case of error.
 */
static struct task_manager_client_ns0_unlockUserByLogin *xmlTextReaderReadNs0UnlockUserByLoginType(xmlTextReaderPtr reader);

/**
 * Writes a UnlockUserByLogin to XML.
 *
 * @param writer The XML writer.
 * @param _unlockUserByLogin The UnlockUserByLogin to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0UnlockUserByLoginType(xmlTextWriterPtr writer, struct task_manager_client_ns0_unlockUserByLogin *_unlockUserByLogin);

/**
 * Frees the elements of a UnlockUserByLogin.
 *
 * @param _unlockUserByLogin The UnlockUserByLogin to free.
 */
static void freeNs0UnlockUserByLoginType(struct task_manager_client_ns0_unlockUserByLogin *_unlockUserByLogin);

#endif /* DEF_task_manager_client_ns0_unlockUserByLogin_H */
#ifndef DEF_task_manager_client_ns0_unlockUserByLoginResponse_H
#define DEF_task_manager_client_ns0_unlockUserByLoginResponse_H

/**
 *  <p>Java class for unlockUserByLoginResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="unlockUserByLoginResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="return" type="{http://endpoint.tm.amster.ru/}user" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_unlockUserByLoginResponse {


  /**
   * (no documentation provided)
   */
  struct task_manager_client_ns0_user *_return;
};

/**
 * Reads a UnlockUserByLoginResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}unlockUserByLoginResponse", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The UnlockUserByLoginResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_unlockUserByLoginResponse *xml_read_task_manager_client_ns0_unlockUserByLoginResponse(xmlTextReaderPtr reader);

/**
 * Writes a UnlockUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}unlockUserByLoginResponse".
 *
 * @param writer The XML writer.
 * @param _unlockUserByLoginResponse The UnlockUserByLoginResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
int xml_write_task_manager_client_ns0_unlockUserByLoginResponse(xmlTextWriterPtr writer, struct task_manager_client_ns0_unlockUserByLoginResponse *_unlockUserByLoginResponse);

/**
 * Frees a UnlockUserByLoginResponse.
 *
 * @param _unlockUserByLoginResponse The UnlockUserByLoginResponse to free.
 */
void free_task_manager_client_ns0_unlockUserByLoginResponse(struct task_manager_client_ns0_unlockUserByLoginResponse *_unlockUserByLoginResponse);

/**
 * Reads a UnlockUserByLoginResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}unlockUserByLoginResponse", and
 * it is assumed that the reader is already pointing to the element.
 *
 * @param reader The XML reader.
 * @return The UnlockUserByLoginResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_unlockUserByLoginResponse *xmlTextReaderReadNs0UnlockUserByLoginResponseElement(xmlTextReaderPtr reader);

/**
 * Writes a UnlockUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}unlockUserByLoginResponse".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _unlockUserByLoginResponse The UnlockUserByLoginResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0UnlockUserByLoginResponseElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_unlockUserByLoginResponse *_unlockUserByLoginResponse);

/**
 * Writes a UnlockUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}unlockUserByLoginResponse".
 *
 * @param writer The XML writer.
 * @param _unlockUserByLoginResponse The UnlockUserByLoginResponse to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0UnlockUserByLoginResponseElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_unlockUserByLoginResponse *_unlockUserByLoginResponse, int writeNamespaces);

/**
 * Frees the children of a UnlockUserByLoginResponse.
 *
 * @param _unlockUserByLoginResponse The UnlockUserByLoginResponse whose children are to be free.
 */
static void freeNs0UnlockUserByLoginResponseElement(struct task_manager_client_ns0_unlockUserByLoginResponse *_unlockUserByLoginResponse);

/**
 * Reads a UnlockUserByLoginResponse from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The UnlockUserByLoginResponse, or NULL in case of error.
 */
static struct task_manager_client_ns0_unlockUserByLoginResponse *xmlTextReaderReadNs0UnlockUserByLoginResponseType(xmlTextReaderPtr reader);

/**
 * Writes a UnlockUserByLoginResponse to XML.
 *
 * @param writer The XML writer.
 * @param _unlockUserByLoginResponse The UnlockUserByLoginResponse to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0UnlockUserByLoginResponseType(xmlTextWriterPtr writer, struct task_manager_client_ns0_unlockUserByLoginResponse *_unlockUserByLoginResponse);

/**
 * Frees the elements of a UnlockUserByLoginResponse.
 *
 * @param _unlockUserByLoginResponse The UnlockUserByLoginResponse to free.
 */
static void freeNs0UnlockUserByLoginResponseType(struct task_manager_client_ns0_unlockUserByLoginResponse *_unlockUserByLoginResponse);

#endif /* DEF_task_manager_client_ns0_unlockUserByLoginResponse_H */
#ifndef DEF_task_manager_client_ns0_user_H
#define DEF_task_manager_client_ns0_user_H

/**
 *  <p>Java class for user complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="user"&gt;
 *    &lt;complexContent&gt;
 *      &lt;extension base="{http://endpoint.tm.amster.ru/}abstractEntity"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="fistName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="locked" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *          &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="middleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="passwordHash" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="role" type="{http://endpoint.tm.amster.ru/}role" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/extension&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
struct task_manager_client_ns0_user {


  /**
   * (no documentation provided)
   */
  xmlChar *id;

  /**
   * (no documentation provided)
   */
  xmlChar *email;

  /**
   * (no documentation provided)
   */
  xmlChar *fistName;

  /**
   * (no documentation provided)
   */
  xmlChar *lastName;

  /**
   * (no documentation provided)
   */
  int *locked;

  /**
   * (no documentation provided)
   */
  xmlChar *login;

  /**
   * (no documentation provided)
   */
  xmlChar *middleName;

  /**
   * (no documentation provided)
   */
  xmlChar *passwordHash;

  /**
   * (no documentation provided)
   */
  enum task_manager_client_ns0_role *role;
};

/**
 * Reads a User from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The User, or NULL in case of error.
 */
static struct task_manager_client_ns0_user *xmlTextReaderReadNs0UserType(xmlTextReaderPtr reader);

/**
 * Writes a User to XML.
 *
 * @param writer The XML writer.
 * @param _user The User to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0UserType(xmlTextWriterPtr writer, struct task_manager_client_ns0_user *_user);

/**
 * Frees the elements of a User.
 *
 * @param _user The User to free.
 */
static void freeNs0UserType(struct task_manager_client_ns0_user *_user);

#endif /* DEF_task_manager_client_ns0_user_H */
#ifndef DEF_task_manager_client_ns0_domain_M
#define DEF_task_manager_client_ns0_domain_M

/**
 * Reads a Domain from XML. The reader is assumed to be at the start element.
 *
 * @return the Domain, or NULL in case of error.
 */
static struct task_manager_client_ns0_domain *xmlTextReaderReadNs0DomainType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_domain *_domain = calloc(1, sizeof(struct task_manager_client_ns0_domain));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs0DomainType(_domain);
        free(_domain);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "projects", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

          status = 1;
          if (xmlTextReaderMoveToAttributeNs(reader, BAD_CAST "nil", BAD_CAST "http://www.w3.org/2001/XMLSchema-instance")) {
            if (xmlStrcmp(BAD_CAST "true", xmlTextReaderConstValue(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
              printf("Choice {}projects was nil according to the xsi:nil attribute.\n");
#endif
              status = 0;
            }
          }

          xmlTextReaderMoveToElement(reader); //move back to the element
          if (status) { //if not "nil"...
#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}projects of type {http://endpoint.tm.amster.ru/}project.\n");
#endif
        _child_accessor = xmlTextReaderReadNs0ProjectType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}projects of type {http://endpoint.tm.amster.ru/}project.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0DomainType(_domain);
          free(_domain);
          return NULL;
        }

        _domain->projects = realloc(_domain->projects, (_domain->_sizeof_projects + 1) * sizeof(struct task_manager_client_ns0_project));
        memcpy(&(_domain->projects[_domain->_sizeof_projects++]), _child_accessor, sizeof(struct task_manager_client_ns0_project));
        free(_child_accessor);
          } //end "if not nil" clause
          else if (xmlTextReaderIsEmptyElement(reader) == 0) {
            //if it's not the empty element, skip it because it's nil.
            xmlTextReaderSkipElement(reader);
          }
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "tasks", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

          status = 1;
          if (xmlTextReaderMoveToAttributeNs(reader, BAD_CAST "nil", BAD_CAST "http://www.w3.org/2001/XMLSchema-instance")) {
            if (xmlStrcmp(BAD_CAST "true", xmlTextReaderConstValue(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
              printf("Choice {}tasks was nil according to the xsi:nil attribute.\n");
#endif
              status = 0;
            }
          }

          xmlTextReaderMoveToElement(reader); //move back to the element
          if (status) { //if not "nil"...
#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}tasks of type {http://endpoint.tm.amster.ru/}task.\n");
#endif
        _child_accessor = xmlTextReaderReadNs0TaskType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}tasks of type {http://endpoint.tm.amster.ru/}task.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0DomainType(_domain);
          free(_domain);
          return NULL;
        }

        _domain->tasks = realloc(_domain->tasks, (_domain->_sizeof_tasks + 1) * sizeof(struct task_manager_client_ns0_task));
        memcpy(&(_domain->tasks[_domain->_sizeof_tasks++]), _child_accessor, sizeof(struct task_manager_client_ns0_task));
        free(_child_accessor);
          } //end "if not nil" clause
          else if (xmlTextReaderIsEmptyElement(reader) == 0) {
            //if it's not the empty element, skip it because it's nil.
            xmlTextReaderSkipElement(reader);
          }
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "users", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

          status = 1;
          if (xmlTextReaderMoveToAttributeNs(reader, BAD_CAST "nil", BAD_CAST "http://www.w3.org/2001/XMLSchema-instance")) {
            if (xmlStrcmp(BAD_CAST "true", xmlTextReaderConstValue(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
              printf("Choice {}users was nil according to the xsi:nil attribute.\n");
#endif
              status = 0;
            }
          }

          xmlTextReaderMoveToElement(reader); //move back to the element
          if (status) { //if not "nil"...
#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}users of type {http://endpoint.tm.amster.ru/}user.\n");
#endif
        _child_accessor = xmlTextReaderReadNs0UserType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}users of type {http://endpoint.tm.amster.ru/}user.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0DomainType(_domain);
          free(_domain);
          return NULL;
        }

        _domain->users = realloc(_domain->users, (_domain->_sizeof_users + 1) * sizeof(struct task_manager_client_ns0_user));
        memcpy(&(_domain->users[_domain->_sizeof_users++]), _child_accessor, sizeof(struct task_manager_client_ns0_user));
        free(_child_accessor);
          } //end "if not nil" clause
          else if (xmlTextReaderIsEmptyElement(reader) == 0) {
            //if it's not the empty element, skip it because it's nil.
            xmlTextReaderSkipElement(reader);
          }
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {http://endpoint.tm.amster.ru/}domain.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {http://endpoint.tm.amster.ru/}domain. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _domain;
}

/**
 * Writes a Domain to XML.
 *
 * @param writer The XML writer.
 * @param _domain The Domain to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0DomainType(xmlTextWriterPtr writer, struct task_manager_client_ns0_domain *_domain) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  for (i = 0; i < _domain->_sizeof_projects; i++) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "projects", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}projects. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://endpoint.tm.amster.ru/}project for element {}projects...\n");
#endif
    status = xmlTextWriterWriteNs0ProjectType(writer, &(_domain->projects[i]));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://endpoint.tm.amster.ru/}project for element {}projects. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}projects. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  for (i = 0; i < _domain->_sizeof_tasks; i++) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "tasks", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}tasks. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://endpoint.tm.amster.ru/}task for element {}tasks...\n");
#endif
    status = xmlTextWriterWriteNs0TaskType(writer, &(_domain->tasks[i]));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://endpoint.tm.amster.ru/}task for element {}tasks. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}tasks. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  for (i = 0; i < _domain->_sizeof_users; i++) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "users", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}users. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://endpoint.tm.amster.ru/}user for element {}users...\n");
#endif
    status = xmlTextWriterWriteNs0UserType(writer, &(_domain->users[i]));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://endpoint.tm.amster.ru/}user for element {}users. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}users. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a Domain.
 *
 * @param _domain The Domain to free.
 */
static void freeNs0DomainType(struct task_manager_client_ns0_domain *_domain) {
  int i;
  if (_domain->projects != NULL) {
    for (i = 0; i < _domain->_sizeof_projects; i++) {
#if DEBUG_ENUNCIATE > 1
      printf("Freeing accessor projects[%i] of type task_manager_client_ns0_domain...\n", i);
#endif
      freeNs0ProjectType(&(_domain->projects[i]));
    }
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor projects of type task_manager_client_ns0_domain...\n");
#endif
    free(_domain->projects);
  }
  if (_domain->tasks != NULL) {
    for (i = 0; i < _domain->_sizeof_tasks; i++) {
#if DEBUG_ENUNCIATE > 1
      printf("Freeing accessor tasks[%i] of type task_manager_client_ns0_domain...\n", i);
#endif
      freeNs0TaskType(&(_domain->tasks[i]));
    }
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor tasks of type task_manager_client_ns0_domain...\n");
#endif
    free(_domain->tasks);
  }
  if (_domain->users != NULL) {
    for (i = 0; i < _domain->_sizeof_users; i++) {
#if DEBUG_ENUNCIATE > 1
      printf("Freeing accessor users[%i] of type task_manager_client_ns0_domain...\n", i);
#endif
      freeNs0UserType(&(_domain->users[i]));
    }
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor users of type task_manager_client_ns0_domain...\n");
#endif
    free(_domain->users);
  }
}
#endif /* DEF_task_manager_client_ns0_domain_M */
#ifndef DEF_task_manager_client_ns0_export_M
#define DEF_task_manager_client_ns0_export_M

/**
 * Reads a Export element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}export", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The Export, or NULL in case of error.
 */
struct task_manager_client_ns0_export *xml_read_task_manager_client_ns0_export(xmlTextReaderPtr reader) {
  int status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
  return xmlTextReaderReadNs0ExportElement(reader);
}

/**
 * Writes a Export to XML under element name "{http://endpoint.tm.amster.ru/}export".
 *
 * @param writer The XML writer.
 * @param _export The Export to write.
 * @return 1 if successful, 0 otherwise.
 */
int xml_write_task_manager_client_ns0_export(xmlTextWriterPtr writer, struct task_manager_client_ns0_export *_export) {
  return xmlTextWriterWriteNs0ExportElementNS(writer, _export, 1);
}

/**
 * Frees a Export.
 *
 * @param _export The Export to free.
 */
void free_task_manager_client_ns0_export(struct task_manager_client_ns0_export *_export) {
  freeNs0ExportType(_export);
  free(_export);
}

/**
 * Reads a Export element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}export", and
 * it is assumed that the reader is pointing to that element.
 *
 * @param reader The XML reader.
 * @return The Export, or NULL in case of error.
 */
struct task_manager_client_ns0_export *xmlTextReaderReadNs0ExportElement(xmlTextReaderPtr reader) {
  struct task_manager_client_ns0_export *_export = NULL;

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "export", xmlTextReaderConstLocalName(reader)) == 0
    && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    printf("Attempting to read root element {http://endpoint.tm.amster.ru/}export.\n");
#endif
    _export = xmlTextReaderReadNs0ExportType(reader);
  }
#if DEBUG_ENUNCIATE
  if (_export == NULL) {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      printf("attempt to read {http://endpoint.tm.amster.ru/}export failed. current element: {}%s\n",  xmlTextReaderConstLocalName(reader));
    }
    else {
      printf("attempt to read {http://endpoint.tm.amster.ru/}export failed. current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
    }
  }
#endif

  return _export;
}

/**
 * Writes a Export to XML under element name "{http://endpoint.tm.amster.ru/}export".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _export The Export to write.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0ExportElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_export *_export) {
  return xmlTextWriterWriteNs0ExportElementNS(writer, _export, 0);
}

/**
 * Writes a Export to XML under element name "{http://endpoint.tm.amster.ru/}export".
 *
 * @param writer The XML writer.
 * @param _export The Export to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0ExportElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_export *_export, int writeNamespaces) {
  int totalBytes = 0;
  int status;

  status = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "export", NULL);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write start element {http://endpoint.tm.amster.ru/}export. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  if (writeNamespaces) {
#if DEBUG_ENUNCIATE > 1
    printf("writing namespaces for start element {http://endpoint.tm.amster.ru/}export...\n");
#endif

    status = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("unable to write namespace attribute xmlns:ns0. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

#if DEBUG_ENUNCIATE > 1
  printf("writing type {http://endpoint.tm.amster.ru/}export for root element {http://endpoint.tm.amster.ru/}export...\n");
#endif
  status = xmlTextWriterWriteNs0ExportType(writer, _export);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write type for start element {http://endpoint.tm.amster.ru/}export. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  status = xmlTextWriterEndElement(writer);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to end element {http://endpoint.tm.amster.ru/}export. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  return totalBytes;
}

/**
 * Frees the children of a Export.
 *
 * @param _export The Export whose children are to be free.
 */
static void freeNs0ExportElement(struct task_manager_client_ns0_export *_export) {
  freeNs0ExportType(_export);
}

/**
 * Reads a Export from XML. The reader is assumed to be at the start element.
 *
 * @return the Export, or NULL in case of error.
 */
static struct task_manager_client_ns0_export *xmlTextReaderReadNs0ExportType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_export *_export = calloc(1, sizeof(struct task_manager_client_ns0_export));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs0ExportType(_export);
        free(_export);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "session", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}session of type {http://endpoint.tm.amster.ru/}session.\n");
#endif
        _child_accessor = xmlTextReaderReadNs0SessionType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}session of type {http://endpoint.tm.amster.ru/}session.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0ExportType(_export);
          free(_export);
          return NULL;
        }

        _export->session = ((struct task_manager_client_ns0_session*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {http://endpoint.tm.amster.ru/}export.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {http://endpoint.tm.amster.ru/}export. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _export;
}

/**
 * Writes a Export to XML.
 *
 * @param writer The XML writer.
 * @param _export The Export to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0ExportType(xmlTextWriterPtr writer, struct task_manager_client_ns0_export *_export) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_export->session != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "session", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://endpoint.tm.amster.ru/}session for element {}session...\n");
#endif
    status = xmlTextWriterWriteNs0SessionType(writer, (_export->session));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://endpoint.tm.amster.ru/}session for element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a Export.
 *
 * @param _export The Export to free.
 */
static void freeNs0ExportType(struct task_manager_client_ns0_export *_export) {
  int i;
  if (_export->session != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor session of type task_manager_client_ns0_export...\n");
#endif
    freeNs0SessionType(_export->session);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor session of type task_manager_client_ns0_export...\n");
#endif
    free(_export->session);
  }
}
#endif /* DEF_task_manager_client_ns0_export_M */
#ifndef DEF_task_manager_client_ns0_exportResponse_M
#define DEF_task_manager_client_ns0_exportResponse_M

/**
 * Reads a ExportResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}exportResponse", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The ExportResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_exportResponse *xml_read_task_manager_client_ns0_exportResponse(xmlTextReaderPtr reader) {
  int status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
  return xmlTextReaderReadNs0ExportResponseElement(reader);
}

/**
 * Writes a ExportResponse to XML under element name "{http://endpoint.tm.amster.ru/}exportResponse".
 *
 * @param writer The XML writer.
 * @param _exportResponse The ExportResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
int xml_write_task_manager_client_ns0_exportResponse(xmlTextWriterPtr writer, struct task_manager_client_ns0_exportResponse *_exportResponse) {
  return xmlTextWriterWriteNs0ExportResponseElementNS(writer, _exportResponse, 1);
}

/**
 * Frees a ExportResponse.
 *
 * @param _exportResponse The ExportResponse to free.
 */
void free_task_manager_client_ns0_exportResponse(struct task_manager_client_ns0_exportResponse *_exportResponse) {
  freeNs0ExportResponseType(_exportResponse);
  free(_exportResponse);
}

/**
 * Reads a ExportResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}exportResponse", and
 * it is assumed that the reader is pointing to that element.
 *
 * @param reader The XML reader.
 * @return The ExportResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_exportResponse *xmlTextReaderReadNs0ExportResponseElement(xmlTextReaderPtr reader) {
  struct task_manager_client_ns0_exportResponse *_exportResponse = NULL;

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "exportResponse", xmlTextReaderConstLocalName(reader)) == 0
    && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    printf("Attempting to read root element {http://endpoint.tm.amster.ru/}exportResponse.\n");
#endif
    _exportResponse = xmlTextReaderReadNs0ExportResponseType(reader);
  }
#if DEBUG_ENUNCIATE
  if (_exportResponse == NULL) {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      printf("attempt to read {http://endpoint.tm.amster.ru/}exportResponse failed. current element: {}%s\n",  xmlTextReaderConstLocalName(reader));
    }
    else {
      printf("attempt to read {http://endpoint.tm.amster.ru/}exportResponse failed. current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
    }
  }
#endif

  return _exportResponse;
}

/**
 * Writes a ExportResponse to XML under element name "{http://endpoint.tm.amster.ru/}exportResponse".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _exportResponse The ExportResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0ExportResponseElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_exportResponse *_exportResponse) {
  return xmlTextWriterWriteNs0ExportResponseElementNS(writer, _exportResponse, 0);
}

/**
 * Writes a ExportResponse to XML under element name "{http://endpoint.tm.amster.ru/}exportResponse".
 *
 * @param writer The XML writer.
 * @param _exportResponse The ExportResponse to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0ExportResponseElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_exportResponse *_exportResponse, int writeNamespaces) {
  int totalBytes = 0;
  int status;

  status = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "exportResponse", NULL);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write start element {http://endpoint.tm.amster.ru/}exportResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  if (writeNamespaces) {
#if DEBUG_ENUNCIATE > 1
    printf("writing namespaces for start element {http://endpoint.tm.amster.ru/}exportResponse...\n");
#endif

    status = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("unable to write namespace attribute xmlns:ns0. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

#if DEBUG_ENUNCIATE > 1
  printf("writing type {http://endpoint.tm.amster.ru/}exportResponse for root element {http://endpoint.tm.amster.ru/}exportResponse...\n");
#endif
  status = xmlTextWriterWriteNs0ExportResponseType(writer, _exportResponse);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write type for start element {http://endpoint.tm.amster.ru/}exportResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  status = xmlTextWriterEndElement(writer);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to end element {http://endpoint.tm.amster.ru/}exportResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  return totalBytes;
}

/**
 * Frees the children of a ExportResponse.
 *
 * @param _exportResponse The ExportResponse whose children are to be free.
 */
static void freeNs0ExportResponseElement(struct task_manager_client_ns0_exportResponse *_exportResponse) {
  freeNs0ExportResponseType(_exportResponse);
}

/**
 * Reads a ExportResponse from XML. The reader is assumed to be at the start element.
 *
 * @return the ExportResponse, or NULL in case of error.
 */
static struct task_manager_client_ns0_exportResponse *xmlTextReaderReadNs0ExportResponseType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_exportResponse *_exportResponse = calloc(1, sizeof(struct task_manager_client_ns0_exportResponse));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs0ExportResponseType(_exportResponse);
        free(_exportResponse);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "return", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}return of type {http://endpoint.tm.amster.ru/}domain.\n");
#endif
        _child_accessor = xmlTextReaderReadNs0DomainType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}return of type {http://endpoint.tm.amster.ru/}domain.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0ExportResponseType(_exportResponse);
          free(_exportResponse);
          return NULL;
        }

        _exportResponse->_return = ((struct task_manager_client_ns0_domain*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {http://endpoint.tm.amster.ru/}exportResponse.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {http://endpoint.tm.amster.ru/}exportResponse. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _exportResponse;
}

/**
 * Writes a ExportResponse to XML.
 *
 * @param writer The XML writer.
 * @param _exportResponse The ExportResponse to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0ExportResponseType(xmlTextWriterPtr writer, struct task_manager_client_ns0_exportResponse *_exportResponse) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_exportResponse->_return != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "return", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}return. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://endpoint.tm.amster.ru/}domain for element {}return...\n");
#endif
    status = xmlTextWriterWriteNs0DomainType(writer, (_exportResponse->_return));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://endpoint.tm.amster.ru/}domain for element {}return. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}return. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a ExportResponse.
 *
 * @param _exportResponse The ExportResponse to free.
 */
static void freeNs0ExportResponseType(struct task_manager_client_ns0_exportResponse *_exportResponse) {
  int i;
  if (_exportResponse->_return != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor _return of type task_manager_client_ns0_exportResponse...\n");
#endif
    freeNs0DomainType(_exportResponse->_return);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor _return of type task_manager_client_ns0_exportResponse...\n");
#endif
    free(_exportResponse->_return);
  }
}
#endif /* DEF_task_manager_client_ns0_exportResponse_M */
#ifndef DEF_task_manager_client_ns0_findAllUser_M
#define DEF_task_manager_client_ns0_findAllUser_M

/**
 * Reads a FindAllUser element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findAllUser", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The FindAllUser, or NULL in case of error.
 */
struct task_manager_client_ns0_findAllUser *xml_read_task_manager_client_ns0_findAllUser(xmlTextReaderPtr reader) {
  int status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
  return xmlTextReaderReadNs0FindAllUserElement(reader);
}

/**
 * Writes a FindAllUser to XML under element name "{http://endpoint.tm.amster.ru/}findAllUser".
 *
 * @param writer The XML writer.
 * @param _findAllUser The FindAllUser to write.
 * @return 1 if successful, 0 otherwise.
 */
int xml_write_task_manager_client_ns0_findAllUser(xmlTextWriterPtr writer, struct task_manager_client_ns0_findAllUser *_findAllUser) {
  return xmlTextWriterWriteNs0FindAllUserElementNS(writer, _findAllUser, 1);
}

/**
 * Frees a FindAllUser.
 *
 * @param _findAllUser The FindAllUser to free.
 */
void free_task_manager_client_ns0_findAllUser(struct task_manager_client_ns0_findAllUser *_findAllUser) {
  freeNs0FindAllUserType(_findAllUser);
  free(_findAllUser);
}

/**
 * Reads a FindAllUser element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findAllUser", and
 * it is assumed that the reader is pointing to that element.
 *
 * @param reader The XML reader.
 * @return The FindAllUser, or NULL in case of error.
 */
struct task_manager_client_ns0_findAllUser *xmlTextReaderReadNs0FindAllUserElement(xmlTextReaderPtr reader) {
  struct task_manager_client_ns0_findAllUser *_findAllUser = NULL;

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "findAllUser", xmlTextReaderConstLocalName(reader)) == 0
    && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    printf("Attempting to read root element {http://endpoint.tm.amster.ru/}findAllUser.\n");
#endif
    _findAllUser = xmlTextReaderReadNs0FindAllUserType(reader);
  }
#if DEBUG_ENUNCIATE
  if (_findAllUser == NULL) {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      printf("attempt to read {http://endpoint.tm.amster.ru/}findAllUser failed. current element: {}%s\n",  xmlTextReaderConstLocalName(reader));
    }
    else {
      printf("attempt to read {http://endpoint.tm.amster.ru/}findAllUser failed. current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
    }
  }
#endif

  return _findAllUser;
}

/**
 * Writes a FindAllUser to XML under element name "{http://endpoint.tm.amster.ru/}findAllUser".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _findAllUser The FindAllUser to write.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0FindAllUserElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_findAllUser *_findAllUser) {
  return xmlTextWriterWriteNs0FindAllUserElementNS(writer, _findAllUser, 0);
}

/**
 * Writes a FindAllUser to XML under element name "{http://endpoint.tm.amster.ru/}findAllUser".
 *
 * @param writer The XML writer.
 * @param _findAllUser The FindAllUser to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0FindAllUserElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_findAllUser *_findAllUser, int writeNamespaces) {
  int totalBytes = 0;
  int status;

  status = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "findAllUser", NULL);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write start element {http://endpoint.tm.amster.ru/}findAllUser. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  if (writeNamespaces) {
#if DEBUG_ENUNCIATE > 1
    printf("writing namespaces for start element {http://endpoint.tm.amster.ru/}findAllUser...\n");
#endif

    status = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("unable to write namespace attribute xmlns:ns0. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

#if DEBUG_ENUNCIATE > 1
  printf("writing type {http://endpoint.tm.amster.ru/}findAllUser for root element {http://endpoint.tm.amster.ru/}findAllUser...\n");
#endif
  status = xmlTextWriterWriteNs0FindAllUserType(writer, _findAllUser);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write type for start element {http://endpoint.tm.amster.ru/}findAllUser. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  status = xmlTextWriterEndElement(writer);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to end element {http://endpoint.tm.amster.ru/}findAllUser. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  return totalBytes;
}

/**
 * Frees the children of a FindAllUser.
 *
 * @param _findAllUser The FindAllUser whose children are to be free.
 */
static void freeNs0FindAllUserElement(struct task_manager_client_ns0_findAllUser *_findAllUser) {
  freeNs0FindAllUserType(_findAllUser);
}

/**
 * Reads a FindAllUser from XML. The reader is assumed to be at the start element.
 *
 * @return the FindAllUser, or NULL in case of error.
 */
static struct task_manager_client_ns0_findAllUser *xmlTextReaderReadNs0FindAllUserType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_findAllUser *_findAllUser = calloc(1, sizeof(struct task_manager_client_ns0_findAllUser));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs0FindAllUserType(_findAllUser);
        free(_findAllUser);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "session", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}session of type {http://endpoint.tm.amster.ru/}session.\n");
#endif
        _child_accessor = xmlTextReaderReadNs0SessionType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}session of type {http://endpoint.tm.amster.ru/}session.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0FindAllUserType(_findAllUser);
          free(_findAllUser);
          return NULL;
        }

        _findAllUser->session = ((struct task_manager_client_ns0_session*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {http://endpoint.tm.amster.ru/}findAllUser.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {http://endpoint.tm.amster.ru/}findAllUser. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _findAllUser;
}

/**
 * Writes a FindAllUser to XML.
 *
 * @param writer The XML writer.
 * @param _findAllUser The FindAllUser to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0FindAllUserType(xmlTextWriterPtr writer, struct task_manager_client_ns0_findAllUser *_findAllUser) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_findAllUser->session != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "session", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://endpoint.tm.amster.ru/}session for element {}session...\n");
#endif
    status = xmlTextWriterWriteNs0SessionType(writer, (_findAllUser->session));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://endpoint.tm.amster.ru/}session for element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a FindAllUser.
 *
 * @param _findAllUser The FindAllUser to free.
 */
static void freeNs0FindAllUserType(struct task_manager_client_ns0_findAllUser *_findAllUser) {
  int i;
  if (_findAllUser->session != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor session of type task_manager_client_ns0_findAllUser...\n");
#endif
    freeNs0SessionType(_findAllUser->session);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor session of type task_manager_client_ns0_findAllUser...\n");
#endif
    free(_findAllUser->session);
  }
}
#endif /* DEF_task_manager_client_ns0_findAllUser_M */
#ifndef DEF_task_manager_client_ns0_findAllUserResponse_M
#define DEF_task_manager_client_ns0_findAllUserResponse_M

/**
 * Reads a FindAllUserResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findAllUserResponse", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The FindAllUserResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_findAllUserResponse *xml_read_task_manager_client_ns0_findAllUserResponse(xmlTextReaderPtr reader) {
  int status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
  return xmlTextReaderReadNs0FindAllUserResponseElement(reader);
}

/**
 * Writes a FindAllUserResponse to XML under element name "{http://endpoint.tm.amster.ru/}findAllUserResponse".
 *
 * @param writer The XML writer.
 * @param _findAllUserResponse The FindAllUserResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
int xml_write_task_manager_client_ns0_findAllUserResponse(xmlTextWriterPtr writer, struct task_manager_client_ns0_findAllUserResponse *_findAllUserResponse) {
  return xmlTextWriterWriteNs0FindAllUserResponseElementNS(writer, _findAllUserResponse, 1);
}

/**
 * Frees a FindAllUserResponse.
 *
 * @param _findAllUserResponse The FindAllUserResponse to free.
 */
void free_task_manager_client_ns0_findAllUserResponse(struct task_manager_client_ns0_findAllUserResponse *_findAllUserResponse) {
  freeNs0FindAllUserResponseType(_findAllUserResponse);
  free(_findAllUserResponse);
}

/**
 * Reads a FindAllUserResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findAllUserResponse", and
 * it is assumed that the reader is pointing to that element.
 *
 * @param reader The XML reader.
 * @return The FindAllUserResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_findAllUserResponse *xmlTextReaderReadNs0FindAllUserResponseElement(xmlTextReaderPtr reader) {
  struct task_manager_client_ns0_findAllUserResponse *_findAllUserResponse = NULL;

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "findAllUserResponse", xmlTextReaderConstLocalName(reader)) == 0
    && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    printf("Attempting to read root element {http://endpoint.tm.amster.ru/}findAllUserResponse.\n");
#endif
    _findAllUserResponse = xmlTextReaderReadNs0FindAllUserResponseType(reader);
  }
#if DEBUG_ENUNCIATE
  if (_findAllUserResponse == NULL) {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      printf("attempt to read {http://endpoint.tm.amster.ru/}findAllUserResponse failed. current element: {}%s\n",  xmlTextReaderConstLocalName(reader));
    }
    else {
      printf("attempt to read {http://endpoint.tm.amster.ru/}findAllUserResponse failed. current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
    }
  }
#endif

  return _findAllUserResponse;
}

/**
 * Writes a FindAllUserResponse to XML under element name "{http://endpoint.tm.amster.ru/}findAllUserResponse".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _findAllUserResponse The FindAllUserResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0FindAllUserResponseElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_findAllUserResponse *_findAllUserResponse) {
  return xmlTextWriterWriteNs0FindAllUserResponseElementNS(writer, _findAllUserResponse, 0);
}

/**
 * Writes a FindAllUserResponse to XML under element name "{http://endpoint.tm.amster.ru/}findAllUserResponse".
 *
 * @param writer The XML writer.
 * @param _findAllUserResponse The FindAllUserResponse to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0FindAllUserResponseElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_findAllUserResponse *_findAllUserResponse, int writeNamespaces) {
  int totalBytes = 0;
  int status;

  status = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "findAllUserResponse", NULL);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write start element {http://endpoint.tm.amster.ru/}findAllUserResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  if (writeNamespaces) {
#if DEBUG_ENUNCIATE > 1
    printf("writing namespaces for start element {http://endpoint.tm.amster.ru/}findAllUserResponse...\n");
#endif

    status = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("unable to write namespace attribute xmlns:ns0. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

#if DEBUG_ENUNCIATE > 1
  printf("writing type {http://endpoint.tm.amster.ru/}findAllUserResponse for root element {http://endpoint.tm.amster.ru/}findAllUserResponse...\n");
#endif
  status = xmlTextWriterWriteNs0FindAllUserResponseType(writer, _findAllUserResponse);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write type for start element {http://endpoint.tm.amster.ru/}findAllUserResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  status = xmlTextWriterEndElement(writer);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to end element {http://endpoint.tm.amster.ru/}findAllUserResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  return totalBytes;
}

/**
 * Frees the children of a FindAllUserResponse.
 *
 * @param _findAllUserResponse The FindAllUserResponse whose children are to be free.
 */
static void freeNs0FindAllUserResponseElement(struct task_manager_client_ns0_findAllUserResponse *_findAllUserResponse) {
  freeNs0FindAllUserResponseType(_findAllUserResponse);
}

/**
 * Reads a FindAllUserResponse from XML. The reader is assumed to be at the start element.
 *
 * @return the FindAllUserResponse, or NULL in case of error.
 */
static struct task_manager_client_ns0_findAllUserResponse *xmlTextReaderReadNs0FindAllUserResponseType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_findAllUserResponse *_findAllUserResponse = calloc(1, sizeof(struct task_manager_client_ns0_findAllUserResponse));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs0FindAllUserResponseType(_findAllUserResponse);
        free(_findAllUserResponse);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "return", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}return of type {http://endpoint.tm.amster.ru/}user.\n");
#endif
        _child_accessor = xmlTextReaderReadNs0UserType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}return of type {http://endpoint.tm.amster.ru/}user.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0FindAllUserResponseType(_findAllUserResponse);
          free(_findAllUserResponse);
          return NULL;
        }

        _findAllUserResponse->_return = realloc(_findAllUserResponse->_return, (_findAllUserResponse->_sizeof__return + 1) * sizeof(struct task_manager_client_ns0_user));
        memcpy(&(_findAllUserResponse->_return[_findAllUserResponse->_sizeof__return++]), _child_accessor, sizeof(struct task_manager_client_ns0_user));
        free(_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {http://endpoint.tm.amster.ru/}findAllUserResponse.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {http://endpoint.tm.amster.ru/}findAllUserResponse. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _findAllUserResponse;
}

/**
 * Writes a FindAllUserResponse to XML.
 *
 * @param writer The XML writer.
 * @param _findAllUserResponse The FindAllUserResponse to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0FindAllUserResponseType(xmlTextWriterPtr writer, struct task_manager_client_ns0_findAllUserResponse *_findAllUserResponse) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  for (i = 0; i < _findAllUserResponse->_sizeof__return; i++) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "return", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}return. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://endpoint.tm.amster.ru/}user for element {}return...\n");
#endif
    status = xmlTextWriterWriteNs0UserType(writer, &(_findAllUserResponse->_return[i]));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://endpoint.tm.amster.ru/}user for element {}return. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}return. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a FindAllUserResponse.
 *
 * @param _findAllUserResponse The FindAllUserResponse to free.
 */
static void freeNs0FindAllUserResponseType(struct task_manager_client_ns0_findAllUserResponse *_findAllUserResponse) {
  int i;
  if (_findAllUserResponse->_return != NULL) {
    for (i = 0; i < _findAllUserResponse->_sizeof__return; i++) {
#if DEBUG_ENUNCIATE > 1
      printf("Freeing accessor _return[%i] of type task_manager_client_ns0_findAllUserResponse...\n", i);
#endif
      freeNs0UserType(&(_findAllUserResponse->_return[i]));
    }
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor _return of type task_manager_client_ns0_findAllUserResponse...\n");
#endif
    free(_findAllUserResponse->_return);
  }
}
#endif /* DEF_task_manager_client_ns0_findAllUserResponse_M */
#ifndef DEF_task_manager_client_ns0_findUserById_M
#define DEF_task_manager_client_ns0_findUserById_M

/**
 * Reads a FindUserById element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findUserById", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The FindUserById, or NULL in case of error.
 */
struct task_manager_client_ns0_findUserById *xml_read_task_manager_client_ns0_findUserById(xmlTextReaderPtr reader) {
  int status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
  return xmlTextReaderReadNs0FindUserByIdElement(reader);
}

/**
 * Writes a FindUserById to XML under element name "{http://endpoint.tm.amster.ru/}findUserById".
 *
 * @param writer The XML writer.
 * @param _findUserById The FindUserById to write.
 * @return 1 if successful, 0 otherwise.
 */
int xml_write_task_manager_client_ns0_findUserById(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserById *_findUserById) {
  return xmlTextWriterWriteNs0FindUserByIdElementNS(writer, _findUserById, 1);
}

/**
 * Frees a FindUserById.
 *
 * @param _findUserById The FindUserById to free.
 */
void free_task_manager_client_ns0_findUserById(struct task_manager_client_ns0_findUserById *_findUserById) {
  freeNs0FindUserByIdType(_findUserById);
  free(_findUserById);
}

/**
 * Reads a FindUserById element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findUserById", and
 * it is assumed that the reader is pointing to that element.
 *
 * @param reader The XML reader.
 * @return The FindUserById, or NULL in case of error.
 */
struct task_manager_client_ns0_findUserById *xmlTextReaderReadNs0FindUserByIdElement(xmlTextReaderPtr reader) {
  struct task_manager_client_ns0_findUserById *_findUserById = NULL;

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "findUserById", xmlTextReaderConstLocalName(reader)) == 0
    && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    printf("Attempting to read root element {http://endpoint.tm.amster.ru/}findUserById.\n");
#endif
    _findUserById = xmlTextReaderReadNs0FindUserByIdType(reader);
  }
#if DEBUG_ENUNCIATE
  if (_findUserById == NULL) {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      printf("attempt to read {http://endpoint.tm.amster.ru/}findUserById failed. current element: {}%s\n",  xmlTextReaderConstLocalName(reader));
    }
    else {
      printf("attempt to read {http://endpoint.tm.amster.ru/}findUserById failed. current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
    }
  }
#endif

  return _findUserById;
}

/**
 * Writes a FindUserById to XML under element name "{http://endpoint.tm.amster.ru/}findUserById".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _findUserById The FindUserById to write.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0FindUserByIdElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserById *_findUserById) {
  return xmlTextWriterWriteNs0FindUserByIdElementNS(writer, _findUserById, 0);
}

/**
 * Writes a FindUserById to XML under element name "{http://endpoint.tm.amster.ru/}findUserById".
 *
 * @param writer The XML writer.
 * @param _findUserById The FindUserById to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0FindUserByIdElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserById *_findUserById, int writeNamespaces) {
  int totalBytes = 0;
  int status;

  status = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "findUserById", NULL);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write start element {http://endpoint.tm.amster.ru/}findUserById. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  if (writeNamespaces) {
#if DEBUG_ENUNCIATE > 1
    printf("writing namespaces for start element {http://endpoint.tm.amster.ru/}findUserById...\n");
#endif

    status = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("unable to write namespace attribute xmlns:ns0. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

#if DEBUG_ENUNCIATE > 1
  printf("writing type {http://endpoint.tm.amster.ru/}findUserById for root element {http://endpoint.tm.amster.ru/}findUserById...\n");
#endif
  status = xmlTextWriterWriteNs0FindUserByIdType(writer, _findUserById);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write type for start element {http://endpoint.tm.amster.ru/}findUserById. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  status = xmlTextWriterEndElement(writer);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to end element {http://endpoint.tm.amster.ru/}findUserById. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  return totalBytes;
}

/**
 * Frees the children of a FindUserById.
 *
 * @param _findUserById The FindUserById whose children are to be free.
 */
static void freeNs0FindUserByIdElement(struct task_manager_client_ns0_findUserById *_findUserById) {
  freeNs0FindUserByIdType(_findUserById);
}

/**
 * Reads a FindUserById from XML. The reader is assumed to be at the start element.
 *
 * @return the FindUserById, or NULL in case of error.
 */
static struct task_manager_client_ns0_findUserById *xmlTextReaderReadNs0FindUserByIdType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_findUserById *_findUserById = calloc(1, sizeof(struct task_manager_client_ns0_findUserById));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs0FindUserByIdType(_findUserById);
        free(_findUserById);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "session", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}session of type {http://endpoint.tm.amster.ru/}session.\n");
#endif
        _child_accessor = xmlTextReaderReadNs0SessionType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}session of type {http://endpoint.tm.amster.ru/}session.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0FindUserByIdType(_findUserById);
          free(_findUserById);
          return NULL;
        }

        _findUserById->session = ((struct task_manager_client_ns0_session*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "id", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0FindUserByIdType(_findUserById);
          free(_findUserById);
          return NULL;
        }

        _findUserById->id = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {http://endpoint.tm.amster.ru/}findUserById.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {http://endpoint.tm.amster.ru/}findUserById. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _findUserById;
}

/**
 * Writes a FindUserById to XML.
 *
 * @param writer The XML writer.
 * @param _findUserById The FindUserById to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0FindUserByIdType(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserById *_findUserById) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_findUserById->session != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "session", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://endpoint.tm.amster.ru/}session for element {}session...\n");
#endif
    status = xmlTextWriterWriteNs0SessionType(writer, (_findUserById->session));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://endpoint.tm.amster.ru/}session for element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_findUserById->id != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "id", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}id...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_findUserById->id));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a FindUserById.
 *
 * @param _findUserById The FindUserById to free.
 */
static void freeNs0FindUserByIdType(struct task_manager_client_ns0_findUserById *_findUserById) {
  int i;
  if (_findUserById->session != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor session of type task_manager_client_ns0_findUserById...\n");
#endif
    freeNs0SessionType(_findUserById->session);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor session of type task_manager_client_ns0_findUserById...\n");
#endif
    free(_findUserById->session);
  }
  if (_findUserById->id != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor id of type task_manager_client_ns0_findUserById...\n");
#endif
    freeXsStringType(_findUserById->id);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor id of type task_manager_client_ns0_findUserById...\n");
#endif
    free(_findUserById->id);
  }
}
#endif /* DEF_task_manager_client_ns0_findUserById_M */
#ifndef DEF_task_manager_client_ns0_findUserByIdResponse_M
#define DEF_task_manager_client_ns0_findUserByIdResponse_M

/**
 * Reads a FindUserByIdResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findUserByIdResponse", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The FindUserByIdResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_findUserByIdResponse *xml_read_task_manager_client_ns0_findUserByIdResponse(xmlTextReaderPtr reader) {
  int status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
  return xmlTextReaderReadNs0FindUserByIdResponseElement(reader);
}

/**
 * Writes a FindUserByIdResponse to XML under element name "{http://endpoint.tm.amster.ru/}findUserByIdResponse".
 *
 * @param writer The XML writer.
 * @param _findUserByIdResponse The FindUserByIdResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
int xml_write_task_manager_client_ns0_findUserByIdResponse(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByIdResponse *_findUserByIdResponse) {
  return xmlTextWriterWriteNs0FindUserByIdResponseElementNS(writer, _findUserByIdResponse, 1);
}

/**
 * Frees a FindUserByIdResponse.
 *
 * @param _findUserByIdResponse The FindUserByIdResponse to free.
 */
void free_task_manager_client_ns0_findUserByIdResponse(struct task_manager_client_ns0_findUserByIdResponse *_findUserByIdResponse) {
  freeNs0FindUserByIdResponseType(_findUserByIdResponse);
  free(_findUserByIdResponse);
}

/**
 * Reads a FindUserByIdResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findUserByIdResponse", and
 * it is assumed that the reader is pointing to that element.
 *
 * @param reader The XML reader.
 * @return The FindUserByIdResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_findUserByIdResponse *xmlTextReaderReadNs0FindUserByIdResponseElement(xmlTextReaderPtr reader) {
  struct task_manager_client_ns0_findUserByIdResponse *_findUserByIdResponse = NULL;

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "findUserByIdResponse", xmlTextReaderConstLocalName(reader)) == 0
    && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    printf("Attempting to read root element {http://endpoint.tm.amster.ru/}findUserByIdResponse.\n");
#endif
    _findUserByIdResponse = xmlTextReaderReadNs0FindUserByIdResponseType(reader);
  }
#if DEBUG_ENUNCIATE
  if (_findUserByIdResponse == NULL) {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      printf("attempt to read {http://endpoint.tm.amster.ru/}findUserByIdResponse failed. current element: {}%s\n",  xmlTextReaderConstLocalName(reader));
    }
    else {
      printf("attempt to read {http://endpoint.tm.amster.ru/}findUserByIdResponse failed. current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
    }
  }
#endif

  return _findUserByIdResponse;
}

/**
 * Writes a FindUserByIdResponse to XML under element name "{http://endpoint.tm.amster.ru/}findUserByIdResponse".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _findUserByIdResponse The FindUserByIdResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0FindUserByIdResponseElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByIdResponse *_findUserByIdResponse) {
  return xmlTextWriterWriteNs0FindUserByIdResponseElementNS(writer, _findUserByIdResponse, 0);
}

/**
 * Writes a FindUserByIdResponse to XML under element name "{http://endpoint.tm.amster.ru/}findUserByIdResponse".
 *
 * @param writer The XML writer.
 * @param _findUserByIdResponse The FindUserByIdResponse to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0FindUserByIdResponseElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByIdResponse *_findUserByIdResponse, int writeNamespaces) {
  int totalBytes = 0;
  int status;

  status = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "findUserByIdResponse", NULL);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write start element {http://endpoint.tm.amster.ru/}findUserByIdResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  if (writeNamespaces) {
#if DEBUG_ENUNCIATE > 1
    printf("writing namespaces for start element {http://endpoint.tm.amster.ru/}findUserByIdResponse...\n");
#endif

    status = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("unable to write namespace attribute xmlns:ns0. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

#if DEBUG_ENUNCIATE > 1
  printf("writing type {http://endpoint.tm.amster.ru/}findUserByIdResponse for root element {http://endpoint.tm.amster.ru/}findUserByIdResponse...\n");
#endif
  status = xmlTextWriterWriteNs0FindUserByIdResponseType(writer, _findUserByIdResponse);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write type for start element {http://endpoint.tm.amster.ru/}findUserByIdResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  status = xmlTextWriterEndElement(writer);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to end element {http://endpoint.tm.amster.ru/}findUserByIdResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  return totalBytes;
}

/**
 * Frees the children of a FindUserByIdResponse.
 *
 * @param _findUserByIdResponse The FindUserByIdResponse whose children are to be free.
 */
static void freeNs0FindUserByIdResponseElement(struct task_manager_client_ns0_findUserByIdResponse *_findUserByIdResponse) {
  freeNs0FindUserByIdResponseType(_findUserByIdResponse);
}

/**
 * Reads a FindUserByIdResponse from XML. The reader is assumed to be at the start element.
 *
 * @return the FindUserByIdResponse, or NULL in case of error.
 */
static struct task_manager_client_ns0_findUserByIdResponse *xmlTextReaderReadNs0FindUserByIdResponseType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_findUserByIdResponse *_findUserByIdResponse = calloc(1, sizeof(struct task_manager_client_ns0_findUserByIdResponse));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs0FindUserByIdResponseType(_findUserByIdResponse);
        free(_findUserByIdResponse);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "return", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}return of type {http://endpoint.tm.amster.ru/}user.\n");
#endif
        _child_accessor = xmlTextReaderReadNs0UserType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}return of type {http://endpoint.tm.amster.ru/}user.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0FindUserByIdResponseType(_findUserByIdResponse);
          free(_findUserByIdResponse);
          return NULL;
        }

        _findUserByIdResponse->_return = ((struct task_manager_client_ns0_user*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {http://endpoint.tm.amster.ru/}findUserByIdResponse.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {http://endpoint.tm.amster.ru/}findUserByIdResponse. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _findUserByIdResponse;
}

/**
 * Writes a FindUserByIdResponse to XML.
 *
 * @param writer The XML writer.
 * @param _findUserByIdResponse The FindUserByIdResponse to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0FindUserByIdResponseType(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByIdResponse *_findUserByIdResponse) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_findUserByIdResponse->_return != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "return", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}return. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://endpoint.tm.amster.ru/}user for element {}return...\n");
#endif
    status = xmlTextWriterWriteNs0UserType(writer, (_findUserByIdResponse->_return));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://endpoint.tm.amster.ru/}user for element {}return. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}return. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a FindUserByIdResponse.
 *
 * @param _findUserByIdResponse The FindUserByIdResponse to free.
 */
static void freeNs0FindUserByIdResponseType(struct task_manager_client_ns0_findUserByIdResponse *_findUserByIdResponse) {
  int i;
  if (_findUserByIdResponse->_return != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor _return of type task_manager_client_ns0_findUserByIdResponse...\n");
#endif
    freeNs0UserType(_findUserByIdResponse->_return);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor _return of type task_manager_client_ns0_findUserByIdResponse...\n");
#endif
    free(_findUserByIdResponse->_return);
  }
}
#endif /* DEF_task_manager_client_ns0_findUserByIdResponse_M */
#ifndef DEF_task_manager_client_ns0_findUserByLogin_M
#define DEF_task_manager_client_ns0_findUserByLogin_M

/**
 * Reads a FindUserByLogin element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findUserByLogin", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The FindUserByLogin, or NULL in case of error.
 */
struct task_manager_client_ns0_findUserByLogin *xml_read_task_manager_client_ns0_findUserByLogin(xmlTextReaderPtr reader) {
  int status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
  return xmlTextReaderReadNs0FindUserByLoginElement(reader);
}

/**
 * Writes a FindUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}findUserByLogin".
 *
 * @param writer The XML writer.
 * @param _findUserByLogin The FindUserByLogin to write.
 * @return 1 if successful, 0 otherwise.
 */
int xml_write_task_manager_client_ns0_findUserByLogin(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByLogin *_findUserByLogin) {
  return xmlTextWriterWriteNs0FindUserByLoginElementNS(writer, _findUserByLogin, 1);
}

/**
 * Frees a FindUserByLogin.
 *
 * @param _findUserByLogin The FindUserByLogin to free.
 */
void free_task_manager_client_ns0_findUserByLogin(struct task_manager_client_ns0_findUserByLogin *_findUserByLogin) {
  freeNs0FindUserByLoginType(_findUserByLogin);
  free(_findUserByLogin);
}

/**
 * Reads a FindUserByLogin element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findUserByLogin", and
 * it is assumed that the reader is pointing to that element.
 *
 * @param reader The XML reader.
 * @return The FindUserByLogin, or NULL in case of error.
 */
struct task_manager_client_ns0_findUserByLogin *xmlTextReaderReadNs0FindUserByLoginElement(xmlTextReaderPtr reader) {
  struct task_manager_client_ns0_findUserByLogin *_findUserByLogin = NULL;

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "findUserByLogin", xmlTextReaderConstLocalName(reader)) == 0
    && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    printf("Attempting to read root element {http://endpoint.tm.amster.ru/}findUserByLogin.\n");
#endif
    _findUserByLogin = xmlTextReaderReadNs0FindUserByLoginType(reader);
  }
#if DEBUG_ENUNCIATE
  if (_findUserByLogin == NULL) {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      printf("attempt to read {http://endpoint.tm.amster.ru/}findUserByLogin failed. current element: {}%s\n",  xmlTextReaderConstLocalName(reader));
    }
    else {
      printf("attempt to read {http://endpoint.tm.amster.ru/}findUserByLogin failed. current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
    }
  }
#endif

  return _findUserByLogin;
}

/**
 * Writes a FindUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}findUserByLogin".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _findUserByLogin The FindUserByLogin to write.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0FindUserByLoginElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByLogin *_findUserByLogin) {
  return xmlTextWriterWriteNs0FindUserByLoginElementNS(writer, _findUserByLogin, 0);
}

/**
 * Writes a FindUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}findUserByLogin".
 *
 * @param writer The XML writer.
 * @param _findUserByLogin The FindUserByLogin to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0FindUserByLoginElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByLogin *_findUserByLogin, int writeNamespaces) {
  int totalBytes = 0;
  int status;

  status = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "findUserByLogin", NULL);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write start element {http://endpoint.tm.amster.ru/}findUserByLogin. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  if (writeNamespaces) {
#if DEBUG_ENUNCIATE > 1
    printf("writing namespaces for start element {http://endpoint.tm.amster.ru/}findUserByLogin...\n");
#endif

    status = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("unable to write namespace attribute xmlns:ns0. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

#if DEBUG_ENUNCIATE > 1
  printf("writing type {http://endpoint.tm.amster.ru/}findUserByLogin for root element {http://endpoint.tm.amster.ru/}findUserByLogin...\n");
#endif
  status = xmlTextWriterWriteNs0FindUserByLoginType(writer, _findUserByLogin);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write type for start element {http://endpoint.tm.amster.ru/}findUserByLogin. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  status = xmlTextWriterEndElement(writer);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to end element {http://endpoint.tm.amster.ru/}findUserByLogin. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  return totalBytes;
}

/**
 * Frees the children of a FindUserByLogin.
 *
 * @param _findUserByLogin The FindUserByLogin whose children are to be free.
 */
static void freeNs0FindUserByLoginElement(struct task_manager_client_ns0_findUserByLogin *_findUserByLogin) {
  freeNs0FindUserByLoginType(_findUserByLogin);
}

/**
 * Reads a FindUserByLogin from XML. The reader is assumed to be at the start element.
 *
 * @return the FindUserByLogin, or NULL in case of error.
 */
static struct task_manager_client_ns0_findUserByLogin *xmlTextReaderReadNs0FindUserByLoginType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_findUserByLogin *_findUserByLogin = calloc(1, sizeof(struct task_manager_client_ns0_findUserByLogin));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs0FindUserByLoginType(_findUserByLogin);
        free(_findUserByLogin);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "session", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}session of type {http://endpoint.tm.amster.ru/}session.\n");
#endif
        _child_accessor = xmlTextReaderReadNs0SessionType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}session of type {http://endpoint.tm.amster.ru/}session.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0FindUserByLoginType(_findUserByLogin);
          free(_findUserByLogin);
          return NULL;
        }

        _findUserByLogin->session = ((struct task_manager_client_ns0_session*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "login", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0FindUserByLoginType(_findUserByLogin);
          free(_findUserByLogin);
          return NULL;
        }

        _findUserByLogin->login = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {http://endpoint.tm.amster.ru/}findUserByLogin.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {http://endpoint.tm.amster.ru/}findUserByLogin. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _findUserByLogin;
}

/**
 * Writes a FindUserByLogin to XML.
 *
 * @param writer The XML writer.
 * @param _findUserByLogin The FindUserByLogin to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0FindUserByLoginType(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByLogin *_findUserByLogin) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_findUserByLogin->session != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "session", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://endpoint.tm.amster.ru/}session for element {}session...\n");
#endif
    status = xmlTextWriterWriteNs0SessionType(writer, (_findUserByLogin->session));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://endpoint.tm.amster.ru/}session for element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_findUserByLogin->login != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "login", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}login. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}login...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_findUserByLogin->login));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}login. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}login. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a FindUserByLogin.
 *
 * @param _findUserByLogin The FindUserByLogin to free.
 */
static void freeNs0FindUserByLoginType(struct task_manager_client_ns0_findUserByLogin *_findUserByLogin) {
  int i;
  if (_findUserByLogin->session != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor session of type task_manager_client_ns0_findUserByLogin...\n");
#endif
    freeNs0SessionType(_findUserByLogin->session);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor session of type task_manager_client_ns0_findUserByLogin...\n");
#endif
    free(_findUserByLogin->session);
  }
  if (_findUserByLogin->login != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor login of type task_manager_client_ns0_findUserByLogin...\n");
#endif
    freeXsStringType(_findUserByLogin->login);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor login of type task_manager_client_ns0_findUserByLogin...\n");
#endif
    free(_findUserByLogin->login);
  }
}
#endif /* DEF_task_manager_client_ns0_findUserByLogin_M */
#ifndef DEF_task_manager_client_ns0_findUserByLoginResponse_M
#define DEF_task_manager_client_ns0_findUserByLoginResponse_M

/**
 * Reads a FindUserByLoginResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findUserByLoginResponse", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The FindUserByLoginResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_findUserByLoginResponse *xml_read_task_manager_client_ns0_findUserByLoginResponse(xmlTextReaderPtr reader) {
  int status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
  return xmlTextReaderReadNs0FindUserByLoginResponseElement(reader);
}

/**
 * Writes a FindUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}findUserByLoginResponse".
 *
 * @param writer The XML writer.
 * @param _findUserByLoginResponse The FindUserByLoginResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
int xml_write_task_manager_client_ns0_findUserByLoginResponse(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByLoginResponse *_findUserByLoginResponse) {
  return xmlTextWriterWriteNs0FindUserByLoginResponseElementNS(writer, _findUserByLoginResponse, 1);
}

/**
 * Frees a FindUserByLoginResponse.
 *
 * @param _findUserByLoginResponse The FindUserByLoginResponse to free.
 */
void free_task_manager_client_ns0_findUserByLoginResponse(struct task_manager_client_ns0_findUserByLoginResponse *_findUserByLoginResponse) {
  freeNs0FindUserByLoginResponseType(_findUserByLoginResponse);
  free(_findUserByLoginResponse);
}

/**
 * Reads a FindUserByLoginResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}findUserByLoginResponse", and
 * it is assumed that the reader is pointing to that element.
 *
 * @param reader The XML reader.
 * @return The FindUserByLoginResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_findUserByLoginResponse *xmlTextReaderReadNs0FindUserByLoginResponseElement(xmlTextReaderPtr reader) {
  struct task_manager_client_ns0_findUserByLoginResponse *_findUserByLoginResponse = NULL;

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "findUserByLoginResponse", xmlTextReaderConstLocalName(reader)) == 0
    && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    printf("Attempting to read root element {http://endpoint.tm.amster.ru/}findUserByLoginResponse.\n");
#endif
    _findUserByLoginResponse = xmlTextReaderReadNs0FindUserByLoginResponseType(reader);
  }
#if DEBUG_ENUNCIATE
  if (_findUserByLoginResponse == NULL) {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      printf("attempt to read {http://endpoint.tm.amster.ru/}findUserByLoginResponse failed. current element: {}%s\n",  xmlTextReaderConstLocalName(reader));
    }
    else {
      printf("attempt to read {http://endpoint.tm.amster.ru/}findUserByLoginResponse failed. current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
    }
  }
#endif

  return _findUserByLoginResponse;
}

/**
 * Writes a FindUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}findUserByLoginResponse".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _findUserByLoginResponse The FindUserByLoginResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0FindUserByLoginResponseElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByLoginResponse *_findUserByLoginResponse) {
  return xmlTextWriterWriteNs0FindUserByLoginResponseElementNS(writer, _findUserByLoginResponse, 0);
}

/**
 * Writes a FindUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}findUserByLoginResponse".
 *
 * @param writer The XML writer.
 * @param _findUserByLoginResponse The FindUserByLoginResponse to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0FindUserByLoginResponseElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByLoginResponse *_findUserByLoginResponse, int writeNamespaces) {
  int totalBytes = 0;
  int status;

  status = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "findUserByLoginResponse", NULL);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write start element {http://endpoint.tm.amster.ru/}findUserByLoginResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  if (writeNamespaces) {
#if DEBUG_ENUNCIATE > 1
    printf("writing namespaces for start element {http://endpoint.tm.amster.ru/}findUserByLoginResponse...\n");
#endif

    status = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("unable to write namespace attribute xmlns:ns0. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

#if DEBUG_ENUNCIATE > 1
  printf("writing type {http://endpoint.tm.amster.ru/}findUserByLoginResponse for root element {http://endpoint.tm.amster.ru/}findUserByLoginResponse...\n");
#endif
  status = xmlTextWriterWriteNs0FindUserByLoginResponseType(writer, _findUserByLoginResponse);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write type for start element {http://endpoint.tm.amster.ru/}findUserByLoginResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  status = xmlTextWriterEndElement(writer);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to end element {http://endpoint.tm.amster.ru/}findUserByLoginResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  return totalBytes;
}

/**
 * Frees the children of a FindUserByLoginResponse.
 *
 * @param _findUserByLoginResponse The FindUserByLoginResponse whose children are to be free.
 */
static void freeNs0FindUserByLoginResponseElement(struct task_manager_client_ns0_findUserByLoginResponse *_findUserByLoginResponse) {
  freeNs0FindUserByLoginResponseType(_findUserByLoginResponse);
}

/**
 * Reads a FindUserByLoginResponse from XML. The reader is assumed to be at the start element.
 *
 * @return the FindUserByLoginResponse, or NULL in case of error.
 */
static struct task_manager_client_ns0_findUserByLoginResponse *xmlTextReaderReadNs0FindUserByLoginResponseType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_findUserByLoginResponse *_findUserByLoginResponse = calloc(1, sizeof(struct task_manager_client_ns0_findUserByLoginResponse));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs0FindUserByLoginResponseType(_findUserByLoginResponse);
        free(_findUserByLoginResponse);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "return", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}return of type {http://endpoint.tm.amster.ru/}user.\n");
#endif
        _child_accessor = xmlTextReaderReadNs0UserType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}return of type {http://endpoint.tm.amster.ru/}user.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0FindUserByLoginResponseType(_findUserByLoginResponse);
          free(_findUserByLoginResponse);
          return NULL;
        }

        _findUserByLoginResponse->_return = ((struct task_manager_client_ns0_user*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {http://endpoint.tm.amster.ru/}findUserByLoginResponse.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {http://endpoint.tm.amster.ru/}findUserByLoginResponse. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _findUserByLoginResponse;
}

/**
 * Writes a FindUserByLoginResponse to XML.
 *
 * @param writer The XML writer.
 * @param _findUserByLoginResponse The FindUserByLoginResponse to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0FindUserByLoginResponseType(xmlTextWriterPtr writer, struct task_manager_client_ns0_findUserByLoginResponse *_findUserByLoginResponse) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_findUserByLoginResponse->_return != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "return", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}return. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://endpoint.tm.amster.ru/}user for element {}return...\n");
#endif
    status = xmlTextWriterWriteNs0UserType(writer, (_findUserByLoginResponse->_return));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://endpoint.tm.amster.ru/}user for element {}return. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}return. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a FindUserByLoginResponse.
 *
 * @param _findUserByLoginResponse The FindUserByLoginResponse to free.
 */
static void freeNs0FindUserByLoginResponseType(struct task_manager_client_ns0_findUserByLoginResponse *_findUserByLoginResponse) {
  int i;
  if (_findUserByLoginResponse->_return != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor _return of type task_manager_client_ns0_findUserByLoginResponse...\n");
#endif
    freeNs0UserType(_findUserByLoginResponse->_return);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor _return of type task_manager_client_ns0_findUserByLoginResponse...\n");
#endif
    free(_findUserByLoginResponse->_return);
  }
}
#endif /* DEF_task_manager_client_ns0_findUserByLoginResponse_M */
#ifndef DEF_task_manager_client_ns0_load_M
#define DEF_task_manager_client_ns0_load_M

/**
 * Reads a Load element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}load", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The Load, or NULL in case of error.
 */
struct task_manager_client_ns0_load *xml_read_task_manager_client_ns0_load(xmlTextReaderPtr reader) {
  int status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
  return xmlTextReaderReadNs0LoadElement(reader);
}

/**
 * Writes a Load to XML under element name "{http://endpoint.tm.amster.ru/}load".
 *
 * @param writer The XML writer.
 * @param _load The Load to write.
 * @return 1 if successful, 0 otherwise.
 */
int xml_write_task_manager_client_ns0_load(xmlTextWriterPtr writer, struct task_manager_client_ns0_load *_load) {
  return xmlTextWriterWriteNs0LoadElementNS(writer, _load, 1);
}

/**
 * Frees a Load.
 *
 * @param _load The Load to free.
 */
void free_task_manager_client_ns0_load(struct task_manager_client_ns0_load *_load) {
  freeNs0LoadType(_load);
  free(_load);
}

/**
 * Reads a Load element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}load", and
 * it is assumed that the reader is pointing to that element.
 *
 * @param reader The XML reader.
 * @return The Load, or NULL in case of error.
 */
struct task_manager_client_ns0_load *xmlTextReaderReadNs0LoadElement(xmlTextReaderPtr reader) {
  struct task_manager_client_ns0_load *_load = NULL;

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "load", xmlTextReaderConstLocalName(reader)) == 0
    && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    printf("Attempting to read root element {http://endpoint.tm.amster.ru/}load.\n");
#endif
    _load = xmlTextReaderReadNs0LoadType(reader);
  }
#if DEBUG_ENUNCIATE
  if (_load == NULL) {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      printf("attempt to read {http://endpoint.tm.amster.ru/}load failed. current element: {}%s\n",  xmlTextReaderConstLocalName(reader));
    }
    else {
      printf("attempt to read {http://endpoint.tm.amster.ru/}load failed. current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
    }
  }
#endif

  return _load;
}

/**
 * Writes a Load to XML under element name "{http://endpoint.tm.amster.ru/}load".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _load The Load to write.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0LoadElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_load *_load) {
  return xmlTextWriterWriteNs0LoadElementNS(writer, _load, 0);
}

/**
 * Writes a Load to XML under element name "{http://endpoint.tm.amster.ru/}load".
 *
 * @param writer The XML writer.
 * @param _load The Load to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0LoadElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_load *_load, int writeNamespaces) {
  int totalBytes = 0;
  int status;

  status = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "load", NULL);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write start element {http://endpoint.tm.amster.ru/}load. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  if (writeNamespaces) {
#if DEBUG_ENUNCIATE > 1
    printf("writing namespaces for start element {http://endpoint.tm.amster.ru/}load...\n");
#endif

    status = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("unable to write namespace attribute xmlns:ns0. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

#if DEBUG_ENUNCIATE > 1
  printf("writing type {http://endpoint.tm.amster.ru/}load for root element {http://endpoint.tm.amster.ru/}load...\n");
#endif
  status = xmlTextWriterWriteNs0LoadType(writer, _load);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write type for start element {http://endpoint.tm.amster.ru/}load. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  status = xmlTextWriterEndElement(writer);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to end element {http://endpoint.tm.amster.ru/}load. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  return totalBytes;
}

/**
 * Frees the children of a Load.
 *
 * @param _load The Load whose children are to be free.
 */
static void freeNs0LoadElement(struct task_manager_client_ns0_load *_load) {
  freeNs0LoadType(_load);
}

/**
 * Reads a Load from XML. The reader is assumed to be at the start element.
 *
 * @return the Load, or NULL in case of error.
 */
static struct task_manager_client_ns0_load *xmlTextReaderReadNs0LoadType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_load *_load = calloc(1, sizeof(struct task_manager_client_ns0_load));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs0LoadType(_load);
        free(_load);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "session", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}session of type {http://endpoint.tm.amster.ru/}session.\n");
#endif
        _child_accessor = xmlTextReaderReadNs0SessionType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}session of type {http://endpoint.tm.amster.ru/}session.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0LoadType(_load);
          free(_load);
          return NULL;
        }

        _load->session = ((struct task_manager_client_ns0_session*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "domain", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}domain of type {http://endpoint.tm.amster.ru/}domain.\n");
#endif
        _child_accessor = xmlTextReaderReadNs0DomainType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}domain of type {http://endpoint.tm.amster.ru/}domain.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0LoadType(_load);
          free(_load);
          return NULL;
        }

        _load->domain = ((struct task_manager_client_ns0_domain*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {http://endpoint.tm.amster.ru/}load.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {http://endpoint.tm.amster.ru/}load. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _load;
}

/**
 * Writes a Load to XML.
 *
 * @param writer The XML writer.
 * @param _load The Load to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0LoadType(xmlTextWriterPtr writer, struct task_manager_client_ns0_load *_load) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_load->session != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "session", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://endpoint.tm.amster.ru/}session for element {}session...\n");
#endif
    status = xmlTextWriterWriteNs0SessionType(writer, (_load->session));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://endpoint.tm.amster.ru/}session for element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_load->domain != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "domain", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}domain. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://endpoint.tm.amster.ru/}domain for element {}domain...\n");
#endif
    status = xmlTextWriterWriteNs0DomainType(writer, (_load->domain));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://endpoint.tm.amster.ru/}domain for element {}domain. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}domain. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a Load.
 *
 * @param _load The Load to free.
 */
static void freeNs0LoadType(struct task_manager_client_ns0_load *_load) {
  int i;
  if (_load->session != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor session of type task_manager_client_ns0_load...\n");
#endif
    freeNs0SessionType(_load->session);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor session of type task_manager_client_ns0_load...\n");
#endif
    free(_load->session);
  }
  if (_load->domain != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor domain of type task_manager_client_ns0_load...\n");
#endif
    freeNs0DomainType(_load->domain);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor domain of type task_manager_client_ns0_load...\n");
#endif
    free(_load->domain);
  }
}
#endif /* DEF_task_manager_client_ns0_load_M */
#ifndef DEF_task_manager_client_ns0_loadResponse_M
#define DEF_task_manager_client_ns0_loadResponse_M

/**
 * Reads a LoadResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}loadResponse", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The LoadResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_loadResponse *xml_read_task_manager_client_ns0_loadResponse(xmlTextReaderPtr reader) {
  int status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
  return xmlTextReaderReadNs0LoadResponseElement(reader);
}

/**
 * Writes a LoadResponse to XML under element name "{http://endpoint.tm.amster.ru/}loadResponse".
 *
 * @param writer The XML writer.
 * @param _loadResponse The LoadResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
int xml_write_task_manager_client_ns0_loadResponse(xmlTextWriterPtr writer, struct task_manager_client_ns0_loadResponse *_loadResponse) {
  return xmlTextWriterWriteNs0LoadResponseElementNS(writer, _loadResponse, 1);
}

/**
 * Frees a LoadResponse.
 *
 * @param _loadResponse The LoadResponse to free.
 */
void free_task_manager_client_ns0_loadResponse(struct task_manager_client_ns0_loadResponse *_loadResponse) {
  freeNs0LoadResponseType(_loadResponse);
  free(_loadResponse);
}

/**
 * Reads a LoadResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}loadResponse", and
 * it is assumed that the reader is pointing to that element.
 *
 * @param reader The XML reader.
 * @return The LoadResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_loadResponse *xmlTextReaderReadNs0LoadResponseElement(xmlTextReaderPtr reader) {
  struct task_manager_client_ns0_loadResponse *_loadResponse = NULL;

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "loadResponse", xmlTextReaderConstLocalName(reader)) == 0
    && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    printf("Attempting to read root element {http://endpoint.tm.amster.ru/}loadResponse.\n");
#endif
    _loadResponse = xmlTextReaderReadNs0LoadResponseType(reader);
  }
#if DEBUG_ENUNCIATE
  if (_loadResponse == NULL) {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      printf("attempt to read {http://endpoint.tm.amster.ru/}loadResponse failed. current element: {}%s\n",  xmlTextReaderConstLocalName(reader));
    }
    else {
      printf("attempt to read {http://endpoint.tm.amster.ru/}loadResponse failed. current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
    }
  }
#endif

  return _loadResponse;
}

/**
 * Writes a LoadResponse to XML under element name "{http://endpoint.tm.amster.ru/}loadResponse".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _loadResponse The LoadResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0LoadResponseElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_loadResponse *_loadResponse) {
  return xmlTextWriterWriteNs0LoadResponseElementNS(writer, _loadResponse, 0);
}

/**
 * Writes a LoadResponse to XML under element name "{http://endpoint.tm.amster.ru/}loadResponse".
 *
 * @param writer The XML writer.
 * @param _loadResponse The LoadResponse to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0LoadResponseElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_loadResponse *_loadResponse, int writeNamespaces) {
  int totalBytes = 0;
  int status;

  status = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "loadResponse", NULL);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write start element {http://endpoint.tm.amster.ru/}loadResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  if (writeNamespaces) {
#if DEBUG_ENUNCIATE > 1
    printf("writing namespaces for start element {http://endpoint.tm.amster.ru/}loadResponse...\n");
#endif

    status = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("unable to write namespace attribute xmlns:ns0. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

#if DEBUG_ENUNCIATE > 1
  printf("writing type {http://endpoint.tm.amster.ru/}loadResponse for root element {http://endpoint.tm.amster.ru/}loadResponse...\n");
#endif
  status = xmlTextWriterWriteNs0LoadResponseType(writer, _loadResponse);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write type for start element {http://endpoint.tm.amster.ru/}loadResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  status = xmlTextWriterEndElement(writer);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to end element {http://endpoint.tm.amster.ru/}loadResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  return totalBytes;
}

/**
 * Frees the children of a LoadResponse.
 *
 * @param _loadResponse The LoadResponse whose children are to be free.
 */
static void freeNs0LoadResponseElement(struct task_manager_client_ns0_loadResponse *_loadResponse) {
  freeNs0LoadResponseType(_loadResponse);
}

/**
 * Reads a LoadResponse from XML. The reader is assumed to be at the start element.
 *
 * @return the LoadResponse, or NULL in case of error.
 */
static struct task_manager_client_ns0_loadResponse *xmlTextReaderReadNs0LoadResponseType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_loadResponse *_loadResponse = calloc(1, sizeof(struct task_manager_client_ns0_loadResponse));




  return _loadResponse;
}

/**
 * Writes a LoadResponse to XML.
 *
 * @param writer The XML writer.
 * @param _loadResponse The LoadResponse to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0LoadResponseType(xmlTextWriterPtr writer, struct task_manager_client_ns0_loadResponse *_loadResponse) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;

  return totalBytes;
}

/**
 * Frees the elements of a LoadResponse.
 *
 * @param _loadResponse The LoadResponse to free.
 */
static void freeNs0LoadResponseType(struct task_manager_client_ns0_loadResponse *_loadResponse) {
  int i;
}
#endif /* DEF_task_manager_client_ns0_loadResponse_M */
#ifndef DEF_task_manager_client_ns0_lockUserByLogin_M
#define DEF_task_manager_client_ns0_lockUserByLogin_M

/**
 * Reads a LockUserByLogin element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}lockUserByLogin", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The LockUserByLogin, or NULL in case of error.
 */
struct task_manager_client_ns0_lockUserByLogin *xml_read_task_manager_client_ns0_lockUserByLogin(xmlTextReaderPtr reader) {
  int status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
  return xmlTextReaderReadNs0LockUserByLoginElement(reader);
}

/**
 * Writes a LockUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}lockUserByLogin".
 *
 * @param writer The XML writer.
 * @param _lockUserByLogin The LockUserByLogin to write.
 * @return 1 if successful, 0 otherwise.
 */
int xml_write_task_manager_client_ns0_lockUserByLogin(xmlTextWriterPtr writer, struct task_manager_client_ns0_lockUserByLogin *_lockUserByLogin) {
  return xmlTextWriterWriteNs0LockUserByLoginElementNS(writer, _lockUserByLogin, 1);
}

/**
 * Frees a LockUserByLogin.
 *
 * @param _lockUserByLogin The LockUserByLogin to free.
 */
void free_task_manager_client_ns0_lockUserByLogin(struct task_manager_client_ns0_lockUserByLogin *_lockUserByLogin) {
  freeNs0LockUserByLoginType(_lockUserByLogin);
  free(_lockUserByLogin);
}

/**
 * Reads a LockUserByLogin element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}lockUserByLogin", and
 * it is assumed that the reader is pointing to that element.
 *
 * @param reader The XML reader.
 * @return The LockUserByLogin, or NULL in case of error.
 */
struct task_manager_client_ns0_lockUserByLogin *xmlTextReaderReadNs0LockUserByLoginElement(xmlTextReaderPtr reader) {
  struct task_manager_client_ns0_lockUserByLogin *_lockUserByLogin = NULL;

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "lockUserByLogin", xmlTextReaderConstLocalName(reader)) == 0
    && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    printf("Attempting to read root element {http://endpoint.tm.amster.ru/}lockUserByLogin.\n");
#endif
    _lockUserByLogin = xmlTextReaderReadNs0LockUserByLoginType(reader);
  }
#if DEBUG_ENUNCIATE
  if (_lockUserByLogin == NULL) {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      printf("attempt to read {http://endpoint.tm.amster.ru/}lockUserByLogin failed. current element: {}%s\n",  xmlTextReaderConstLocalName(reader));
    }
    else {
      printf("attempt to read {http://endpoint.tm.amster.ru/}lockUserByLogin failed. current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
    }
  }
#endif

  return _lockUserByLogin;
}

/**
 * Writes a LockUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}lockUserByLogin".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _lockUserByLogin The LockUserByLogin to write.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0LockUserByLoginElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_lockUserByLogin *_lockUserByLogin) {
  return xmlTextWriterWriteNs0LockUserByLoginElementNS(writer, _lockUserByLogin, 0);
}

/**
 * Writes a LockUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}lockUserByLogin".
 *
 * @param writer The XML writer.
 * @param _lockUserByLogin The LockUserByLogin to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0LockUserByLoginElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_lockUserByLogin *_lockUserByLogin, int writeNamespaces) {
  int totalBytes = 0;
  int status;

  status = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "lockUserByLogin", NULL);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write start element {http://endpoint.tm.amster.ru/}lockUserByLogin. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  if (writeNamespaces) {
#if DEBUG_ENUNCIATE > 1
    printf("writing namespaces for start element {http://endpoint.tm.amster.ru/}lockUserByLogin...\n");
#endif

    status = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("unable to write namespace attribute xmlns:ns0. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

#if DEBUG_ENUNCIATE > 1
  printf("writing type {http://endpoint.tm.amster.ru/}lockUserByLogin for root element {http://endpoint.tm.amster.ru/}lockUserByLogin...\n");
#endif
  status = xmlTextWriterWriteNs0LockUserByLoginType(writer, _lockUserByLogin);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write type for start element {http://endpoint.tm.amster.ru/}lockUserByLogin. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  status = xmlTextWriterEndElement(writer);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to end element {http://endpoint.tm.amster.ru/}lockUserByLogin. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  return totalBytes;
}

/**
 * Frees the children of a LockUserByLogin.
 *
 * @param _lockUserByLogin The LockUserByLogin whose children are to be free.
 */
static void freeNs0LockUserByLoginElement(struct task_manager_client_ns0_lockUserByLogin *_lockUserByLogin) {
  freeNs0LockUserByLoginType(_lockUserByLogin);
}

/**
 * Reads a LockUserByLogin from XML. The reader is assumed to be at the start element.
 *
 * @return the LockUserByLogin, or NULL in case of error.
 */
static struct task_manager_client_ns0_lockUserByLogin *xmlTextReaderReadNs0LockUserByLoginType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_lockUserByLogin *_lockUserByLogin = calloc(1, sizeof(struct task_manager_client_ns0_lockUserByLogin));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs0LockUserByLoginType(_lockUserByLogin);
        free(_lockUserByLogin);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "session", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}session of type {http://endpoint.tm.amster.ru/}session.\n");
#endif
        _child_accessor = xmlTextReaderReadNs0SessionType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}session of type {http://endpoint.tm.amster.ru/}session.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0LockUserByLoginType(_lockUserByLogin);
          free(_lockUserByLogin);
          return NULL;
        }

        _lockUserByLogin->session = ((struct task_manager_client_ns0_session*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "login", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0LockUserByLoginType(_lockUserByLogin);
          free(_lockUserByLogin);
          return NULL;
        }

        _lockUserByLogin->login = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {http://endpoint.tm.amster.ru/}lockUserByLogin.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {http://endpoint.tm.amster.ru/}lockUserByLogin. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _lockUserByLogin;
}

/**
 * Writes a LockUserByLogin to XML.
 *
 * @param writer The XML writer.
 * @param _lockUserByLogin The LockUserByLogin to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0LockUserByLoginType(xmlTextWriterPtr writer, struct task_manager_client_ns0_lockUserByLogin *_lockUserByLogin) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_lockUserByLogin->session != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "session", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://endpoint.tm.amster.ru/}session for element {}session...\n");
#endif
    status = xmlTextWriterWriteNs0SessionType(writer, (_lockUserByLogin->session));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://endpoint.tm.amster.ru/}session for element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_lockUserByLogin->login != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "login", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}login. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}login...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_lockUserByLogin->login));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}login. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}login. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a LockUserByLogin.
 *
 * @param _lockUserByLogin The LockUserByLogin to free.
 */
static void freeNs0LockUserByLoginType(struct task_manager_client_ns0_lockUserByLogin *_lockUserByLogin) {
  int i;
  if (_lockUserByLogin->session != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor session of type task_manager_client_ns0_lockUserByLogin...\n");
#endif
    freeNs0SessionType(_lockUserByLogin->session);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor session of type task_manager_client_ns0_lockUserByLogin...\n");
#endif
    free(_lockUserByLogin->session);
  }
  if (_lockUserByLogin->login != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor login of type task_manager_client_ns0_lockUserByLogin...\n");
#endif
    freeXsStringType(_lockUserByLogin->login);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor login of type task_manager_client_ns0_lockUserByLogin...\n");
#endif
    free(_lockUserByLogin->login);
  }
}
#endif /* DEF_task_manager_client_ns0_lockUserByLogin_M */
#ifndef DEF_task_manager_client_ns0_lockUserByLoginResponse_M
#define DEF_task_manager_client_ns0_lockUserByLoginResponse_M

/**
 * Reads a LockUserByLoginResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}lockUserByLoginResponse", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The LockUserByLoginResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_lockUserByLoginResponse *xml_read_task_manager_client_ns0_lockUserByLoginResponse(xmlTextReaderPtr reader) {
  int status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
  return xmlTextReaderReadNs0LockUserByLoginResponseElement(reader);
}

/**
 * Writes a LockUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}lockUserByLoginResponse".
 *
 * @param writer The XML writer.
 * @param _lockUserByLoginResponse The LockUserByLoginResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
int xml_write_task_manager_client_ns0_lockUserByLoginResponse(xmlTextWriterPtr writer, struct task_manager_client_ns0_lockUserByLoginResponse *_lockUserByLoginResponse) {
  return xmlTextWriterWriteNs0LockUserByLoginResponseElementNS(writer, _lockUserByLoginResponse, 1);
}

/**
 * Frees a LockUserByLoginResponse.
 *
 * @param _lockUserByLoginResponse The LockUserByLoginResponse to free.
 */
void free_task_manager_client_ns0_lockUserByLoginResponse(struct task_manager_client_ns0_lockUserByLoginResponse *_lockUserByLoginResponse) {
  freeNs0LockUserByLoginResponseType(_lockUserByLoginResponse);
  free(_lockUserByLoginResponse);
}

/**
 * Reads a LockUserByLoginResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}lockUserByLoginResponse", and
 * it is assumed that the reader is pointing to that element.
 *
 * @param reader The XML reader.
 * @return The LockUserByLoginResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_lockUserByLoginResponse *xmlTextReaderReadNs0LockUserByLoginResponseElement(xmlTextReaderPtr reader) {
  struct task_manager_client_ns0_lockUserByLoginResponse *_lockUserByLoginResponse = NULL;

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "lockUserByLoginResponse", xmlTextReaderConstLocalName(reader)) == 0
    && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    printf("Attempting to read root element {http://endpoint.tm.amster.ru/}lockUserByLoginResponse.\n");
#endif
    _lockUserByLoginResponse = xmlTextReaderReadNs0LockUserByLoginResponseType(reader);
  }
#if DEBUG_ENUNCIATE
  if (_lockUserByLoginResponse == NULL) {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      printf("attempt to read {http://endpoint.tm.amster.ru/}lockUserByLoginResponse failed. current element: {}%s\n",  xmlTextReaderConstLocalName(reader));
    }
    else {
      printf("attempt to read {http://endpoint.tm.amster.ru/}lockUserByLoginResponse failed. current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
    }
  }
#endif

  return _lockUserByLoginResponse;
}

/**
 * Writes a LockUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}lockUserByLoginResponse".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _lockUserByLoginResponse The LockUserByLoginResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0LockUserByLoginResponseElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_lockUserByLoginResponse *_lockUserByLoginResponse) {
  return xmlTextWriterWriteNs0LockUserByLoginResponseElementNS(writer, _lockUserByLoginResponse, 0);
}

/**
 * Writes a LockUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}lockUserByLoginResponse".
 *
 * @param writer The XML writer.
 * @param _lockUserByLoginResponse The LockUserByLoginResponse to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0LockUserByLoginResponseElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_lockUserByLoginResponse *_lockUserByLoginResponse, int writeNamespaces) {
  int totalBytes = 0;
  int status;

  status = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "lockUserByLoginResponse", NULL);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write start element {http://endpoint.tm.amster.ru/}lockUserByLoginResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  if (writeNamespaces) {
#if DEBUG_ENUNCIATE > 1
    printf("writing namespaces for start element {http://endpoint.tm.amster.ru/}lockUserByLoginResponse...\n");
#endif

    status = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("unable to write namespace attribute xmlns:ns0. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

#if DEBUG_ENUNCIATE > 1
  printf("writing type {http://endpoint.tm.amster.ru/}lockUserByLoginResponse for root element {http://endpoint.tm.amster.ru/}lockUserByLoginResponse...\n");
#endif
  status = xmlTextWriterWriteNs0LockUserByLoginResponseType(writer, _lockUserByLoginResponse);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write type for start element {http://endpoint.tm.amster.ru/}lockUserByLoginResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  status = xmlTextWriterEndElement(writer);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to end element {http://endpoint.tm.amster.ru/}lockUserByLoginResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  return totalBytes;
}

/**
 * Frees the children of a LockUserByLoginResponse.
 *
 * @param _lockUserByLoginResponse The LockUserByLoginResponse whose children are to be free.
 */
static void freeNs0LockUserByLoginResponseElement(struct task_manager_client_ns0_lockUserByLoginResponse *_lockUserByLoginResponse) {
  freeNs0LockUserByLoginResponseType(_lockUserByLoginResponse);
}

/**
 * Reads a LockUserByLoginResponse from XML. The reader is assumed to be at the start element.
 *
 * @return the LockUserByLoginResponse, or NULL in case of error.
 */
static struct task_manager_client_ns0_lockUserByLoginResponse *xmlTextReaderReadNs0LockUserByLoginResponseType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_lockUserByLoginResponse *_lockUserByLoginResponse = calloc(1, sizeof(struct task_manager_client_ns0_lockUserByLoginResponse));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs0LockUserByLoginResponseType(_lockUserByLoginResponse);
        free(_lockUserByLoginResponse);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "return", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}return of type {http://endpoint.tm.amster.ru/}user.\n");
#endif
        _child_accessor = xmlTextReaderReadNs0UserType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}return of type {http://endpoint.tm.amster.ru/}user.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0LockUserByLoginResponseType(_lockUserByLoginResponse);
          free(_lockUserByLoginResponse);
          return NULL;
        }

        _lockUserByLoginResponse->_return = ((struct task_manager_client_ns0_user*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {http://endpoint.tm.amster.ru/}lockUserByLoginResponse.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {http://endpoint.tm.amster.ru/}lockUserByLoginResponse. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _lockUserByLoginResponse;
}

/**
 * Writes a LockUserByLoginResponse to XML.
 *
 * @param writer The XML writer.
 * @param _lockUserByLoginResponse The LockUserByLoginResponse to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0LockUserByLoginResponseType(xmlTextWriterPtr writer, struct task_manager_client_ns0_lockUserByLoginResponse *_lockUserByLoginResponse) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_lockUserByLoginResponse->_return != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "return", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}return. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://endpoint.tm.amster.ru/}user for element {}return...\n");
#endif
    status = xmlTextWriterWriteNs0UserType(writer, (_lockUserByLoginResponse->_return));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://endpoint.tm.amster.ru/}user for element {}return. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}return. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a LockUserByLoginResponse.
 *
 * @param _lockUserByLoginResponse The LockUserByLoginResponse to free.
 */
static void freeNs0LockUserByLoginResponseType(struct task_manager_client_ns0_lockUserByLoginResponse *_lockUserByLoginResponse) {
  int i;
  if (_lockUserByLoginResponse->_return != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor _return of type task_manager_client_ns0_lockUserByLoginResponse...\n");
#endif
    freeNs0UserType(_lockUserByLoginResponse->_return);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor _return of type task_manager_client_ns0_lockUserByLoginResponse...\n");
#endif
    free(_lockUserByLoginResponse->_return);
  }
}
#endif /* DEF_task_manager_client_ns0_lockUserByLoginResponse_M */
#ifndef DEF_task_manager_client_ns0_project_M
#define DEF_task_manager_client_ns0_project_M

/**
 * Reads a Project from XML. The reader is assumed to be at the start element.
 *
 * @return the Project, or NULL in case of error.
 */
static struct task_manager_client_ns0_project *xmlTextReaderReadNs0ProjectType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_project *_project = calloc(1, sizeof(struct task_manager_client_ns0_project));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs0ProjectType(_project);
        free(_project);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "id", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0ProjectType(_project);
          free(_project);
          return NULL;
        }

        _project->id = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "description", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}description of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}description of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0ProjectType(_project);
          free(_project);
          return NULL;
        }

        _project->description = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "name", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}name of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}name of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0ProjectType(_project);
          free(_project);
          return NULL;
        }

        _project->name = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "userId", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0ProjectType(_project);
          free(_project);
          return NULL;
        }

        _project->userId = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {http://endpoint.tm.amster.ru/}project.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {http://endpoint.tm.amster.ru/}project. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _project;
}

/**
 * Writes a Project to XML.
 *
 * @param writer The XML writer.
 * @param _project The Project to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0ProjectType(xmlTextWriterPtr writer, struct task_manager_client_ns0_project *_project) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_project->id != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "id", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}id...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_project->id));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_project->description != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "description", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}description. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}description...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_project->description));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}description. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}description. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_project->name != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "name", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}name. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}name...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_project->name));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}name. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}name. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_project->userId != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "userId", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}userId. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}userId...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_project->userId));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}userId. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}userId. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a Project.
 *
 * @param _project The Project to free.
 */
static void freeNs0ProjectType(struct task_manager_client_ns0_project *_project) {
  int i;
  if (_project->id != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor id of type task_manager_client_ns0_project...\n");
#endif
    freeXsStringType(_project->id);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor id of type task_manager_client_ns0_project...\n");
#endif
    free(_project->id);
  }
  if (_project->description != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor description of type task_manager_client_ns0_project...\n");
#endif
    freeXsStringType(_project->description);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor description of type task_manager_client_ns0_project...\n");
#endif
    free(_project->description);
  }
  if (_project->name != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor name of type task_manager_client_ns0_project...\n");
#endif
    freeXsStringType(_project->name);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor name of type task_manager_client_ns0_project...\n");
#endif
    free(_project->name);
  }
  if (_project->userId != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor userId of type task_manager_client_ns0_project...\n");
#endif
    freeXsStringType(_project->userId);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor userId of type task_manager_client_ns0_project...\n");
#endif
    free(_project->userId);
  }
}
#endif /* DEF_task_manager_client_ns0_project_M */
#ifndef DEF_task_manager_client_ns0_removeUserById_M
#define DEF_task_manager_client_ns0_removeUserById_M

/**
 * Reads a RemoveUserById element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}removeUserById", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The RemoveUserById, or NULL in case of error.
 */
struct task_manager_client_ns0_removeUserById *xml_read_task_manager_client_ns0_removeUserById(xmlTextReaderPtr reader) {
  int status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
  return xmlTextReaderReadNs0RemoveUserByIdElement(reader);
}

/**
 * Writes a RemoveUserById to XML under element name "{http://endpoint.tm.amster.ru/}removeUserById".
 *
 * @param writer The XML writer.
 * @param _removeUserById The RemoveUserById to write.
 * @return 1 if successful, 0 otherwise.
 */
int xml_write_task_manager_client_ns0_removeUserById(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserById *_removeUserById) {
  return xmlTextWriterWriteNs0RemoveUserByIdElementNS(writer, _removeUserById, 1);
}

/**
 * Frees a RemoveUserById.
 *
 * @param _removeUserById The RemoveUserById to free.
 */
void free_task_manager_client_ns0_removeUserById(struct task_manager_client_ns0_removeUserById *_removeUserById) {
  freeNs0RemoveUserByIdType(_removeUserById);
  free(_removeUserById);
}

/**
 * Reads a RemoveUserById element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}removeUserById", and
 * it is assumed that the reader is pointing to that element.
 *
 * @param reader The XML reader.
 * @return The RemoveUserById, or NULL in case of error.
 */
struct task_manager_client_ns0_removeUserById *xmlTextReaderReadNs0RemoveUserByIdElement(xmlTextReaderPtr reader) {
  struct task_manager_client_ns0_removeUserById *_removeUserById = NULL;

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "removeUserById", xmlTextReaderConstLocalName(reader)) == 0
    && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    printf("Attempting to read root element {http://endpoint.tm.amster.ru/}removeUserById.\n");
#endif
    _removeUserById = xmlTextReaderReadNs0RemoveUserByIdType(reader);
  }
#if DEBUG_ENUNCIATE
  if (_removeUserById == NULL) {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      printf("attempt to read {http://endpoint.tm.amster.ru/}removeUserById failed. current element: {}%s\n",  xmlTextReaderConstLocalName(reader));
    }
    else {
      printf("attempt to read {http://endpoint.tm.amster.ru/}removeUserById failed. current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
    }
  }
#endif

  return _removeUserById;
}

/**
 * Writes a RemoveUserById to XML under element name "{http://endpoint.tm.amster.ru/}removeUserById".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _removeUserById The RemoveUserById to write.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0RemoveUserByIdElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserById *_removeUserById) {
  return xmlTextWriterWriteNs0RemoveUserByIdElementNS(writer, _removeUserById, 0);
}

/**
 * Writes a RemoveUserById to XML under element name "{http://endpoint.tm.amster.ru/}removeUserById".
 *
 * @param writer The XML writer.
 * @param _removeUserById The RemoveUserById to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0RemoveUserByIdElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserById *_removeUserById, int writeNamespaces) {
  int totalBytes = 0;
  int status;

  status = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "removeUserById", NULL);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write start element {http://endpoint.tm.amster.ru/}removeUserById. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  if (writeNamespaces) {
#if DEBUG_ENUNCIATE > 1
    printf("writing namespaces for start element {http://endpoint.tm.amster.ru/}removeUserById...\n");
#endif

    status = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("unable to write namespace attribute xmlns:ns0. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

#if DEBUG_ENUNCIATE > 1
  printf("writing type {http://endpoint.tm.amster.ru/}removeUserById for root element {http://endpoint.tm.amster.ru/}removeUserById...\n");
#endif
  status = xmlTextWriterWriteNs0RemoveUserByIdType(writer, _removeUserById);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write type for start element {http://endpoint.tm.amster.ru/}removeUserById. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  status = xmlTextWriterEndElement(writer);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to end element {http://endpoint.tm.amster.ru/}removeUserById. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  return totalBytes;
}

/**
 * Frees the children of a RemoveUserById.
 *
 * @param _removeUserById The RemoveUserById whose children are to be free.
 */
static void freeNs0RemoveUserByIdElement(struct task_manager_client_ns0_removeUserById *_removeUserById) {
  freeNs0RemoveUserByIdType(_removeUserById);
}

/**
 * Reads a RemoveUserById from XML. The reader is assumed to be at the start element.
 *
 * @return the RemoveUserById, or NULL in case of error.
 */
static struct task_manager_client_ns0_removeUserById *xmlTextReaderReadNs0RemoveUserByIdType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_removeUserById *_removeUserById = calloc(1, sizeof(struct task_manager_client_ns0_removeUserById));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs0RemoveUserByIdType(_removeUserById);
        free(_removeUserById);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "session", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}session of type {http://endpoint.tm.amster.ru/}session.\n");
#endif
        _child_accessor = xmlTextReaderReadNs0SessionType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}session of type {http://endpoint.tm.amster.ru/}session.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0RemoveUserByIdType(_removeUserById);
          free(_removeUserById);
          return NULL;
        }

        _removeUserById->session = ((struct task_manager_client_ns0_session*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "id", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0RemoveUserByIdType(_removeUserById);
          free(_removeUserById);
          return NULL;
        }

        _removeUserById->id = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {http://endpoint.tm.amster.ru/}removeUserById.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {http://endpoint.tm.amster.ru/}removeUserById. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _removeUserById;
}

/**
 * Writes a RemoveUserById to XML.
 *
 * @param writer The XML writer.
 * @param _removeUserById The RemoveUserById to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0RemoveUserByIdType(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserById *_removeUserById) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_removeUserById->session != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "session", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://endpoint.tm.amster.ru/}session for element {}session...\n");
#endif
    status = xmlTextWriterWriteNs0SessionType(writer, (_removeUserById->session));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://endpoint.tm.amster.ru/}session for element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_removeUserById->id != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "id", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}id...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_removeUserById->id));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a RemoveUserById.
 *
 * @param _removeUserById The RemoveUserById to free.
 */
static void freeNs0RemoveUserByIdType(struct task_manager_client_ns0_removeUserById *_removeUserById) {
  int i;
  if (_removeUserById->session != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor session of type task_manager_client_ns0_removeUserById...\n");
#endif
    freeNs0SessionType(_removeUserById->session);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor session of type task_manager_client_ns0_removeUserById...\n");
#endif
    free(_removeUserById->session);
  }
  if (_removeUserById->id != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor id of type task_manager_client_ns0_removeUserById...\n");
#endif
    freeXsStringType(_removeUserById->id);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor id of type task_manager_client_ns0_removeUserById...\n");
#endif
    free(_removeUserById->id);
  }
}
#endif /* DEF_task_manager_client_ns0_removeUserById_M */
#ifndef DEF_task_manager_client_ns0_removeUserByIdResponse_M
#define DEF_task_manager_client_ns0_removeUserByIdResponse_M

/**
 * Reads a RemoveUserByIdResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}removeUserByIdResponse", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The RemoveUserByIdResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_removeUserByIdResponse *xml_read_task_manager_client_ns0_removeUserByIdResponse(xmlTextReaderPtr reader) {
  int status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
  return xmlTextReaderReadNs0RemoveUserByIdResponseElement(reader);
}

/**
 * Writes a RemoveUserByIdResponse to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByIdResponse".
 *
 * @param writer The XML writer.
 * @param _removeUserByIdResponse The RemoveUserByIdResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
int xml_write_task_manager_client_ns0_removeUserByIdResponse(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByIdResponse *_removeUserByIdResponse) {
  return xmlTextWriterWriteNs0RemoveUserByIdResponseElementNS(writer, _removeUserByIdResponse, 1);
}

/**
 * Frees a RemoveUserByIdResponse.
 *
 * @param _removeUserByIdResponse The RemoveUserByIdResponse to free.
 */
void free_task_manager_client_ns0_removeUserByIdResponse(struct task_manager_client_ns0_removeUserByIdResponse *_removeUserByIdResponse) {
  freeNs0RemoveUserByIdResponseType(_removeUserByIdResponse);
  free(_removeUserByIdResponse);
}

/**
 * Reads a RemoveUserByIdResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}removeUserByIdResponse", and
 * it is assumed that the reader is pointing to that element.
 *
 * @param reader The XML reader.
 * @return The RemoveUserByIdResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_removeUserByIdResponse *xmlTextReaderReadNs0RemoveUserByIdResponseElement(xmlTextReaderPtr reader) {
  struct task_manager_client_ns0_removeUserByIdResponse *_removeUserByIdResponse = NULL;

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "removeUserByIdResponse", xmlTextReaderConstLocalName(reader)) == 0
    && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    printf("Attempting to read root element {http://endpoint.tm.amster.ru/}removeUserByIdResponse.\n");
#endif
    _removeUserByIdResponse = xmlTextReaderReadNs0RemoveUserByIdResponseType(reader);
  }
#if DEBUG_ENUNCIATE
  if (_removeUserByIdResponse == NULL) {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      printf("attempt to read {http://endpoint.tm.amster.ru/}removeUserByIdResponse failed. current element: {}%s\n",  xmlTextReaderConstLocalName(reader));
    }
    else {
      printf("attempt to read {http://endpoint.tm.amster.ru/}removeUserByIdResponse failed. current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
    }
  }
#endif

  return _removeUserByIdResponse;
}

/**
 * Writes a RemoveUserByIdResponse to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByIdResponse".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _removeUserByIdResponse The RemoveUserByIdResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0RemoveUserByIdResponseElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByIdResponse *_removeUserByIdResponse) {
  return xmlTextWriterWriteNs0RemoveUserByIdResponseElementNS(writer, _removeUserByIdResponse, 0);
}

/**
 * Writes a RemoveUserByIdResponse to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByIdResponse".
 *
 * @param writer The XML writer.
 * @param _removeUserByIdResponse The RemoveUserByIdResponse to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0RemoveUserByIdResponseElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByIdResponse *_removeUserByIdResponse, int writeNamespaces) {
  int totalBytes = 0;
  int status;

  status = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "removeUserByIdResponse", NULL);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write start element {http://endpoint.tm.amster.ru/}removeUserByIdResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  if (writeNamespaces) {
#if DEBUG_ENUNCIATE > 1
    printf("writing namespaces for start element {http://endpoint.tm.amster.ru/}removeUserByIdResponse...\n");
#endif

    status = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("unable to write namespace attribute xmlns:ns0. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

#if DEBUG_ENUNCIATE > 1
  printf("writing type {http://endpoint.tm.amster.ru/}removeUserByIdResponse for root element {http://endpoint.tm.amster.ru/}removeUserByIdResponse...\n");
#endif
  status = xmlTextWriterWriteNs0RemoveUserByIdResponseType(writer, _removeUserByIdResponse);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write type for start element {http://endpoint.tm.amster.ru/}removeUserByIdResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  status = xmlTextWriterEndElement(writer);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to end element {http://endpoint.tm.amster.ru/}removeUserByIdResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  return totalBytes;
}

/**
 * Frees the children of a RemoveUserByIdResponse.
 *
 * @param _removeUserByIdResponse The RemoveUserByIdResponse whose children are to be free.
 */
static void freeNs0RemoveUserByIdResponseElement(struct task_manager_client_ns0_removeUserByIdResponse *_removeUserByIdResponse) {
  freeNs0RemoveUserByIdResponseType(_removeUserByIdResponse);
}

/**
 * Reads a RemoveUserByIdResponse from XML. The reader is assumed to be at the start element.
 *
 * @return the RemoveUserByIdResponse, or NULL in case of error.
 */
static struct task_manager_client_ns0_removeUserByIdResponse *xmlTextReaderReadNs0RemoveUserByIdResponseType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_removeUserByIdResponse *_removeUserByIdResponse = calloc(1, sizeof(struct task_manager_client_ns0_removeUserByIdResponse));




  return _removeUserByIdResponse;
}

/**
 * Writes a RemoveUserByIdResponse to XML.
 *
 * @param writer The XML writer.
 * @param _removeUserByIdResponse The RemoveUserByIdResponse to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0RemoveUserByIdResponseType(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByIdResponse *_removeUserByIdResponse) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;

  return totalBytes;
}

/**
 * Frees the elements of a RemoveUserByIdResponse.
 *
 * @param _removeUserByIdResponse The RemoveUserByIdResponse to free.
 */
static void freeNs0RemoveUserByIdResponseType(struct task_manager_client_ns0_removeUserByIdResponse *_removeUserByIdResponse) {
  int i;
}
#endif /* DEF_task_manager_client_ns0_removeUserByIdResponse_M */
#ifndef DEF_task_manager_client_ns0_removeUserByLogin_M
#define DEF_task_manager_client_ns0_removeUserByLogin_M

/**
 * Reads a RemoveUserByLogin element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}removeUserByLogin", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The RemoveUserByLogin, or NULL in case of error.
 */
struct task_manager_client_ns0_removeUserByLogin *xml_read_task_manager_client_ns0_removeUserByLogin(xmlTextReaderPtr reader) {
  int status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
  return xmlTextReaderReadNs0RemoveUserByLoginElement(reader);
}

/**
 * Writes a RemoveUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByLogin".
 *
 * @param writer The XML writer.
 * @param _removeUserByLogin The RemoveUserByLogin to write.
 * @return 1 if successful, 0 otherwise.
 */
int xml_write_task_manager_client_ns0_removeUserByLogin(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByLogin *_removeUserByLogin) {
  return xmlTextWriterWriteNs0RemoveUserByLoginElementNS(writer, _removeUserByLogin, 1);
}

/**
 * Frees a RemoveUserByLogin.
 *
 * @param _removeUserByLogin The RemoveUserByLogin to free.
 */
void free_task_manager_client_ns0_removeUserByLogin(struct task_manager_client_ns0_removeUserByLogin *_removeUserByLogin) {
  freeNs0RemoveUserByLoginType(_removeUserByLogin);
  free(_removeUserByLogin);
}

/**
 * Reads a RemoveUserByLogin element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}removeUserByLogin", and
 * it is assumed that the reader is pointing to that element.
 *
 * @param reader The XML reader.
 * @return The RemoveUserByLogin, or NULL in case of error.
 */
struct task_manager_client_ns0_removeUserByLogin *xmlTextReaderReadNs0RemoveUserByLoginElement(xmlTextReaderPtr reader) {
  struct task_manager_client_ns0_removeUserByLogin *_removeUserByLogin = NULL;

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "removeUserByLogin", xmlTextReaderConstLocalName(reader)) == 0
    && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    printf("Attempting to read root element {http://endpoint.tm.amster.ru/}removeUserByLogin.\n");
#endif
    _removeUserByLogin = xmlTextReaderReadNs0RemoveUserByLoginType(reader);
  }
#if DEBUG_ENUNCIATE
  if (_removeUserByLogin == NULL) {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      printf("attempt to read {http://endpoint.tm.amster.ru/}removeUserByLogin failed. current element: {}%s\n",  xmlTextReaderConstLocalName(reader));
    }
    else {
      printf("attempt to read {http://endpoint.tm.amster.ru/}removeUserByLogin failed. current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
    }
  }
#endif

  return _removeUserByLogin;
}

/**
 * Writes a RemoveUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByLogin".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _removeUserByLogin The RemoveUserByLogin to write.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0RemoveUserByLoginElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByLogin *_removeUserByLogin) {
  return xmlTextWriterWriteNs0RemoveUserByLoginElementNS(writer, _removeUserByLogin, 0);
}

/**
 * Writes a RemoveUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByLogin".
 *
 * @param writer The XML writer.
 * @param _removeUserByLogin The RemoveUserByLogin to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0RemoveUserByLoginElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByLogin *_removeUserByLogin, int writeNamespaces) {
  int totalBytes = 0;
  int status;

  status = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "removeUserByLogin", NULL);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write start element {http://endpoint.tm.amster.ru/}removeUserByLogin. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  if (writeNamespaces) {
#if DEBUG_ENUNCIATE > 1
    printf("writing namespaces for start element {http://endpoint.tm.amster.ru/}removeUserByLogin...\n");
#endif

    status = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("unable to write namespace attribute xmlns:ns0. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

#if DEBUG_ENUNCIATE > 1
  printf("writing type {http://endpoint.tm.amster.ru/}removeUserByLogin for root element {http://endpoint.tm.amster.ru/}removeUserByLogin...\n");
#endif
  status = xmlTextWriterWriteNs0RemoveUserByLoginType(writer, _removeUserByLogin);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write type for start element {http://endpoint.tm.amster.ru/}removeUserByLogin. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  status = xmlTextWriterEndElement(writer);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to end element {http://endpoint.tm.amster.ru/}removeUserByLogin. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  return totalBytes;
}

/**
 * Frees the children of a RemoveUserByLogin.
 *
 * @param _removeUserByLogin The RemoveUserByLogin whose children are to be free.
 */
static void freeNs0RemoveUserByLoginElement(struct task_manager_client_ns0_removeUserByLogin *_removeUserByLogin) {
  freeNs0RemoveUserByLoginType(_removeUserByLogin);
}

/**
 * Reads a RemoveUserByLogin from XML. The reader is assumed to be at the start element.
 *
 * @return the RemoveUserByLogin, or NULL in case of error.
 */
static struct task_manager_client_ns0_removeUserByLogin *xmlTextReaderReadNs0RemoveUserByLoginType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_removeUserByLogin *_removeUserByLogin = calloc(1, sizeof(struct task_manager_client_ns0_removeUserByLogin));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs0RemoveUserByLoginType(_removeUserByLogin);
        free(_removeUserByLogin);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "session", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}session of type {http://endpoint.tm.amster.ru/}session.\n");
#endif
        _child_accessor = xmlTextReaderReadNs0SessionType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}session of type {http://endpoint.tm.amster.ru/}session.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0RemoveUserByLoginType(_removeUserByLogin);
          free(_removeUserByLogin);
          return NULL;
        }

        _removeUserByLogin->session = ((struct task_manager_client_ns0_session*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "login", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0RemoveUserByLoginType(_removeUserByLogin);
          free(_removeUserByLogin);
          return NULL;
        }

        _removeUserByLogin->login = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {http://endpoint.tm.amster.ru/}removeUserByLogin.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {http://endpoint.tm.amster.ru/}removeUserByLogin. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _removeUserByLogin;
}

/**
 * Writes a RemoveUserByLogin to XML.
 *
 * @param writer The XML writer.
 * @param _removeUserByLogin The RemoveUserByLogin to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0RemoveUserByLoginType(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByLogin *_removeUserByLogin) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_removeUserByLogin->session != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "session", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://endpoint.tm.amster.ru/}session for element {}session...\n");
#endif
    status = xmlTextWriterWriteNs0SessionType(writer, (_removeUserByLogin->session));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://endpoint.tm.amster.ru/}session for element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_removeUserByLogin->login != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "login", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}login. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}login...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_removeUserByLogin->login));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}login. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}login. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a RemoveUserByLogin.
 *
 * @param _removeUserByLogin The RemoveUserByLogin to free.
 */
static void freeNs0RemoveUserByLoginType(struct task_manager_client_ns0_removeUserByLogin *_removeUserByLogin) {
  int i;
  if (_removeUserByLogin->session != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor session of type task_manager_client_ns0_removeUserByLogin...\n");
#endif
    freeNs0SessionType(_removeUserByLogin->session);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor session of type task_manager_client_ns0_removeUserByLogin...\n");
#endif
    free(_removeUserByLogin->session);
  }
  if (_removeUserByLogin->login != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor login of type task_manager_client_ns0_removeUserByLogin...\n");
#endif
    freeXsStringType(_removeUserByLogin->login);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor login of type task_manager_client_ns0_removeUserByLogin...\n");
#endif
    free(_removeUserByLogin->login);
  }
}
#endif /* DEF_task_manager_client_ns0_removeUserByLogin_M */
#ifndef DEF_task_manager_client_ns0_removeUserByLoginResponse_M
#define DEF_task_manager_client_ns0_removeUserByLoginResponse_M

/**
 * Reads a RemoveUserByLoginResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}removeUserByLoginResponse", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The RemoveUserByLoginResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_removeUserByLoginResponse *xml_read_task_manager_client_ns0_removeUserByLoginResponse(xmlTextReaderPtr reader) {
  int status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
  return xmlTextReaderReadNs0RemoveUserByLoginResponseElement(reader);
}

/**
 * Writes a RemoveUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByLoginResponse".
 *
 * @param writer The XML writer.
 * @param _removeUserByLoginResponse The RemoveUserByLoginResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
int xml_write_task_manager_client_ns0_removeUserByLoginResponse(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByLoginResponse *_removeUserByLoginResponse) {
  return xmlTextWriterWriteNs0RemoveUserByLoginResponseElementNS(writer, _removeUserByLoginResponse, 1);
}

/**
 * Frees a RemoveUserByLoginResponse.
 *
 * @param _removeUserByLoginResponse The RemoveUserByLoginResponse to free.
 */
void free_task_manager_client_ns0_removeUserByLoginResponse(struct task_manager_client_ns0_removeUserByLoginResponse *_removeUserByLoginResponse) {
  freeNs0RemoveUserByLoginResponseType(_removeUserByLoginResponse);
  free(_removeUserByLoginResponse);
}

/**
 * Reads a RemoveUserByLoginResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}removeUserByLoginResponse", and
 * it is assumed that the reader is pointing to that element.
 *
 * @param reader The XML reader.
 * @return The RemoveUserByLoginResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_removeUserByLoginResponse *xmlTextReaderReadNs0RemoveUserByLoginResponseElement(xmlTextReaderPtr reader) {
  struct task_manager_client_ns0_removeUserByLoginResponse *_removeUserByLoginResponse = NULL;

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "removeUserByLoginResponse", xmlTextReaderConstLocalName(reader)) == 0
    && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    printf("Attempting to read root element {http://endpoint.tm.amster.ru/}removeUserByLoginResponse.\n");
#endif
    _removeUserByLoginResponse = xmlTextReaderReadNs0RemoveUserByLoginResponseType(reader);
  }
#if DEBUG_ENUNCIATE
  if (_removeUserByLoginResponse == NULL) {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      printf("attempt to read {http://endpoint.tm.amster.ru/}removeUserByLoginResponse failed. current element: {}%s\n",  xmlTextReaderConstLocalName(reader));
    }
    else {
      printf("attempt to read {http://endpoint.tm.amster.ru/}removeUserByLoginResponse failed. current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
    }
  }
#endif

  return _removeUserByLoginResponse;
}

/**
 * Writes a RemoveUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByLoginResponse".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _removeUserByLoginResponse The RemoveUserByLoginResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0RemoveUserByLoginResponseElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByLoginResponse *_removeUserByLoginResponse) {
  return xmlTextWriterWriteNs0RemoveUserByLoginResponseElementNS(writer, _removeUserByLoginResponse, 0);
}

/**
 * Writes a RemoveUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByLoginResponse".
 *
 * @param writer The XML writer.
 * @param _removeUserByLoginResponse The RemoveUserByLoginResponse to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0RemoveUserByLoginResponseElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByLoginResponse *_removeUserByLoginResponse, int writeNamespaces) {
  int totalBytes = 0;
  int status;

  status = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "removeUserByLoginResponse", NULL);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write start element {http://endpoint.tm.amster.ru/}removeUserByLoginResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  if (writeNamespaces) {
#if DEBUG_ENUNCIATE > 1
    printf("writing namespaces for start element {http://endpoint.tm.amster.ru/}removeUserByLoginResponse...\n");
#endif

    status = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("unable to write namespace attribute xmlns:ns0. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

#if DEBUG_ENUNCIATE > 1
  printf("writing type {http://endpoint.tm.amster.ru/}removeUserByLoginResponse for root element {http://endpoint.tm.amster.ru/}removeUserByLoginResponse...\n");
#endif
  status = xmlTextWriterWriteNs0RemoveUserByLoginResponseType(writer, _removeUserByLoginResponse);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write type for start element {http://endpoint.tm.amster.ru/}removeUserByLoginResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  status = xmlTextWriterEndElement(writer);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to end element {http://endpoint.tm.amster.ru/}removeUserByLoginResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  return totalBytes;
}

/**
 * Frees the children of a RemoveUserByLoginResponse.
 *
 * @param _removeUserByLoginResponse The RemoveUserByLoginResponse whose children are to be free.
 */
static void freeNs0RemoveUserByLoginResponseElement(struct task_manager_client_ns0_removeUserByLoginResponse *_removeUserByLoginResponse) {
  freeNs0RemoveUserByLoginResponseType(_removeUserByLoginResponse);
}

/**
 * Reads a RemoveUserByLoginResponse from XML. The reader is assumed to be at the start element.
 *
 * @return the RemoveUserByLoginResponse, or NULL in case of error.
 */
static struct task_manager_client_ns0_removeUserByLoginResponse *xmlTextReaderReadNs0RemoveUserByLoginResponseType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_removeUserByLoginResponse *_removeUserByLoginResponse = calloc(1, sizeof(struct task_manager_client_ns0_removeUserByLoginResponse));




  return _removeUserByLoginResponse;
}

/**
 * Writes a RemoveUserByLoginResponse to XML.
 *
 * @param writer The XML writer.
 * @param _removeUserByLoginResponse The RemoveUserByLoginResponse to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0RemoveUserByLoginResponseType(xmlTextWriterPtr writer, struct task_manager_client_ns0_removeUserByLoginResponse *_removeUserByLoginResponse) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;

  return totalBytes;
}

/**
 * Frees the elements of a RemoveUserByLoginResponse.
 *
 * @param _removeUserByLoginResponse The RemoveUserByLoginResponse to free.
 */
static void freeNs0RemoveUserByLoginResponseType(struct task_manager_client_ns0_removeUserByLoginResponse *_removeUserByLoginResponse) {
  int i;
}
#endif /* DEF_task_manager_client_ns0_removeUserByLoginResponse_M */
#ifndef DEF_task_manager_client_ns0_role_M
#define DEF_task_manager_client_ns0_role_M

/**
 * Reads a Role from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Role, or NULL if unable to be read.
 */
static enum task_manager_client_ns0_role *xmlTextReaderReadNs0RoleType(xmlTextReaderPtr reader) {
  xmlChar *enumValue = xmlTextReaderReadEntireNodeValue(reader);
  enum task_manager_client_ns0_role *value = calloc(1, sizeof(enum task_manager_client_ns0_role));
  if (enumValue != NULL) {
    if (xmlStrcmp(enumValue, BAD_CAST "USER") == 0) {
      *value = TASK_MANAGER_CLIENT_NS0_ROLE_USER;
      free(enumValue);
      return value;
    }
    if (xmlStrcmp(enumValue, BAD_CAST "ADMIN") == 0) {
      *value = TASK_MANAGER_CLIENT_NS0_ROLE_ADMIN;
      free(enumValue);
      return value;
    }
#if DEBUG_ENUNCIATE
    printf("Attempt to read enum value failed: %s doesn't match an enum value.\n", enumValue);
#endif
  }
#if DEBUG_ENUNCIATE
  else {
    printf("Attempt to read enum value failed: NULL value.\n");
  }
#endif

  return NULL;
}

/**
 * Writes a Role to XML.
 *
 * @param writer The XML writer.
 * @param _role The Role to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs0RoleType(xmlTextWriterPtr writer, enum task_manager_client_ns0_role *_role) {
  switch (*_role) {
    case TASK_MANAGER_CLIENT_NS0_ROLE_USER:
      return xmlTextWriterWriteString(writer, BAD_CAST "USER");
    case TASK_MANAGER_CLIENT_NS0_ROLE_ADMIN:
      return xmlTextWriterWriteString(writer, BAD_CAST "ADMIN");
  }

#if DEBUG_ENUNCIATE
  printf("Unable to write enum value (no valid value found).\n");
#endif
  return -1;
}

/**
 * Frees a Role.
 *
 * @param _role The Role to free.
 */
static void freeNs0RoleType(enum task_manager_client_ns0_role *_role) {
  //no-op
}
#endif /* DEF_task_manager_client_ns0_role_M */
#ifndef DEF_task_manager_client_ns0_session_M
#define DEF_task_manager_client_ns0_session_M

/**
 * Reads a Session from XML. The reader is assumed to be at the start element.
 *
 * @return the Session, or NULL in case of error.
 */
static struct task_manager_client_ns0_session *xmlTextReaderReadNs0SessionType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_session *_session = calloc(1, sizeof(struct task_manager_client_ns0_session));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs0SessionType(_session);
        free(_session);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "id", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0SessionType(_session);
          free(_session);
          return NULL;
        }

        _session->id = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "signature", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}signature of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}signature of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0SessionType(_session);
          free(_session);
          return NULL;
        }

        _session->signature = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "timestamp", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}timestamp of type {http://www.w3.org/2001/XMLSchema}long.\n");
#endif
        _child_accessor = xmlTextReaderReadXsLongType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}timestamp of type {http://www.w3.org/2001/XMLSchema}long.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0SessionType(_session);
          free(_session);
          return NULL;
        }

        _session->timestamp = ((long long*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "userId", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0SessionType(_session);
          free(_session);
          return NULL;
        }

        _session->userId = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {http://endpoint.tm.amster.ru/}session.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {http://endpoint.tm.amster.ru/}session. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _session;
}

/**
 * Writes a Session to XML.
 *
 * @param writer The XML writer.
 * @param _session The Session to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0SessionType(xmlTextWriterPtr writer, struct task_manager_client_ns0_session *_session) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_session->id != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "id", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}id...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_session->id));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_session->signature != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "signature", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}signature. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}signature...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_session->signature));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}signature. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}signature. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_session->timestamp != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "timestamp", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}timestamp. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}long for element {}timestamp...\n");
#endif
    status = xmlTextWriterWriteXsLongType(writer, (_session->timestamp));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}long for element {}timestamp. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}timestamp. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_session->userId != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "userId", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}userId. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}userId...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_session->userId));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}userId. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}userId. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a Session.
 *
 * @param _session The Session to free.
 */
static void freeNs0SessionType(struct task_manager_client_ns0_session *_session) {
  int i;
  if (_session->id != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor id of type task_manager_client_ns0_session...\n");
#endif
    freeXsStringType(_session->id);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor id of type task_manager_client_ns0_session...\n");
#endif
    free(_session->id);
  }
  if (_session->signature != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor signature of type task_manager_client_ns0_session...\n");
#endif
    freeXsStringType(_session->signature);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor signature of type task_manager_client_ns0_session...\n");
#endif
    free(_session->signature);
  }
  if (_session->timestamp != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor timestamp of type task_manager_client_ns0_session...\n");
#endif
    freeXsLongType(_session->timestamp);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor timestamp of type task_manager_client_ns0_session...\n");
#endif
    free(_session->timestamp);
  }
  if (_session->userId != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor userId of type task_manager_client_ns0_session...\n");
#endif
    freeXsStringType(_session->userId);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor userId of type task_manager_client_ns0_session...\n");
#endif
    free(_session->userId);
  }
}
#endif /* DEF_task_manager_client_ns0_session_M */
#ifndef DEF_task_manager_client_ns0_task_M
#define DEF_task_manager_client_ns0_task_M

/**
 * Reads a Task from XML. The reader is assumed to be at the start element.
 *
 * @return the Task, or NULL in case of error.
 */
static struct task_manager_client_ns0_task *xmlTextReaderReadNs0TaskType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_task *_task = calloc(1, sizeof(struct task_manager_client_ns0_task));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs0TaskType(_task);
        free(_task);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "id", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0TaskType(_task);
          free(_task);
          return NULL;
        }

        _task->id = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "description", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}description of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}description of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0TaskType(_task);
          free(_task);
          return NULL;
        }

        _task->description = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "name", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}name of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}name of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0TaskType(_task);
          free(_task);
          return NULL;
        }

        _task->name = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "userId", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0TaskType(_task);
          free(_task);
          return NULL;
        }

        _task->userId = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {http://endpoint.tm.amster.ru/}task.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {http://endpoint.tm.amster.ru/}task. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _task;
}

/**
 * Writes a Task to XML.
 *
 * @param writer The XML writer.
 * @param _task The Task to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0TaskType(xmlTextWriterPtr writer, struct task_manager_client_ns0_task *_task) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_task->id != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "id", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}id...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_task->id));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_task->description != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "description", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}description. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}description...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_task->description));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}description. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}description. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_task->name != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "name", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}name. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}name...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_task->name));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}name. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}name. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_task->userId != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "userId", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}userId. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}userId...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_task->userId));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}userId. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}userId. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a Task.
 *
 * @param _task The Task to free.
 */
static void freeNs0TaskType(struct task_manager_client_ns0_task *_task) {
  int i;
  if (_task->id != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor id of type task_manager_client_ns0_task...\n");
#endif
    freeXsStringType(_task->id);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor id of type task_manager_client_ns0_task...\n");
#endif
    free(_task->id);
  }
  if (_task->description != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor description of type task_manager_client_ns0_task...\n");
#endif
    freeXsStringType(_task->description);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor description of type task_manager_client_ns0_task...\n");
#endif
    free(_task->description);
  }
  if (_task->name != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor name of type task_manager_client_ns0_task...\n");
#endif
    freeXsStringType(_task->name);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor name of type task_manager_client_ns0_task...\n");
#endif
    free(_task->name);
  }
  if (_task->userId != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor userId of type task_manager_client_ns0_task...\n");
#endif
    freeXsStringType(_task->userId);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor userId of type task_manager_client_ns0_task...\n");
#endif
    free(_task->userId);
  }
}
#endif /* DEF_task_manager_client_ns0_task_M */
#ifndef DEF_task_manager_client_ns0_unlockUserByLogin_M
#define DEF_task_manager_client_ns0_unlockUserByLogin_M

/**
 * Reads a UnlockUserByLogin element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}unlockUserByLogin", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The UnlockUserByLogin, or NULL in case of error.
 */
struct task_manager_client_ns0_unlockUserByLogin *xml_read_task_manager_client_ns0_unlockUserByLogin(xmlTextReaderPtr reader) {
  int status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
  return xmlTextReaderReadNs0UnlockUserByLoginElement(reader);
}

/**
 * Writes a UnlockUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}unlockUserByLogin".
 *
 * @param writer The XML writer.
 * @param _unlockUserByLogin The UnlockUserByLogin to write.
 * @return 1 if successful, 0 otherwise.
 */
int xml_write_task_manager_client_ns0_unlockUserByLogin(xmlTextWriterPtr writer, struct task_manager_client_ns0_unlockUserByLogin *_unlockUserByLogin) {
  return xmlTextWriterWriteNs0UnlockUserByLoginElementNS(writer, _unlockUserByLogin, 1);
}

/**
 * Frees a UnlockUserByLogin.
 *
 * @param _unlockUserByLogin The UnlockUserByLogin to free.
 */
void free_task_manager_client_ns0_unlockUserByLogin(struct task_manager_client_ns0_unlockUserByLogin *_unlockUserByLogin) {
  freeNs0UnlockUserByLoginType(_unlockUserByLogin);
  free(_unlockUserByLogin);
}

/**
 * Reads a UnlockUserByLogin element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}unlockUserByLogin", and
 * it is assumed that the reader is pointing to that element.
 *
 * @param reader The XML reader.
 * @return The UnlockUserByLogin, or NULL in case of error.
 */
struct task_manager_client_ns0_unlockUserByLogin *xmlTextReaderReadNs0UnlockUserByLoginElement(xmlTextReaderPtr reader) {
  struct task_manager_client_ns0_unlockUserByLogin *_unlockUserByLogin = NULL;

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "unlockUserByLogin", xmlTextReaderConstLocalName(reader)) == 0
    && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    printf("Attempting to read root element {http://endpoint.tm.amster.ru/}unlockUserByLogin.\n");
#endif
    _unlockUserByLogin = xmlTextReaderReadNs0UnlockUserByLoginType(reader);
  }
#if DEBUG_ENUNCIATE
  if (_unlockUserByLogin == NULL) {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      printf("attempt to read {http://endpoint.tm.amster.ru/}unlockUserByLogin failed. current element: {}%s\n",  xmlTextReaderConstLocalName(reader));
    }
    else {
      printf("attempt to read {http://endpoint.tm.amster.ru/}unlockUserByLogin failed. current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
    }
  }
#endif

  return _unlockUserByLogin;
}

/**
 * Writes a UnlockUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}unlockUserByLogin".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _unlockUserByLogin The UnlockUserByLogin to write.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0UnlockUserByLoginElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_unlockUserByLogin *_unlockUserByLogin) {
  return xmlTextWriterWriteNs0UnlockUserByLoginElementNS(writer, _unlockUserByLogin, 0);
}

/**
 * Writes a UnlockUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}unlockUserByLogin".
 *
 * @param writer The XML writer.
 * @param _unlockUserByLogin The UnlockUserByLogin to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0UnlockUserByLoginElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_unlockUserByLogin *_unlockUserByLogin, int writeNamespaces) {
  int totalBytes = 0;
  int status;

  status = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "unlockUserByLogin", NULL);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write start element {http://endpoint.tm.amster.ru/}unlockUserByLogin. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  if (writeNamespaces) {
#if DEBUG_ENUNCIATE > 1
    printf("writing namespaces for start element {http://endpoint.tm.amster.ru/}unlockUserByLogin...\n");
#endif

    status = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("unable to write namespace attribute xmlns:ns0. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

#if DEBUG_ENUNCIATE > 1
  printf("writing type {http://endpoint.tm.amster.ru/}unlockUserByLogin for root element {http://endpoint.tm.amster.ru/}unlockUserByLogin...\n");
#endif
  status = xmlTextWriterWriteNs0UnlockUserByLoginType(writer, _unlockUserByLogin);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write type for start element {http://endpoint.tm.amster.ru/}unlockUserByLogin. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  status = xmlTextWriterEndElement(writer);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to end element {http://endpoint.tm.amster.ru/}unlockUserByLogin. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  return totalBytes;
}

/**
 * Frees the children of a UnlockUserByLogin.
 *
 * @param _unlockUserByLogin The UnlockUserByLogin whose children are to be free.
 */
static void freeNs0UnlockUserByLoginElement(struct task_manager_client_ns0_unlockUserByLogin *_unlockUserByLogin) {
  freeNs0UnlockUserByLoginType(_unlockUserByLogin);
}

/**
 * Reads a UnlockUserByLogin from XML. The reader is assumed to be at the start element.
 *
 * @return the UnlockUserByLogin, or NULL in case of error.
 */
static struct task_manager_client_ns0_unlockUserByLogin *xmlTextReaderReadNs0UnlockUserByLoginType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_unlockUserByLogin *_unlockUserByLogin = calloc(1, sizeof(struct task_manager_client_ns0_unlockUserByLogin));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs0UnlockUserByLoginType(_unlockUserByLogin);
        free(_unlockUserByLogin);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "session", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}session of type {http://endpoint.tm.amster.ru/}session.\n");
#endif
        _child_accessor = xmlTextReaderReadNs0SessionType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}session of type {http://endpoint.tm.amster.ru/}session.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0UnlockUserByLoginType(_unlockUserByLogin);
          free(_unlockUserByLogin);
          return NULL;
        }

        _unlockUserByLogin->session = ((struct task_manager_client_ns0_session*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "login", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0UnlockUserByLoginType(_unlockUserByLogin);
          free(_unlockUserByLogin);
          return NULL;
        }

        _unlockUserByLogin->login = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {http://endpoint.tm.amster.ru/}unlockUserByLogin.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {http://endpoint.tm.amster.ru/}unlockUserByLogin. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _unlockUserByLogin;
}

/**
 * Writes a UnlockUserByLogin to XML.
 *
 * @param writer The XML writer.
 * @param _unlockUserByLogin The UnlockUserByLogin to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0UnlockUserByLoginType(xmlTextWriterPtr writer, struct task_manager_client_ns0_unlockUserByLogin *_unlockUserByLogin) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_unlockUserByLogin->session != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "session", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://endpoint.tm.amster.ru/}session for element {}session...\n");
#endif
    status = xmlTextWriterWriteNs0SessionType(writer, (_unlockUserByLogin->session));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://endpoint.tm.amster.ru/}session for element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}session. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_unlockUserByLogin->login != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "login", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}login. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}login...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_unlockUserByLogin->login));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}login. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}login. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a UnlockUserByLogin.
 *
 * @param _unlockUserByLogin The UnlockUserByLogin to free.
 */
static void freeNs0UnlockUserByLoginType(struct task_manager_client_ns0_unlockUserByLogin *_unlockUserByLogin) {
  int i;
  if (_unlockUserByLogin->session != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor session of type task_manager_client_ns0_unlockUserByLogin...\n");
#endif
    freeNs0SessionType(_unlockUserByLogin->session);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor session of type task_manager_client_ns0_unlockUserByLogin...\n");
#endif
    free(_unlockUserByLogin->session);
  }
  if (_unlockUserByLogin->login != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor login of type task_manager_client_ns0_unlockUserByLogin...\n");
#endif
    freeXsStringType(_unlockUserByLogin->login);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor login of type task_manager_client_ns0_unlockUserByLogin...\n");
#endif
    free(_unlockUserByLogin->login);
  }
}
#endif /* DEF_task_manager_client_ns0_unlockUserByLogin_M */
#ifndef DEF_task_manager_client_ns0_unlockUserByLoginResponse_M
#define DEF_task_manager_client_ns0_unlockUserByLoginResponse_M

/**
 * Reads a UnlockUserByLoginResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}unlockUserByLoginResponse", and
 * it is assumed that the reader is pointing to the XML document (not the element).
 *
 * @param reader The XML reader.
 * @return The UnlockUserByLoginResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_unlockUserByLoginResponse *xml_read_task_manager_client_ns0_unlockUserByLoginResponse(xmlTextReaderPtr reader) {
  int status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
  return xmlTextReaderReadNs0UnlockUserByLoginResponseElement(reader);
}

/**
 * Writes a UnlockUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}unlockUserByLoginResponse".
 *
 * @param writer The XML writer.
 * @param _unlockUserByLoginResponse The UnlockUserByLoginResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
int xml_write_task_manager_client_ns0_unlockUserByLoginResponse(xmlTextWriterPtr writer, struct task_manager_client_ns0_unlockUserByLoginResponse *_unlockUserByLoginResponse) {
  return xmlTextWriterWriteNs0UnlockUserByLoginResponseElementNS(writer, _unlockUserByLoginResponse, 1);
}

/**
 * Frees a UnlockUserByLoginResponse.
 *
 * @param _unlockUserByLoginResponse The UnlockUserByLoginResponse to free.
 */
void free_task_manager_client_ns0_unlockUserByLoginResponse(struct task_manager_client_ns0_unlockUserByLoginResponse *_unlockUserByLoginResponse) {
  freeNs0UnlockUserByLoginResponseType(_unlockUserByLoginResponse);
  free(_unlockUserByLoginResponse);
}

/**
 * Reads a UnlockUserByLoginResponse element from XML. The element to be read is "{http://endpoint.tm.amster.ru/}unlockUserByLoginResponse", and
 * it is assumed that the reader is pointing to that element.
 *
 * @param reader The XML reader.
 * @return The UnlockUserByLoginResponse, or NULL in case of error.
 */
struct task_manager_client_ns0_unlockUserByLoginResponse *xmlTextReaderReadNs0UnlockUserByLoginResponseElement(xmlTextReaderPtr reader) {
  struct task_manager_client_ns0_unlockUserByLoginResponse *_unlockUserByLoginResponse = NULL;

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "unlockUserByLoginResponse", xmlTextReaderConstLocalName(reader)) == 0
    && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    printf("Attempting to read root element {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse.\n");
#endif
    _unlockUserByLoginResponse = xmlTextReaderReadNs0UnlockUserByLoginResponseType(reader);
  }
#if DEBUG_ENUNCIATE
  if (_unlockUserByLoginResponse == NULL) {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      printf("attempt to read {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse failed. current element: {}%s\n",  xmlTextReaderConstLocalName(reader));
    }
    else {
      printf("attempt to read {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse failed. current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
    }
  }
#endif

  return _unlockUserByLoginResponse;
}

/**
 * Writes a UnlockUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}unlockUserByLoginResponse".
 * Does NOT write the namespace prefixes.
 *
 * @param writer The XML writer.
 * @param _unlockUserByLoginResponse The UnlockUserByLoginResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0UnlockUserByLoginResponseElement(xmlTextWriterPtr writer, struct task_manager_client_ns0_unlockUserByLoginResponse *_unlockUserByLoginResponse) {
  return xmlTextWriterWriteNs0UnlockUserByLoginResponseElementNS(writer, _unlockUserByLoginResponse, 0);
}

/**
 * Writes a UnlockUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}unlockUserByLoginResponse".
 *
 * @param writer The XML writer.
 * @param _unlockUserByLoginResponse The UnlockUserByLoginResponse to write.
 * @param writeNamespaces Whether to write the namespace prefixes.
 * @return 1 if successful, 0 otherwise.
 */
static int xmlTextWriterWriteNs0UnlockUserByLoginResponseElementNS(xmlTextWriterPtr writer, struct task_manager_client_ns0_unlockUserByLoginResponse *_unlockUserByLoginResponse, int writeNamespaces) {
  int totalBytes = 0;
  int status;

  status = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "unlockUserByLoginResponse", NULL);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write start element {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  if (writeNamespaces) {
#if DEBUG_ENUNCIATE > 1
    printf("writing namespaces for start element {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse...\n");
#endif

    status = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("unable to write namespace attribute xmlns:ns0. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

#if DEBUG_ENUNCIATE > 1
  printf("writing type {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse for root element {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse...\n");
#endif
  status = xmlTextWriterWriteNs0UnlockUserByLoginResponseType(writer, _unlockUserByLoginResponse);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to write type for start element {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  status = xmlTextWriterEndElement(writer);
  if (status < 0) {
#if DEBUG_ENUNCIATE
    printf("unable to end element {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse. status: %i\n", status);
#endif
    return status;
  }
  totalBytes += status;

  return totalBytes;
}

/**
 * Frees the children of a UnlockUserByLoginResponse.
 *
 * @param _unlockUserByLoginResponse The UnlockUserByLoginResponse whose children are to be free.
 */
static void freeNs0UnlockUserByLoginResponseElement(struct task_manager_client_ns0_unlockUserByLoginResponse *_unlockUserByLoginResponse) {
  freeNs0UnlockUserByLoginResponseType(_unlockUserByLoginResponse);
}

/**
 * Reads a UnlockUserByLoginResponse from XML. The reader is assumed to be at the start element.
 *
 * @return the UnlockUserByLoginResponse, or NULL in case of error.
 */
static struct task_manager_client_ns0_unlockUserByLoginResponse *xmlTextReaderReadNs0UnlockUserByLoginResponseType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_unlockUserByLoginResponse *_unlockUserByLoginResponse = calloc(1, sizeof(struct task_manager_client_ns0_unlockUserByLoginResponse));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs0UnlockUserByLoginResponseType(_unlockUserByLoginResponse);
        free(_unlockUserByLoginResponse);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "return", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}return of type {http://endpoint.tm.amster.ru/}user.\n");
#endif
        _child_accessor = xmlTextReaderReadNs0UserType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}return of type {http://endpoint.tm.amster.ru/}user.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0UnlockUserByLoginResponseType(_unlockUserByLoginResponse);
          free(_unlockUserByLoginResponse);
          return NULL;
        }

        _unlockUserByLoginResponse->_return = ((struct task_manager_client_ns0_user*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _unlockUserByLoginResponse;
}

/**
 * Writes a UnlockUserByLoginResponse to XML.
 *
 * @param writer The XML writer.
 * @param _unlockUserByLoginResponse The UnlockUserByLoginResponse to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0UnlockUserByLoginResponseType(xmlTextWriterPtr writer, struct task_manager_client_ns0_unlockUserByLoginResponse *_unlockUserByLoginResponse) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_unlockUserByLoginResponse->_return != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "return", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}return. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://endpoint.tm.amster.ru/}user for element {}return...\n");
#endif
    status = xmlTextWriterWriteNs0UserType(writer, (_unlockUserByLoginResponse->_return));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://endpoint.tm.amster.ru/}user for element {}return. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}return. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a UnlockUserByLoginResponse.
 *
 * @param _unlockUserByLoginResponse The UnlockUserByLoginResponse to free.
 */
static void freeNs0UnlockUserByLoginResponseType(struct task_manager_client_ns0_unlockUserByLoginResponse *_unlockUserByLoginResponse) {
  int i;
  if (_unlockUserByLoginResponse->_return != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor _return of type task_manager_client_ns0_unlockUserByLoginResponse...\n");
#endif
    freeNs0UserType(_unlockUserByLoginResponse->_return);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor _return of type task_manager_client_ns0_unlockUserByLoginResponse...\n");
#endif
    free(_unlockUserByLoginResponse->_return);
  }
}
#endif /* DEF_task_manager_client_ns0_unlockUserByLoginResponse_M */
#ifndef DEF_task_manager_client_ns0_user_M
#define DEF_task_manager_client_ns0_user_M

/**
 * Reads a User from XML. The reader is assumed to be at the start element.
 *
 * @return the User, or NULL in case of error.
 */
static struct task_manager_client_ns0_user *xmlTextReaderReadNs0UserType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_client_ns0_user *_user = calloc(1, sizeof(struct task_manager_client_ns0_user));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs0UserType(_user);
        free(_user);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "id", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0UserType(_user);
          free(_user);
          return NULL;
        }

        _user->id = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "email", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}email of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}email of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0UserType(_user);
          free(_user);
          return NULL;
        }

        _user->email = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "fistName", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}fistName of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}fistName of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0UserType(_user);
          free(_user);
          return NULL;
        }

        _user->fistName = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "lastName", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}lastName of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}lastName of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0UserType(_user);
          free(_user);
          return NULL;
        }

        _user->lastName = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "locked", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}locked of type {http://www.w3.org/2001/XMLSchema}boolean.\n");
#endif
        _child_accessor = xmlTextReaderReadXsBooleanType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}locked of type {http://www.w3.org/2001/XMLSchema}boolean.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0UserType(_user);
          free(_user);
          return NULL;
        }

        _user->locked = ((int*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "login", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0UserType(_user);
          free(_user);
          return NULL;
        }

        _user->login = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "middleName", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}middleName of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}middleName of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0UserType(_user);
          free(_user);
          return NULL;
        }

        _user->middleName = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "passwordHash", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}passwordHash of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}passwordHash of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0UserType(_user);
          free(_user);
          return NULL;
        }

        _user->passwordHash = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "role", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}role of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}role of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs0UserType(_user);
          free(_user);
          return NULL;
        }

        _user->role = ((enum task_manager_client_ns0_role*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {http://endpoint.tm.amster.ru/}user.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {http://endpoint.tm.amster.ru/}user. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _user;
}

/**
 * Writes a User to XML.
 *
 * @param writer The XML writer.
 * @param _user The User to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs0UserType(xmlTextWriterPtr writer, struct task_manager_client_ns0_user *_user) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_user->id != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "id", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}id...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_user->id));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_user->email != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "email", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}email. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}email...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_user->email));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}email. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}email. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_user->fistName != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "fistName", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}fistName. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}fistName...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_user->fistName));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}fistName. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}fistName. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_user->lastName != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "lastName", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}lastName. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}lastName...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_user->lastName));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}lastName. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}lastName. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_user->locked != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "locked", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}locked. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}boolean for element {}locked...\n");
#endif
    status = xmlTextWriterWriteXsBooleanType(writer, (_user->locked));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}boolean for element {}locked. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}locked. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_user->login != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "login", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}login. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}login...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_user->login));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}login. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}login. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_user->middleName != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "middleName", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}middleName. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}middleName...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_user->middleName));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}middleName. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}middleName. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_user->passwordHash != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "passwordHash", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}passwordHash. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}passwordHash...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_user->passwordHash));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}passwordHash. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}passwordHash. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_user->role != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "role", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}role. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}role...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_user->role));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}role. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}role. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a User.
 *
 * @param _user The User to free.
 */
static void freeNs0UserType(struct task_manager_client_ns0_user *_user) {
  int i;
  if (_user->id != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor id of type task_manager_client_ns0_user...\n");
#endif
    freeXsStringType(_user->id);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor id of type task_manager_client_ns0_user...\n");
#endif
    free(_user->id);
  }
  if (_user->email != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor email of type task_manager_client_ns0_user...\n");
#endif
    freeXsStringType(_user->email);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor email of type task_manager_client_ns0_user...\n");
#endif
    free(_user->email);
  }
  if (_user->fistName != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor fistName of type task_manager_client_ns0_user...\n");
#endif
    freeXsStringType(_user->fistName);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor fistName of type task_manager_client_ns0_user...\n");
#endif
    free(_user->fistName);
  }
  if (_user->lastName != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor lastName of type task_manager_client_ns0_user...\n");
#endif
    freeXsStringType(_user->lastName);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor lastName of type task_manager_client_ns0_user...\n");
#endif
    free(_user->lastName);
  }
  if (_user->locked != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor locked of type task_manager_client_ns0_user...\n");
#endif
    freeXsBooleanType(_user->locked);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor locked of type task_manager_client_ns0_user...\n");
#endif
    free(_user->locked);
  }
  if (_user->login != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor login of type task_manager_client_ns0_user...\n");
#endif
    freeXsStringType(_user->login);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor login of type task_manager_client_ns0_user...\n");
#endif
    free(_user->login);
  }
  if (_user->middleName != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor middleName of type task_manager_client_ns0_user...\n");
#endif
    freeXsStringType(_user->middleName);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor middleName of type task_manager_client_ns0_user...\n");
#endif
    free(_user->middleName);
  }
  if (_user->passwordHash != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor passwordHash of type task_manager_client_ns0_user...\n");
#endif
    freeXsStringType(_user->passwordHash);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor passwordHash of type task_manager_client_ns0_user...\n");
#endif
    free(_user->passwordHash);
  }
  if (_user->role != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor role of type task_manager_client_ns0_user...\n");
#endif
    freeXsStringType(_user->role);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor role of type task_manager_client_ns0_user...\n");
#endif
    free(_user->role);
  }
}
#endif /* DEF_task_manager_client_ns0_user_M */
