#import "task-manager-client.h"
#ifndef DEF_TASK_MANAGER_CLIENTNS0Export_M
#define DEF_TASK_MANAGER_CLIENTNS0Export_M

/**
 *  <p>Java class for export complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="export"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0Export

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Session *) session
{
  return _session;
}

/**
 * (no documentation provided)
 */
- (void) setSession: (TASK_MANAGER_CLIENTNS0Session *) newSession
{
  [newSession retain];
  [_session release];
  _session = newSession;
}

- (void) dealloc
{
  [self setSession: nil];
  [super dealloc];
}

//documentation inherited.
+ (id<EnunciateXML>) readFromXML: (NSData *) xml
{
  TASK_MANAGER_CLIENTNS0Export *_tASK_MANAGER_CLIENTNS0Export;
  xmlTextReaderPtr reader = xmlReaderForMemory([xml bytes], [xml length], NULL, NULL, 0);
  if (reader == NULL) {
    [NSException raise: @"XMLReadError"
                 format: @"Error instantiating an XML reader."];
    return nil;
  }

  _tASK_MANAGER_CLIENTNS0Export = (TASK_MANAGER_CLIENTNS0Export *) [TASK_MANAGER_CLIENTNS0Export readXMLElement: reader];
  xmlFreeTextReader(reader); //free the reader
  return _tASK_MANAGER_CLIENTNS0Export;
}

//documentation inherited.
- (NSData *) writeToXML
{
  xmlBufferPtr buf;
  xmlTextWriterPtr writer;
  int rc;
  NSData *data;

  buf = xmlBufferCreate();
  if (buf == NULL) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML buffer."];
    return nil;
  }

  writer = xmlNewTextWriterMemory(buf, 0);
  if (writer == NULL) {
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML writer."];
    return nil;
  }

  rc = xmlTextWriterStartDocument(writer, NULL, "utf-8", NULL);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML start document."];
    return nil;
  }

  NS_DURING
  {
    [self writeXMLElement: writer];
  }
  NS_HANDLER
  {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [localException raise];
  }
  NS_ENDHANDLER

  rc = xmlTextWriterEndDocument(writer);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML end document."];
    return nil;
  }

  xmlFreeTextWriter(writer);
  data = [NSData dataWithBytes: buf->content length: buf->use];
  xmlBufferFree(buf);
  return data;
}
@end /* implementation TASK_MANAGER_CLIENTNS0Export */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0Export (JAXB) <JAXBReading, JAXBWriting, JAXBType, JAXBElement>

@end /*interface TASK_MANAGER_CLIENTNS0Export (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0Export (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0Export from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0Export defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0Export *_tASK_MANAGER_CLIENTNS0Export = [[TASK_MANAGER_CLIENTNS0Export alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0Export initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0Export = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0Export autorelease];
  return _tASK_MANAGER_CLIENTNS0Export;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0Export according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0Export to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

/**
 * Reads a TASK_MANAGER_CLIENTNS0Export from an XML reader. The element to be read is
 * "{http://endpoint.tm.amster.ru/}export".
 *
 * @param reader The XML reader.
 * @return The TASK_MANAGER_CLIENTNS0Export.
 */
+ (id<JAXBElement>) readXMLElement: (xmlTextReaderPtr) reader {
  int status;
  TASK_MANAGER_CLIENTNS0Export *_export = nil;

  if (xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) {
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
    if (status < 1) {
      [NSException raise: @"XMLReadError"
                   format: @"Error advancing the reader to start element {http://endpoint.tm.amster.ru/}export."];
    }
  }

  if (xmlStrcmp(BAD_CAST "export", xmlTextReaderConstLocalName(reader)) == 0
      && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read root element {http://endpoint.tm.amster.ru/}export.");
#endif
    _export = (TASK_MANAGER_CLIENTNS0Export *)[TASK_MANAGER_CLIENTNS0Export readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Successfully read root element {http://endpoint.tm.amster.ru/}export.");
#endif
  }
  else {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0Export. Expected element {http://endpoint.tm.amster.ru/}export. Current element: {}%s", xmlTextReaderConstLocalName(reader)];
    }
    else {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0Export. Expected element {http://endpoint.tm.amster.ru/}export. Current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader)];
    }
  }

  return _export;
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0Export to XML under element name "{http://endpoint.tm.amster.ru/}export".
 * The namespace declarations for the element will be written.
 *
 * @param writer The XML writer.
 * @param _export The Export to write.
 * @return 1 if successful, 0 otherwise.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer
{
  [self writeXMLElement: writer writeNamespaces: YES];
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0Export to an XML writer.
 *
 * @param writer The writer.
 * @param writeNs Whether to write the namespaces for this element to the xml writer.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer writeNamespaces: (BOOL) writeNs
{
  int rc = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "export", NULL);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing start element {http://endpoint.tm.amster.ru/}export. XML writer status: %i\n", rc];
  }

  if (writeNs) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing namespaces for start element {http://endpoint.tm.amster.ru/}export...");
#endif

    rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (rc < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing attribute 'xmlns:ns0' on '{http://endpoint.tm.amster.ru/}export'. XML writer status: %i\n", rc];
    }
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote namespaces for start element {http://endpoint.tm.amster.ru/}export...");
#endif
  }

#if DEBUG_ENUNCIATE > 1
  NSLog(@"writing type {http://endpoint.tm.amster.ru/}export for root element {http://endpoint.tm.amster.ru/}export...");
#endif
  [self writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
  NSLog(@"successfully wrote type {http://endpoint.tm.amster.ru/}export for root element {http://endpoint.tm.amster.ru/}export...");
#endif
  rc = xmlTextWriterEndElement(writer);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing end element {http://endpoint.tm.amster.ru/}export. XML writer status: %i\n", rc];
  }
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "session", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}session of type {http://endpoint.tm.amster.ru/}session.");
#endif
    __child = [TASK_MANAGER_CLIENTNS0Session readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}session of type {http://endpoint.tm.amster.ru/}session.");
#endif

    [self setSession: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self session]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "session", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}session."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}session...");
#endif
    [[self session] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}session...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}session."];
    }
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0Export (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0Export_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0FindAllUser_M
#define DEF_TASK_MANAGER_CLIENTNS0FindAllUser_M

/**
 *  <p>Java class for findAllUser complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="findAllUser"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0FindAllUser

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Session *) session
{
  return _session;
}

/**
 * (no documentation provided)
 */
- (void) setSession: (TASK_MANAGER_CLIENTNS0Session *) newSession
{
  [newSession retain];
  [_session release];
  _session = newSession;
}

- (void) dealloc
{
  [self setSession: nil];
  [super dealloc];
}

//documentation inherited.
+ (id<EnunciateXML>) readFromXML: (NSData *) xml
{
  TASK_MANAGER_CLIENTNS0FindAllUser *_tASK_MANAGER_CLIENTNS0FindAllUser;
  xmlTextReaderPtr reader = xmlReaderForMemory([xml bytes], [xml length], NULL, NULL, 0);
  if (reader == NULL) {
    [NSException raise: @"XMLReadError"
                 format: @"Error instantiating an XML reader."];
    return nil;
  }

  _tASK_MANAGER_CLIENTNS0FindAllUser = (TASK_MANAGER_CLIENTNS0FindAllUser *) [TASK_MANAGER_CLIENTNS0FindAllUser readXMLElement: reader];
  xmlFreeTextReader(reader); //free the reader
  return _tASK_MANAGER_CLIENTNS0FindAllUser;
}

//documentation inherited.
- (NSData *) writeToXML
{
  xmlBufferPtr buf;
  xmlTextWriterPtr writer;
  int rc;
  NSData *data;

  buf = xmlBufferCreate();
  if (buf == NULL) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML buffer."];
    return nil;
  }

  writer = xmlNewTextWriterMemory(buf, 0);
  if (writer == NULL) {
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML writer."];
    return nil;
  }

  rc = xmlTextWriterStartDocument(writer, NULL, "utf-8", NULL);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML start document."];
    return nil;
  }

  NS_DURING
  {
    [self writeXMLElement: writer];
  }
  NS_HANDLER
  {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [localException raise];
  }
  NS_ENDHANDLER

  rc = xmlTextWriterEndDocument(writer);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML end document."];
    return nil;
  }

  xmlFreeTextWriter(writer);
  data = [NSData dataWithBytes: buf->content length: buf->use];
  xmlBufferFree(buf);
  return data;
}
@end /* implementation TASK_MANAGER_CLIENTNS0FindAllUser */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0FindAllUser (JAXB) <JAXBReading, JAXBWriting, JAXBType, JAXBElement>

@end /*interface TASK_MANAGER_CLIENTNS0FindAllUser (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0FindAllUser (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0FindAllUser from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0FindAllUser defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0FindAllUser *_tASK_MANAGER_CLIENTNS0FindAllUser = [[TASK_MANAGER_CLIENTNS0FindAllUser alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0FindAllUser initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0FindAllUser = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0FindAllUser autorelease];
  return _tASK_MANAGER_CLIENTNS0FindAllUser;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0FindAllUser according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0FindAllUser to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

/**
 * Reads a TASK_MANAGER_CLIENTNS0FindAllUser from an XML reader. The element to be read is
 * "{http://endpoint.tm.amster.ru/}findAllUser".
 *
 * @param reader The XML reader.
 * @return The TASK_MANAGER_CLIENTNS0FindAllUser.
 */
+ (id<JAXBElement>) readXMLElement: (xmlTextReaderPtr) reader {
  int status;
  TASK_MANAGER_CLIENTNS0FindAllUser *_findAllUser = nil;

  if (xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) {
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
    if (status < 1) {
      [NSException raise: @"XMLReadError"
                   format: @"Error advancing the reader to start element {http://endpoint.tm.amster.ru/}findAllUser."];
    }
  }

  if (xmlStrcmp(BAD_CAST "findAllUser", xmlTextReaderConstLocalName(reader)) == 0
      && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read root element {http://endpoint.tm.amster.ru/}findAllUser.");
#endif
    _findAllUser = (TASK_MANAGER_CLIENTNS0FindAllUser *)[TASK_MANAGER_CLIENTNS0FindAllUser readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Successfully read root element {http://endpoint.tm.amster.ru/}findAllUser.");
#endif
  }
  else {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0FindAllUser. Expected element {http://endpoint.tm.amster.ru/}findAllUser. Current element: {}%s", xmlTextReaderConstLocalName(reader)];
    }
    else {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0FindAllUser. Expected element {http://endpoint.tm.amster.ru/}findAllUser. Current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader)];
    }
  }

  return _findAllUser;
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0FindAllUser to XML under element name "{http://endpoint.tm.amster.ru/}findAllUser".
 * The namespace declarations for the element will be written.
 *
 * @param writer The XML writer.
 * @param _findAllUser The FindAllUser to write.
 * @return 1 if successful, 0 otherwise.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer
{
  [self writeXMLElement: writer writeNamespaces: YES];
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0FindAllUser to an XML writer.
 *
 * @param writer The writer.
 * @param writeNs Whether to write the namespaces for this element to the xml writer.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer writeNamespaces: (BOOL) writeNs
{
  int rc = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "findAllUser", NULL);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing start element {http://endpoint.tm.amster.ru/}findAllUser. XML writer status: %i\n", rc];
  }

  if (writeNs) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing namespaces for start element {http://endpoint.tm.amster.ru/}findAllUser...");
#endif

    rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (rc < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing attribute 'xmlns:ns0' on '{http://endpoint.tm.amster.ru/}findAllUser'. XML writer status: %i\n", rc];
    }
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote namespaces for start element {http://endpoint.tm.amster.ru/}findAllUser...");
#endif
  }

#if DEBUG_ENUNCIATE > 1
  NSLog(@"writing type {http://endpoint.tm.amster.ru/}findAllUser for root element {http://endpoint.tm.amster.ru/}findAllUser...");
#endif
  [self writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
  NSLog(@"successfully wrote type {http://endpoint.tm.amster.ru/}findAllUser for root element {http://endpoint.tm.amster.ru/}findAllUser...");
#endif
  rc = xmlTextWriterEndElement(writer);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing end element {http://endpoint.tm.amster.ru/}findAllUser. XML writer status: %i\n", rc];
  }
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "session", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}session of type {http://endpoint.tm.amster.ru/}session.");
#endif
    __child = [TASK_MANAGER_CLIENTNS0Session readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}session of type {http://endpoint.tm.amster.ru/}session.");
#endif

    [self setSession: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self session]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "session", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}session."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}session...");
#endif
    [[self session] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}session...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}session."];
    }
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0FindAllUser (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0FindAllUser_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0FindUserById_M
#define DEF_TASK_MANAGER_CLIENTNS0FindUserById_M

/**
 *  <p>Java class for findUserById complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="findUserById"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0FindUserById

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Session *) session
{
  return _session;
}

/**
 * (no documentation provided)
 */
- (void) setSession: (TASK_MANAGER_CLIENTNS0Session *) newSession
{
  [newSession retain];
  [_session release];
  _session = newSession;
}

/**
 * (no documentation provided)
 */
- (NSString *) identifier
{
  return _identifier;
}

/**
 * (no documentation provided)
 */
- (void) setIdentifier: (NSString *) newIdentifier
{
  [newIdentifier retain];
  [_identifier release];
  _identifier = newIdentifier;
}

- (void) dealloc
{
  [self setSession: nil];
  [self setIdentifier: nil];
  [super dealloc];
}

//documentation inherited.
+ (id<EnunciateXML>) readFromXML: (NSData *) xml
{
  TASK_MANAGER_CLIENTNS0FindUserById *_tASK_MANAGER_CLIENTNS0FindUserById;
  xmlTextReaderPtr reader = xmlReaderForMemory([xml bytes], [xml length], NULL, NULL, 0);
  if (reader == NULL) {
    [NSException raise: @"XMLReadError"
                 format: @"Error instantiating an XML reader."];
    return nil;
  }

  _tASK_MANAGER_CLIENTNS0FindUserById = (TASK_MANAGER_CLIENTNS0FindUserById *) [TASK_MANAGER_CLIENTNS0FindUserById readXMLElement: reader];
  xmlFreeTextReader(reader); //free the reader
  return _tASK_MANAGER_CLIENTNS0FindUserById;
}

//documentation inherited.
- (NSData *) writeToXML
{
  xmlBufferPtr buf;
  xmlTextWriterPtr writer;
  int rc;
  NSData *data;

  buf = xmlBufferCreate();
  if (buf == NULL) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML buffer."];
    return nil;
  }

  writer = xmlNewTextWriterMemory(buf, 0);
  if (writer == NULL) {
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML writer."];
    return nil;
  }

  rc = xmlTextWriterStartDocument(writer, NULL, "utf-8", NULL);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML start document."];
    return nil;
  }

  NS_DURING
  {
    [self writeXMLElement: writer];
  }
  NS_HANDLER
  {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [localException raise];
  }
  NS_ENDHANDLER

  rc = xmlTextWriterEndDocument(writer);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML end document."];
    return nil;
  }

  xmlFreeTextWriter(writer);
  data = [NSData dataWithBytes: buf->content length: buf->use];
  xmlBufferFree(buf);
  return data;
}
@end /* implementation TASK_MANAGER_CLIENTNS0FindUserById */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0FindUserById (JAXB) <JAXBReading, JAXBWriting, JAXBType, JAXBElement>

@end /*interface TASK_MANAGER_CLIENTNS0FindUserById (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0FindUserById (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0FindUserById from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0FindUserById defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0FindUserById *_tASK_MANAGER_CLIENTNS0FindUserById = [[TASK_MANAGER_CLIENTNS0FindUserById alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0FindUserById initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0FindUserById = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0FindUserById autorelease];
  return _tASK_MANAGER_CLIENTNS0FindUserById;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0FindUserById according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0FindUserById to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

/**
 * Reads a TASK_MANAGER_CLIENTNS0FindUserById from an XML reader. The element to be read is
 * "{http://endpoint.tm.amster.ru/}findUserById".
 *
 * @param reader The XML reader.
 * @return The TASK_MANAGER_CLIENTNS0FindUserById.
 */
+ (id<JAXBElement>) readXMLElement: (xmlTextReaderPtr) reader {
  int status;
  TASK_MANAGER_CLIENTNS0FindUserById *_findUserById = nil;

  if (xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) {
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
    if (status < 1) {
      [NSException raise: @"XMLReadError"
                   format: @"Error advancing the reader to start element {http://endpoint.tm.amster.ru/}findUserById."];
    }
  }

  if (xmlStrcmp(BAD_CAST "findUserById", xmlTextReaderConstLocalName(reader)) == 0
      && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read root element {http://endpoint.tm.amster.ru/}findUserById.");
#endif
    _findUserById = (TASK_MANAGER_CLIENTNS0FindUserById *)[TASK_MANAGER_CLIENTNS0FindUserById readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Successfully read root element {http://endpoint.tm.amster.ru/}findUserById.");
#endif
  }
  else {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0FindUserById. Expected element {http://endpoint.tm.amster.ru/}findUserById. Current element: {}%s", xmlTextReaderConstLocalName(reader)];
    }
    else {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0FindUserById. Expected element {http://endpoint.tm.amster.ru/}findUserById. Current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader)];
    }
  }

  return _findUserById;
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0FindUserById to XML under element name "{http://endpoint.tm.amster.ru/}findUserById".
 * The namespace declarations for the element will be written.
 *
 * @param writer The XML writer.
 * @param _findUserById The FindUserById to write.
 * @return 1 if successful, 0 otherwise.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer
{
  [self writeXMLElement: writer writeNamespaces: YES];
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0FindUserById to an XML writer.
 *
 * @param writer The writer.
 * @param writeNs Whether to write the namespaces for this element to the xml writer.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer writeNamespaces: (BOOL) writeNs
{
  int rc = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "findUserById", NULL);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing start element {http://endpoint.tm.amster.ru/}findUserById. XML writer status: %i\n", rc];
  }

  if (writeNs) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing namespaces for start element {http://endpoint.tm.amster.ru/}findUserById...");
#endif

    rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (rc < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing attribute 'xmlns:ns0' on '{http://endpoint.tm.amster.ru/}findUserById'. XML writer status: %i\n", rc];
    }
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote namespaces for start element {http://endpoint.tm.amster.ru/}findUserById...");
#endif
  }

#if DEBUG_ENUNCIATE > 1
  NSLog(@"writing type {http://endpoint.tm.amster.ru/}findUserById for root element {http://endpoint.tm.amster.ru/}findUserById...");
#endif
  [self writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
  NSLog(@"successfully wrote type {http://endpoint.tm.amster.ru/}findUserById for root element {http://endpoint.tm.amster.ru/}findUserById...");
#endif
  rc = xmlTextWriterEndElement(writer);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing end element {http://endpoint.tm.amster.ru/}findUserById. XML writer status: %i\n", rc];
  }
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "session", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}session of type {http://endpoint.tm.amster.ru/}session.");
#endif
    __child = [TASK_MANAGER_CLIENTNS0Session readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}session of type {http://endpoint.tm.amster.ru/}session.");
#endif

    [self setSession: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "id", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setIdentifier: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self session]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "session", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}session."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}session...");
#endif
    [[self session] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}session...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}session."];
    }
  }
  if ([self identifier]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "id", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}id."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}id...");
#endif
    [[self identifier] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}id...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}id."];
    }
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0FindUserById (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0FindUserById_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0FindUserByLogin_M
#define DEF_TASK_MANAGER_CLIENTNS0FindUserByLogin_M

/**
 *  <p>Java class for findUserByLogin complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="findUserByLogin"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0FindUserByLogin

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Session *) session
{
  return _session;
}

/**
 * (no documentation provided)
 */
- (void) setSession: (TASK_MANAGER_CLIENTNS0Session *) newSession
{
  [newSession retain];
  [_session release];
  _session = newSession;
}

/**
 * (no documentation provided)
 */
- (NSString *) login
{
  return _login;
}

/**
 * (no documentation provided)
 */
- (void) setLogin: (NSString *) newLogin
{
  [newLogin retain];
  [_login release];
  _login = newLogin;
}

- (void) dealloc
{
  [self setSession: nil];
  [self setLogin: nil];
  [super dealloc];
}

//documentation inherited.
+ (id<EnunciateXML>) readFromXML: (NSData *) xml
{
  TASK_MANAGER_CLIENTNS0FindUserByLogin *_tASK_MANAGER_CLIENTNS0FindUserByLogin;
  xmlTextReaderPtr reader = xmlReaderForMemory([xml bytes], [xml length], NULL, NULL, 0);
  if (reader == NULL) {
    [NSException raise: @"XMLReadError"
                 format: @"Error instantiating an XML reader."];
    return nil;
  }

  _tASK_MANAGER_CLIENTNS0FindUserByLogin = (TASK_MANAGER_CLIENTNS0FindUserByLogin *) [TASK_MANAGER_CLIENTNS0FindUserByLogin readXMLElement: reader];
  xmlFreeTextReader(reader); //free the reader
  return _tASK_MANAGER_CLIENTNS0FindUserByLogin;
}

//documentation inherited.
- (NSData *) writeToXML
{
  xmlBufferPtr buf;
  xmlTextWriterPtr writer;
  int rc;
  NSData *data;

  buf = xmlBufferCreate();
  if (buf == NULL) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML buffer."];
    return nil;
  }

  writer = xmlNewTextWriterMemory(buf, 0);
  if (writer == NULL) {
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML writer."];
    return nil;
  }

  rc = xmlTextWriterStartDocument(writer, NULL, "utf-8", NULL);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML start document."];
    return nil;
  }

  NS_DURING
  {
    [self writeXMLElement: writer];
  }
  NS_HANDLER
  {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [localException raise];
  }
  NS_ENDHANDLER

  rc = xmlTextWriterEndDocument(writer);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML end document."];
    return nil;
  }

  xmlFreeTextWriter(writer);
  data = [NSData dataWithBytes: buf->content length: buf->use];
  xmlBufferFree(buf);
  return data;
}
@end /* implementation TASK_MANAGER_CLIENTNS0FindUserByLogin */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0FindUserByLogin (JAXB) <JAXBReading, JAXBWriting, JAXBType, JAXBElement>

@end /*interface TASK_MANAGER_CLIENTNS0FindUserByLogin (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0FindUserByLogin (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0FindUserByLogin from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0FindUserByLogin defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0FindUserByLogin *_tASK_MANAGER_CLIENTNS0FindUserByLogin = [[TASK_MANAGER_CLIENTNS0FindUserByLogin alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0FindUserByLogin initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0FindUserByLogin = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0FindUserByLogin autorelease];
  return _tASK_MANAGER_CLIENTNS0FindUserByLogin;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0FindUserByLogin according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0FindUserByLogin to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

/**
 * Reads a TASK_MANAGER_CLIENTNS0FindUserByLogin from an XML reader. The element to be read is
 * "{http://endpoint.tm.amster.ru/}findUserByLogin".
 *
 * @param reader The XML reader.
 * @return The TASK_MANAGER_CLIENTNS0FindUserByLogin.
 */
+ (id<JAXBElement>) readXMLElement: (xmlTextReaderPtr) reader {
  int status;
  TASK_MANAGER_CLIENTNS0FindUserByLogin *_findUserByLogin = nil;

  if (xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) {
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
    if (status < 1) {
      [NSException raise: @"XMLReadError"
                   format: @"Error advancing the reader to start element {http://endpoint.tm.amster.ru/}findUserByLogin."];
    }
  }

  if (xmlStrcmp(BAD_CAST "findUserByLogin", xmlTextReaderConstLocalName(reader)) == 0
      && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read root element {http://endpoint.tm.amster.ru/}findUserByLogin.");
#endif
    _findUserByLogin = (TASK_MANAGER_CLIENTNS0FindUserByLogin *)[TASK_MANAGER_CLIENTNS0FindUserByLogin readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Successfully read root element {http://endpoint.tm.amster.ru/}findUserByLogin.");
#endif
  }
  else {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0FindUserByLogin. Expected element {http://endpoint.tm.amster.ru/}findUserByLogin. Current element: {}%s", xmlTextReaderConstLocalName(reader)];
    }
    else {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0FindUserByLogin. Expected element {http://endpoint.tm.amster.ru/}findUserByLogin. Current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader)];
    }
  }

  return _findUserByLogin;
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0FindUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}findUserByLogin".
 * The namespace declarations for the element will be written.
 *
 * @param writer The XML writer.
 * @param _findUserByLogin The FindUserByLogin to write.
 * @return 1 if successful, 0 otherwise.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer
{
  [self writeXMLElement: writer writeNamespaces: YES];
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0FindUserByLogin to an XML writer.
 *
 * @param writer The writer.
 * @param writeNs Whether to write the namespaces for this element to the xml writer.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer writeNamespaces: (BOOL) writeNs
{
  int rc = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "findUserByLogin", NULL);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing start element {http://endpoint.tm.amster.ru/}findUserByLogin. XML writer status: %i\n", rc];
  }

  if (writeNs) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing namespaces for start element {http://endpoint.tm.amster.ru/}findUserByLogin...");
#endif

    rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (rc < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing attribute 'xmlns:ns0' on '{http://endpoint.tm.amster.ru/}findUserByLogin'. XML writer status: %i\n", rc];
    }
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote namespaces for start element {http://endpoint.tm.amster.ru/}findUserByLogin...");
#endif
  }

#if DEBUG_ENUNCIATE > 1
  NSLog(@"writing type {http://endpoint.tm.amster.ru/}findUserByLogin for root element {http://endpoint.tm.amster.ru/}findUserByLogin...");
#endif
  [self writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
  NSLog(@"successfully wrote type {http://endpoint.tm.amster.ru/}findUserByLogin for root element {http://endpoint.tm.amster.ru/}findUserByLogin...");
#endif
  rc = xmlTextWriterEndElement(writer);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing end element {http://endpoint.tm.amster.ru/}findUserByLogin. XML writer status: %i\n", rc];
  }
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "session", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}session of type {http://endpoint.tm.amster.ru/}session.");
#endif
    __child = [TASK_MANAGER_CLIENTNS0Session readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}session of type {http://endpoint.tm.amster.ru/}session.");
#endif

    [self setSession: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "login", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setLogin: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self session]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "session", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}session."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}session...");
#endif
    [[self session] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}session...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}session."];
    }
  }
  if ([self login]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "login", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}login."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}login...");
#endif
    [[self login] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}login...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}login."];
    }
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0FindUserByLogin (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0FindUserByLogin_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0Load_M
#define DEF_TASK_MANAGER_CLIENTNS0Load_M

/**
 *  <p>Java class for load complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="load"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="domain" type="{http://endpoint.tm.amster.ru/}domain" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0Load

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Session *) session
{
  return _session;
}

/**
 * (no documentation provided)
 */
- (void) setSession: (TASK_MANAGER_CLIENTNS0Session *) newSession
{
  [newSession retain];
  [_session release];
  _session = newSession;
}

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Domain *) domain
{
  return _domain;
}

/**
 * (no documentation provided)
 */
- (void) setDomain: (TASK_MANAGER_CLIENTNS0Domain *) newDomain
{
  [newDomain retain];
  [_domain release];
  _domain = newDomain;
}

- (void) dealloc
{
  [self setSession: nil];
  [self setDomain: nil];
  [super dealloc];
}

//documentation inherited.
+ (id<EnunciateXML>) readFromXML: (NSData *) xml
{
  TASK_MANAGER_CLIENTNS0Load *_tASK_MANAGER_CLIENTNS0Load;
  xmlTextReaderPtr reader = xmlReaderForMemory([xml bytes], [xml length], NULL, NULL, 0);
  if (reader == NULL) {
    [NSException raise: @"XMLReadError"
                 format: @"Error instantiating an XML reader."];
    return nil;
  }

  _tASK_MANAGER_CLIENTNS0Load = (TASK_MANAGER_CLIENTNS0Load *) [TASK_MANAGER_CLIENTNS0Load readXMLElement: reader];
  xmlFreeTextReader(reader); //free the reader
  return _tASK_MANAGER_CLIENTNS0Load;
}

//documentation inherited.
- (NSData *) writeToXML
{
  xmlBufferPtr buf;
  xmlTextWriterPtr writer;
  int rc;
  NSData *data;

  buf = xmlBufferCreate();
  if (buf == NULL) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML buffer."];
    return nil;
  }

  writer = xmlNewTextWriterMemory(buf, 0);
  if (writer == NULL) {
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML writer."];
    return nil;
  }

  rc = xmlTextWriterStartDocument(writer, NULL, "utf-8", NULL);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML start document."];
    return nil;
  }

  NS_DURING
  {
    [self writeXMLElement: writer];
  }
  NS_HANDLER
  {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [localException raise];
  }
  NS_ENDHANDLER

  rc = xmlTextWriterEndDocument(writer);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML end document."];
    return nil;
  }

  xmlFreeTextWriter(writer);
  data = [NSData dataWithBytes: buf->content length: buf->use];
  xmlBufferFree(buf);
  return data;
}
@end /* implementation TASK_MANAGER_CLIENTNS0Load */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0Load (JAXB) <JAXBReading, JAXBWriting, JAXBType, JAXBElement>

@end /*interface TASK_MANAGER_CLIENTNS0Load (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0Load (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0Load from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0Load defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0Load *_tASK_MANAGER_CLIENTNS0Load = [[TASK_MANAGER_CLIENTNS0Load alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0Load initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0Load = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0Load autorelease];
  return _tASK_MANAGER_CLIENTNS0Load;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0Load according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0Load to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

/**
 * Reads a TASK_MANAGER_CLIENTNS0Load from an XML reader. The element to be read is
 * "{http://endpoint.tm.amster.ru/}load".
 *
 * @param reader The XML reader.
 * @return The TASK_MANAGER_CLIENTNS0Load.
 */
+ (id<JAXBElement>) readXMLElement: (xmlTextReaderPtr) reader {
  int status;
  TASK_MANAGER_CLIENTNS0Load *_load = nil;

  if (xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) {
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
    if (status < 1) {
      [NSException raise: @"XMLReadError"
                   format: @"Error advancing the reader to start element {http://endpoint.tm.amster.ru/}load."];
    }
  }

  if (xmlStrcmp(BAD_CAST "load", xmlTextReaderConstLocalName(reader)) == 0
      && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read root element {http://endpoint.tm.amster.ru/}load.");
#endif
    _load = (TASK_MANAGER_CLIENTNS0Load *)[TASK_MANAGER_CLIENTNS0Load readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Successfully read root element {http://endpoint.tm.amster.ru/}load.");
#endif
  }
  else {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0Load. Expected element {http://endpoint.tm.amster.ru/}load. Current element: {}%s", xmlTextReaderConstLocalName(reader)];
    }
    else {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0Load. Expected element {http://endpoint.tm.amster.ru/}load. Current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader)];
    }
  }

  return _load;
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0Load to XML under element name "{http://endpoint.tm.amster.ru/}load".
 * The namespace declarations for the element will be written.
 *
 * @param writer The XML writer.
 * @param _load The Load to write.
 * @return 1 if successful, 0 otherwise.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer
{
  [self writeXMLElement: writer writeNamespaces: YES];
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0Load to an XML writer.
 *
 * @param writer The writer.
 * @param writeNs Whether to write the namespaces for this element to the xml writer.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer writeNamespaces: (BOOL) writeNs
{
  int rc = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "load", NULL);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing start element {http://endpoint.tm.amster.ru/}load. XML writer status: %i\n", rc];
  }

  if (writeNs) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing namespaces for start element {http://endpoint.tm.amster.ru/}load...");
#endif

    rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (rc < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing attribute 'xmlns:ns0' on '{http://endpoint.tm.amster.ru/}load'. XML writer status: %i\n", rc];
    }
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote namespaces for start element {http://endpoint.tm.amster.ru/}load...");
#endif
  }

#if DEBUG_ENUNCIATE > 1
  NSLog(@"writing type {http://endpoint.tm.amster.ru/}load for root element {http://endpoint.tm.amster.ru/}load...");
#endif
  [self writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
  NSLog(@"successfully wrote type {http://endpoint.tm.amster.ru/}load for root element {http://endpoint.tm.amster.ru/}load...");
#endif
  rc = xmlTextWriterEndElement(writer);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing end element {http://endpoint.tm.amster.ru/}load. XML writer status: %i\n", rc];
  }
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "session", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}session of type {http://endpoint.tm.amster.ru/}session.");
#endif
    __child = [TASK_MANAGER_CLIENTNS0Session readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}session of type {http://endpoint.tm.amster.ru/}session.");
#endif

    [self setSession: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "domain", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}domain of type {http://endpoint.tm.amster.ru/}domain.");
#endif
    __child = [TASK_MANAGER_CLIENTNS0Domain readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}domain of type {http://endpoint.tm.amster.ru/}domain.");
#endif

    [self setDomain: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self session]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "session", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}session."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}session...");
#endif
    [[self session] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}session...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}session."];
    }
  }
  if ([self domain]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "domain", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}domain."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}domain...");
#endif
    [[self domain] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}domain...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}domain."];
    }
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0Load (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0Load_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0LockUserByLogin_M
#define DEF_TASK_MANAGER_CLIENTNS0LockUserByLogin_M

/**
 *  <p>Java class for lockUserByLogin complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="lockUserByLogin"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0LockUserByLogin

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Session *) session
{
  return _session;
}

/**
 * (no documentation provided)
 */
- (void) setSession: (TASK_MANAGER_CLIENTNS0Session *) newSession
{
  [newSession retain];
  [_session release];
  _session = newSession;
}

/**
 * (no documentation provided)
 */
- (NSString *) login
{
  return _login;
}

/**
 * (no documentation provided)
 */
- (void) setLogin: (NSString *) newLogin
{
  [newLogin retain];
  [_login release];
  _login = newLogin;
}

- (void) dealloc
{
  [self setSession: nil];
  [self setLogin: nil];
  [super dealloc];
}

//documentation inherited.
+ (id<EnunciateXML>) readFromXML: (NSData *) xml
{
  TASK_MANAGER_CLIENTNS0LockUserByLogin *_tASK_MANAGER_CLIENTNS0LockUserByLogin;
  xmlTextReaderPtr reader = xmlReaderForMemory([xml bytes], [xml length], NULL, NULL, 0);
  if (reader == NULL) {
    [NSException raise: @"XMLReadError"
                 format: @"Error instantiating an XML reader."];
    return nil;
  }

  _tASK_MANAGER_CLIENTNS0LockUserByLogin = (TASK_MANAGER_CLIENTNS0LockUserByLogin *) [TASK_MANAGER_CLIENTNS0LockUserByLogin readXMLElement: reader];
  xmlFreeTextReader(reader); //free the reader
  return _tASK_MANAGER_CLIENTNS0LockUserByLogin;
}

//documentation inherited.
- (NSData *) writeToXML
{
  xmlBufferPtr buf;
  xmlTextWriterPtr writer;
  int rc;
  NSData *data;

  buf = xmlBufferCreate();
  if (buf == NULL) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML buffer."];
    return nil;
  }

  writer = xmlNewTextWriterMemory(buf, 0);
  if (writer == NULL) {
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML writer."];
    return nil;
  }

  rc = xmlTextWriterStartDocument(writer, NULL, "utf-8", NULL);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML start document."];
    return nil;
  }

  NS_DURING
  {
    [self writeXMLElement: writer];
  }
  NS_HANDLER
  {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [localException raise];
  }
  NS_ENDHANDLER

  rc = xmlTextWriterEndDocument(writer);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML end document."];
    return nil;
  }

  xmlFreeTextWriter(writer);
  data = [NSData dataWithBytes: buf->content length: buf->use];
  xmlBufferFree(buf);
  return data;
}
@end /* implementation TASK_MANAGER_CLIENTNS0LockUserByLogin */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0LockUserByLogin (JAXB) <JAXBReading, JAXBWriting, JAXBType, JAXBElement>

@end /*interface TASK_MANAGER_CLIENTNS0LockUserByLogin (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0LockUserByLogin (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0LockUserByLogin from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0LockUserByLogin defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0LockUserByLogin *_tASK_MANAGER_CLIENTNS0LockUserByLogin = [[TASK_MANAGER_CLIENTNS0LockUserByLogin alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0LockUserByLogin initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0LockUserByLogin = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0LockUserByLogin autorelease];
  return _tASK_MANAGER_CLIENTNS0LockUserByLogin;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0LockUserByLogin according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0LockUserByLogin to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

/**
 * Reads a TASK_MANAGER_CLIENTNS0LockUserByLogin from an XML reader. The element to be read is
 * "{http://endpoint.tm.amster.ru/}lockUserByLogin".
 *
 * @param reader The XML reader.
 * @return The TASK_MANAGER_CLIENTNS0LockUserByLogin.
 */
+ (id<JAXBElement>) readXMLElement: (xmlTextReaderPtr) reader {
  int status;
  TASK_MANAGER_CLIENTNS0LockUserByLogin *_lockUserByLogin = nil;

  if (xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) {
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
    if (status < 1) {
      [NSException raise: @"XMLReadError"
                   format: @"Error advancing the reader to start element {http://endpoint.tm.amster.ru/}lockUserByLogin."];
    }
  }

  if (xmlStrcmp(BAD_CAST "lockUserByLogin", xmlTextReaderConstLocalName(reader)) == 0
      && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read root element {http://endpoint.tm.amster.ru/}lockUserByLogin.");
#endif
    _lockUserByLogin = (TASK_MANAGER_CLIENTNS0LockUserByLogin *)[TASK_MANAGER_CLIENTNS0LockUserByLogin readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Successfully read root element {http://endpoint.tm.amster.ru/}lockUserByLogin.");
#endif
  }
  else {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0LockUserByLogin. Expected element {http://endpoint.tm.amster.ru/}lockUserByLogin. Current element: {}%s", xmlTextReaderConstLocalName(reader)];
    }
    else {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0LockUserByLogin. Expected element {http://endpoint.tm.amster.ru/}lockUserByLogin. Current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader)];
    }
  }

  return _lockUserByLogin;
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0LockUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}lockUserByLogin".
 * The namespace declarations for the element will be written.
 *
 * @param writer The XML writer.
 * @param _lockUserByLogin The LockUserByLogin to write.
 * @return 1 if successful, 0 otherwise.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer
{
  [self writeXMLElement: writer writeNamespaces: YES];
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0LockUserByLogin to an XML writer.
 *
 * @param writer The writer.
 * @param writeNs Whether to write the namespaces for this element to the xml writer.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer writeNamespaces: (BOOL) writeNs
{
  int rc = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "lockUserByLogin", NULL);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing start element {http://endpoint.tm.amster.ru/}lockUserByLogin. XML writer status: %i\n", rc];
  }

  if (writeNs) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing namespaces for start element {http://endpoint.tm.amster.ru/}lockUserByLogin...");
#endif

    rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (rc < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing attribute 'xmlns:ns0' on '{http://endpoint.tm.amster.ru/}lockUserByLogin'. XML writer status: %i\n", rc];
    }
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote namespaces for start element {http://endpoint.tm.amster.ru/}lockUserByLogin...");
#endif
  }

#if DEBUG_ENUNCIATE > 1
  NSLog(@"writing type {http://endpoint.tm.amster.ru/}lockUserByLogin for root element {http://endpoint.tm.amster.ru/}lockUserByLogin...");
#endif
  [self writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
  NSLog(@"successfully wrote type {http://endpoint.tm.amster.ru/}lockUserByLogin for root element {http://endpoint.tm.amster.ru/}lockUserByLogin...");
#endif
  rc = xmlTextWriterEndElement(writer);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing end element {http://endpoint.tm.amster.ru/}lockUserByLogin. XML writer status: %i\n", rc];
  }
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "session", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}session of type {http://endpoint.tm.amster.ru/}session.");
#endif
    __child = [TASK_MANAGER_CLIENTNS0Session readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}session of type {http://endpoint.tm.amster.ru/}session.");
#endif

    [self setSession: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "login", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setLogin: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self session]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "session", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}session."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}session...");
#endif
    [[self session] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}session...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}session."];
    }
  }
  if ([self login]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "login", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}login."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}login...");
#endif
    [[self login] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}login...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}login."];
    }
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0LockUserByLogin (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0LockUserByLogin_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0LockUserByLoginResponse_M
#define DEF_TASK_MANAGER_CLIENTNS0LockUserByLoginResponse_M

/**
 *  <p>Java class for lockUserByLoginResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="lockUserByLoginResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="return" type="{http://endpoint.tm.amster.ru/}user" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0LockUserByLoginResponse

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0User *) _return
{
  return __return;
}

/**
 * (no documentation provided)
 */
- (void) set_return: (TASK_MANAGER_CLIENTNS0User *) new_return
{
  [new_return retain];
  [__return release];
  __return = new_return;
}

- (void) dealloc
{
  [self set_return: nil];
  [super dealloc];
}

//documentation inherited.
+ (id<EnunciateXML>) readFromXML: (NSData *) xml
{
  TASK_MANAGER_CLIENTNS0LockUserByLoginResponse *_tASK_MANAGER_CLIENTNS0LockUserByLoginResponse;
  xmlTextReaderPtr reader = xmlReaderForMemory([xml bytes], [xml length], NULL, NULL, 0);
  if (reader == NULL) {
    [NSException raise: @"XMLReadError"
                 format: @"Error instantiating an XML reader."];
    return nil;
  }

  _tASK_MANAGER_CLIENTNS0LockUserByLoginResponse = (TASK_MANAGER_CLIENTNS0LockUserByLoginResponse *) [TASK_MANAGER_CLIENTNS0LockUserByLoginResponse readXMLElement: reader];
  xmlFreeTextReader(reader); //free the reader
  return _tASK_MANAGER_CLIENTNS0LockUserByLoginResponse;
}

//documentation inherited.
- (NSData *) writeToXML
{
  xmlBufferPtr buf;
  xmlTextWriterPtr writer;
  int rc;
  NSData *data;

  buf = xmlBufferCreate();
  if (buf == NULL) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML buffer."];
    return nil;
  }

  writer = xmlNewTextWriterMemory(buf, 0);
  if (writer == NULL) {
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML writer."];
    return nil;
  }

  rc = xmlTextWriterStartDocument(writer, NULL, "utf-8", NULL);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML start document."];
    return nil;
  }

  NS_DURING
  {
    [self writeXMLElement: writer];
  }
  NS_HANDLER
  {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [localException raise];
  }
  NS_ENDHANDLER

  rc = xmlTextWriterEndDocument(writer);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML end document."];
    return nil;
  }

  xmlFreeTextWriter(writer);
  data = [NSData dataWithBytes: buf->content length: buf->use];
  xmlBufferFree(buf);
  return data;
}
@end /* implementation TASK_MANAGER_CLIENTNS0LockUserByLoginResponse */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0LockUserByLoginResponse (JAXB) <JAXBReading, JAXBWriting, JAXBType, JAXBElement>

@end /*interface TASK_MANAGER_CLIENTNS0LockUserByLoginResponse (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0LockUserByLoginResponse (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0LockUserByLoginResponse from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0LockUserByLoginResponse defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0LockUserByLoginResponse *_tASK_MANAGER_CLIENTNS0LockUserByLoginResponse = [[TASK_MANAGER_CLIENTNS0LockUserByLoginResponse alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0LockUserByLoginResponse initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0LockUserByLoginResponse = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0LockUserByLoginResponse autorelease];
  return _tASK_MANAGER_CLIENTNS0LockUserByLoginResponse;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0LockUserByLoginResponse according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0LockUserByLoginResponse to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

/**
 * Reads a TASK_MANAGER_CLIENTNS0LockUserByLoginResponse from an XML reader. The element to be read is
 * "{http://endpoint.tm.amster.ru/}lockUserByLoginResponse".
 *
 * @param reader The XML reader.
 * @return The TASK_MANAGER_CLIENTNS0LockUserByLoginResponse.
 */
+ (id<JAXBElement>) readXMLElement: (xmlTextReaderPtr) reader {
  int status;
  TASK_MANAGER_CLIENTNS0LockUserByLoginResponse *_lockUserByLoginResponse = nil;

  if (xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) {
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
    if (status < 1) {
      [NSException raise: @"XMLReadError"
                   format: @"Error advancing the reader to start element {http://endpoint.tm.amster.ru/}lockUserByLoginResponse."];
    }
  }

  if (xmlStrcmp(BAD_CAST "lockUserByLoginResponse", xmlTextReaderConstLocalName(reader)) == 0
      && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read root element {http://endpoint.tm.amster.ru/}lockUserByLoginResponse.");
#endif
    _lockUserByLoginResponse = (TASK_MANAGER_CLIENTNS0LockUserByLoginResponse *)[TASK_MANAGER_CLIENTNS0LockUserByLoginResponse readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Successfully read root element {http://endpoint.tm.amster.ru/}lockUserByLoginResponse.");
#endif
  }
  else {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0LockUserByLoginResponse. Expected element {http://endpoint.tm.amster.ru/}lockUserByLoginResponse. Current element: {}%s", xmlTextReaderConstLocalName(reader)];
    }
    else {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0LockUserByLoginResponse. Expected element {http://endpoint.tm.amster.ru/}lockUserByLoginResponse. Current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader)];
    }
  }

  return _lockUserByLoginResponse;
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0LockUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}lockUserByLoginResponse".
 * The namespace declarations for the element will be written.
 *
 * @param writer The XML writer.
 * @param _lockUserByLoginResponse The LockUserByLoginResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer
{
  [self writeXMLElement: writer writeNamespaces: YES];
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0LockUserByLoginResponse to an XML writer.
 *
 * @param writer The writer.
 * @param writeNs Whether to write the namespaces for this element to the xml writer.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer writeNamespaces: (BOOL) writeNs
{
  int rc = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "lockUserByLoginResponse", NULL);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing start element {http://endpoint.tm.amster.ru/}lockUserByLoginResponse. XML writer status: %i\n", rc];
  }

  if (writeNs) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing namespaces for start element {http://endpoint.tm.amster.ru/}lockUserByLoginResponse...");
#endif

    rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (rc < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing attribute 'xmlns:ns0' on '{http://endpoint.tm.amster.ru/}lockUserByLoginResponse'. XML writer status: %i\n", rc];
    }
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote namespaces for start element {http://endpoint.tm.amster.ru/}lockUserByLoginResponse...");
#endif
  }

#if DEBUG_ENUNCIATE > 1
  NSLog(@"writing type {http://endpoint.tm.amster.ru/}lockUserByLoginResponse for root element {http://endpoint.tm.amster.ru/}lockUserByLoginResponse...");
#endif
  [self writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
  NSLog(@"successfully wrote type {http://endpoint.tm.amster.ru/}lockUserByLoginResponse for root element {http://endpoint.tm.amster.ru/}lockUserByLoginResponse...");
#endif
  rc = xmlTextWriterEndElement(writer);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing end element {http://endpoint.tm.amster.ru/}lockUserByLoginResponse. XML writer status: %i\n", rc];
  }
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "return", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}return of type {http://endpoint.tm.amster.ru/}user.");
#endif
    __child = [TASK_MANAGER_CLIENTNS0User readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}return of type {http://endpoint.tm.amster.ru/}user.");
#endif

    [self set_return: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self _return]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "return", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}return."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}return...");
#endif
    [[self _return] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}return...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}return."];
    }
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0LockUserByLoginResponse (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0LockUserByLoginResponse_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse_M
#define DEF_TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse_M

/**
 *  <p>Java class for removeUserByIdResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="removeUserByIdResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse

- (void) dealloc
{
  [super dealloc];
}

//documentation inherited.
+ (id<EnunciateXML>) readFromXML: (NSData *) xml
{
  TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse *_tASK_MANAGER_CLIENTNS0RemoveUserByIdResponse;
  xmlTextReaderPtr reader = xmlReaderForMemory([xml bytes], [xml length], NULL, NULL, 0);
  if (reader == NULL) {
    [NSException raise: @"XMLReadError"
                 format: @"Error instantiating an XML reader."];
    return nil;
  }

  _tASK_MANAGER_CLIENTNS0RemoveUserByIdResponse = (TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse *) [TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse readXMLElement: reader];
  xmlFreeTextReader(reader); //free the reader
  return _tASK_MANAGER_CLIENTNS0RemoveUserByIdResponse;
}

//documentation inherited.
- (NSData *) writeToXML
{
  xmlBufferPtr buf;
  xmlTextWriterPtr writer;
  int rc;
  NSData *data;

  buf = xmlBufferCreate();
  if (buf == NULL) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML buffer."];
    return nil;
  }

  writer = xmlNewTextWriterMemory(buf, 0);
  if (writer == NULL) {
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML writer."];
    return nil;
  }

  rc = xmlTextWriterStartDocument(writer, NULL, "utf-8", NULL);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML start document."];
    return nil;
  }

  NS_DURING
  {
    [self writeXMLElement: writer];
  }
  NS_HANDLER
  {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [localException raise];
  }
  NS_ENDHANDLER

  rc = xmlTextWriterEndDocument(writer);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML end document."];
    return nil;
  }

  xmlFreeTextWriter(writer);
  data = [NSData dataWithBytes: buf->content length: buf->use];
  xmlBufferFree(buf);
  return data;
}
@end /* implementation TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse (JAXB) <JAXBReading, JAXBWriting, JAXBType, JAXBElement>

@end /*interface TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse *_tASK_MANAGER_CLIENTNS0RemoveUserByIdResponse = [[TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0RemoveUserByIdResponse initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0RemoveUserByIdResponse = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0RemoveUserByIdResponse autorelease];
  return _tASK_MANAGER_CLIENTNS0RemoveUserByIdResponse;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

/**
 * Reads a TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse from an XML reader. The element to be read is
 * "{http://endpoint.tm.amster.ru/}removeUserByIdResponse".
 *
 * @param reader The XML reader.
 * @return The TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse.
 */
+ (id<JAXBElement>) readXMLElement: (xmlTextReaderPtr) reader {
  int status;
  TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse *_removeUserByIdResponse = nil;

  if (xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) {
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
    if (status < 1) {
      [NSException raise: @"XMLReadError"
                   format: @"Error advancing the reader to start element {http://endpoint.tm.amster.ru/}removeUserByIdResponse."];
    }
  }

  if (xmlStrcmp(BAD_CAST "removeUserByIdResponse", xmlTextReaderConstLocalName(reader)) == 0
      && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read root element {http://endpoint.tm.amster.ru/}removeUserByIdResponse.");
#endif
    _removeUserByIdResponse = (TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse *)[TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Successfully read root element {http://endpoint.tm.amster.ru/}removeUserByIdResponse.");
#endif
  }
  else {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse. Expected element {http://endpoint.tm.amster.ru/}removeUserByIdResponse. Current element: {}%s", xmlTextReaderConstLocalName(reader)];
    }
    else {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse. Expected element {http://endpoint.tm.amster.ru/}removeUserByIdResponse. Current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader)];
    }
  }

  return _removeUserByIdResponse;
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByIdResponse".
 * The namespace declarations for the element will be written.
 *
 * @param writer The XML writer.
 * @param _removeUserByIdResponse The RemoveUserByIdResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer
{
  [self writeXMLElement: writer writeNamespaces: YES];
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse to an XML writer.
 *
 * @param writer The writer.
 * @param writeNs Whether to write the namespaces for this element to the xml writer.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer writeNamespaces: (BOOL) writeNs
{
  int rc = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "removeUserByIdResponse", NULL);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing start element {http://endpoint.tm.amster.ru/}removeUserByIdResponse. XML writer status: %i\n", rc];
  }

  if (writeNs) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing namespaces for start element {http://endpoint.tm.amster.ru/}removeUserByIdResponse...");
#endif

    rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (rc < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing attribute 'xmlns:ns0' on '{http://endpoint.tm.amster.ru/}removeUserByIdResponse'. XML writer status: %i\n", rc];
    }
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote namespaces for start element {http://endpoint.tm.amster.ru/}removeUserByIdResponse...");
#endif
  }

#if DEBUG_ENUNCIATE > 1
  NSLog(@"writing type {http://endpoint.tm.amster.ru/}removeUserByIdResponse for root element {http://endpoint.tm.amster.ru/}removeUserByIdResponse...");
#endif
  [self writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
  NSLog(@"successfully wrote type {http://endpoint.tm.amster.ru/}removeUserByIdResponse for root element {http://endpoint.tm.amster.ru/}removeUserByIdResponse...");
#endif
  rc = xmlTextWriterEndElement(writer);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing end element {http://endpoint.tm.amster.ru/}removeUserByIdResponse. XML writer status: %i\n", rc];
  }
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

}
@end /* implementation TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse_M
#define DEF_TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse_M

/**
 *  <p>Java class for removeUserByLoginResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="removeUserByLoginResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse

- (void) dealloc
{
  [super dealloc];
}

//documentation inherited.
+ (id<EnunciateXML>) readFromXML: (NSData *) xml
{
  TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse *_tASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse;
  xmlTextReaderPtr reader = xmlReaderForMemory([xml bytes], [xml length], NULL, NULL, 0);
  if (reader == NULL) {
    [NSException raise: @"XMLReadError"
                 format: @"Error instantiating an XML reader."];
    return nil;
  }

  _tASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse = (TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse *) [TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse readXMLElement: reader];
  xmlFreeTextReader(reader); //free the reader
  return _tASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse;
}

//documentation inherited.
- (NSData *) writeToXML
{
  xmlBufferPtr buf;
  xmlTextWriterPtr writer;
  int rc;
  NSData *data;

  buf = xmlBufferCreate();
  if (buf == NULL) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML buffer."];
    return nil;
  }

  writer = xmlNewTextWriterMemory(buf, 0);
  if (writer == NULL) {
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML writer."];
    return nil;
  }

  rc = xmlTextWriterStartDocument(writer, NULL, "utf-8", NULL);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML start document."];
    return nil;
  }

  NS_DURING
  {
    [self writeXMLElement: writer];
  }
  NS_HANDLER
  {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [localException raise];
  }
  NS_ENDHANDLER

  rc = xmlTextWriterEndDocument(writer);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML end document."];
    return nil;
  }

  xmlFreeTextWriter(writer);
  data = [NSData dataWithBytes: buf->content length: buf->use];
  xmlBufferFree(buf);
  return data;
}
@end /* implementation TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse (JAXB) <JAXBReading, JAXBWriting, JAXBType, JAXBElement>

@end /*interface TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse *_tASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse = [[TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse autorelease];
  return _tASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

/**
 * Reads a TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse from an XML reader. The element to be read is
 * "{http://endpoint.tm.amster.ru/}removeUserByLoginResponse".
 *
 * @param reader The XML reader.
 * @return The TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse.
 */
+ (id<JAXBElement>) readXMLElement: (xmlTextReaderPtr) reader {
  int status;
  TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse *_removeUserByLoginResponse = nil;

  if (xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) {
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
    if (status < 1) {
      [NSException raise: @"XMLReadError"
                   format: @"Error advancing the reader to start element {http://endpoint.tm.amster.ru/}removeUserByLoginResponse."];
    }
  }

  if (xmlStrcmp(BAD_CAST "removeUserByLoginResponse", xmlTextReaderConstLocalName(reader)) == 0
      && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read root element {http://endpoint.tm.amster.ru/}removeUserByLoginResponse.");
#endif
    _removeUserByLoginResponse = (TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse *)[TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Successfully read root element {http://endpoint.tm.amster.ru/}removeUserByLoginResponse.");
#endif
  }
  else {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse. Expected element {http://endpoint.tm.amster.ru/}removeUserByLoginResponse. Current element: {}%s", xmlTextReaderConstLocalName(reader)];
    }
    else {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse. Expected element {http://endpoint.tm.amster.ru/}removeUserByLoginResponse. Current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader)];
    }
  }

  return _removeUserByLoginResponse;
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByLoginResponse".
 * The namespace declarations for the element will be written.
 *
 * @param writer The XML writer.
 * @param _removeUserByLoginResponse The RemoveUserByLoginResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer
{
  [self writeXMLElement: writer writeNamespaces: YES];
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse to an XML writer.
 *
 * @param writer The writer.
 * @param writeNs Whether to write the namespaces for this element to the xml writer.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer writeNamespaces: (BOOL) writeNs
{
  int rc = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "removeUserByLoginResponse", NULL);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing start element {http://endpoint.tm.amster.ru/}removeUserByLoginResponse. XML writer status: %i\n", rc];
  }

  if (writeNs) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing namespaces for start element {http://endpoint.tm.amster.ru/}removeUserByLoginResponse...");
#endif

    rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (rc < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing attribute 'xmlns:ns0' on '{http://endpoint.tm.amster.ru/}removeUserByLoginResponse'. XML writer status: %i\n", rc];
    }
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote namespaces for start element {http://endpoint.tm.amster.ru/}removeUserByLoginResponse...");
#endif
  }

#if DEBUG_ENUNCIATE > 1
  NSLog(@"writing type {http://endpoint.tm.amster.ru/}removeUserByLoginResponse for root element {http://endpoint.tm.amster.ru/}removeUserByLoginResponse...");
#endif
  [self writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
  NSLog(@"successfully wrote type {http://endpoint.tm.amster.ru/}removeUserByLoginResponse for root element {http://endpoint.tm.amster.ru/}removeUserByLoginResponse...");
#endif
  rc = xmlTextWriterEndElement(writer);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing end element {http://endpoint.tm.amster.ru/}removeUserByLoginResponse. XML writer status: %i\n", rc];
  }
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

}
@end /* implementation TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0Role_M
#define DEF_TASK_MANAGER_CLIENTNS0Role_M

/**
 * Reads a Role from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Role, or NULL if unable to be read.
 */
static enum TASK_MANAGER_CLIENTNS0Role *xmlTextReaderReadTASK_MANAGER_CLIENTNS0RoleType(xmlTextReaderPtr reader)
{
  xmlChar *enumValue = xmlTextReaderReadEntireNodeValue(reader);
  enum TASK_MANAGER_CLIENTNS0Role *value = calloc(1, sizeof(enum TASK_MANAGER_CLIENTNS0Role));
  if (enumValue != NULL) {
    if (xmlStrcmp(enumValue, BAD_CAST "USER") == 0) {
      *value = TASK_MANAGER_CLIENT_NS0_ROLE_USER;
      free(enumValue);
      return value;
    }
    if (xmlStrcmp(enumValue, BAD_CAST "ADMIN") == 0) {
      *value = TASK_MANAGER_CLIENT_NS0_ROLE_ADMIN;
      free(enumValue);
      return value;
    }
#if DEBUG_ENUNCIATE
    NSLog(@"Attempt to read enum value failed: %s doesn't match an enum value.", enumValue);
#endif
  }
#if DEBUG_ENUNCIATE
  else {
    NSLog(@"Attempt to read enum value failed: NULL value.");
  }
#endif

  return NULL;
}

/**
 * Utility method for getting the enum value for a string.
 *
 * @param _role The string to format.
 * @return The enum value or NULL on error.
 */
enum TASK_MANAGER_CLIENTNS0Role *formatStringToTASK_MANAGER_CLIENTNS0RoleType(NSString *_role)
{
  enum TASK_MANAGER_CLIENTNS0Role *value = calloc(1, sizeof(enum TASK_MANAGER_CLIENTNS0Role));
  if ([@"USER" isEqualToString:_role]) {
    *value = TASK_MANAGER_CLIENT_NS0_ROLE_USER;
  }
  else if ([@"ADMIN" isEqualToString:_role]) {
    *value = TASK_MANAGER_CLIENT_NS0_ROLE_ADMIN;
  }
  else{
#if DEBUG_ENUNCIATE
  NSLog(@"Attempt to read enum value failed: %s doesn't match an enum value.", [_role UTF8String]);
#endif
    value = NULL;
  }
  return value;
}

/**
 * Writes a Role to XML.
 *
 * @param writer The XML writer.
 * @param _role The Role to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteTASK_MANAGER_CLIENTNS0RoleType(xmlTextWriterPtr writer, enum TASK_MANAGER_CLIENTNS0Role *_role)
{
  switch (*_role) {
    case TASK_MANAGER_CLIENT_NS0_ROLE_USER:
      return xmlTextWriterWriteString(writer, BAD_CAST "USER");
    case TASK_MANAGER_CLIENT_NS0_ROLE_ADMIN:
      return xmlTextWriterWriteString(writer, BAD_CAST "ADMIN");
  }

#if DEBUG_ENUNCIATE
  NSLog(@"Unable to write enum value (no valid value found).");
#endif
  return -1;
}

/**
 * Utility method for getting the string value of Role.
 *
 * @param _role The Role to format.
 * @return The string value or NULL on error.
 */
static NSString *formatTASK_MANAGER_CLIENTNS0RoleTypeToString(enum TASK_MANAGER_CLIENTNS0Role *_role)
{
  switch (*_role) {
    case TASK_MANAGER_CLIENT_NS0_ROLE_USER:
      return @"USER";
    case TASK_MANAGER_CLIENT_NS0_ROLE_ADMIN:
      return @"ADMIN";
    default:
      return NULL;
  }

  return NULL;
}
#endif /* DEF_TASK_MANAGER_CLIENTNS0Role_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0UnlockUserByLogin_M
#define DEF_TASK_MANAGER_CLIENTNS0UnlockUserByLogin_M

/**
 *  <p>Java class for unlockUserByLogin complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="unlockUserByLogin"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0UnlockUserByLogin

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Session *) session
{
  return _session;
}

/**
 * (no documentation provided)
 */
- (void) setSession: (TASK_MANAGER_CLIENTNS0Session *) newSession
{
  [newSession retain];
  [_session release];
  _session = newSession;
}

/**
 * (no documentation provided)
 */
- (NSString *) login
{
  return _login;
}

/**
 * (no documentation provided)
 */
- (void) setLogin: (NSString *) newLogin
{
  [newLogin retain];
  [_login release];
  _login = newLogin;
}

- (void) dealloc
{
  [self setSession: nil];
  [self setLogin: nil];
  [super dealloc];
}

//documentation inherited.
+ (id<EnunciateXML>) readFromXML: (NSData *) xml
{
  TASK_MANAGER_CLIENTNS0UnlockUserByLogin *_tASK_MANAGER_CLIENTNS0UnlockUserByLogin;
  xmlTextReaderPtr reader = xmlReaderForMemory([xml bytes], [xml length], NULL, NULL, 0);
  if (reader == NULL) {
    [NSException raise: @"XMLReadError"
                 format: @"Error instantiating an XML reader."];
    return nil;
  }

  _tASK_MANAGER_CLIENTNS0UnlockUserByLogin = (TASK_MANAGER_CLIENTNS0UnlockUserByLogin *) [TASK_MANAGER_CLIENTNS0UnlockUserByLogin readXMLElement: reader];
  xmlFreeTextReader(reader); //free the reader
  return _tASK_MANAGER_CLIENTNS0UnlockUserByLogin;
}

//documentation inherited.
- (NSData *) writeToXML
{
  xmlBufferPtr buf;
  xmlTextWriterPtr writer;
  int rc;
  NSData *data;

  buf = xmlBufferCreate();
  if (buf == NULL) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML buffer."];
    return nil;
  }

  writer = xmlNewTextWriterMemory(buf, 0);
  if (writer == NULL) {
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML writer."];
    return nil;
  }

  rc = xmlTextWriterStartDocument(writer, NULL, "utf-8", NULL);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML start document."];
    return nil;
  }

  NS_DURING
  {
    [self writeXMLElement: writer];
  }
  NS_HANDLER
  {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [localException raise];
  }
  NS_ENDHANDLER

  rc = xmlTextWriterEndDocument(writer);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML end document."];
    return nil;
  }

  xmlFreeTextWriter(writer);
  data = [NSData dataWithBytes: buf->content length: buf->use];
  xmlBufferFree(buf);
  return data;
}
@end /* implementation TASK_MANAGER_CLIENTNS0UnlockUserByLogin */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0UnlockUserByLogin (JAXB) <JAXBReading, JAXBWriting, JAXBType, JAXBElement>

@end /*interface TASK_MANAGER_CLIENTNS0UnlockUserByLogin (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0UnlockUserByLogin (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0UnlockUserByLogin from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0UnlockUserByLogin defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0UnlockUserByLogin *_tASK_MANAGER_CLIENTNS0UnlockUserByLogin = [[TASK_MANAGER_CLIENTNS0UnlockUserByLogin alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0UnlockUserByLogin initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0UnlockUserByLogin = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0UnlockUserByLogin autorelease];
  return _tASK_MANAGER_CLIENTNS0UnlockUserByLogin;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0UnlockUserByLogin according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0UnlockUserByLogin to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

/**
 * Reads a TASK_MANAGER_CLIENTNS0UnlockUserByLogin from an XML reader. The element to be read is
 * "{http://endpoint.tm.amster.ru/}unlockUserByLogin".
 *
 * @param reader The XML reader.
 * @return The TASK_MANAGER_CLIENTNS0UnlockUserByLogin.
 */
+ (id<JAXBElement>) readXMLElement: (xmlTextReaderPtr) reader {
  int status;
  TASK_MANAGER_CLIENTNS0UnlockUserByLogin *_unlockUserByLogin = nil;

  if (xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) {
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
    if (status < 1) {
      [NSException raise: @"XMLReadError"
                   format: @"Error advancing the reader to start element {http://endpoint.tm.amster.ru/}unlockUserByLogin."];
    }
  }

  if (xmlStrcmp(BAD_CAST "unlockUserByLogin", xmlTextReaderConstLocalName(reader)) == 0
      && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read root element {http://endpoint.tm.amster.ru/}unlockUserByLogin.");
#endif
    _unlockUserByLogin = (TASK_MANAGER_CLIENTNS0UnlockUserByLogin *)[TASK_MANAGER_CLIENTNS0UnlockUserByLogin readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Successfully read root element {http://endpoint.tm.amster.ru/}unlockUserByLogin.");
#endif
  }
  else {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0UnlockUserByLogin. Expected element {http://endpoint.tm.amster.ru/}unlockUserByLogin. Current element: {}%s", xmlTextReaderConstLocalName(reader)];
    }
    else {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0UnlockUserByLogin. Expected element {http://endpoint.tm.amster.ru/}unlockUserByLogin. Current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader)];
    }
  }

  return _unlockUserByLogin;
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0UnlockUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}unlockUserByLogin".
 * The namespace declarations for the element will be written.
 *
 * @param writer The XML writer.
 * @param _unlockUserByLogin The UnlockUserByLogin to write.
 * @return 1 if successful, 0 otherwise.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer
{
  [self writeXMLElement: writer writeNamespaces: YES];
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0UnlockUserByLogin to an XML writer.
 *
 * @param writer The writer.
 * @param writeNs Whether to write the namespaces for this element to the xml writer.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer writeNamespaces: (BOOL) writeNs
{
  int rc = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "unlockUserByLogin", NULL);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing start element {http://endpoint.tm.amster.ru/}unlockUserByLogin. XML writer status: %i\n", rc];
  }

  if (writeNs) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing namespaces for start element {http://endpoint.tm.amster.ru/}unlockUserByLogin...");
#endif

    rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (rc < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing attribute 'xmlns:ns0' on '{http://endpoint.tm.amster.ru/}unlockUserByLogin'. XML writer status: %i\n", rc];
    }
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote namespaces for start element {http://endpoint.tm.amster.ru/}unlockUserByLogin...");
#endif
  }

#if DEBUG_ENUNCIATE > 1
  NSLog(@"writing type {http://endpoint.tm.amster.ru/}unlockUserByLogin for root element {http://endpoint.tm.amster.ru/}unlockUserByLogin...");
#endif
  [self writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
  NSLog(@"successfully wrote type {http://endpoint.tm.amster.ru/}unlockUserByLogin for root element {http://endpoint.tm.amster.ru/}unlockUserByLogin...");
#endif
  rc = xmlTextWriterEndElement(writer);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing end element {http://endpoint.tm.amster.ru/}unlockUserByLogin. XML writer status: %i\n", rc];
  }
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "session", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}session of type {http://endpoint.tm.amster.ru/}session.");
#endif
    __child = [TASK_MANAGER_CLIENTNS0Session readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}session of type {http://endpoint.tm.amster.ru/}session.");
#endif

    [self setSession: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "login", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setLogin: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self session]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "session", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}session."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}session...");
#endif
    [[self session] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}session...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}session."];
    }
  }
  if ([self login]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "login", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}login."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}login...");
#endif
    [[self login] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}login...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}login."];
    }
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0UnlockUserByLogin (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0UnlockUserByLogin_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse_M
#define DEF_TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse_M

/**
 *  <p>Java class for unlockUserByLoginResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="unlockUserByLoginResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="return" type="{http://endpoint.tm.amster.ru/}user" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0User *) _return
{
  return __return;
}

/**
 * (no documentation provided)
 */
- (void) set_return: (TASK_MANAGER_CLIENTNS0User *) new_return
{
  [new_return retain];
  [__return release];
  __return = new_return;
}

- (void) dealloc
{
  [self set_return: nil];
  [super dealloc];
}

//documentation inherited.
+ (id<EnunciateXML>) readFromXML: (NSData *) xml
{
  TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse *_tASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse;
  xmlTextReaderPtr reader = xmlReaderForMemory([xml bytes], [xml length], NULL, NULL, 0);
  if (reader == NULL) {
    [NSException raise: @"XMLReadError"
                 format: @"Error instantiating an XML reader."];
    return nil;
  }

  _tASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse = (TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse *) [TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse readXMLElement: reader];
  xmlFreeTextReader(reader); //free the reader
  return _tASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse;
}

//documentation inherited.
- (NSData *) writeToXML
{
  xmlBufferPtr buf;
  xmlTextWriterPtr writer;
  int rc;
  NSData *data;

  buf = xmlBufferCreate();
  if (buf == NULL) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML buffer."];
    return nil;
  }

  writer = xmlNewTextWriterMemory(buf, 0);
  if (writer == NULL) {
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML writer."];
    return nil;
  }

  rc = xmlTextWriterStartDocument(writer, NULL, "utf-8", NULL);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML start document."];
    return nil;
  }

  NS_DURING
  {
    [self writeXMLElement: writer];
  }
  NS_HANDLER
  {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [localException raise];
  }
  NS_ENDHANDLER

  rc = xmlTextWriterEndDocument(writer);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML end document."];
    return nil;
  }

  xmlFreeTextWriter(writer);
  data = [NSData dataWithBytes: buf->content length: buf->use];
  xmlBufferFree(buf);
  return data;
}
@end /* implementation TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse (JAXB) <JAXBReading, JAXBWriting, JAXBType, JAXBElement>

@end /*interface TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse *_tASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse = [[TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse autorelease];
  return _tASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

/**
 * Reads a TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse from an XML reader. The element to be read is
 * "{http://endpoint.tm.amster.ru/}unlockUserByLoginResponse".
 *
 * @param reader The XML reader.
 * @return The TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse.
 */
+ (id<JAXBElement>) readXMLElement: (xmlTextReaderPtr) reader {
  int status;
  TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse *_unlockUserByLoginResponse = nil;

  if (xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) {
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
    if (status < 1) {
      [NSException raise: @"XMLReadError"
                   format: @"Error advancing the reader to start element {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse."];
    }
  }

  if (xmlStrcmp(BAD_CAST "unlockUserByLoginResponse", xmlTextReaderConstLocalName(reader)) == 0
      && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read root element {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse.");
#endif
    _unlockUserByLoginResponse = (TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse *)[TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Successfully read root element {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse.");
#endif
  }
  else {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse. Expected element {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse. Current element: {}%s", xmlTextReaderConstLocalName(reader)];
    }
    else {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse. Expected element {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse. Current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader)];
    }
  }

  return _unlockUserByLoginResponse;
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}unlockUserByLoginResponse".
 * The namespace declarations for the element will be written.
 *
 * @param writer The XML writer.
 * @param _unlockUserByLoginResponse The UnlockUserByLoginResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer
{
  [self writeXMLElement: writer writeNamespaces: YES];
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse to an XML writer.
 *
 * @param writer The writer.
 * @param writeNs Whether to write the namespaces for this element to the xml writer.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer writeNamespaces: (BOOL) writeNs
{
  int rc = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "unlockUserByLoginResponse", NULL);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing start element {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse. XML writer status: %i\n", rc];
  }

  if (writeNs) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing namespaces for start element {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse...");
#endif

    rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (rc < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing attribute 'xmlns:ns0' on '{http://endpoint.tm.amster.ru/}unlockUserByLoginResponse'. XML writer status: %i\n", rc];
    }
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote namespaces for start element {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse...");
#endif
  }

#if DEBUG_ENUNCIATE > 1
  NSLog(@"writing type {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse for root element {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse...");
#endif
  [self writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
  NSLog(@"successfully wrote type {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse for root element {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse...");
#endif
  rc = xmlTextWriterEndElement(writer);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing end element {http://endpoint.tm.amster.ru/}unlockUserByLoginResponse. XML writer status: %i\n", rc];
  }
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "return", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}return of type {http://endpoint.tm.amster.ru/}user.");
#endif
    __child = [TASK_MANAGER_CLIENTNS0User readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}return of type {http://endpoint.tm.amster.ru/}user.");
#endif

    [self set_return: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self _return]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "return", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}return."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}return...");
#endif
    [[self _return] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}return...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}return."];
    }
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0RemoveUserByLogin_M
#define DEF_TASK_MANAGER_CLIENTNS0RemoveUserByLogin_M

/**
 *  <p>Java class for removeUserByLogin complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="removeUserByLogin"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0RemoveUserByLogin

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Session *) session
{
  return _session;
}

/**
 * (no documentation provided)
 */
- (void) setSession: (TASK_MANAGER_CLIENTNS0Session *) newSession
{
  [newSession retain];
  [_session release];
  _session = newSession;
}

/**
 * (no documentation provided)
 */
- (NSString *) login
{
  return _login;
}

/**
 * (no documentation provided)
 */
- (void) setLogin: (NSString *) newLogin
{
  [newLogin retain];
  [_login release];
  _login = newLogin;
}

- (void) dealloc
{
  [self setSession: nil];
  [self setLogin: nil];
  [super dealloc];
}

//documentation inherited.
+ (id<EnunciateXML>) readFromXML: (NSData *) xml
{
  TASK_MANAGER_CLIENTNS0RemoveUserByLogin *_tASK_MANAGER_CLIENTNS0RemoveUserByLogin;
  xmlTextReaderPtr reader = xmlReaderForMemory([xml bytes], [xml length], NULL, NULL, 0);
  if (reader == NULL) {
    [NSException raise: @"XMLReadError"
                 format: @"Error instantiating an XML reader."];
    return nil;
  }

  _tASK_MANAGER_CLIENTNS0RemoveUserByLogin = (TASK_MANAGER_CLIENTNS0RemoveUserByLogin *) [TASK_MANAGER_CLIENTNS0RemoveUserByLogin readXMLElement: reader];
  xmlFreeTextReader(reader); //free the reader
  return _tASK_MANAGER_CLIENTNS0RemoveUserByLogin;
}

//documentation inherited.
- (NSData *) writeToXML
{
  xmlBufferPtr buf;
  xmlTextWriterPtr writer;
  int rc;
  NSData *data;

  buf = xmlBufferCreate();
  if (buf == NULL) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML buffer."];
    return nil;
  }

  writer = xmlNewTextWriterMemory(buf, 0);
  if (writer == NULL) {
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML writer."];
    return nil;
  }

  rc = xmlTextWriterStartDocument(writer, NULL, "utf-8", NULL);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML start document."];
    return nil;
  }

  NS_DURING
  {
    [self writeXMLElement: writer];
  }
  NS_HANDLER
  {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [localException raise];
  }
  NS_ENDHANDLER

  rc = xmlTextWriterEndDocument(writer);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML end document."];
    return nil;
  }

  xmlFreeTextWriter(writer);
  data = [NSData dataWithBytes: buf->content length: buf->use];
  xmlBufferFree(buf);
  return data;
}
@end /* implementation TASK_MANAGER_CLIENTNS0RemoveUserByLogin */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0RemoveUserByLogin (JAXB) <JAXBReading, JAXBWriting, JAXBType, JAXBElement>

@end /*interface TASK_MANAGER_CLIENTNS0RemoveUserByLogin (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0RemoveUserByLogin (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0RemoveUserByLogin from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0RemoveUserByLogin defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0RemoveUserByLogin *_tASK_MANAGER_CLIENTNS0RemoveUserByLogin = [[TASK_MANAGER_CLIENTNS0RemoveUserByLogin alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0RemoveUserByLogin initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0RemoveUserByLogin = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0RemoveUserByLogin autorelease];
  return _tASK_MANAGER_CLIENTNS0RemoveUserByLogin;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0RemoveUserByLogin according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0RemoveUserByLogin to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

/**
 * Reads a TASK_MANAGER_CLIENTNS0RemoveUserByLogin from an XML reader. The element to be read is
 * "{http://endpoint.tm.amster.ru/}removeUserByLogin".
 *
 * @param reader The XML reader.
 * @return The TASK_MANAGER_CLIENTNS0RemoveUserByLogin.
 */
+ (id<JAXBElement>) readXMLElement: (xmlTextReaderPtr) reader {
  int status;
  TASK_MANAGER_CLIENTNS0RemoveUserByLogin *_removeUserByLogin = nil;

  if (xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) {
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
    if (status < 1) {
      [NSException raise: @"XMLReadError"
                   format: @"Error advancing the reader to start element {http://endpoint.tm.amster.ru/}removeUserByLogin."];
    }
  }

  if (xmlStrcmp(BAD_CAST "removeUserByLogin", xmlTextReaderConstLocalName(reader)) == 0
      && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read root element {http://endpoint.tm.amster.ru/}removeUserByLogin.");
#endif
    _removeUserByLogin = (TASK_MANAGER_CLIENTNS0RemoveUserByLogin *)[TASK_MANAGER_CLIENTNS0RemoveUserByLogin readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Successfully read root element {http://endpoint.tm.amster.ru/}removeUserByLogin.");
#endif
  }
  else {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0RemoveUserByLogin. Expected element {http://endpoint.tm.amster.ru/}removeUserByLogin. Current element: {}%s", xmlTextReaderConstLocalName(reader)];
    }
    else {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0RemoveUserByLogin. Expected element {http://endpoint.tm.amster.ru/}removeUserByLogin. Current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader)];
    }
  }

  return _removeUserByLogin;
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0RemoveUserByLogin to XML under element name "{http://endpoint.tm.amster.ru/}removeUserByLogin".
 * The namespace declarations for the element will be written.
 *
 * @param writer The XML writer.
 * @param _removeUserByLogin The RemoveUserByLogin to write.
 * @return 1 if successful, 0 otherwise.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer
{
  [self writeXMLElement: writer writeNamespaces: YES];
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0RemoveUserByLogin to an XML writer.
 *
 * @param writer The writer.
 * @param writeNs Whether to write the namespaces for this element to the xml writer.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer writeNamespaces: (BOOL) writeNs
{
  int rc = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "removeUserByLogin", NULL);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing start element {http://endpoint.tm.amster.ru/}removeUserByLogin. XML writer status: %i\n", rc];
  }

  if (writeNs) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing namespaces for start element {http://endpoint.tm.amster.ru/}removeUserByLogin...");
#endif

    rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (rc < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing attribute 'xmlns:ns0' on '{http://endpoint.tm.amster.ru/}removeUserByLogin'. XML writer status: %i\n", rc];
    }
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote namespaces for start element {http://endpoint.tm.amster.ru/}removeUserByLogin...");
#endif
  }

#if DEBUG_ENUNCIATE > 1
  NSLog(@"writing type {http://endpoint.tm.amster.ru/}removeUserByLogin for root element {http://endpoint.tm.amster.ru/}removeUserByLogin...");
#endif
  [self writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
  NSLog(@"successfully wrote type {http://endpoint.tm.amster.ru/}removeUserByLogin for root element {http://endpoint.tm.amster.ru/}removeUserByLogin...");
#endif
  rc = xmlTextWriterEndElement(writer);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing end element {http://endpoint.tm.amster.ru/}removeUserByLogin. XML writer status: %i\n", rc];
  }
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "session", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}session of type {http://endpoint.tm.amster.ru/}session.");
#endif
    __child = [TASK_MANAGER_CLIENTNS0Session readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}session of type {http://endpoint.tm.amster.ru/}session.");
#endif

    [self setSession: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "login", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setLogin: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self session]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "session", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}session."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}session...");
#endif
    [[self session] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}session...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}session."];
    }
  }
  if ([self login]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "login", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}login."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}login...");
#endif
    [[self login] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}login...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}login."];
    }
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0RemoveUserByLogin (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0RemoveUserByLogin_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0RemoveUserById_M
#define DEF_TASK_MANAGER_CLIENTNS0RemoveUserById_M

/**
 *  <p>Java class for removeUserById complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="removeUserById"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0RemoveUserById

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Session *) session
{
  return _session;
}

/**
 * (no documentation provided)
 */
- (void) setSession: (TASK_MANAGER_CLIENTNS0Session *) newSession
{
  [newSession retain];
  [_session release];
  _session = newSession;
}

/**
 * (no documentation provided)
 */
- (NSString *) identifier
{
  return _identifier;
}

/**
 * (no documentation provided)
 */
- (void) setIdentifier: (NSString *) newIdentifier
{
  [newIdentifier retain];
  [_identifier release];
  _identifier = newIdentifier;
}

- (void) dealloc
{
  [self setSession: nil];
  [self setIdentifier: nil];
  [super dealloc];
}

//documentation inherited.
+ (id<EnunciateXML>) readFromXML: (NSData *) xml
{
  TASK_MANAGER_CLIENTNS0RemoveUserById *_tASK_MANAGER_CLIENTNS0RemoveUserById;
  xmlTextReaderPtr reader = xmlReaderForMemory([xml bytes], [xml length], NULL, NULL, 0);
  if (reader == NULL) {
    [NSException raise: @"XMLReadError"
                 format: @"Error instantiating an XML reader."];
    return nil;
  }

  _tASK_MANAGER_CLIENTNS0RemoveUserById = (TASK_MANAGER_CLIENTNS0RemoveUserById *) [TASK_MANAGER_CLIENTNS0RemoveUserById readXMLElement: reader];
  xmlFreeTextReader(reader); //free the reader
  return _tASK_MANAGER_CLIENTNS0RemoveUserById;
}

//documentation inherited.
- (NSData *) writeToXML
{
  xmlBufferPtr buf;
  xmlTextWriterPtr writer;
  int rc;
  NSData *data;

  buf = xmlBufferCreate();
  if (buf == NULL) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML buffer."];
    return nil;
  }

  writer = xmlNewTextWriterMemory(buf, 0);
  if (writer == NULL) {
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML writer."];
    return nil;
  }

  rc = xmlTextWriterStartDocument(writer, NULL, "utf-8", NULL);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML start document."];
    return nil;
  }

  NS_DURING
  {
    [self writeXMLElement: writer];
  }
  NS_HANDLER
  {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [localException raise];
  }
  NS_ENDHANDLER

  rc = xmlTextWriterEndDocument(writer);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML end document."];
    return nil;
  }

  xmlFreeTextWriter(writer);
  data = [NSData dataWithBytes: buf->content length: buf->use];
  xmlBufferFree(buf);
  return data;
}
@end /* implementation TASK_MANAGER_CLIENTNS0RemoveUserById */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0RemoveUserById (JAXB) <JAXBReading, JAXBWriting, JAXBType, JAXBElement>

@end /*interface TASK_MANAGER_CLIENTNS0RemoveUserById (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0RemoveUserById (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0RemoveUserById from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0RemoveUserById defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0RemoveUserById *_tASK_MANAGER_CLIENTNS0RemoveUserById = [[TASK_MANAGER_CLIENTNS0RemoveUserById alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0RemoveUserById initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0RemoveUserById = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0RemoveUserById autorelease];
  return _tASK_MANAGER_CLIENTNS0RemoveUserById;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0RemoveUserById according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0RemoveUserById to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

/**
 * Reads a TASK_MANAGER_CLIENTNS0RemoveUserById from an XML reader. The element to be read is
 * "{http://endpoint.tm.amster.ru/}removeUserById".
 *
 * @param reader The XML reader.
 * @return The TASK_MANAGER_CLIENTNS0RemoveUserById.
 */
+ (id<JAXBElement>) readXMLElement: (xmlTextReaderPtr) reader {
  int status;
  TASK_MANAGER_CLIENTNS0RemoveUserById *_removeUserById = nil;

  if (xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) {
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
    if (status < 1) {
      [NSException raise: @"XMLReadError"
                   format: @"Error advancing the reader to start element {http://endpoint.tm.amster.ru/}removeUserById."];
    }
  }

  if (xmlStrcmp(BAD_CAST "removeUserById", xmlTextReaderConstLocalName(reader)) == 0
      && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read root element {http://endpoint.tm.amster.ru/}removeUserById.");
#endif
    _removeUserById = (TASK_MANAGER_CLIENTNS0RemoveUserById *)[TASK_MANAGER_CLIENTNS0RemoveUserById readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Successfully read root element {http://endpoint.tm.amster.ru/}removeUserById.");
#endif
  }
  else {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0RemoveUserById. Expected element {http://endpoint.tm.amster.ru/}removeUserById. Current element: {}%s", xmlTextReaderConstLocalName(reader)];
    }
    else {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0RemoveUserById. Expected element {http://endpoint.tm.amster.ru/}removeUserById. Current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader)];
    }
  }

  return _removeUserById;
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0RemoveUserById to XML under element name "{http://endpoint.tm.amster.ru/}removeUserById".
 * The namespace declarations for the element will be written.
 *
 * @param writer The XML writer.
 * @param _removeUserById The RemoveUserById to write.
 * @return 1 if successful, 0 otherwise.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer
{
  [self writeXMLElement: writer writeNamespaces: YES];
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0RemoveUserById to an XML writer.
 *
 * @param writer The writer.
 * @param writeNs Whether to write the namespaces for this element to the xml writer.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer writeNamespaces: (BOOL) writeNs
{
  int rc = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "removeUserById", NULL);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing start element {http://endpoint.tm.amster.ru/}removeUserById. XML writer status: %i\n", rc];
  }

  if (writeNs) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing namespaces for start element {http://endpoint.tm.amster.ru/}removeUserById...");
#endif

    rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (rc < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing attribute 'xmlns:ns0' on '{http://endpoint.tm.amster.ru/}removeUserById'. XML writer status: %i\n", rc];
    }
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote namespaces for start element {http://endpoint.tm.amster.ru/}removeUserById...");
#endif
  }

#if DEBUG_ENUNCIATE > 1
  NSLog(@"writing type {http://endpoint.tm.amster.ru/}removeUserById for root element {http://endpoint.tm.amster.ru/}removeUserById...");
#endif
  [self writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
  NSLog(@"successfully wrote type {http://endpoint.tm.amster.ru/}removeUserById for root element {http://endpoint.tm.amster.ru/}removeUserById...");
#endif
  rc = xmlTextWriterEndElement(writer);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing end element {http://endpoint.tm.amster.ru/}removeUserById. XML writer status: %i\n", rc];
  }
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "session", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}session of type {http://endpoint.tm.amster.ru/}session.");
#endif
    __child = [TASK_MANAGER_CLIENTNS0Session readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}session of type {http://endpoint.tm.amster.ru/}session.");
#endif

    [self setSession: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "id", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setIdentifier: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self session]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "session", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}session."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}session...");
#endif
    [[self session] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}session...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}session."];
    }
  }
  if ([self identifier]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "id", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}id."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}id...");
#endif
    [[self identifier] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}id...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}id."];
    }
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0RemoveUserById (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0RemoveUserById_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0LoadResponse_M
#define DEF_TASK_MANAGER_CLIENTNS0LoadResponse_M

/**
 *  <p>Java class for loadResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="loadResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0LoadResponse

- (void) dealloc
{
  [super dealloc];
}

//documentation inherited.
+ (id<EnunciateXML>) readFromXML: (NSData *) xml
{
  TASK_MANAGER_CLIENTNS0LoadResponse *_tASK_MANAGER_CLIENTNS0LoadResponse;
  xmlTextReaderPtr reader = xmlReaderForMemory([xml bytes], [xml length], NULL, NULL, 0);
  if (reader == NULL) {
    [NSException raise: @"XMLReadError"
                 format: @"Error instantiating an XML reader."];
    return nil;
  }

  _tASK_MANAGER_CLIENTNS0LoadResponse = (TASK_MANAGER_CLIENTNS0LoadResponse *) [TASK_MANAGER_CLIENTNS0LoadResponse readXMLElement: reader];
  xmlFreeTextReader(reader); //free the reader
  return _tASK_MANAGER_CLIENTNS0LoadResponse;
}

//documentation inherited.
- (NSData *) writeToXML
{
  xmlBufferPtr buf;
  xmlTextWriterPtr writer;
  int rc;
  NSData *data;

  buf = xmlBufferCreate();
  if (buf == NULL) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML buffer."];
    return nil;
  }

  writer = xmlNewTextWriterMemory(buf, 0);
  if (writer == NULL) {
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML writer."];
    return nil;
  }

  rc = xmlTextWriterStartDocument(writer, NULL, "utf-8", NULL);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML start document."];
    return nil;
  }

  NS_DURING
  {
    [self writeXMLElement: writer];
  }
  NS_HANDLER
  {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [localException raise];
  }
  NS_ENDHANDLER

  rc = xmlTextWriterEndDocument(writer);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML end document."];
    return nil;
  }

  xmlFreeTextWriter(writer);
  data = [NSData dataWithBytes: buf->content length: buf->use];
  xmlBufferFree(buf);
  return data;
}
@end /* implementation TASK_MANAGER_CLIENTNS0LoadResponse */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0LoadResponse (JAXB) <JAXBReading, JAXBWriting, JAXBType, JAXBElement>

@end /*interface TASK_MANAGER_CLIENTNS0LoadResponse (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0LoadResponse (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0LoadResponse from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0LoadResponse defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0LoadResponse *_tASK_MANAGER_CLIENTNS0LoadResponse = [[TASK_MANAGER_CLIENTNS0LoadResponse alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0LoadResponse initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0LoadResponse = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0LoadResponse autorelease];
  return _tASK_MANAGER_CLIENTNS0LoadResponse;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0LoadResponse according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0LoadResponse to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

/**
 * Reads a TASK_MANAGER_CLIENTNS0LoadResponse from an XML reader. The element to be read is
 * "{http://endpoint.tm.amster.ru/}loadResponse".
 *
 * @param reader The XML reader.
 * @return The TASK_MANAGER_CLIENTNS0LoadResponse.
 */
+ (id<JAXBElement>) readXMLElement: (xmlTextReaderPtr) reader {
  int status;
  TASK_MANAGER_CLIENTNS0LoadResponse *_loadResponse = nil;

  if (xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) {
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
    if (status < 1) {
      [NSException raise: @"XMLReadError"
                   format: @"Error advancing the reader to start element {http://endpoint.tm.amster.ru/}loadResponse."];
    }
  }

  if (xmlStrcmp(BAD_CAST "loadResponse", xmlTextReaderConstLocalName(reader)) == 0
      && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read root element {http://endpoint.tm.amster.ru/}loadResponse.");
#endif
    _loadResponse = (TASK_MANAGER_CLIENTNS0LoadResponse *)[TASK_MANAGER_CLIENTNS0LoadResponse readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Successfully read root element {http://endpoint.tm.amster.ru/}loadResponse.");
#endif
  }
  else {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0LoadResponse. Expected element {http://endpoint.tm.amster.ru/}loadResponse. Current element: {}%s", xmlTextReaderConstLocalName(reader)];
    }
    else {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0LoadResponse. Expected element {http://endpoint.tm.amster.ru/}loadResponse. Current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader)];
    }
  }

  return _loadResponse;
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0LoadResponse to XML under element name "{http://endpoint.tm.amster.ru/}loadResponse".
 * The namespace declarations for the element will be written.
 *
 * @param writer The XML writer.
 * @param _loadResponse The LoadResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer
{
  [self writeXMLElement: writer writeNamespaces: YES];
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0LoadResponse to an XML writer.
 *
 * @param writer The writer.
 * @param writeNs Whether to write the namespaces for this element to the xml writer.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer writeNamespaces: (BOOL) writeNs
{
  int rc = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "loadResponse", NULL);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing start element {http://endpoint.tm.amster.ru/}loadResponse. XML writer status: %i\n", rc];
  }

  if (writeNs) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing namespaces for start element {http://endpoint.tm.amster.ru/}loadResponse...");
#endif

    rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (rc < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing attribute 'xmlns:ns0' on '{http://endpoint.tm.amster.ru/}loadResponse'. XML writer status: %i\n", rc];
    }
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote namespaces for start element {http://endpoint.tm.amster.ru/}loadResponse...");
#endif
  }

#if DEBUG_ENUNCIATE > 1
  NSLog(@"writing type {http://endpoint.tm.amster.ru/}loadResponse for root element {http://endpoint.tm.amster.ru/}loadResponse...");
#endif
  [self writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
  NSLog(@"successfully wrote type {http://endpoint.tm.amster.ru/}loadResponse for root element {http://endpoint.tm.amster.ru/}loadResponse...");
#endif
  rc = xmlTextWriterEndElement(writer);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing end element {http://endpoint.tm.amster.ru/}loadResponse. XML writer status: %i\n", rc];
  }
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

}
@end /* implementation TASK_MANAGER_CLIENTNS0LoadResponse (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0LoadResponse_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0FindUserByLoginResponse_M
#define DEF_TASK_MANAGER_CLIENTNS0FindUserByLoginResponse_M

/**
 *  <p>Java class for findUserByLoginResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="findUserByLoginResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="return" type="{http://endpoint.tm.amster.ru/}user" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0FindUserByLoginResponse

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0User *) _return
{
  return __return;
}

/**
 * (no documentation provided)
 */
- (void) set_return: (TASK_MANAGER_CLIENTNS0User *) new_return
{
  [new_return retain];
  [__return release];
  __return = new_return;
}

- (void) dealloc
{
  [self set_return: nil];
  [super dealloc];
}

//documentation inherited.
+ (id<EnunciateXML>) readFromXML: (NSData *) xml
{
  TASK_MANAGER_CLIENTNS0FindUserByLoginResponse *_tASK_MANAGER_CLIENTNS0FindUserByLoginResponse;
  xmlTextReaderPtr reader = xmlReaderForMemory([xml bytes], [xml length], NULL, NULL, 0);
  if (reader == NULL) {
    [NSException raise: @"XMLReadError"
                 format: @"Error instantiating an XML reader."];
    return nil;
  }

  _tASK_MANAGER_CLIENTNS0FindUserByLoginResponse = (TASK_MANAGER_CLIENTNS0FindUserByLoginResponse *) [TASK_MANAGER_CLIENTNS0FindUserByLoginResponse readXMLElement: reader];
  xmlFreeTextReader(reader); //free the reader
  return _tASK_MANAGER_CLIENTNS0FindUserByLoginResponse;
}

//documentation inherited.
- (NSData *) writeToXML
{
  xmlBufferPtr buf;
  xmlTextWriterPtr writer;
  int rc;
  NSData *data;

  buf = xmlBufferCreate();
  if (buf == NULL) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML buffer."];
    return nil;
  }

  writer = xmlNewTextWriterMemory(buf, 0);
  if (writer == NULL) {
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML writer."];
    return nil;
  }

  rc = xmlTextWriterStartDocument(writer, NULL, "utf-8", NULL);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML start document."];
    return nil;
  }

  NS_DURING
  {
    [self writeXMLElement: writer];
  }
  NS_HANDLER
  {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [localException raise];
  }
  NS_ENDHANDLER

  rc = xmlTextWriterEndDocument(writer);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML end document."];
    return nil;
  }

  xmlFreeTextWriter(writer);
  data = [NSData dataWithBytes: buf->content length: buf->use];
  xmlBufferFree(buf);
  return data;
}
@end /* implementation TASK_MANAGER_CLIENTNS0FindUserByLoginResponse */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0FindUserByLoginResponse (JAXB) <JAXBReading, JAXBWriting, JAXBType, JAXBElement>

@end /*interface TASK_MANAGER_CLIENTNS0FindUserByLoginResponse (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0FindUserByLoginResponse (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0FindUserByLoginResponse from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0FindUserByLoginResponse defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0FindUserByLoginResponse *_tASK_MANAGER_CLIENTNS0FindUserByLoginResponse = [[TASK_MANAGER_CLIENTNS0FindUserByLoginResponse alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0FindUserByLoginResponse initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0FindUserByLoginResponse = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0FindUserByLoginResponse autorelease];
  return _tASK_MANAGER_CLIENTNS0FindUserByLoginResponse;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0FindUserByLoginResponse according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0FindUserByLoginResponse to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

/**
 * Reads a TASK_MANAGER_CLIENTNS0FindUserByLoginResponse from an XML reader. The element to be read is
 * "{http://endpoint.tm.amster.ru/}findUserByLoginResponse".
 *
 * @param reader The XML reader.
 * @return The TASK_MANAGER_CLIENTNS0FindUserByLoginResponse.
 */
+ (id<JAXBElement>) readXMLElement: (xmlTextReaderPtr) reader {
  int status;
  TASK_MANAGER_CLIENTNS0FindUserByLoginResponse *_findUserByLoginResponse = nil;

  if (xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) {
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
    if (status < 1) {
      [NSException raise: @"XMLReadError"
                   format: @"Error advancing the reader to start element {http://endpoint.tm.amster.ru/}findUserByLoginResponse."];
    }
  }

  if (xmlStrcmp(BAD_CAST "findUserByLoginResponse", xmlTextReaderConstLocalName(reader)) == 0
      && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read root element {http://endpoint.tm.amster.ru/}findUserByLoginResponse.");
#endif
    _findUserByLoginResponse = (TASK_MANAGER_CLIENTNS0FindUserByLoginResponse *)[TASK_MANAGER_CLIENTNS0FindUserByLoginResponse readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Successfully read root element {http://endpoint.tm.amster.ru/}findUserByLoginResponse.");
#endif
  }
  else {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0FindUserByLoginResponse. Expected element {http://endpoint.tm.amster.ru/}findUserByLoginResponse. Current element: {}%s", xmlTextReaderConstLocalName(reader)];
    }
    else {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0FindUserByLoginResponse. Expected element {http://endpoint.tm.amster.ru/}findUserByLoginResponse. Current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader)];
    }
  }

  return _findUserByLoginResponse;
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0FindUserByLoginResponse to XML under element name "{http://endpoint.tm.amster.ru/}findUserByLoginResponse".
 * The namespace declarations for the element will be written.
 *
 * @param writer The XML writer.
 * @param _findUserByLoginResponse The FindUserByLoginResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer
{
  [self writeXMLElement: writer writeNamespaces: YES];
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0FindUserByLoginResponse to an XML writer.
 *
 * @param writer The writer.
 * @param writeNs Whether to write the namespaces for this element to the xml writer.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer writeNamespaces: (BOOL) writeNs
{
  int rc = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "findUserByLoginResponse", NULL);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing start element {http://endpoint.tm.amster.ru/}findUserByLoginResponse. XML writer status: %i\n", rc];
  }

  if (writeNs) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing namespaces for start element {http://endpoint.tm.amster.ru/}findUserByLoginResponse...");
#endif

    rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (rc < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing attribute 'xmlns:ns0' on '{http://endpoint.tm.amster.ru/}findUserByLoginResponse'. XML writer status: %i\n", rc];
    }
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote namespaces for start element {http://endpoint.tm.amster.ru/}findUserByLoginResponse...");
#endif
  }

#if DEBUG_ENUNCIATE > 1
  NSLog(@"writing type {http://endpoint.tm.amster.ru/}findUserByLoginResponse for root element {http://endpoint.tm.amster.ru/}findUserByLoginResponse...");
#endif
  [self writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
  NSLog(@"successfully wrote type {http://endpoint.tm.amster.ru/}findUserByLoginResponse for root element {http://endpoint.tm.amster.ru/}findUserByLoginResponse...");
#endif
  rc = xmlTextWriterEndElement(writer);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing end element {http://endpoint.tm.amster.ru/}findUserByLoginResponse. XML writer status: %i\n", rc];
  }
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "return", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}return of type {http://endpoint.tm.amster.ru/}user.");
#endif
    __child = [TASK_MANAGER_CLIENTNS0User readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}return of type {http://endpoint.tm.amster.ru/}user.");
#endif

    [self set_return: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self _return]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "return", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}return."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}return...");
#endif
    [[self _return] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}return...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}return."];
    }
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0FindUserByLoginResponse (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0FindUserByLoginResponse_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0FindUserByIdResponse_M
#define DEF_TASK_MANAGER_CLIENTNS0FindUserByIdResponse_M

/**
 *  <p>Java class for findUserByIdResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="findUserByIdResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="return" type="{http://endpoint.tm.amster.ru/}user" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0FindUserByIdResponse

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0User *) _return
{
  return __return;
}

/**
 * (no documentation provided)
 */
- (void) set_return: (TASK_MANAGER_CLIENTNS0User *) new_return
{
  [new_return retain];
  [__return release];
  __return = new_return;
}

- (void) dealloc
{
  [self set_return: nil];
  [super dealloc];
}

//documentation inherited.
+ (id<EnunciateXML>) readFromXML: (NSData *) xml
{
  TASK_MANAGER_CLIENTNS0FindUserByIdResponse *_tASK_MANAGER_CLIENTNS0FindUserByIdResponse;
  xmlTextReaderPtr reader = xmlReaderForMemory([xml bytes], [xml length], NULL, NULL, 0);
  if (reader == NULL) {
    [NSException raise: @"XMLReadError"
                 format: @"Error instantiating an XML reader."];
    return nil;
  }

  _tASK_MANAGER_CLIENTNS0FindUserByIdResponse = (TASK_MANAGER_CLIENTNS0FindUserByIdResponse *) [TASK_MANAGER_CLIENTNS0FindUserByIdResponse readXMLElement: reader];
  xmlFreeTextReader(reader); //free the reader
  return _tASK_MANAGER_CLIENTNS0FindUserByIdResponse;
}

//documentation inherited.
- (NSData *) writeToXML
{
  xmlBufferPtr buf;
  xmlTextWriterPtr writer;
  int rc;
  NSData *data;

  buf = xmlBufferCreate();
  if (buf == NULL) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML buffer."];
    return nil;
  }

  writer = xmlNewTextWriterMemory(buf, 0);
  if (writer == NULL) {
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML writer."];
    return nil;
  }

  rc = xmlTextWriterStartDocument(writer, NULL, "utf-8", NULL);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML start document."];
    return nil;
  }

  NS_DURING
  {
    [self writeXMLElement: writer];
  }
  NS_HANDLER
  {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [localException raise];
  }
  NS_ENDHANDLER

  rc = xmlTextWriterEndDocument(writer);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML end document."];
    return nil;
  }

  xmlFreeTextWriter(writer);
  data = [NSData dataWithBytes: buf->content length: buf->use];
  xmlBufferFree(buf);
  return data;
}
@end /* implementation TASK_MANAGER_CLIENTNS0FindUserByIdResponse */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0FindUserByIdResponse (JAXB) <JAXBReading, JAXBWriting, JAXBType, JAXBElement>

@end /*interface TASK_MANAGER_CLIENTNS0FindUserByIdResponse (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0FindUserByIdResponse (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0FindUserByIdResponse from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0FindUserByIdResponse defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0FindUserByIdResponse *_tASK_MANAGER_CLIENTNS0FindUserByIdResponse = [[TASK_MANAGER_CLIENTNS0FindUserByIdResponse alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0FindUserByIdResponse initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0FindUserByIdResponse = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0FindUserByIdResponse autorelease];
  return _tASK_MANAGER_CLIENTNS0FindUserByIdResponse;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0FindUserByIdResponse according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0FindUserByIdResponse to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

/**
 * Reads a TASK_MANAGER_CLIENTNS0FindUserByIdResponse from an XML reader. The element to be read is
 * "{http://endpoint.tm.amster.ru/}findUserByIdResponse".
 *
 * @param reader The XML reader.
 * @return The TASK_MANAGER_CLIENTNS0FindUserByIdResponse.
 */
+ (id<JAXBElement>) readXMLElement: (xmlTextReaderPtr) reader {
  int status;
  TASK_MANAGER_CLIENTNS0FindUserByIdResponse *_findUserByIdResponse = nil;

  if (xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) {
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
    if (status < 1) {
      [NSException raise: @"XMLReadError"
                   format: @"Error advancing the reader to start element {http://endpoint.tm.amster.ru/}findUserByIdResponse."];
    }
  }

  if (xmlStrcmp(BAD_CAST "findUserByIdResponse", xmlTextReaderConstLocalName(reader)) == 0
      && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read root element {http://endpoint.tm.amster.ru/}findUserByIdResponse.");
#endif
    _findUserByIdResponse = (TASK_MANAGER_CLIENTNS0FindUserByIdResponse *)[TASK_MANAGER_CLIENTNS0FindUserByIdResponse readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Successfully read root element {http://endpoint.tm.amster.ru/}findUserByIdResponse.");
#endif
  }
  else {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0FindUserByIdResponse. Expected element {http://endpoint.tm.amster.ru/}findUserByIdResponse. Current element: {}%s", xmlTextReaderConstLocalName(reader)];
    }
    else {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0FindUserByIdResponse. Expected element {http://endpoint.tm.amster.ru/}findUserByIdResponse. Current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader)];
    }
  }

  return _findUserByIdResponse;
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0FindUserByIdResponse to XML under element name "{http://endpoint.tm.amster.ru/}findUserByIdResponse".
 * The namespace declarations for the element will be written.
 *
 * @param writer The XML writer.
 * @param _findUserByIdResponse The FindUserByIdResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer
{
  [self writeXMLElement: writer writeNamespaces: YES];
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0FindUserByIdResponse to an XML writer.
 *
 * @param writer The writer.
 * @param writeNs Whether to write the namespaces for this element to the xml writer.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer writeNamespaces: (BOOL) writeNs
{
  int rc = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "findUserByIdResponse", NULL);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing start element {http://endpoint.tm.amster.ru/}findUserByIdResponse. XML writer status: %i\n", rc];
  }

  if (writeNs) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing namespaces for start element {http://endpoint.tm.amster.ru/}findUserByIdResponse...");
#endif

    rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (rc < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing attribute 'xmlns:ns0' on '{http://endpoint.tm.amster.ru/}findUserByIdResponse'. XML writer status: %i\n", rc];
    }
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote namespaces for start element {http://endpoint.tm.amster.ru/}findUserByIdResponse...");
#endif
  }

#if DEBUG_ENUNCIATE > 1
  NSLog(@"writing type {http://endpoint.tm.amster.ru/}findUserByIdResponse for root element {http://endpoint.tm.amster.ru/}findUserByIdResponse...");
#endif
  [self writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
  NSLog(@"successfully wrote type {http://endpoint.tm.amster.ru/}findUserByIdResponse for root element {http://endpoint.tm.amster.ru/}findUserByIdResponse...");
#endif
  rc = xmlTextWriterEndElement(writer);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing end element {http://endpoint.tm.amster.ru/}findUserByIdResponse. XML writer status: %i\n", rc];
  }
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "return", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}return of type {http://endpoint.tm.amster.ru/}user.");
#endif
    __child = [TASK_MANAGER_CLIENTNS0User readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}return of type {http://endpoint.tm.amster.ru/}user.");
#endif

    [self set_return: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self _return]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "return", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}return."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}return...");
#endif
    [[self _return] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}return...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}return."];
    }
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0FindUserByIdResponse (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0FindUserByIdResponse_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0FindAllUserResponse_M
#define DEF_TASK_MANAGER_CLIENTNS0FindAllUserResponse_M

/**
 *  <p>Java class for findAllUserResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="findAllUserResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="return" type="{http://endpoint.tm.amster.ru/}user" maxOccurs="unbounded" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0FindAllUserResponse

/**
 * (no documentation provided)
 */
- (NSArray *) _return
{
  return __return;
}

/**
 * (no documentation provided)
 */
- (void) set_return: (NSArray *) new_return
{
  [new_return retain];
  [__return release];
  __return = new_return;
}

- (void) dealloc
{
  [self set_return: nil];
  [super dealloc];
}

//documentation inherited.
+ (id<EnunciateXML>) readFromXML: (NSData *) xml
{
  TASK_MANAGER_CLIENTNS0FindAllUserResponse *_tASK_MANAGER_CLIENTNS0FindAllUserResponse;
  xmlTextReaderPtr reader = xmlReaderForMemory([xml bytes], [xml length], NULL, NULL, 0);
  if (reader == NULL) {
    [NSException raise: @"XMLReadError"
                 format: @"Error instantiating an XML reader."];
    return nil;
  }

  _tASK_MANAGER_CLIENTNS0FindAllUserResponse = (TASK_MANAGER_CLIENTNS0FindAllUserResponse *) [TASK_MANAGER_CLIENTNS0FindAllUserResponse readXMLElement: reader];
  xmlFreeTextReader(reader); //free the reader
  return _tASK_MANAGER_CLIENTNS0FindAllUserResponse;
}

//documentation inherited.
- (NSData *) writeToXML
{
  xmlBufferPtr buf;
  xmlTextWriterPtr writer;
  int rc;
  NSData *data;

  buf = xmlBufferCreate();
  if (buf == NULL) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML buffer."];
    return nil;
  }

  writer = xmlNewTextWriterMemory(buf, 0);
  if (writer == NULL) {
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML writer."];
    return nil;
  }

  rc = xmlTextWriterStartDocument(writer, NULL, "utf-8", NULL);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML start document."];
    return nil;
  }

  NS_DURING
  {
    [self writeXMLElement: writer];
  }
  NS_HANDLER
  {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [localException raise];
  }
  NS_ENDHANDLER

  rc = xmlTextWriterEndDocument(writer);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML end document."];
    return nil;
  }

  xmlFreeTextWriter(writer);
  data = [NSData dataWithBytes: buf->content length: buf->use];
  xmlBufferFree(buf);
  return data;
}
@end /* implementation TASK_MANAGER_CLIENTNS0FindAllUserResponse */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0FindAllUserResponse (JAXB) <JAXBReading, JAXBWriting, JAXBType, JAXBElement>

@end /*interface TASK_MANAGER_CLIENTNS0FindAllUserResponse (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0FindAllUserResponse (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0FindAllUserResponse from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0FindAllUserResponse defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0FindAllUserResponse *_tASK_MANAGER_CLIENTNS0FindAllUserResponse = [[TASK_MANAGER_CLIENTNS0FindAllUserResponse alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0FindAllUserResponse initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0FindAllUserResponse = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0FindAllUserResponse autorelease];
  return _tASK_MANAGER_CLIENTNS0FindAllUserResponse;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0FindAllUserResponse according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0FindAllUserResponse to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

/**
 * Reads a TASK_MANAGER_CLIENTNS0FindAllUserResponse from an XML reader. The element to be read is
 * "{http://endpoint.tm.amster.ru/}findAllUserResponse".
 *
 * @param reader The XML reader.
 * @return The TASK_MANAGER_CLIENTNS0FindAllUserResponse.
 */
+ (id<JAXBElement>) readXMLElement: (xmlTextReaderPtr) reader {
  int status;
  TASK_MANAGER_CLIENTNS0FindAllUserResponse *_findAllUserResponse = nil;

  if (xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) {
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
    if (status < 1) {
      [NSException raise: @"XMLReadError"
                   format: @"Error advancing the reader to start element {http://endpoint.tm.amster.ru/}findAllUserResponse."];
    }
  }

  if (xmlStrcmp(BAD_CAST "findAllUserResponse", xmlTextReaderConstLocalName(reader)) == 0
      && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read root element {http://endpoint.tm.amster.ru/}findAllUserResponse.");
#endif
    _findAllUserResponse = (TASK_MANAGER_CLIENTNS0FindAllUserResponse *)[TASK_MANAGER_CLIENTNS0FindAllUserResponse readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Successfully read root element {http://endpoint.tm.amster.ru/}findAllUserResponse.");
#endif
  }
  else {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0FindAllUserResponse. Expected element {http://endpoint.tm.amster.ru/}findAllUserResponse. Current element: {}%s", xmlTextReaderConstLocalName(reader)];
    }
    else {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0FindAllUserResponse. Expected element {http://endpoint.tm.amster.ru/}findAllUserResponse. Current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader)];
    }
  }

  return _findAllUserResponse;
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0FindAllUserResponse to XML under element name "{http://endpoint.tm.amster.ru/}findAllUserResponse".
 * The namespace declarations for the element will be written.
 *
 * @param writer The XML writer.
 * @param _findAllUserResponse The FindAllUserResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer
{
  [self writeXMLElement: writer writeNamespaces: YES];
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0FindAllUserResponse to an XML writer.
 *
 * @param writer The writer.
 * @param writeNs Whether to write the namespaces for this element to the xml writer.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer writeNamespaces: (BOOL) writeNs
{
  int rc = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "findAllUserResponse", NULL);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing start element {http://endpoint.tm.amster.ru/}findAllUserResponse. XML writer status: %i\n", rc];
  }

  if (writeNs) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing namespaces for start element {http://endpoint.tm.amster.ru/}findAllUserResponse...");
#endif

    rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (rc < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing attribute 'xmlns:ns0' on '{http://endpoint.tm.amster.ru/}findAllUserResponse'. XML writer status: %i\n", rc];
    }
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote namespaces for start element {http://endpoint.tm.amster.ru/}findAllUserResponse...");
#endif
  }

#if DEBUG_ENUNCIATE > 1
  NSLog(@"writing type {http://endpoint.tm.amster.ru/}findAllUserResponse for root element {http://endpoint.tm.amster.ru/}findAllUserResponse...");
#endif
  [self writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
  NSLog(@"successfully wrote type {http://endpoint.tm.amster.ru/}findAllUserResponse for root element {http://endpoint.tm.amster.ru/}findAllUserResponse...");
#endif
  rc = xmlTextWriterEndElement(writer);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing end element {http://endpoint.tm.amster.ru/}findAllUserResponse. XML writer status: %i\n", rc];
  }
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "return", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}return of type {http://endpoint.tm.amster.ru/}user.");
#endif

     __child = [TASK_MANAGER_CLIENTNS0User readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}return of type {http://endpoint.tm.amster.ru/}user.");
#endif

    if ([self _return]) {
      [self set_return: [[self _return] arrayByAddingObject: __child]];
    }
    else {
      [self set_return: [NSArray arrayWithObject: __child]];
    }
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self _return]) {
    __enumerator = [[self _return] objectEnumerator];

    while ( (__item = [__enumerator nextObject]) ) {
      status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "return", NULL);
      if (status < 0) {
        [NSException raise: @"XMLWriteError"
                     format: @"Error writing start child element {}return."];
      }

#if DEBUG_ENUNCIATE > 1
      NSLog(@"writing element {}return...");
#endif
      [__item writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
      NSLog(@"successfully wrote element {}return...");
#endif

      status = xmlTextWriterEndElement(writer);
      if (status < 0) {
        [NSException raise: @"XMLWriteError"
                     format: @"Error writing end child element {}return."];
      }
    } //end item iterator.
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0FindAllUserResponse (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0FindAllUserResponse_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0ExportResponse_M
#define DEF_TASK_MANAGER_CLIENTNS0ExportResponse_M

/**
 *  <p>Java class for exportResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="exportResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="return" type="{http://endpoint.tm.amster.ru/}domain" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0ExportResponse

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Domain *) _return
{
  return __return;
}

/**
 * (no documentation provided)
 */
- (void) set_return: (TASK_MANAGER_CLIENTNS0Domain *) new_return
{
  [new_return retain];
  [__return release];
  __return = new_return;
}

- (void) dealloc
{
  [self set_return: nil];
  [super dealloc];
}

//documentation inherited.
+ (id<EnunciateXML>) readFromXML: (NSData *) xml
{
  TASK_MANAGER_CLIENTNS0ExportResponse *_tASK_MANAGER_CLIENTNS0ExportResponse;
  xmlTextReaderPtr reader = xmlReaderForMemory([xml bytes], [xml length], NULL, NULL, 0);
  if (reader == NULL) {
    [NSException raise: @"XMLReadError"
                 format: @"Error instantiating an XML reader."];
    return nil;
  }

  _tASK_MANAGER_CLIENTNS0ExportResponse = (TASK_MANAGER_CLIENTNS0ExportResponse *) [TASK_MANAGER_CLIENTNS0ExportResponse readXMLElement: reader];
  xmlFreeTextReader(reader); //free the reader
  return _tASK_MANAGER_CLIENTNS0ExportResponse;
}

//documentation inherited.
- (NSData *) writeToXML
{
  xmlBufferPtr buf;
  xmlTextWriterPtr writer;
  int rc;
  NSData *data;

  buf = xmlBufferCreate();
  if (buf == NULL) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML buffer."];
    return nil;
  }

  writer = xmlNewTextWriterMemory(buf, 0);
  if (writer == NULL) {
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error creating an XML writer."];
    return nil;
  }

  rc = xmlTextWriterStartDocument(writer, NULL, "utf-8", NULL);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML start document."];
    return nil;
  }

  NS_DURING
  {
    [self writeXMLElement: writer];
  }
  NS_HANDLER
  {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [localException raise];
  }
  NS_ENDHANDLER

  rc = xmlTextWriterEndDocument(writer);
  if (rc < 0) {
    xmlFreeTextWriter(writer);
    xmlBufferFree(buf);
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing XML end document."];
    return nil;
  }

  xmlFreeTextWriter(writer);
  data = [NSData dataWithBytes: buf->content length: buf->use];
  xmlBufferFree(buf);
  return data;
}
@end /* implementation TASK_MANAGER_CLIENTNS0ExportResponse */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0ExportResponse (JAXB) <JAXBReading, JAXBWriting, JAXBType, JAXBElement>

@end /*interface TASK_MANAGER_CLIENTNS0ExportResponse (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0ExportResponse (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0ExportResponse from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0ExportResponse defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0ExportResponse *_tASK_MANAGER_CLIENTNS0ExportResponse = [[TASK_MANAGER_CLIENTNS0ExportResponse alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0ExportResponse initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0ExportResponse = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0ExportResponse autorelease];
  return _tASK_MANAGER_CLIENTNS0ExportResponse;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0ExportResponse according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0ExportResponse to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

/**
 * Reads a TASK_MANAGER_CLIENTNS0ExportResponse from an XML reader. The element to be read is
 * "{http://endpoint.tm.amster.ru/}exportResponse".
 *
 * @param reader The XML reader.
 * @return The TASK_MANAGER_CLIENTNS0ExportResponse.
 */
+ (id<JAXBElement>) readXMLElement: (xmlTextReaderPtr) reader {
  int status;
  TASK_MANAGER_CLIENTNS0ExportResponse *_exportResponse = nil;

  if (xmlTextReaderNodeType(reader) != XML_READER_TYPE_ELEMENT) {
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
    if (status < 1) {
      [NSException raise: @"XMLReadError"
                   format: @"Error advancing the reader to start element {http://endpoint.tm.amster.ru/}exportResponse."];
    }
  }

  if (xmlStrcmp(BAD_CAST "exportResponse", xmlTextReaderConstLocalName(reader)) == 0
      && xmlStrcmp(BAD_CAST "http://endpoint.tm.amster.ru/", xmlTextReaderConstNamespaceUri(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read root element {http://endpoint.tm.amster.ru/}exportResponse.");
#endif
    _exportResponse = (TASK_MANAGER_CLIENTNS0ExportResponse *)[TASK_MANAGER_CLIENTNS0ExportResponse readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Successfully read root element {http://endpoint.tm.amster.ru/}exportResponse.");
#endif
  }
  else {
    if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0ExportResponse. Expected element {http://endpoint.tm.amster.ru/}exportResponse. Current element: {}%s", xmlTextReaderConstLocalName(reader)];
    }
    else {
      [NSException raise: @"XMLReadError"
                   format: @"Unable to read TASK_MANAGER_CLIENTNS0ExportResponse. Expected element {http://endpoint.tm.amster.ru/}exportResponse. Current element: {%s}%s\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader)];
    }
  }

  return _exportResponse;
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0ExportResponse to XML under element name "{http://endpoint.tm.amster.ru/}exportResponse".
 * The namespace declarations for the element will be written.
 *
 * @param writer The XML writer.
 * @param _exportResponse The ExportResponse to write.
 * @return 1 if successful, 0 otherwise.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer
{
  [self writeXMLElement: writer writeNamespaces: YES];
}

/**
 * Writes this TASK_MANAGER_CLIENTNS0ExportResponse to an XML writer.
 *
 * @param writer The writer.
 * @param writeNs Whether to write the namespaces for this element to the xml writer.
 */
- (void) writeXMLElement: (xmlTextWriterPtr) writer writeNamespaces: (BOOL) writeNs
{
  int rc = xmlTextWriterStartElementNS(writer, BAD_CAST "ns0", BAD_CAST "exportResponse", NULL);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing start element {http://endpoint.tm.amster.ru/}exportResponse. XML writer status: %i\n", rc];
  }

  if (writeNs) {
#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing namespaces for start element {http://endpoint.tm.amster.ru/}exportResponse...");
#endif

    rc = xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns:ns0", BAD_CAST "http://endpoint.tm.amster.ru/");
    if (rc < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing attribute 'xmlns:ns0' on '{http://endpoint.tm.amster.ru/}exportResponse'. XML writer status: %i\n", rc];
    }
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote namespaces for start element {http://endpoint.tm.amster.ru/}exportResponse...");
#endif
  }

#if DEBUG_ENUNCIATE > 1
  NSLog(@"writing type {http://endpoint.tm.amster.ru/}exportResponse for root element {http://endpoint.tm.amster.ru/}exportResponse...");
#endif
  [self writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
  NSLog(@"successfully wrote type {http://endpoint.tm.amster.ru/}exportResponse for root element {http://endpoint.tm.amster.ru/}exportResponse...");
#endif
  rc = xmlTextWriterEndElement(writer);
  if (rc < 0) {
    [NSException raise: @"XMLWriteError"
                 format: @"Error writing end element {http://endpoint.tm.amster.ru/}exportResponse. XML writer status: %i\n", rc];
  }
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "return", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}return of type {http://endpoint.tm.amster.ru/}domain.");
#endif
    __child = [TASK_MANAGER_CLIENTNS0Domain readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}return of type {http://endpoint.tm.amster.ru/}domain.");
#endif

    [self set_return: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self _return]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "return", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}return."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}return...");
#endif
    [[self _return] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}return...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}return."];
    }
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0ExportResponse (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0ExportResponse_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0Domain_M
#define DEF_TASK_MANAGER_CLIENTNS0Domain_M

/**
 *  <p>Java class for domain complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="domain"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="projects" type="{http://endpoint.tm.amster.ru/}project" maxOccurs="unbounded" minOccurs="0"/&gt;
 *          &lt;element name="tasks" type="{http://endpoint.tm.amster.ru/}task" maxOccurs="unbounded" minOccurs="0"/&gt;
 *          &lt;element name="users" type="{http://endpoint.tm.amster.ru/}user" maxOccurs="unbounded" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0Domain

/**
 * (no documentation provided)
 */
- (NSArray *) projects
{
  return _projects;
}

/**
 * (no documentation provided)
 */
- (void) setProjects: (NSArray *) newProjects
{
  [newProjects retain];
  [_projects release];
  _projects = newProjects;
}

/**
 * (no documentation provided)
 */
- (NSArray *) tasks
{
  return _tasks;
}

/**
 * (no documentation provided)
 */
- (void) setTasks: (NSArray *) newTasks
{
  [newTasks retain];
  [_tasks release];
  _tasks = newTasks;
}

/**
 * (no documentation provided)
 */
- (NSArray *) users
{
  return _users;
}

/**
 * (no documentation provided)
 */
- (void) setUsers: (NSArray *) newUsers
{
  [newUsers retain];
  [_users release];
  _users = newUsers;
}

- (void) dealloc
{
  [self setProjects: nil];
  [self setTasks: nil];
  [self setUsers: nil];
  [super dealloc];
}
@end /* implementation TASK_MANAGER_CLIENTNS0Domain */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0Domain (JAXB) <JAXBReading, JAXBWriting, JAXBType>

@end /*interface TASK_MANAGER_CLIENTNS0Domain (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0Domain (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0Domain from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0Domain defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0Domain *_tASK_MANAGER_CLIENTNS0Domain = [[TASK_MANAGER_CLIENTNS0Domain alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0Domain initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0Domain = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0Domain autorelease];
  return _tASK_MANAGER_CLIENTNS0Domain;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0Domain according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0Domain to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "projects", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

    status = 1;
    if (xmlTextReaderMoveToAttributeNs(reader, BAD_CAST "nil", BAD_CAST "http://www.w3.org/2001/XMLSchema-instance")) {
      if (xmlStrcmp(BAD_CAST "true", xmlTextReaderConstValue(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
        NSLog(@"Choice {}projects was nil according to the xsi:nil attribute.");
#endif
        __child = [NSNull null];
        status = 0;
      }
    }

    xmlTextReaderMoveToElement(reader); //move back to the element
    if (status) { //if not "nil"...
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}projects of type {http://endpoint.tm.amster.ru/}project.");
#endif

     __child = [TASK_MANAGER_CLIENTNS0Project readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}projects of type {http://endpoint.tm.amster.ru/}project.");
#endif
    } //end "if not nil" clause
    else if (xmlTextReaderIsEmptyElement(reader) == 0) {
      //if it's not the empty element, skip it because it's nil.
      xmlTextReaderSkipElement(reader);
    }

    if ([self projects]) {
      [self setProjects: [[self projects] arrayByAddingObject: __child]];
    }
    else {
      [self setProjects: [NSArray arrayWithObject: __child]];
    }
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "tasks", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

    status = 1;
    if (xmlTextReaderMoveToAttributeNs(reader, BAD_CAST "nil", BAD_CAST "http://www.w3.org/2001/XMLSchema-instance")) {
      if (xmlStrcmp(BAD_CAST "true", xmlTextReaderConstValue(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
        NSLog(@"Choice {}tasks was nil according to the xsi:nil attribute.");
#endif
        __child = [NSNull null];
        status = 0;
      }
    }

    xmlTextReaderMoveToElement(reader); //move back to the element
    if (status) { //if not "nil"...
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}tasks of type {http://endpoint.tm.amster.ru/}task.");
#endif

     __child = [TASK_MANAGER_CLIENTNS0Task readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}tasks of type {http://endpoint.tm.amster.ru/}task.");
#endif
    } //end "if not nil" clause
    else if (xmlTextReaderIsEmptyElement(reader) == 0) {
      //if it's not the empty element, skip it because it's nil.
      xmlTextReaderSkipElement(reader);
    }

    if ([self tasks]) {
      [self setTasks: [[self tasks] arrayByAddingObject: __child]];
    }
    else {
      [self setTasks: [NSArray arrayWithObject: __child]];
    }
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "users", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

    status = 1;
    if (xmlTextReaderMoveToAttributeNs(reader, BAD_CAST "nil", BAD_CAST "http://www.w3.org/2001/XMLSchema-instance")) {
      if (xmlStrcmp(BAD_CAST "true", xmlTextReaderConstValue(reader)) == 0) {
#if DEBUG_ENUNCIATE > 1
        NSLog(@"Choice {}users was nil according to the xsi:nil attribute.");
#endif
        __child = [NSNull null];
        status = 0;
      }
    }

    xmlTextReaderMoveToElement(reader); //move back to the element
    if (status) { //if not "nil"...
#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}users of type {http://endpoint.tm.amster.ru/}user.");
#endif

     __child = [TASK_MANAGER_CLIENTNS0User readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}users of type {http://endpoint.tm.amster.ru/}user.");
#endif
    } //end "if not nil" clause
    else if (xmlTextReaderIsEmptyElement(reader) == 0) {
      //if it's not the empty element, skip it because it's nil.
      xmlTextReaderSkipElement(reader);
    }

    if ([self users]) {
      [self setUsers: [[self users] arrayByAddingObject: __child]];
    }
    else {
      [self setUsers: [NSArray arrayWithObject: __child]];
    }
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self projects]) {
    __enumerator = [[self projects] objectEnumerator];

    while ( (__item = [__enumerator nextObject]) ) {
      status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "projects", NULL);
      if (status < 0) {
        [NSException raise: @"XMLWriteError"
                     format: @"Error writing start child element {}projects."];
      }

#if DEBUG_ENUNCIATE > 1
      NSLog(@"writing element {}projects...");
#endif
      [__item writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
      NSLog(@"successfully wrote element {}projects...");
#endif

      status = xmlTextWriterEndElement(writer);
      if (status < 0) {
        [NSException raise: @"XMLWriteError"
                     format: @"Error writing end child element {}projects."];
      }
    } //end item iterator.
  }
  if ([self tasks]) {
    __enumerator = [[self tasks] objectEnumerator];

    while ( (__item = [__enumerator nextObject]) ) {
      status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "tasks", NULL);
      if (status < 0) {
        [NSException raise: @"XMLWriteError"
                     format: @"Error writing start child element {}tasks."];
      }

#if DEBUG_ENUNCIATE > 1
      NSLog(@"writing element {}tasks...");
#endif
      [__item writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
      NSLog(@"successfully wrote element {}tasks...");
#endif

      status = xmlTextWriterEndElement(writer);
      if (status < 0) {
        [NSException raise: @"XMLWriteError"
                     format: @"Error writing end child element {}tasks."];
      }
    } //end item iterator.
  }
  if ([self users]) {
    __enumerator = [[self users] objectEnumerator];

    while ( (__item = [__enumerator nextObject]) ) {
      status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "users", NULL);
      if (status < 0) {
        [NSException raise: @"XMLWriteError"
                     format: @"Error writing start child element {}users."];
      }

#if DEBUG_ENUNCIATE > 1
      NSLog(@"writing element {}users...");
#endif
      [__item writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
      NSLog(@"successfully wrote element {}users...");
#endif

      status = xmlTextWriterEndElement(writer);
      if (status < 0) {
        [NSException raise: @"XMLWriteError"
                     format: @"Error writing end child element {}users."];
      }
    } //end item iterator.
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0Domain (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0Domain_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0AbstractEntity_M
#define DEF_TASK_MANAGER_CLIENTNS0AbstractEntity_M

/**
 *  <p>Java class for abstractEntity complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="abstractEntity"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0AbstractEntity

/**
 * (no documentation provided)
 */
- (NSString *) identifier
{
  return _identifier;
}

/**
 * (no documentation provided)
 */
- (void) setIdentifier: (NSString *) newIdentifier
{
  [newIdentifier retain];
  [_identifier release];
  _identifier = newIdentifier;
}

- (void) dealloc
{
  [self setIdentifier: nil];
  [super dealloc];
}
@end /* implementation TASK_MANAGER_CLIENTNS0AbstractEntity */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0AbstractEntity (JAXB) <JAXBReading, JAXBWriting, JAXBType>

@end /*interface TASK_MANAGER_CLIENTNS0AbstractEntity (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0AbstractEntity (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0AbstractEntity from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0AbstractEntity defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0AbstractEntity *_tASK_MANAGER_CLIENTNS0AbstractEntity = [[TASK_MANAGER_CLIENTNS0AbstractEntity alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0AbstractEntity initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0AbstractEntity = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0AbstractEntity autorelease];
  return _tASK_MANAGER_CLIENTNS0AbstractEntity;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0AbstractEntity according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0AbstractEntity to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "id", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setIdentifier: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self identifier]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "id", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}id."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}id...");
#endif
    [[self identifier] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}id...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}id."];
    }
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0AbstractEntity (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0AbstractEntity_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0Task_M
#define DEF_TASK_MANAGER_CLIENTNS0Task_M

/**
 *  <p>Java class for task complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="task"&gt;
 *    &lt;complexContent&gt;
 *      &lt;extension base="{http://endpoint.tm.amster.ru/}abstractEntity"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/extension&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0Task

/**
 * (no documentation provided)
 */
- (NSString *) description
{
  return _description;
}

/**
 * (no documentation provided)
 */
- (void) setDescription: (NSString *) newDescription
{
  [newDescription retain];
  [_description release];
  _description = newDescription;
}

/**
 * (no documentation provided)
 */
- (NSString *) name
{
  return _name;
}

/**
 * (no documentation provided)
 */
- (void) setName: (NSString *) newName
{
  [newName retain];
  [_name release];
  _name = newName;
}

/**
 * (no documentation provided)
 */
- (NSString *) userId
{
  return _userId;
}

/**
 * (no documentation provided)
 */
- (void) setUserId: (NSString *) newUserId
{
  [newUserId retain];
  [_userId release];
  _userId = newUserId;
}

- (void) dealloc
{
  [self setDescription: nil];
  [self setName: nil];
  [self setUserId: nil];
  [super dealloc];
}
@end /* implementation TASK_MANAGER_CLIENTNS0Task */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0Task (JAXB) <JAXBReading, JAXBWriting, JAXBType>

@end /*interface TASK_MANAGER_CLIENTNS0Task (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0Task (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0Task from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0Task defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0Task *_tASK_MANAGER_CLIENTNS0Task = [[TASK_MANAGER_CLIENTNS0Task alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0Task initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0Task = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0Task autorelease];
  return _tASK_MANAGER_CLIENTNS0Task;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0Task according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0Task to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "description", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}description of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}description of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setDescription: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "name", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}name of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}name of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setName: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "userId", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setUserId: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self description]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "description", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}description."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}description...");
#endif
    [[self description] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}description...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}description."];
    }
  }
  if ([self name]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "name", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}name."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}name...");
#endif
    [[self name] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}name...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}name."];
    }
  }
  if ([self userId]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "userId", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}userId."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}userId...");
#endif
    [[self userId] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}userId...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}userId."];
    }
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0Task (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0Task_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0User_M
#define DEF_TASK_MANAGER_CLIENTNS0User_M

/**
 *  <p>Java class for user complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="user"&gt;
 *    &lt;complexContent&gt;
 *      &lt;extension base="{http://endpoint.tm.amster.ru/}abstractEntity"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="fistName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="locked" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *          &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="middleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="passwordHash" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="role" type="{http://endpoint.tm.amster.ru/}role" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/extension&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0User

/**
 * (no documentation provided)
 */
- (NSString *) email
{
  return _email;
}

/**
 * (no documentation provided)
 */
- (void) setEmail: (NSString *) newEmail
{
  [newEmail retain];
  [_email release];
  _email = newEmail;
}

/**
 * (no documentation provided)
 */
- (NSString *) fistName
{
  return _fistName;
}

/**
 * (no documentation provided)
 */
- (void) setFistName: (NSString *) newFistName
{
  [newFistName retain];
  [_fistName release];
  _fistName = newFistName;
}

/**
 * (no documentation provided)
 */
- (NSString *) lastName
{
  return _lastName;
}

/**
 * (no documentation provided)
 */
- (void) setLastName: (NSString *) newLastName
{
  [newLastName retain];
  [_lastName release];
  _lastName = newLastName;
}

/**
 * (no documentation provided)
 */
- (BOOL *) locked
{
  return _locked;
}

/**
 * (no documentation provided)
 */
- (void) setLocked: (BOOL *) newLocked
{
  if (_locked != NULL) {
    free(_locked);
  }
  _locked = newLocked;
}

/**
 * (no documentation provided)
 */
- (NSString *) login
{
  return _login;
}

/**
 * (no documentation provided)
 */
- (void) setLogin: (NSString *) newLogin
{
  [newLogin retain];
  [_login release];
  _login = newLogin;
}

/**
 * (no documentation provided)
 */
- (NSString *) middleName
{
  return _middleName;
}

/**
 * (no documentation provided)
 */
- (void) setMiddleName: (NSString *) newMiddleName
{
  [newMiddleName retain];
  [_middleName release];
  _middleName = newMiddleName;
}

/**
 * (no documentation provided)
 */
- (NSString *) passwordHash
{
  return _passwordHash;
}

/**
 * (no documentation provided)
 */
- (void) setPasswordHash: (NSString *) newPasswordHash
{
  [newPasswordHash retain];
  [_passwordHash release];
  _passwordHash = newPasswordHash;
}

/**
 * (no documentation provided)
 */
- (enum TASK_MANAGER_CLIENTNS0Role *) role
{
  return _role;
}

/**
 * (no documentation provided)
 */
- (void) setRole: (enum TASK_MANAGER_CLIENTNS0Role *) newRole
{
  if (_role != NULL) {
    free(_role);
  }
  _role = newRole;
}

- (void) dealloc
{
  [self setEmail: nil];
  [self setFistName: nil];
  [self setLastName: nil];
  [self setLocked: NULL];
  [self setLogin: nil];
  [self setMiddleName: nil];
  [self setPasswordHash: nil];
  [self setRole: NULL];
  [super dealloc];
}
@end /* implementation TASK_MANAGER_CLIENTNS0User */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0User (JAXB) <JAXBReading, JAXBWriting, JAXBType>

@end /*interface TASK_MANAGER_CLIENTNS0User (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0User (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0User from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0User defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0User *_tASK_MANAGER_CLIENTNS0User = [[TASK_MANAGER_CLIENTNS0User alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0User initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0User = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0User autorelease];
  return _tASK_MANAGER_CLIENTNS0User;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0User according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0User to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "email", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}email of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}email of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setEmail: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "fistName", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}fistName of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}fistName of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setFistName: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "lastName", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}lastName of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}lastName of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setLastName: __child];
    return YES;
  } //end "if choice"


  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "locked", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

    _child_accessor = xmlTextReaderReadBooleanType(reader);
    if (_child_accessor == NULL) {
      //panic: unable to return the value for some reason.
      [NSException raise: @"XMLReadError"
                   format: @"Error reading element value."];
    }
    [self setLocked: ((BOOL*) _child_accessor)];
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "login", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setLogin: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "middleName", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}middleName of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}middleName of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setMiddleName: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "passwordHash", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}passwordHash of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}passwordHash of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setPasswordHash: __child];
    return YES;
  } //end "if choice"


  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "role", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

    _child_accessor = xmlTextReaderReadTASK_MANAGER_CLIENTNS0RoleType(reader);
    if (_child_accessor == NULL) {
      //panic: unable to return the value for some reason.
      [NSException raise: @"XMLReadError"
                   format: @"Error reading element value."];
    }
    [self setRole: ((enum TASK_MANAGER_CLIENTNS0Role*) _child_accessor)];
    return YES;
  }

  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self email]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "email", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}email."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}email...");
#endif
    [[self email] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}email...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}email."];
    }
  }
  if ([self fistName]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "fistName", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}fistName."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}fistName...");
#endif
    [[self fistName] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}fistName...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}fistName."];
    }
  }
  if ([self lastName]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "lastName", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}lastName."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}lastName...");
#endif
    [[self lastName] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}lastName...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}lastName."];
    }
  }
  if ([self locked] != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "locked", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}locked."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}locked...");
#endif
    status = xmlTextWriterWriteBooleanType(writer, [self locked]);
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}locked...");
#endif
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing child element {}locked."];
    }

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}locked."];
    }
  }
  if ([self login]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "login", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}login."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}login...");
#endif
    [[self login] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}login...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}login."];
    }
  }
  if ([self middleName]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "middleName", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}middleName."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}middleName...");
#endif
    [[self middleName] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}middleName...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}middleName."];
    }
  }
  if ([self passwordHash]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "passwordHash", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}passwordHash."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}passwordHash...");
#endif
    [[self passwordHash] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}passwordHash...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}passwordHash."];
    }
  }
  if ([self role] != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "role", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}role."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}role...");
#endif
    status = xmlTextWriterWriteTASK_MANAGER_CLIENTNS0RoleType(writer, [self role]);
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}role...");
#endif
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing child element {}role."];
    }

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}role."];
    }
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0User (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0User_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0Session_M
#define DEF_TASK_MANAGER_CLIENTNS0Session_M

/**
 *  <p>Java class for session complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="session"&gt;
 *    &lt;complexContent&gt;
 *      &lt;extension base="{http://endpoint.tm.amster.ru/}abstractEntity"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="signature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="timestamp" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *          &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/extension&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0Session

/**
 * (no documentation provided)
 */
- (NSString *) signature
{
  return _signature;
}

/**
 * (no documentation provided)
 */
- (void) setSignature: (NSString *) newSignature
{
  [newSignature retain];
  [_signature release];
  _signature = newSignature;
}

/**
 * (no documentation provided)
 */
- (long long *) timestamp
{
  return _timestamp;
}

/**
 * (no documentation provided)
 */
- (void) setTimestamp: (long long *) newTimestamp
{
  if (_timestamp != NULL) {
    free(_timestamp);
  }
  _timestamp = newTimestamp;
}

/**
 * (no documentation provided)
 */
- (NSString *) userId
{
  return _userId;
}

/**
 * (no documentation provided)
 */
- (void) setUserId: (NSString *) newUserId
{
  [newUserId retain];
  [_userId release];
  _userId = newUserId;
}

- (void) dealloc
{
  [self setSignature: nil];
  [self setTimestamp: NULL];
  [self setUserId: nil];
  [super dealloc];
}
@end /* implementation TASK_MANAGER_CLIENTNS0Session */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0Session (JAXB) <JAXBReading, JAXBWriting, JAXBType>

@end /*interface TASK_MANAGER_CLIENTNS0Session (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0Session (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0Session from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0Session defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0Session *_tASK_MANAGER_CLIENTNS0Session = [[TASK_MANAGER_CLIENTNS0Session alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0Session initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0Session = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0Session autorelease];
  return _tASK_MANAGER_CLIENTNS0Session;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0Session according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0Session to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "signature", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}signature of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}signature of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setSignature: __child];
    return YES;
  } //end "if choice"


  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "timestamp", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

    _child_accessor = xmlTextReaderReadLongType(reader);
    if (_child_accessor == NULL) {
      //panic: unable to return the value for some reason.
      [NSException raise: @"XMLReadError"
                   format: @"Error reading element value."];
    }
    [self setTimestamp: ((long long*) _child_accessor)];
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "userId", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setUserId: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self signature]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "signature", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}signature."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}signature...");
#endif
    [[self signature] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}signature...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}signature."];
    }
  }
  if ([self timestamp] != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "timestamp", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}timestamp."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}timestamp...");
#endif
    status = xmlTextWriterWriteLongType(writer, [self timestamp]);
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}timestamp...");
#endif
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing child element {}timestamp."];
    }

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}timestamp."];
    }
  }
  if ([self userId]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "userId", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}userId."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}userId...");
#endif
    [[self userId] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}userId...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}userId."];
    }
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0Session (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0Session_M */
#ifndef DEF_TASK_MANAGER_CLIENTNS0Project_M
#define DEF_TASK_MANAGER_CLIENTNS0Project_M

/**
 *  <p>Java class for project complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="project"&gt;
 *    &lt;complexContent&gt;
 *      &lt;extension base="{http://endpoint.tm.amster.ru/}abstractEntity"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/extension&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@implementation TASK_MANAGER_CLIENTNS0Project

/**
 * (no documentation provided)
 */
- (NSString *) description
{
  return _description;
}

/**
 * (no documentation provided)
 */
- (void) setDescription: (NSString *) newDescription
{
  [newDescription retain];
  [_description release];
  _description = newDescription;
}

/**
 * (no documentation provided)
 */
- (NSString *) name
{
  return _name;
}

/**
 * (no documentation provided)
 */
- (void) setName: (NSString *) newName
{
  [newName retain];
  [_name release];
  _name = newName;
}

/**
 * (no documentation provided)
 */
- (NSString *) userId
{
  return _userId;
}

/**
 * (no documentation provided)
 */
- (void) setUserId: (NSString *) newUserId
{
  [newUserId retain];
  [_userId release];
  _userId = newUserId;
}

- (void) dealloc
{
  [self setDescription: nil];
  [self setName: nil];
  [self setUserId: nil];
  [super dealloc];
}
@end /* implementation TASK_MANAGER_CLIENTNS0Project */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TASK_MANAGER_CLIENTNS0Project (JAXB) <JAXBReading, JAXBWriting, JAXBType>

@end /*interface TASK_MANAGER_CLIENTNS0Project (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TASK_MANAGER_CLIENTNS0Project (JAXB)

/**
 * Read an instance of TASK_MANAGER_CLIENTNS0Project from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TASK_MANAGER_CLIENTNS0Project defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TASK_MANAGER_CLIENTNS0Project *_tASK_MANAGER_CLIENTNS0Project = [[TASK_MANAGER_CLIENTNS0Project alloc] init];
  NS_DURING
  {
    [_tASK_MANAGER_CLIENTNS0Project initWithReader: reader];
  }
  NS_HANDLER
  {
    _tASK_MANAGER_CLIENTNS0Project = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tASK_MANAGER_CLIENTNS0Project autorelease];
  return _tASK_MANAGER_CLIENTNS0Project;
}

/**
 * Initialize this instance of TASK_MANAGER_CLIENTNS0Project according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TASK_MANAGER_CLIENTNS0Project to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }
  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "description", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}description of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}description of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setDescription: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "name", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}name of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}name of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setName: __child];
    return YES;
  } //end "if choice"

  if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
    && xmlStrcmp(BAD_CAST "userId", xmlTextReaderConstLocalName(reader)) == 0
    && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
    NSLog(@"Attempting to read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.");
#endif
    __child = [NSString readXMLType: reader];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.");
#endif

    [self setUserId: __child];
    return YES;
  } //end "if choice"


  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

  if ([self description]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "description", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}description."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}description...");
#endif
    [[self description] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}description...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}description."];
    }
  }
  if ([self name]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "name", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}name."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}name...");
#endif
    [[self name] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}name...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}name."];
    }
  }
  if ([self userId]) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "userId", NULL);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing start child element {}userId."];
    }

#if DEBUG_ENUNCIATE > 1
    NSLog(@"writing element {}userId...");
#endif
    [[self userId] writeXMLType: writer];
#if DEBUG_ENUNCIATE > 1
    NSLog(@"successfully wrote element {}userId...");
#endif

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
      [NSException raise: @"XMLWriteError"
                   format: @"Error writing end child element {}userId."];
    }
  }
}
@end /* implementation TASK_MANAGER_CLIENTNS0Project (JAXB) */

#endif /* DEF_TASK_MANAGER_CLIENTNS0Project_M */
