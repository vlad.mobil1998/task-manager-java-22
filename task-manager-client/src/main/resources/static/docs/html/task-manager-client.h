#import "enunciate-common.h"
#ifndef DEF_TASK_MANAGER_CLIENTNS0Role_H
#define DEF_TASK_MANAGER_CLIENTNS0Role_H

/**
 *  <p>Java class for role.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  <p>
 *  <pre>
 *  &lt;simpleType name="role"&gt;
 *    &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *      &lt;enumeration value="USER"/&gt;
 *      &lt;enumeration value="ADMIN"/&gt;
 *    &lt;/restriction&gt;
 *  &lt;/simpleType&gt;
 *  </pre>
 *  
 */
enum TASK_MANAGER_CLIENTNS0Role
{

  /**
   * (no documentation provided)
   */
  TASK_MANAGER_CLIENT_NS0_ROLE_USER,

  /**
   * (no documentation provided)
   */
  TASK_MANAGER_CLIENT_NS0_ROLE_ADMIN
};
/**
 * Reads a Role from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Role, or NULL if unable to be read.
 */
static enum TASK_MANAGER_CLIENTNS0Role *xmlTextReaderReadTASK_MANAGER_CLIENTNS0RoleType(xmlTextReaderPtr reader);

/**
 * Writes a Role to XML.
 *
 * @param writer The XML writer.
 * @param _role The Role to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteTASK_MANAGER_CLIENTNS0RoleType(xmlTextWriterPtr writer, enum TASK_MANAGER_CLIENTNS0Role *_role);

/**
 * Utility method for getting the enum value for a string.
 *
 * @param _role The string to format.
 * @return The enum value or NULL on error.
 */
static enum TASK_MANAGER_CLIENTNS0Role *formatStringToTASK_MANAGER_CLIENTNS0RoleType(NSString *_role);

/**
 * Utility method for getting the string value of Role.
 *
 * @param _role The Role to format.
 * @return The string value or NULL on error.
 */
static NSString *formatTASK_MANAGER_CLIENTNS0RoleTypeToString(enum TASK_MANAGER_CLIENTNS0Role *_role);
#endif /* DEF_TASK_MANAGER_CLIENTNS0Role_H */

@class TASK_MANAGER_CLIENTNS0Export;
@class TASK_MANAGER_CLIENTNS0FindAllUser;
@class TASK_MANAGER_CLIENTNS0FindUserById;
@class TASK_MANAGER_CLIENTNS0FindUserByLogin;
@class TASK_MANAGER_CLIENTNS0Load;
@class TASK_MANAGER_CLIENTNS0LockUserByLogin;
@class TASK_MANAGER_CLIENTNS0LockUserByLoginResponse;
@class TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse;
@class TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse;
@class TASK_MANAGER_CLIENTNS0UnlockUserByLogin;
@class TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse;
@class TASK_MANAGER_CLIENTNS0RemoveUserByLogin;
@class TASK_MANAGER_CLIENTNS0RemoveUserById;
@class TASK_MANAGER_CLIENTNS0LoadResponse;
@class TASK_MANAGER_CLIENTNS0FindUserByLoginResponse;
@class TASK_MANAGER_CLIENTNS0FindUserByIdResponse;
@class TASK_MANAGER_CLIENTNS0FindAllUserResponse;
@class TASK_MANAGER_CLIENTNS0ExportResponse;
@class TASK_MANAGER_CLIENTNS0Domain;
@class TASK_MANAGER_CLIENTNS0AbstractEntity;
@class TASK_MANAGER_CLIENTNS0Task;
@class TASK_MANAGER_CLIENTNS0User;
@class TASK_MANAGER_CLIENTNS0Session;
@class TASK_MANAGER_CLIENTNS0Project;

#ifndef DEF_TASK_MANAGER_CLIENTNS0Export_H
#define DEF_TASK_MANAGER_CLIENTNS0Export_H

/**
 *  <p>Java class for export complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="export"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0Export : NSObject <EnunciateXML>
{
  @private
    TASK_MANAGER_CLIENTNS0Session *_session;
}

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Session *) session;

/**
 * (no documentation provided)
 */
- (void) setSession: (TASK_MANAGER_CLIENTNS0Session *) newSession;
@end /* interface TASK_MANAGER_CLIENTNS0Export */

#endif /* DEF_TASK_MANAGER_CLIENTNS0Export_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0FindAllUser_H
#define DEF_TASK_MANAGER_CLIENTNS0FindAllUser_H

/**
 *  <p>Java class for findAllUser complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="findAllUser"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0FindAllUser : NSObject <EnunciateXML>
{
  @private
    TASK_MANAGER_CLIENTNS0Session *_session;
}

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Session *) session;

/**
 * (no documentation provided)
 */
- (void) setSession: (TASK_MANAGER_CLIENTNS0Session *) newSession;
@end /* interface TASK_MANAGER_CLIENTNS0FindAllUser */

#endif /* DEF_TASK_MANAGER_CLIENTNS0FindAllUser_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0FindUserById_H
#define DEF_TASK_MANAGER_CLIENTNS0FindUserById_H

/**
 *  <p>Java class for findUserById complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="findUserById"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0FindUserById : NSObject <EnunciateXML>
{
  @private
    TASK_MANAGER_CLIENTNS0Session *_session;
    NSString *_identifier;
}

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Session *) session;

/**
 * (no documentation provided)
 */
- (void) setSession: (TASK_MANAGER_CLIENTNS0Session *) newSession;

/**
 * (no documentation provided)
 */
- (NSString *) identifier;

/**
 * (no documentation provided)
 */
- (void) setIdentifier: (NSString *) newIdentifier;
@end /* interface TASK_MANAGER_CLIENTNS0FindUserById */

#endif /* DEF_TASK_MANAGER_CLIENTNS0FindUserById_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0FindUserByLogin_H
#define DEF_TASK_MANAGER_CLIENTNS0FindUserByLogin_H

/**
 *  <p>Java class for findUserByLogin complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="findUserByLogin"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0FindUserByLogin : NSObject <EnunciateXML>
{
  @private
    TASK_MANAGER_CLIENTNS0Session *_session;
    NSString *_login;
}

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Session *) session;

/**
 * (no documentation provided)
 */
- (void) setSession: (TASK_MANAGER_CLIENTNS0Session *) newSession;

/**
 * (no documentation provided)
 */
- (NSString *) login;

/**
 * (no documentation provided)
 */
- (void) setLogin: (NSString *) newLogin;
@end /* interface TASK_MANAGER_CLIENTNS0FindUserByLogin */

#endif /* DEF_TASK_MANAGER_CLIENTNS0FindUserByLogin_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0Load_H
#define DEF_TASK_MANAGER_CLIENTNS0Load_H

/**
 *  <p>Java class for load complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="load"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="domain" type="{http://endpoint.tm.amster.ru/}domain" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0Load : NSObject <EnunciateXML>
{
  @private
    TASK_MANAGER_CLIENTNS0Session *_session;
    TASK_MANAGER_CLIENTNS0Domain *_domain;
}

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Session *) session;

/**
 * (no documentation provided)
 */
- (void) setSession: (TASK_MANAGER_CLIENTNS0Session *) newSession;

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Domain *) domain;

/**
 * (no documentation provided)
 */
- (void) setDomain: (TASK_MANAGER_CLIENTNS0Domain *) newDomain;
@end /* interface TASK_MANAGER_CLIENTNS0Load */

#endif /* DEF_TASK_MANAGER_CLIENTNS0Load_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0LockUserByLogin_H
#define DEF_TASK_MANAGER_CLIENTNS0LockUserByLogin_H

/**
 *  <p>Java class for lockUserByLogin complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="lockUserByLogin"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0LockUserByLogin : NSObject <EnunciateXML>
{
  @private
    TASK_MANAGER_CLIENTNS0Session *_session;
    NSString *_login;
}

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Session *) session;

/**
 * (no documentation provided)
 */
- (void) setSession: (TASK_MANAGER_CLIENTNS0Session *) newSession;

/**
 * (no documentation provided)
 */
- (NSString *) login;

/**
 * (no documentation provided)
 */
- (void) setLogin: (NSString *) newLogin;
@end /* interface TASK_MANAGER_CLIENTNS0LockUserByLogin */

#endif /* DEF_TASK_MANAGER_CLIENTNS0LockUserByLogin_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0LockUserByLoginResponse_H
#define DEF_TASK_MANAGER_CLIENTNS0LockUserByLoginResponse_H

/**
 *  <p>Java class for lockUserByLoginResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="lockUserByLoginResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="return" type="{http://endpoint.tm.amster.ru/}user" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0LockUserByLoginResponse : NSObject <EnunciateXML>
{
  @private
    TASK_MANAGER_CLIENTNS0User *__return;
}

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0User *) _return;

/**
 * (no documentation provided)
 */
- (void) set_return: (TASK_MANAGER_CLIENTNS0User *) new_return;
@end /* interface TASK_MANAGER_CLIENTNS0LockUserByLoginResponse */

#endif /* DEF_TASK_MANAGER_CLIENTNS0LockUserByLoginResponse_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse_H
#define DEF_TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse_H

/**
 *  <p>Java class for removeUserByIdResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="removeUserByIdResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse : NSObject <EnunciateXML>
{
  @private
}
@end /* interface TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse */

#endif /* DEF_TASK_MANAGER_CLIENTNS0RemoveUserByIdResponse_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse_H
#define DEF_TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse_H

/**
 *  <p>Java class for removeUserByLoginResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="removeUserByLoginResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse : NSObject <EnunciateXML>
{
  @private
}
@end /* interface TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse */

#endif /* DEF_TASK_MANAGER_CLIENTNS0RemoveUserByLoginResponse_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0UnlockUserByLogin_H
#define DEF_TASK_MANAGER_CLIENTNS0UnlockUserByLogin_H

/**
 *  <p>Java class for unlockUserByLogin complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="unlockUserByLogin"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0UnlockUserByLogin : NSObject <EnunciateXML>
{
  @private
    TASK_MANAGER_CLIENTNS0Session *_session;
    NSString *_login;
}

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Session *) session;

/**
 * (no documentation provided)
 */
- (void) setSession: (TASK_MANAGER_CLIENTNS0Session *) newSession;

/**
 * (no documentation provided)
 */
- (NSString *) login;

/**
 * (no documentation provided)
 */
- (void) setLogin: (NSString *) newLogin;
@end /* interface TASK_MANAGER_CLIENTNS0UnlockUserByLogin */

#endif /* DEF_TASK_MANAGER_CLIENTNS0UnlockUserByLogin_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse_H
#define DEF_TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse_H

/**
 *  <p>Java class for unlockUserByLoginResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="unlockUserByLoginResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="return" type="{http://endpoint.tm.amster.ru/}user" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse : NSObject <EnunciateXML>
{
  @private
    TASK_MANAGER_CLIENTNS0User *__return;
}

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0User *) _return;

/**
 * (no documentation provided)
 */
- (void) set_return: (TASK_MANAGER_CLIENTNS0User *) new_return;
@end /* interface TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse */

#endif /* DEF_TASK_MANAGER_CLIENTNS0UnlockUserByLoginResponse_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0RemoveUserByLogin_H
#define DEF_TASK_MANAGER_CLIENTNS0RemoveUserByLogin_H

/**
 *  <p>Java class for removeUserByLogin complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="removeUserByLogin"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0RemoveUserByLogin : NSObject <EnunciateXML>
{
  @private
    TASK_MANAGER_CLIENTNS0Session *_session;
    NSString *_login;
}

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Session *) session;

/**
 * (no documentation provided)
 */
- (void) setSession: (TASK_MANAGER_CLIENTNS0Session *) newSession;

/**
 * (no documentation provided)
 */
- (NSString *) login;

/**
 * (no documentation provided)
 */
- (void) setLogin: (NSString *) newLogin;
@end /* interface TASK_MANAGER_CLIENTNS0RemoveUserByLogin */

#endif /* DEF_TASK_MANAGER_CLIENTNS0RemoveUserByLogin_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0RemoveUserById_H
#define DEF_TASK_MANAGER_CLIENTNS0RemoveUserById_H

/**
 *  <p>Java class for removeUserById complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="removeUserById"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="session" type="{http://endpoint.tm.amster.ru/}session" minOccurs="0"/&gt;
 *          &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0RemoveUserById : NSObject <EnunciateXML>
{
  @private
    TASK_MANAGER_CLIENTNS0Session *_session;
    NSString *_identifier;
}

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Session *) session;

/**
 * (no documentation provided)
 */
- (void) setSession: (TASK_MANAGER_CLIENTNS0Session *) newSession;

/**
 * (no documentation provided)
 */
- (NSString *) identifier;

/**
 * (no documentation provided)
 */
- (void) setIdentifier: (NSString *) newIdentifier;
@end /* interface TASK_MANAGER_CLIENTNS0RemoveUserById */

#endif /* DEF_TASK_MANAGER_CLIENTNS0RemoveUserById_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0LoadResponse_H
#define DEF_TASK_MANAGER_CLIENTNS0LoadResponse_H

/**
 *  <p>Java class for loadResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="loadResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0LoadResponse : NSObject <EnunciateXML>
{
  @private
}
@end /* interface TASK_MANAGER_CLIENTNS0LoadResponse */

#endif /* DEF_TASK_MANAGER_CLIENTNS0LoadResponse_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0FindUserByLoginResponse_H
#define DEF_TASK_MANAGER_CLIENTNS0FindUserByLoginResponse_H

/**
 *  <p>Java class for findUserByLoginResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="findUserByLoginResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="return" type="{http://endpoint.tm.amster.ru/}user" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0FindUserByLoginResponse : NSObject <EnunciateXML>
{
  @private
    TASK_MANAGER_CLIENTNS0User *__return;
}

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0User *) _return;

/**
 * (no documentation provided)
 */
- (void) set_return: (TASK_MANAGER_CLIENTNS0User *) new_return;
@end /* interface TASK_MANAGER_CLIENTNS0FindUserByLoginResponse */

#endif /* DEF_TASK_MANAGER_CLIENTNS0FindUserByLoginResponse_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0FindUserByIdResponse_H
#define DEF_TASK_MANAGER_CLIENTNS0FindUserByIdResponse_H

/**
 *  <p>Java class for findUserByIdResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="findUserByIdResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="return" type="{http://endpoint.tm.amster.ru/}user" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0FindUserByIdResponse : NSObject <EnunciateXML>
{
  @private
    TASK_MANAGER_CLIENTNS0User *__return;
}

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0User *) _return;

/**
 * (no documentation provided)
 */
- (void) set_return: (TASK_MANAGER_CLIENTNS0User *) new_return;
@end /* interface TASK_MANAGER_CLIENTNS0FindUserByIdResponse */

#endif /* DEF_TASK_MANAGER_CLIENTNS0FindUserByIdResponse_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0FindAllUserResponse_H
#define DEF_TASK_MANAGER_CLIENTNS0FindAllUserResponse_H

/**
 *  <p>Java class for findAllUserResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="findAllUserResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="return" type="{http://endpoint.tm.amster.ru/}user" maxOccurs="unbounded" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0FindAllUserResponse : NSObject <EnunciateXML>
{
  @private
    NSArray *__return;
}

/**
 * (no documentation provided)
 */
- (NSArray *) _return;

/**
 * (no documentation provided)
 */
- (void) set_return: (NSArray *) new_return;
@end /* interface TASK_MANAGER_CLIENTNS0FindAllUserResponse */

#endif /* DEF_TASK_MANAGER_CLIENTNS0FindAllUserResponse_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0ExportResponse_H
#define DEF_TASK_MANAGER_CLIENTNS0ExportResponse_H

/**
 *  <p>Java class for exportResponse complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="exportResponse"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="return" type="{http://endpoint.tm.amster.ru/}domain" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0ExportResponse : NSObject <EnunciateXML>
{
  @private
    TASK_MANAGER_CLIENTNS0Domain *__return;
}

/**
 * (no documentation provided)
 */
- (TASK_MANAGER_CLIENTNS0Domain *) _return;

/**
 * (no documentation provided)
 */
- (void) set_return: (TASK_MANAGER_CLIENTNS0Domain *) new_return;
@end /* interface TASK_MANAGER_CLIENTNS0ExportResponse */

#endif /* DEF_TASK_MANAGER_CLIENTNS0ExportResponse_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0Domain_H
#define DEF_TASK_MANAGER_CLIENTNS0Domain_H

/**
 *  <p>Java class for domain complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="domain"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="projects" type="{http://endpoint.tm.amster.ru/}project" maxOccurs="unbounded" minOccurs="0"/&gt;
 *          &lt;element name="tasks" type="{http://endpoint.tm.amster.ru/}task" maxOccurs="unbounded" minOccurs="0"/&gt;
 *          &lt;element name="users" type="{http://endpoint.tm.amster.ru/}user" maxOccurs="unbounded" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0Domain : NSObject
{
  @private
    NSArray *_projects;
    NSArray *_tasks;
    NSArray *_users;
}

/**
 * (no documentation provided)
 */
- (NSArray *) projects;

/**
 * (no documentation provided)
 */
- (void) setProjects: (NSArray *) newProjects;

/**
 * (no documentation provided)
 */
- (NSArray *) tasks;

/**
 * (no documentation provided)
 */
- (void) setTasks: (NSArray *) newTasks;

/**
 * (no documentation provided)
 */
- (NSArray *) users;

/**
 * (no documentation provided)
 */
- (void) setUsers: (NSArray *) newUsers;
@end /* interface TASK_MANAGER_CLIENTNS0Domain */

#endif /* DEF_TASK_MANAGER_CLIENTNS0Domain_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0AbstractEntity_H
#define DEF_TASK_MANAGER_CLIENTNS0AbstractEntity_H

/**
 *  <p>Java class for abstractEntity complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="abstractEntity"&gt;
 *    &lt;complexContent&gt;
 *      &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/restriction&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0AbstractEntity : NSObject
{
  @private
    NSString *_identifier;
}

/**
 * (no documentation provided)
 */
- (NSString *) identifier;

/**
 * (no documentation provided)
 */
- (void) setIdentifier: (NSString *) newIdentifier;
@end /* interface TASK_MANAGER_CLIENTNS0AbstractEntity */

#endif /* DEF_TASK_MANAGER_CLIENTNS0AbstractEntity_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0Task_H
#define DEF_TASK_MANAGER_CLIENTNS0Task_H

/**
 *  <p>Java class for task complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="task"&gt;
 *    &lt;complexContent&gt;
 *      &lt;extension base="{http://endpoint.tm.amster.ru/}abstractEntity"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/extension&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0Task : TASK_MANAGER_CLIENTNS0AbstractEntity
{
  @private
    NSString *_description;
    NSString *_name;
    NSString *_userId;
}

/**
 * (no documentation provided)
 */
- (NSString *) description;

/**
 * (no documentation provided)
 */
- (void) setDescription: (NSString *) newDescription;

/**
 * (no documentation provided)
 */
- (NSString *) name;

/**
 * (no documentation provided)
 */
- (void) setName: (NSString *) newName;

/**
 * (no documentation provided)
 */
- (NSString *) userId;

/**
 * (no documentation provided)
 */
- (void) setUserId: (NSString *) newUserId;
@end /* interface TASK_MANAGER_CLIENTNS0Task */

#endif /* DEF_TASK_MANAGER_CLIENTNS0Task_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0User_H
#define DEF_TASK_MANAGER_CLIENTNS0User_H

/**
 *  <p>Java class for user complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="user"&gt;
 *    &lt;complexContent&gt;
 *      &lt;extension base="{http://endpoint.tm.amster.ru/}abstractEntity"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="fistName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="lastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="locked" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *          &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="middleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="passwordHash" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="role" type="{http://endpoint.tm.amster.ru/}role" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/extension&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0User : TASK_MANAGER_CLIENTNS0AbstractEntity
{
  @private
    NSString *_email;
    NSString *_fistName;
    NSString *_lastName;
    BOOL *_locked;
    NSString *_login;
    NSString *_middleName;
    NSString *_passwordHash;
    enum TASK_MANAGER_CLIENTNS0Role *_role;
}

/**
 * (no documentation provided)
 */
- (NSString *) email;

/**
 * (no documentation provided)
 */
- (void) setEmail: (NSString *) newEmail;

/**
 * (no documentation provided)
 */
- (NSString *) fistName;

/**
 * (no documentation provided)
 */
- (void) setFistName: (NSString *) newFistName;

/**
 * (no documentation provided)
 */
- (NSString *) lastName;

/**
 * (no documentation provided)
 */
- (void) setLastName: (NSString *) newLastName;

/**
 * (no documentation provided)
 */
- (BOOL *) locked;

/**
 * (no documentation provided)
 */
- (void) setLocked: (BOOL *) newLocked;

/**
 * (no documentation provided)
 */
- (NSString *) login;

/**
 * (no documentation provided)
 */
- (void) setLogin: (NSString *) newLogin;

/**
 * (no documentation provided)
 */
- (NSString *) middleName;

/**
 * (no documentation provided)
 */
- (void) setMiddleName: (NSString *) newMiddleName;

/**
 * (no documentation provided)
 */
- (NSString *) passwordHash;

/**
 * (no documentation provided)
 */
- (void) setPasswordHash: (NSString *) newPasswordHash;

/**
 * (no documentation provided)
 */
- (enum TASK_MANAGER_CLIENTNS0Role *) role;

/**
 * (no documentation provided)
 */
- (void) setRole: (enum TASK_MANAGER_CLIENTNS0Role *) newRole;
@end /* interface TASK_MANAGER_CLIENTNS0User */

#endif /* DEF_TASK_MANAGER_CLIENTNS0User_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0Session_H
#define DEF_TASK_MANAGER_CLIENTNS0Session_H

/**
 *  <p>Java class for session complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="session"&gt;
 *    &lt;complexContent&gt;
 *      &lt;extension base="{http://endpoint.tm.amster.ru/}abstractEntity"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="signature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="timestamp" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *          &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/extension&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0Session : TASK_MANAGER_CLIENTNS0AbstractEntity
{
  @private
    NSString *_signature;
    long long *_timestamp;
    NSString *_userId;
}

/**
 * (no documentation provided)
 */
- (NSString *) signature;

/**
 * (no documentation provided)
 */
- (void) setSignature: (NSString *) newSignature;

/**
 * (no documentation provided)
 */
- (long long *) timestamp;

/**
 * (no documentation provided)
 */
- (void) setTimestamp: (long long *) newTimestamp;

/**
 * (no documentation provided)
 */
- (NSString *) userId;

/**
 * (no documentation provided)
 */
- (void) setUserId: (NSString *) newUserId;
@end /* interface TASK_MANAGER_CLIENTNS0Session */

#endif /* DEF_TASK_MANAGER_CLIENTNS0Session_H */
#ifndef DEF_TASK_MANAGER_CLIENTNS0Project_H
#define DEF_TASK_MANAGER_CLIENTNS0Project_H

/**
 *  <p>Java class for project complex type.
 *  
 *  <p>The following schema fragment specifies the expected content contained within this class.
 *  
 *  <pre>
 *  &lt;complexType name="project"&gt;
 *    &lt;complexContent&gt;
 *      &lt;extension base="{http://endpoint.tm.amster.ru/}abstractEntity"&gt;
 *        &lt;sequence&gt;
 *          &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *          &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *        &lt;/sequence&gt;
 *      &lt;/extension&gt;
 *    &lt;/complexContent&gt;
 *  &lt;/complexType&gt;
 *  </pre>
 *  
 *  
 */
@interface TASK_MANAGER_CLIENTNS0Project : TASK_MANAGER_CLIENTNS0AbstractEntity
{
  @private
    NSString *_description;
    NSString *_name;
    NSString *_userId;
}

/**
 * (no documentation provided)
 */
- (NSString *) description;

/**
 * (no documentation provided)
 */
- (void) setDescription: (NSString *) newDescription;

/**
 * (no documentation provided)
 */
- (NSString *) name;

/**
 * (no documentation provided)
 */
- (void) setName: (NSString *) newName;

/**
 * (no documentation provided)
 */
- (NSString *) userId;

/**
 * (no documentation provided)
 */
- (void) setUserId: (NSString *) newUserId;
@end /* interface TASK_MANAGER_CLIENTNS0Project */

#endif /* DEF_TASK_MANAGER_CLIENTNS0Project_H */
