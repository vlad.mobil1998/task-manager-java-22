package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.service.CommandService;

import java.util.Collection;

public interface ICommandService {

    void initCommandAdmin();

    void initCommand();

    void initArgument();

    @Nullable
    Collection<AbstractCommand> getCommands();

    @Nullable
    Collection<AbstractCommand> getArguments();

    @Nullable
    AbstractCommand getCommand(@NotNull String cmd);

    @Nullable
    AbstractCommand getArgument(@NotNull String arg);

}