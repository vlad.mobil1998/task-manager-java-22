package ru.amster.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.ICommandService;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.command.admin.data.base64.DataBase64ClearCommand;
import ru.amster.tm.command.admin.data.base64.DataBase64LoadCommand;
import ru.amster.tm.command.admin.data.base64.DataBase64SaveCommand;
import ru.amster.tm.command.admin.data.binary.DataBinaryClearCommand;
import ru.amster.tm.command.admin.data.binary.DataBinaryLoadCommand;
import ru.amster.tm.command.admin.data.binary.DataBinarySaveCommand;
import ru.amster.tm.command.admin.data.json.DataJsonClearCommand;
import ru.amster.tm.command.admin.data.json.DataJsonLoadCommand;
import ru.amster.tm.command.admin.data.json.DataJsonSaveCommand;
import ru.amster.tm.command.admin.data.xml.DataXmlClearCommand;
import ru.amster.tm.command.admin.data.xml.DataXmlLoadCommand;
import ru.amster.tm.command.admin.data.xml.DataXmlSaveCommand;
import ru.amster.tm.command.authentication.*;
import ru.amster.tm.command.project.*;
import ru.amster.tm.command.system.*;
import ru.amster.tm.command.task.*;
import ru.amster.tm.command.user.*;
import ru.amster.tm.command.admin.*;
import ru.amster.tm.command.user.UserRemoveCommand;
import ru.amster.tm.command.user.update.*;
import ru.amster.tm.endpoint.Session;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public final class CommandService implements ICommandService {

    @NotNull
    private final IServiceLocator webServiceLocator;

    public CommandService(@NotNull final IServiceLocator webServiceLocator) {
        this.webServiceLocator = webServiceLocator;
    }

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    private static final Class[] COMMANDS = new Class[]{
            SystemHelpCommand.class, SystemAboutCommand.class, SystemInfoCommand.class,
            SystemVersionCommand.class, SystemCommandCommand.class, SystemArgumentCommand.class,

            EmailUpdateCommand.class, FirstNameUpdateCommand.class, LastNameUpdateCommand.class,
            MiddleNameUpdateCommand.class, PasswordUpdateCommand.class, UserUnlockCommand.class,
            UserLockCommand.class, UserRemoveCommand.class, UserProfileShowCommand.class,

            ProjectCreateCommand.class, ProjectShowByIndexCommand.class, ProjectShowByNameCommand.class,
            ProjectShowByIdCommand.class, ProjectShowCommand.class, ProjectUpdateByIdCommand.class,
            ProjectUpdateByIndexCommand.class, ProjectRemoveByIdCommand.class,
            ProjectRemoveByNameCommand.class, ProjectRemoveByIndexCommand.class,

            TaskCreateCommand.class, TaskShowByIndexCommand.class, TaskShowByNameCommand.class,
            TaskShowByIdCommand.class, TaskShowCommand.class, TaskUpdateByIdCommand.class,
            TaskUpdateByIndexCommand.class, TaskRemoveByIdCommand.class, TaskRemoveByNameCommand.class,
            TaskRemoveByIndexCommand.class,

            LoginCommand.class, LogoutCommand.class, RegistrationCommand.class,

            SystemExitCommand.class,
    };

    @NotNull
    private static final Class[] COMMANDS_ADMIN = new Class[] {
            AllUserShowCommand.class, RemoveUserByIdCommand.class, RemoveUserByLoginCommand.class,
            UserLockCommand.class, UserUnlockCommand.class,
            DataBinarySaveCommand.class, DataBinaryClearCommand.class,
            DataBase64SaveCommand.class, DataBase64ClearCommand.class,
            DataJsonSaveCommand.class, DataJsonClearCommand.class,
            DataXmlSaveCommand.class, DataXmlClearCommand.class,
            DataBinaryLoadCommand.class, DataBase64LoadCommand.class,
            DataJsonLoadCommand.class, DataXmlLoadCommand.class,
    };

    @NotNull
    private static final Class[] ARGUMENTS = new Class[]{
            SystemHelpCommand.class, SystemAboutCommand.class,
            SystemInfoCommand.class, SystemVersionCommand.class,
    };

    @Override
    public void initCommandAdmin()
    {
        for (@NotNull final Class clazz : COMMANDS_ADMIN) {
            try {
                @NotNull final Object commandInstance = clazz.newInstance();
                @NotNull final AbstractCommand command = (AbstractCommand) commandInstance;
                command.setWebServiceLocator(webServiceLocator);
                commands.put(command.name(), command);
            } catch (Exception e) {
                throw new RuntimeException();
            }
        }
    }

    @Override
    public void initCommand()
    {
        for (@NotNull final Class clazz : COMMANDS) {
            try {
                @NotNull final Object commandInstance = clazz.newInstance();
                @NotNull final AbstractCommand command = (AbstractCommand) commandInstance;
                command.setWebServiceLocator(webServiceLocator);
                commands.put(command.name(), command);
            } catch (Exception e) {
                throw new RuntimeException();
            }
        }
    }

    @Override
    public void initArgument()
    {
        for (@NotNull final Class clazz : ARGUMENTS) {
            try {
                @NotNull final Object commandInstance = clazz.newInstance();
                @NotNull final AbstractCommand argument = (AbstractCommand) commandInstance;
                arguments.put(argument.arg(), argument);
            } catch (Exception e) {
                throw new RuntimeException();
            }
        }
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @NotNull
    @Override
    public AbstractCommand getCommand(@NotNull final String cmd) {
        return commands.get(cmd);
    }

    @NotNull
    @Override
    public AbstractCommand getArgument(@NotNull final String arg) {
        return arguments.get(arg);
    }

}