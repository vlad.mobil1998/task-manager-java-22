package ru.amster.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.*;
import ru.amster.tm.endpoint.*;

public final class WebServiceLocator implements IServiceLocator {

    @NotNull
    private final AdminUserEndpointService AdminUserEndpointService = new AdminUserEndpointService();

    @NotNull
    private final AdminUserEndpoint adminUserEndpoint =
            AdminUserEndpointService.getAdminUserEndpointPort();

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint =
            sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint =
            projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint =
            taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final UserEndpointService userEndpointService = new UserEndpointService();

    @NotNull
    private final UserEndpoint userEndpoint =
            userEndpointService.getUserEndpointPort();

    @Nullable
    private Session session = new Session();

    @Override
    public void setSession(Session session) {
        this.session = session;
    }

    @Override
    @NotNull
    public Session getSession() {
        return session;
    }

    @NotNull
    private ICommandService commandService;

    @Override
    public void setCommandService(ICommandService commandService) {
        this.commandService = commandService;
    }

    @NotNull
    @Override
    public AdminUserEndpoint getAdminUserEndpoint() {
        return adminUserEndpoint;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public SessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    @NotNull
    @Override
    public ProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @NotNull
    @Override
    public TaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @NotNull
    @Override
    public UserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

}