package ru.amster.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.ProjectEndpoint;
import ru.amster.tm.endpoint.Project;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.ProjectUtil;

import java.util.List;

public final class ProjectShowCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "project-list";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Show project list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECT]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();

        @NotNull final ProjectEndpoint projectEndpoint = webServiceLocator.getProjectEndpoint();
        @Nullable final List<Project> projects = projectEndpoint.findAllProject(
                webServiceLocator.getSession()
        );
        for (@Nullable final Project project : projects) ProjectUtil.showProject(project);
        System.out.println("[OK]");
    }

}