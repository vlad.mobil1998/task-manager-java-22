package ru.amster.tm.command;

import lombok.NoArgsConstructor;
import ru.amster.tm.api.servise.IServiceLocator;

import java.io.IOException;

@NoArgsConstructor
public abstract class AbstractCommand {

    protected IServiceLocator webServiceLocator;

    public void setWebServiceLocator(IServiceLocator webServiceLocator) {
        this.webServiceLocator = webServiceLocator;
    }

    public abstract String name();

    public abstract String arg();

    public abstract String description();

    public abstract void execute() throws IOException, ClassNotFoundException;

}