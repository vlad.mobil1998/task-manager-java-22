package ru.amster.tm.command.user.update;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.UserEndpoint;
import ru.amster.tm.exception.empty.EmptyEmailException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public final class PasswordUpdateCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "upd-password";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - updating user password";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PASSWORD]");
        if (webServiceLocator.getSession().getId() == null) throw new AccessDeniedException();
        System.out.println("ENTER PASSWORD");
        @Nullable final String password = TerminalUtil.nextLine();
        if (password == null || password.isEmpty()) throw new EmptyEmailException();

        @NotNull final UserEndpoint userEndpoint = webServiceLocator.getUserEndpoint();
        userEndpoint.updateUserPassword(webServiceLocator.getSession(), password);
        System.out.println("[OK]");
    }

}