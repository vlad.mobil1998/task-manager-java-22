package ru.amster.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.ProjectEndpoint;
import ru.amster.tm.endpoint.Project;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.ProjectUtil;
import ru.amster.tm.util.TerminalUtil;

public final class ProjectShowByNameCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "project-v-name";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Show project by name";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();

        System.out.println("ENTER NAME");
        @Nullable final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        @NotNull final ProjectEndpoint projectEndpoint = webServiceLocator.getProjectEndpoint();
        @Nullable final Project project = projectEndpoint.findProjectByName(
                webServiceLocator.getSession(),
                name
        );
        if (project == null) throw new EmptyProjectException();
        ProjectUtil.showProject(project);
        System.out.println("[OK]");
    }

}