package ru.amster.tm.command.admin.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.admin.data.AbstractDataCommand;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public final class DataBase64ClearCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String name() {
        return "data-bin-clear";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Remove base64 data";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA BASE64 CLEAR]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();

        @NotNull final File file = new File(FILE_BASE64);
        Files.delete(file.toPath());
        System.out.println("[OK]");
    }

}