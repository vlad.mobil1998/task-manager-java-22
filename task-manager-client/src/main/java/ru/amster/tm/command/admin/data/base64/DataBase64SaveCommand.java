package ru.amster.tm.command.admin.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.admin.data.AbstractDataCommand;
import ru.amster.tm.endpoint.Domain;
import ru.amster.tm.endpoint.AdminUserEndpoint;
import ru.amster.tm.exception.user.AccessDeniedException;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.nio.file.Files;

public final class DataBase64SaveCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String name() {
        return "data-base64-save";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Save base64 data to file";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA BASE64 SAVE]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();

        @NotNull final File file = new File(FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        @NotNull final AdminUserEndpoint adminUserEndpoint = webServiceLocator.getAdminUserEndpoint();
        @NotNull final Domain domain = adminUserEndpoint.export(webServiceLocator.getSession());
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();

        @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = new BASE64Encoder().encode(bytes);
        byteArrayOutputStream.close();

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_BASE64);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();

        System.out.println("[OK]");
    }

}