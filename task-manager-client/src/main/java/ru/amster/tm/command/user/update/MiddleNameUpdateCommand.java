package ru.amster.tm.command.user.update;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.UserEndpoint;
import ru.amster.tm.exception.empty.EmptyEmailException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public final class MiddleNameUpdateCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "upd-middle-name";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - updating user middle name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE MIDDLE NAME]");
        if (webServiceLocator.getSession().getId() == null) throw new AccessDeniedException();

        System.out.println("ENTER MIDDLE NAME");
        @Nullable final String middleName = TerminalUtil.nextLine();
        if (middleName == null || middleName.isEmpty()) throw new EmptyEmailException();

        @NotNull final UserEndpoint userEndpoint = webServiceLocator.getUserEndpoint();
        userEndpoint.updateUserMiddleName(webServiceLocator.getSession(), middleName);
        System.out.println("[OK]");
    }

}