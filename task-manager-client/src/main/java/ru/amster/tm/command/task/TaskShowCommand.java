package ru.amster.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.TaskEndpoint;
import ru.amster.tm.endpoint.Task;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TaskUtil;

import java.util.List;

public final class TaskShowCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "task-list";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Show task list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASK]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();

        @NotNull final TaskEndpoint taskEndpoint = webServiceLocator.getTaskEndpoint();
        @Nullable final List<Task> tasks = taskEndpoint.findAllTask(webServiceLocator.getSession());
        for (@Nullable final Task task : tasks) TaskUtil.showTask(task);
        System.out.println("[OK]");
    }

}