package ru.amster.tm.command.user.update;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.UserEndpoint;
import ru.amster.tm.exception.empty.EmptyEmailException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public final class FirstNameUpdateCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "upd-first-name";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - updating user first name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE FIRST NAME]");
        if (webServiceLocator.getSession().getId() == null) throw new AccessDeniedException();

        System.out.println("ENTER FIRST NAME");
        @Nullable final String firstName = TerminalUtil.nextLine();
        if (firstName == null || firstName.isEmpty()) throw new EmptyEmailException();

        @NotNull final UserEndpoint userEndpoint = webServiceLocator.getUserEndpoint();
        userEndpoint.updateUserFirstName(webServiceLocator.getSession(), firstName);
        System.out.println("[OK]");
    }

}