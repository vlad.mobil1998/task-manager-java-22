package ru.amster.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.servise.ICommandService;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.Session;
import ru.amster.tm.exception.system.InvalidArgumentException;
import ru.amster.tm.exception.system.InvalidCommandException;
import ru.amster.tm.service.CommandService;
import ru.amster.tm.service.WebServiceLocator;
import ru.amster.tm.util.TerminalUtil;
import java.io.IOException;

public final class Bootstrap {

    @NotNull
    private final IServiceLocator webServiceLocator = new WebServiceLocator();

    @NotNull
    private final ICommandService commandService = new CommandService(webServiceLocator);

    public final void run(@NotNull final String[] args) throws IOException, ClassNotFoundException {
        commandService.initCommand();
        commandService.initCommandAdmin();
        commandService.initArgument();
        webServiceLocator.setCommandService(commandService);
        System.out.println("**** WELCOME TO TASK MANAGER ****");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            try {
                parseCmd(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.out.println("[FAIL]");
            }
        }
    }

    private void parseCmd(@NotNull final String cmd) throws IOException, ClassNotFoundException {
        if (cmd == null || cmd.isEmpty()) return;
        ICommandService commandService = webServiceLocator.getCommandService();
        @Nullable final AbstractCommand command = commandService.getCommand(cmd);
        if (command == null) throw new InvalidCommandException();
        command.execute();
    }

    private boolean parseArgs(@Nullable final String[] args) throws IOException, ClassNotFoundException {
        if (args == null || args.length == 0) return false;
        for (@NotNull String arg : args) {
            System.out.println(" ");
            ICommandService commandService = webServiceLocator.getCommandService();
            @Nullable final AbstractCommand argument = commandService.getArgument(arg);
            if (argument == null) throw new InvalidArgumentException();
            argument.execute();
        }
        return true;
    }

}