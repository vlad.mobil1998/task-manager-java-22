package ru.amster.tm.exception.empty;

import ru.amster.tm.exception.AbstractException;

public class EmptyEntityException extends AbstractException {

    public EmptyEntityException() {
        super("ERROR! Object(entity) is empty.");
    }
}