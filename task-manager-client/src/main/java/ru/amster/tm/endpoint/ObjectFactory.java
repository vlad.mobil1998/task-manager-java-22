
package ru.amster.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.amster.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Export_QNAME = new QName("http://endpoint.tm.amster.ru/", "export");
    private final static QName _ExportResponse_QNAME = new QName("http://endpoint.tm.amster.ru/", "exportResponse");
    private final static QName _FindAllUser_QNAME = new QName("http://endpoint.tm.amster.ru/", "findAllUser");
    private final static QName _FindAllUserResponse_QNAME = new QName("http://endpoint.tm.amster.ru/", "findAllUserResponse");
    private final static QName _FindUserById_QNAME = new QName("http://endpoint.tm.amster.ru/", "findUserById");
    private final static QName _FindUserByIdResponse_QNAME = new QName("http://endpoint.tm.amster.ru/", "findUserByIdResponse");
    private final static QName _FindUserByLogin_QNAME = new QName("http://endpoint.tm.amster.ru/", "findUserByLogin");
    private final static QName _FindUserByLoginResponse_QNAME = new QName("http://endpoint.tm.amster.ru/", "findUserByLoginResponse");
    private final static QName _Load_QNAME = new QName("http://endpoint.tm.amster.ru/", "load");
    private final static QName _LoadResponse_QNAME = new QName("http://endpoint.tm.amster.ru/", "loadResponse");
    private final static QName _LockUserByLogin_QNAME = new QName("http://endpoint.tm.amster.ru/", "lockUserByLogin");
    private final static QName _LockUserByLoginResponse_QNAME = new QName("http://endpoint.tm.amster.ru/", "lockUserByLoginResponse");
    private final static QName _RemoveUserById_QNAME = new QName("http://endpoint.tm.amster.ru/", "removeUserById");
    private final static QName _RemoveUserByIdResponse_QNAME = new QName("http://endpoint.tm.amster.ru/", "removeUserByIdResponse");
    private final static QName _RemoveUserByLogin_QNAME = new QName("http://endpoint.tm.amster.ru/", "removeUserByLogin");
    private final static QName _RemoveUserByLoginResponse_QNAME = new QName("http://endpoint.tm.amster.ru/", "removeUserByLoginResponse");
    private final static QName _UnlockUserByLogin_QNAME = new QName("http://endpoint.tm.amster.ru/", "unlockUserByLogin");
    private final static QName _UnlockUserByLoginResponse_QNAME = new QName("http://endpoint.tm.amster.ru/", "unlockUserByLoginResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.amster.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Export }
     * 
     */
    public Export createExport() {
        return new Export();
    }

    /**
     * Create an instance of {@link ExportResponse }
     * 
     */
    public ExportResponse createExportResponse() {
        return new ExportResponse();
    }

    /**
     * Create an instance of {@link FindAllUser }
     * 
     */
    public FindAllUser createFindAllUser() {
        return new FindAllUser();
    }

    /**
     * Create an instance of {@link FindAllUserResponse }
     * 
     */
    public FindAllUserResponse createFindAllUserResponse() {
        return new FindAllUserResponse();
    }

    /**
     * Create an instance of {@link FindUserById }
     * 
     */
    public FindUserById createFindUserById() {
        return new FindUserById();
    }

    /**
     * Create an instance of {@link FindUserByIdResponse }
     * 
     */
    public FindUserByIdResponse createFindUserByIdResponse() {
        return new FindUserByIdResponse();
    }

    /**
     * Create an instance of {@link FindUserByLogin }
     * 
     */
    public FindUserByLogin createFindUserByLogin() {
        return new FindUserByLogin();
    }

    /**
     * Create an instance of {@link FindUserByLoginResponse }
     * 
     */
    public FindUserByLoginResponse createFindUserByLoginResponse() {
        return new FindUserByLoginResponse();
    }

    /**
     * Create an instance of {@link Load }
     * 
     */
    public Load createLoad() {
        return new Load();
    }

    /**
     * Create an instance of {@link LoadResponse }
     * 
     */
    public LoadResponse createLoadResponse() {
        return new LoadResponse();
    }

    /**
     * Create an instance of {@link LockUserByLogin }
     * 
     */
    public LockUserByLogin createLockUserByLogin() {
        return new LockUserByLogin();
    }

    /**
     * Create an instance of {@link LockUserByLoginResponse }
     * 
     */
    public LockUserByLoginResponse createLockUserByLoginResponse() {
        return new LockUserByLoginResponse();
    }

    /**
     * Create an instance of {@link RemoveUserById }
     * 
     */
    public RemoveUserById createRemoveUserById() {
        return new RemoveUserById();
    }

    /**
     * Create an instance of {@link RemoveUserByIdResponse }
     * 
     */
    public RemoveUserByIdResponse createRemoveUserByIdResponse() {
        return new RemoveUserByIdResponse();
    }

    /**
     * Create an instance of {@link RemoveUserByLogin }
     * 
     */
    public RemoveUserByLogin createRemoveUserByLogin() {
        return new RemoveUserByLogin();
    }

    /**
     * Create an instance of {@link RemoveUserByLoginResponse }
     * 
     */
    public RemoveUserByLoginResponse createRemoveUserByLoginResponse() {
        return new RemoveUserByLoginResponse();
    }

    /**
     * Create an instance of {@link UnlockUserByLogin }
     * 
     */
    public UnlockUserByLogin createUnlockUserByLogin() {
        return new UnlockUserByLogin();
    }

    /**
     * Create an instance of {@link UnlockUserByLoginResponse }
     * 
     */
    public UnlockUserByLoginResponse createUnlockUserByLoginResponse() {
        return new UnlockUserByLoginResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link Domain }
     * 
     */
    public Domain createDomain() {
        return new Domain();
    }

    /**
     * Create an instance of {@link Project }
     * 
     */
    public Project createProject() {
        return new Project();
    }

    /**
     * Create an instance of {@link Task }
     * 
     */
    public Task createTask() {
        return new Task();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Export }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.amster.ru/", name = "export")
    public JAXBElement<Export> createExport(Export value) {
        return new JAXBElement<Export>(_Export_QNAME, Export.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExportResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.amster.ru/", name = "exportResponse")
    public JAXBElement<ExportResponse> createExportResponse(ExportResponse value) {
        return new JAXBElement<ExportResponse>(_ExportResponse_QNAME, ExportResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.amster.ru/", name = "findAllUser")
    public JAXBElement<FindAllUser> createFindAllUser(FindAllUser value) {
        return new JAXBElement<FindAllUser>(_FindAllUser_QNAME, FindAllUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.amster.ru/", name = "findAllUserResponse")
    public JAXBElement<FindAllUserResponse> createFindAllUserResponse(FindAllUserResponse value) {
        return new JAXBElement<FindAllUserResponse>(_FindAllUserResponse_QNAME, FindAllUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.amster.ru/", name = "findUserById")
    public JAXBElement<FindUserById> createFindUserById(FindUserById value) {
        return new JAXBElement<FindUserById>(_FindUserById_QNAME, FindUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.amster.ru/", name = "findUserByIdResponse")
    public JAXBElement<FindUserByIdResponse> createFindUserByIdResponse(FindUserByIdResponse value) {
        return new JAXBElement<FindUserByIdResponse>(_FindUserByIdResponse_QNAME, FindUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.amster.ru/", name = "findUserByLogin")
    public JAXBElement<FindUserByLogin> createFindUserByLogin(FindUserByLogin value) {
        return new JAXBElement<FindUserByLogin>(_FindUserByLogin_QNAME, FindUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.amster.ru/", name = "findUserByLoginResponse")
    public JAXBElement<FindUserByLoginResponse> createFindUserByLoginResponse(FindUserByLoginResponse value) {
        return new JAXBElement<FindUserByLoginResponse>(_FindUserByLoginResponse_QNAME, FindUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Load }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.amster.ru/", name = "load")
    public JAXBElement<Load> createLoad(Load value) {
        return new JAXBElement<Load>(_Load_QNAME, Load.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.amster.ru/", name = "loadResponse")
    public JAXBElement<LoadResponse> createLoadResponse(LoadResponse value) {
        return new JAXBElement<LoadResponse>(_LoadResponse_QNAME, LoadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockUserByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.amster.ru/", name = "lockUserByLogin")
    public JAXBElement<LockUserByLogin> createLockUserByLogin(LockUserByLogin value) {
        return new JAXBElement<LockUserByLogin>(_LockUserByLogin_QNAME, LockUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockUserByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.amster.ru/", name = "lockUserByLoginResponse")
    public JAXBElement<LockUserByLoginResponse> createLockUserByLoginResponse(LockUserByLoginResponse value) {
        return new JAXBElement<LockUserByLoginResponse>(_LockUserByLoginResponse_QNAME, LockUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.amster.ru/", name = "removeUserById")
    public JAXBElement<RemoveUserById> createRemoveUserById(RemoveUserById value) {
        return new JAXBElement<RemoveUserById>(_RemoveUserById_QNAME, RemoveUserById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.amster.ru/", name = "removeUserByIdResponse")
    public JAXBElement<RemoveUserByIdResponse> createRemoveUserByIdResponse(RemoveUserByIdResponse value) {
        return new JAXBElement<RemoveUserByIdResponse>(_RemoveUserByIdResponse_QNAME, RemoveUserByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.amster.ru/", name = "removeUserByLogin")
    public JAXBElement<RemoveUserByLogin> createRemoveUserByLogin(RemoveUserByLogin value) {
        return new JAXBElement<RemoveUserByLogin>(_RemoveUserByLogin_QNAME, RemoveUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.amster.ru/", name = "removeUserByLoginResponse")
    public JAXBElement<RemoveUserByLoginResponse> createRemoveUserByLoginResponse(RemoveUserByLoginResponse value) {
        return new JAXBElement<RemoveUserByLoginResponse>(_RemoveUserByLoginResponse_QNAME, RemoveUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockUserByLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.amster.ru/", name = "unlockUserByLogin")
    public JAXBElement<UnlockUserByLogin> createUnlockUserByLogin(UnlockUserByLogin value) {
        return new JAXBElement<UnlockUserByLogin>(_UnlockUserByLogin_QNAME, UnlockUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockUserByLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.amster.ru/", name = "unlockUserByLoginResponse")
    public JAXBElement<UnlockUserByLoginResponse> createUnlockUserByLoginResponse(UnlockUserByLoginResponse value) {
        return new JAXBElement<UnlockUserByLoginResponse>(_UnlockUserByLoginResponse_QNAME, UnlockUserByLoginResponse.class, null, value);
    }

}
